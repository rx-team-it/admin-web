@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h4>Total Product Terjual</h4>
        </div>
        <div class="card-body">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                        <div class="col-md-6 mb-3">
                            <div class="row input-daterange">
                                <div class="col-md-4">
                                    <input type="text" name="from_date" id="from_date" class="form-control form-control-sm" placeholder="From Date" readonly />
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="to_date" id="to_date" class="form-control form-control-sm" placeholder="To Date" readonly />
                                </div>
                                <div class="col-md-4">
                                    <button type="button" name="filter" id="filter" class="btn btn-primary btn-sm">Filter</button>
                                    <button type="button" name="refresh" id="refresh" class="btn btn-secondary btn-sm">Refresh</button>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table id="saldo" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Invoice</th>
                                        <th>Nama</th>
                                        <th>Harga Awal</th>
                                        <th>Komisi</th>
                                        <th>Harga jual@</th>
                                        <th>QTY</th>
                                        <th>Biaya Layanan</th>
                                        <th>Cashback</th>
                                        <th>Ongkir</th>
                                        <th>Gross</th>
                                        <th>Pendapatan</th>
                                    </tr>
                                </thead>
                                {{-- <tfoot>
                                    <tr >
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th colspan="4" style="text-align:right">Total:</th>
                                        <th>{{'Rp. ' . number_format($grand)}}</th>
                                    </tr>
                                </tfoot> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" referrerpolicy="no-referrer"></script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#saldo').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#saldo').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#saldo').DataTable({
                pageLength: 20,
                processing: true,
                serverSide: true,
                ajax: 'https://datatables.yajrabox.com/collection/total-records',
                // dom: 'Bfrtip',
                buttons: [{
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('histori-vendor.penjualan') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                    "data": 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'invoice',
                    name: 'invoice',
                },
                {
                    data: '',
                    name: '',
                    render: function(data, type, row){
                        if(type === "sort" || type === "type"){
                            return data;
                        }
                        return moment(data).format("DD-MM-YYYY");
                    }
                },
                {
                        data: 'name_product',
                        name: 'name_product',
                    },
                    {
                        data: 'harga_awal',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                    },
                    {
                        data: 'komisi',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'komisi'
                    },
                    {
                        data: 'fix_price',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'sub_total',
                    },
                    {
                        data: 'qty',
                        name: 'qty',
                    },
                    {
                        data: 'biaya_layanan',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'biaya_layanan'
                    },
                    {
                        data: 'cashback',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'cashback'
                    },
                    {
                        data: 'ongkir',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'ongkir'
                    },
                    {
                        data: 'pendapatan',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'pendapatan'
                    },
                    {
                        data: 'total',
                        render: $.fn.dataTable.render.number('.', '.', 0, 'Rp. '),
                        name: 'total'
                    },
                ],
                order: [
                    [0, 'desc']
                ]
            });
        }

        // const table = $("#saldo").DataTable();
        // const tr = $("<tr><td>1</td><td>2</td></tr>");
        // $("#saldo").row.add(tr).draw();
        $('#saldo').append('<tr><td>my data</td><td>more data</td></tr>');
        console.log('okok');
    });
</script>

@endsection