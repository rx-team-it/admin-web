@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('produk.list') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Product Detail</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">List Toko</a></div>
            <div class="breadcrumb-item">Product Detail</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row justify-content-lg-center">
            <div class="col-md-12">
                <div class="card card-danger">
                    <div class="card-header">
                        <h4>{{ $product->name }}</h4>
                        <div class="card-header-action">
                            <h4> {{ $product->created_at }} </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-between">
                            <div class="col-lg-6 col-sm-4">
                                <div class="owl-carousel owl-theme slider" id="slider2">
                                    @foreach($product['images'] as $var)
                                    <div>
                                        <img alt="" src="{{Storage::disk('s3')->url('public/images/product/'. $var['img_product'])}}" width="320px" height="340px">
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <ul class="nav nav-pills" id="myTab3" role="tablist">
                                    @if($product['varian'] !== null)
                                    @foreach($product['varian']['varian_id'] as $var)
                                    <li class="nav-item">
                                        <a class="nav-link" id="home-tab{{ $var->id }}" data-toggle="tab" href="#home{{ $var->id }}" role="tab" aria-controls="home" aria-selected="true">{{ $product->varian->name_varian }} -
                                            {{ $var ? $var->name : '' }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="tab-content" id="myTabContent2">
                                    @foreach($product['varian']['varian_id'] as $var)
                                    <div class="tab-pane fade show " id="home{{ $var->id }}" role="tabpanel" aria-labelledby="home-tab{{ $var->id }}">
                                        <h6 class="card-title"> {{ $var['sub'] ? $var['sub']['name_varian'] : ''}} </h6>
                                        <hr>
                                        <table class="table table-bordered">
                                            <tbody>
                                                @if($var['sub']['sub2'] !== null)
                                                @foreach($var['sub']['sub2'] as $sub2)
                                                <tr>
                                                    <th>{{ $sub2 ? $sub2->name : ''}}</th>
                                                    <th>{{ $sub2['detail'] ? $sub2['detail']['fix_price'] : '' }}</th>
                                                    @if($sub2['detail']['qty'])
                                                    <th>
                                                        <div class="input-group">
                                                            <input disabled id="{{ $sub2['detail'] ? $sub2['detail']['id'] : '' }}" value="{{ $sub2['detail'] ? $sub2['detail']['qty'] : '' }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">QTY</span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        {{-- <button class="btn btn-info" disabled onclick="updateQty('{{ $sub2['detail'] ? $sub2['detail']['id'] : '' }}')"><i class="fa fa-save"></i></button> --}}
                                                    </th>
                                                    @endif
                                                    @if($product['varian']['varian_id'] !== null)
                                                    @foreach($product['varian']['varian_id'] as $sub2)
                                                    <th>{{ $sub2['detail'] ? $sub2['detail']['fix_price'] : '' }}</th>
                                                    @endforeach
                                                    @endif
                                                </tr>
                                                @endforeach
                                                @else
                                                @if($product['varian']['varian_id'] !== null)
                                                @foreach($product['varian']['varian_id'] as $sub2)
                                                <tr>
                                                    <th>{{ $sub2 ? $sub2->name : ''}}</th>
                                                    <th>{{ $sub2['detail'] ? $sub2['detail']['fix_price'] : '' }}</th>
                                                    @if($sub2['detail']['qty'])
                                                    <th>
                                                        <div class="input-group">
                                                            <input disabled id="{{ $sub2['detail'] ? $sub2['detail']['id'] : '' }}" value="{{ $sub2['detail'] ? $sub2['detail']['qty'] : '' }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">QTY</span>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th>
                                                        {{-- <button class="btn btn-info" disabled onclick="updateQty('{{ $sub2['detail'] ? $sub2['detail']['id'] : '' }}')"><i class="fa fa-save"></i></button> --}}
                                                    </th>
                                                    @endif
                                                </tr>
                                                @endforeach
                                                @endif
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    @endforeach
                                    {{-- kodisi tidak ada varian --}}
                                    @else
                                    <table class="table table-bordered">
                                      <tbody>
                                        <tr>
                                          <th>Rp. {{ $productDetail->fix_price }}</th>
                                          <th>
                                            <div class="input-group">
                                              <input disabled value="{{ $productDetail->qty }}" type="text" class="form-control" aria-label="Amount (to the nearest dollar)" maxlength="2" size="2">
                                              <div class="input-group-append">
                                                <span class="input-group-text">QTY</span>
                                              </div>
                                            </div>
                                          </th>
                                        </tr>
                                      </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12 mb-5 mt-4">
                                    <div class="section-title">Product Description</div>
                                    <p class="section-lead">{!! $product->description !!}.</p>
                                </div>
                            </div>
                        </div>
                        <form action="{{ route('produk.blok', $product->id) }}" method="POST">
                            @csrf
                            <div class="col-12">
                                @if($product->is_block == 1)
                                <button class="btn btn-danger btn-block" disabled>Produk di blok</button>
                                @else
                                <button class="btn btn-danger btn-block">Blok produk</button>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/page/owl-carousel.js') }}"></script>

<script>
    function updateQty(id) {
        var qty = $(`#${id}`).val();
        $.ajax({
            url: '{{ url("/reseller/product/detailproduct/updateQty/") }}' + '/' + id + '/' + qty,
            type: 'get',
            data: {},
            contentType: false, //untuk upload image
            processData: false, //untuk upload image
            timeout: 300000, // sets timeout to 3 seconds
            dataType: 'json',
            success: function(e) {
                if (e.status == true) {
                    new Noty({
                        text: 'Berhasil Mengupdate Data',
                        type: 'success'
                    }).show();
                } else {
                    new Noty({
                        text: 'Gagal Mengupdate Data',
                        type: 'warning'
                    }).show();
                }
            }
        });
    }
</script>
@endsection