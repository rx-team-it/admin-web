@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Toko</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Toko</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Toko</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <a class="btn btn-primary btn-icon icon-left" href="{{ route('list.produk') }}"><i class="fas fa-print"></i> List Produk Di Blok</a>
                        <br>
                        <br>
                        <table id="table_produk" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>No</th>
                                <th>Nama Toko</th>
                                <th>No Hp</th>
                                <th>Email</th>
                                <th>Alamat</th>
                                <th>Status Toko</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table_produk').DataTable({
            pageLength: 25,
            processing: true,
            serverSide: true,
            dom: '<"html5buttons">lBfrtip',
            buttons: [{
                extend: 'csv'
            }, {
                extend: 'pdf',
                title: 'List Order Sudah Bayar'
            }, {
                extend: 'excel',
                title: 'List Order Sudah Bayar'
            }, {
                extend: 'print',
                title: 'List Order Sudah Bayar'
            }, ],
            ajax: {
                url: "{{ route('produk.index') }}",
                type: 'GET'
            },
            columns: [{
                    "data": 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'name_site',
                    name: 'name_site',
                },
                {
                    data: 'contact_phone',
                    name: 'contact_phone',
                },
                {
                    data: 'contact_email',
                    name: 'contact_email',
                },
                {
                    data: 'contact_address',
                    name: 'contact_address'
                },
                {
                    data: 'status_label',
                    name: 'status_label'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });
</script>
@endsection