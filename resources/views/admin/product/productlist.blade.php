@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <h1>List Produk</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Produk</div>
        </div>
    </div>
    <div class="section-body">
        <!-- <h2 class="section-title">Blog Style C</h2> -->
        <div class="row">
            @foreach($products as $product)
            <div class="col-12 col-md-4 col-lg-4">
                <article class="article article-style-c">
                    <div class="article-header">
                        <div class="owl-carousel owl-theme slider" id="slider1">
                            @foreach($product->product_images as $images)
                            <img class="img-responsive thumbnail" src="{{Storage::disk('s3')->url('public/images/product/' . $images->img_product)}}" width="120" height="220" alt="">
                            @endforeach
                        </div>
                    </div>
                    <div class="article-details">
                        <div class="article-category"><a>{{ $product->category->name }}</a>
                            <div class="bullet"></div> <a href="#">{{ $product->created_at }}</a>
                        </div>
                        <div class="article-title">
                            <h2><a href="{{ route('produk.detail', $product->id) }}">{{ $product->name }}</a></h2>
                        </div>
                        <p>{!! substr($product->description,0, 150) !!} ...</p>
                        <!-- @foreach($product->product_varians as $var)
                        <p>{{ $var->name_varian }}</p>
                        @foreach($var->product_varian_option as $option)
                        <button class="btn btn-primary">{{ $option->name }}</button>
                        @endforeach
                        @endforeach -->
                        <!-- <div class="article-user">
                            <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}">
                            <div class="article-user-details">
                                <div class="user-detail-name">
                                    <a href="#">Sarah Smith</a>
                                </div>
                                <div class="text-job">Java Developer</div>
                            </div>
                        </div> -->
                    </div>
                </article>
            </div>
            @endforeach
        </div>
        {{ $products->links() }}
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/page/owl-carousel.js') }}"></script>
@endsection