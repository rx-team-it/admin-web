@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <h1>{{ $product->name }}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">List Toko</a></div>
            <div class="breadcrumb-item">Detail Product</div>
        </div>
    </div>
    <div class="section-body">
        <div class="invoice">
            <div class="invoice-print">
                <div class="card-body">
                    <div class="owl-carousel owl-theme slider" id="slider2">
                        @foreach($product['varian']['varian_id'] as $var)
                        @foreach($var['sub']['sub2'] as $sub2)
                        <div>
                            <img alt="" src="{{Storage::disk('s3')->url('public/images/product/'. $sub2['img']['img_product'])}}" width="280px" height="580px">
                        </div>
                        @endforeach
                        @endforeach
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <address>
                            <strong>Deskripsi Produk</strong><br>
                            {{ $product->description }}
                        </address>
                        <address>
                            <strong>{{$product['varian']['name_varian']}}</strong><br>
                            @foreach($product['varian']['varian_id'] as $var)
                            <br>
                            {{$var->name}}
                            <br>
                            {{$var['sub']['name_varian']}}
                            <br>
                            @foreach($var['sub']['sub2'] as $sub2)
                            <br>
                            {{$sub2->name}}
                            <br>
                            {{$sub2['detail']['price']}}
                            <br>
                            {{$sub2['detail']['sku']}}
                            @endforeach
                            @endforeach
                        </address>
                    </div>
                </div>
            </div>
            <hr>
            <hr>
            <div class="text-md-right">
                <div class="float-lg-left mb-lg-0 mb-3">
                    <form action="{{ route('produk.blok', $product->id) }}" method="post">
                        @csrf
                        <button class="btn btn-danger btn-icon icon-left"><i class="fas fa-times"></i> Blok</a></button>
                    </form>
                </div>
                <form action="{{ route('produk.delete', $product->id) }}" method="post">
                    @csrf
                    <button class="btn btn-danger btn-icon icon-left"><i class="fas fa-times"></i> Delete Permanen</a></button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/page/owl-carousel.js') }}"></script>
@endsection