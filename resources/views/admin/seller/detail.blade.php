@extends('admin.layouts.admin.index')

@section('admin.content')
    <section class="section">
        <div class="section-header">
            <div class="section-header-back">
                <a href="{{ route('index.seller') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
            </div>
            <h1>Detail Seller</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
                <div class="breadcrumb-item active"><a href="{{ route('index.seller') }}">List Seller Aktif</a></div>
                <div class="breadcrumb-item">Seller</div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-md-4">
                    <article class="article article-style-c">
                        <div class="article-details">
                            <div class="article-category"><a href="#">Mendaftar</a>
                                <div class="bullet"></div> <a href="#">{{ !empty($user) ? $user->created_at : '' }}</a>
                            </div>
                            <div class="article-title float-left">
                                <h3>{{ !empty($user->name) ? $user->name : '' }}</h3>
                            </div>
                            <div class="article-title float-right">
                                <div class="text-job">{{ !empty($user->role_user->role->nm_role) ? $user->role_user->role->nm_role : '' }}</div>
                            </div>
                            <!-- <p>Ini adalah Deskripsi</p> -->
                            <div class="article-user">
                                @if ($user->user_detail->foto_user == null)
                                <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}">
                                @else
                                <img alt="image" src="{{Storage::disk('s3')->url('public/images/profile/' . $user->user_detail->foto_user)}}">
                                @endif
                                <div class="article-user-details">
                                    <div class="user-detail-name">
                                        <a href="#">{{ !empty($user) ? $user->user_detail->address : '' }}</a>
                                    </div>
                                    <div class="text-job">{{ !empty($user->user_detail->phone) ? $user->user_detail->phone : '' }}</div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-8">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title">List Produk</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_produk" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <th>No</th>
                                        <th>Thumbnail</th>
                                        <th>Nama</th>
                                        <th>Berat</th>
                                        <th>Stok</th>
                                        <th>Merek</th>
                                        <th>Sku Induk</th>
                                        <th>Tanggal</th>
                                        <th>Aksi</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
    $('#table_produk').DataTable({
        pageLength: 10, 
        processing: true, 
        serverSide: true, 
        dom: '<"html5buttons">lfrtip', 
        buttons: [{
                extend: 'csv'
            }, {
                extend: 'pdf', 
                title: 'List Order Sudah Bayar'
            }, {
                extend: 'excel', 
                title: 'List Order Sudah Bayar'
            }, {
                extend: 'print', 
                title: 'List Order Sudah Bayar'
        }], 
        ajax: {
            url: "{{ route('seller.detail', $user->id) }}", 
            type: 'GET'
        }, 
        columns: [{
            "data": 'DT_RowIndex', 
            orderable: false, 
            searchable: false
        }, {
            data: 'thumbnail', 
            name: 'thumbnail', 
        }, {
            data: 'name', 
            name: 'name', 
        }, 
        {
            data: 'berat', 
            name: 'berat', 
        }, 
        {
            data: 'stock', 
            name: 'stock'
        }, 
        {
            data: 'merk', 
            name: 'merk'
        }, 
        {
            data: 'sku_induk', 
            name: 'sku_induk'
        }, 
        {
            data: 'created_at', 
            name: 'created_at'
        }, 
        {
            data: 'action', 
            name: 'action'
        }], 
            order: [[0, 'asc']]
        });
    });

</script>
@endsection
