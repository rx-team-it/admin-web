@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Kategori</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Kategori</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('message_error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">                            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-times"></i> &nbsp; {{ session('message_error') }}</strong>
            </div>    
            @endif 
            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">                            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('success') }}</strong>
            </div>    
            @endif
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">                            
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                @foreach ($errors->all() as $error)
                <strong><i class="fa fa-check-times"></i>&nbsp; Gagal update, silahkan cek format kembali! ( {{ $error }} )</strong> <br>
                @endforeach      
            </div>  
          @endif
        </div>
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">Table Kategori</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modal-kategori">
                            <i class="fa fa-plus-circle"></i> Kategori Baru
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="parent_category" class="table table-hover table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Icon</th>
                                    <th>Banner Kategori</th>
                                    <th>Created At</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">Table Sub Kategori</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="child_category" class="table table-hover table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Icon</th>
                                    <th>Created At</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- modal tambah kategori --}}
<div class="modal fade" id="modal-kategori">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('category.store') }}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Kategori Baru</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="kategori">Tambah sub-kategori ?</label>
                                <input type="checkbox" class="" id="sub" checked />
                            </div>
                            <div id="kategori" style="display: none">
                                <div class="form-group" >
                                    <label for="name">Nama Kategori</label>
                                    <input type="text" name="name" class="form-control">
                                    <p class="text-danger">{{ $errors->first('name') }}</p>
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Foto : </label>
                                    <div class="custom-file">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                        <input type="file" name="foto_banner_kategori" class="custom-file-input" id="customFile" required>
                                    </div>
                                </div>
                            </div>
                            <div id="form">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="parent_category">Kategori</label>
                                        <select name="parent_category" id="parent_cat" class="form-control">
                                            <option value="" selected disabled>-- PILIH --</option>
                                            @foreach ($parent as $row)
                                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                                            @endforeach
                                        </select>
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nama Sub-kategori</label>
                                        <input type="text" name="name1" class="form-control">
                                        <p class="text-danger">{{ $errors->first('name1') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{-- end tambah kategori --}}

<!-- Modal Edit -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('category.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name : </label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="parent_category">Kategori</label>
                        <select name="parent_category" class="form-control" id="parent_category">
                            <option value="">Pilih</option>
                            @foreach ($parent as $row)
                            <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Icon : (format: jpg, jpeg, png)</label>
                        <input type='file' name='file' id='file' class='form-control' placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end modal edit --}}

{{-- modal edit parent --}}
<div class="modal fade" id="parent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form action="{{ route('category.update') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama : </label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Ikon : (format: jpg, jpeg, png) </label>
                        <input type='file' name='file' id='file' class='form-control' placeholder="Gambar">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto Banner : </label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="foto_banner_kategori" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

@if(Session::has('error'))
<script>
    swal("Gagal", "{!!Session::get('error')!!}", "error", {
        button: "OK",
    });
</script>
@endif

<script>
    $('#exampleModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var name = button.data('name') // Extract info from data-* attributes
        var parent_category = button.data('parent_category') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Edit Category')
        modal.find('#recipient-name').val(recipient)
        modal.find('#name').val(name)
        modal.find('#parent_category').val(parent_category)
    })
</script>
<script>
    $('#parent').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var name = button.data('name') // Extract info from data-* attributes
        var parent_category = button.data('parent_category') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Edit Category')
        modal.find('#recipient-name').val(recipient)
        modal.find('#name').val(name)
        modal.find('#parent_category').val(parent_category)
    })
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#sub').click(function() {
            if( $(this).is(':checked')) {
                $("#form").show();
                $('#kategori').hide();
                $('input[name="name1"]').val('');
            } else {
                $("#form").hide();
                $('input[name="name"]').val('');
                $('#parent_cat').val('');
                $('#kategori').show();
            }
        });
    });
</script>

<script>
    $('.swal-confirm').click(function(e) {
        id = e.target.dataset.id;
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // swal("Poof! Your imaginary file has been deleted!", {
                    //     icon: "success",
                    // });
                    $(`#delete${id}`).submit();
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    });
</script>

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#parent_category').DataTable({
            pageLength: 10,
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('category.index') }}",
                type: 'GET'
            },
            columns: [{
                    "data": 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'name',
                    name: 'name',
                },
                {
                    data: 'image',
                    name: 'image',
                },
                {
                    data: 'foto_banner_kategori',
                    name: 'foto_banner_kategori',
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#child_category').DataTable({
            pageLength: 10,
            processing: true,
            serverSide: true,
            dom: '<"html5buttons">frtip',
            ajax: {
                url: "{{ route('category.child') }}",
                type: 'GET'
            },
            columns: [{
                    "data": 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                }, {
                    data: 'parent',
                    name: 'parent',
                },
                {
                    data: 'name',
                    name: 'name',
                },
                {
                    data: 'image',
                    name: 'image',
                },
                {
                    data: 'created_at',
                    name: 'created_at',
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
            order: [
                [0, 'asc']
            ]
        });
    });
</script>
@endsection