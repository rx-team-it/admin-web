@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Tiketing</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Tiketing</div>
        </div>
    </div>
    @if(Session::has('delete'))
    <div class="alert alert-success" role="alert">
        A simple success alert—check it out!
    </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4 class="card-title">List Kategori Tiketing</h4>
                    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#tambahKategori">
                        Tambah Kategori
                      </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example1" class="table table-hover table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Created At</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                ?>
                                @foreach($category_tiketing as $data)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->created_at }}</td>
                                    <td>
                                        {{-- <a href="#" data-id="{{ $data->id }}" class="btn btn-danger btn-sm swal-confirm"> --}}
                                            <form action="{{ route('category.delete', $data->id) }}" id="delete{{ $data->id }}" method="POST">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                            </form>
                                        {{-- </a> --}}
                                    </td>
                                </tr>
                                <?php
                                $no++;
                                ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Keluhan</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">Semua</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">Belum Dibalas &nbsp; <span class="badge badge-danger">{{$hitung_blm_balas}}</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">Closed</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            <div class="table-responsive">
                                <table id="semua" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Kategori</th>
                                            <th>Pemasalahan</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($message_tiketing as $tiketing)
                                        @if($tiketing->nama_pembalas == null)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ !empty($tiketing->user) ? $tiketing->user['name'] : '' }}</td>
                                            <td>{{ $tiketing->category['name'] }}</td>
                                            <td>{{ $tiketing->judul }}</td>
                                            <td>{!! $tiketing->status_label !!}</td>
                                            <td>{{ $tiketing->created_at }}</td>
                                            <td>
                                                @if($tiketing->status == 3)
                                                <button class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>
                                                @else
                                                <a href="{{ route('detail.tiketing', $tiketing->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div class="table-responsive">
                                <table id="dibalas" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Kategori</th>
                                            <th>Pemasalahan</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($belum_dibalas as $tiketing)
                                        @if($tiketing->nama_pembalas == null)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ !empty($tiketing->user) ? $tiketing->user['name'] : '' }}</td>
                                            <td>{{ $tiketing->category['name'] }}</td>
                                            <td>{{ $tiketing->judul }}</td>
                                            <td>{!! $tiketing->status_label !!}</td>
                                            <td>{{ $tiketing->created_at }}</td>
                                            <td>
                                                @if($tiketing->status == 3)
                                                <button class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>
                                                @else
                                                <a href="{{ route('detail.tiketing', $tiketing->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                            <div class="table-responsive">
                                <table id="close" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Kategori</th>
                                            <th>Pemasalahan</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($closed as $tiketing)
                                        @if($tiketing->nama_pembalas == null)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ !empty($tiketing->user) ? $tiketing->user['name'] : '' }}</td>
                                            <td>{{ $tiketing->category['name'] }}</td>
                                            <td>{{ $tiketing->judul }}</td>
                                            <td>{!! $tiketing->status_label !!}</td>
                                            <td>{{ $tiketing->created_at }}</td>
                                            <td>
                                                @if($tiketing->status == 3)
                                                <button class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>
                                                @else
                                                <a href="{{ route('detail.tiketing', $tiketing->id) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- modal tambah --}}

<div class="modal fade" id="tambahKategori" tabindex="-1" aria-labelledby="tambahKategoriLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="tambahKategoriLabel">Kategori Baru</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">
            <form action="{{ route('category.post') }}" method="post">
                @csrf
                <label for="name">Kategori</label>
                <input type="text" name="name" class="form-control" required>
                <p class="text-danger">{{ $errors->first('name') }}</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
          <button class="btn btn-primary btn-sm">Tambah</button>
        </form>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

@if(Session::has('error'))
<script>
    swal("Gagal", "{!!Session::get('error')!!}", "error", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script>
    $(function() {
        $("#close").DataTable({
            pageLength: 4,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });
        $('#semua').DataTable({
            pageLength: 4,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });
        $('#dibalas').DataTable({
            pageLength: 4,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });
    });
</script>

<script>
    $('.swal-confirm').click(function(e) {
        id = e.target.dataset.id;
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // swal("Poof! Your imaginary file has been deleted!", {
                    //     icon: "success",
                    // });
                    $(`#delete${id}`).submit();
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    });
</script>

@endsection