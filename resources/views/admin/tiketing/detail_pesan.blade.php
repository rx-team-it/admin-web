@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('tiketing') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Tiketing</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('tiketing') }}">Beranda</a></div>
            <div class="breadcrumb-item">Detail Tiketing</div>
        </div>
    </div>
    <div class="section-body">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="chat">
                    <div class="chat-header clearfix">
                        <img src="{{ ($foto_profile['foto_user'] == null) ? getAWS_BUCKET('images/admin/user-default.png') : getAWS_BUCKET('images/foto_user/' .$foto_profile['foto_user'])}}" alt="{{$foto_profile['foto_user']}}" alt="avatar">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <div class="chat-about">
                                    <div class="chat-with">{{ !empty($message_tiketing->user) ? $message_tiketing->user->name : '' }}</div>
                                    <div class="chat-num-messages"><i class="text-danger fas fa-star"></i> Subjek : {{ $message_tiketing->judul }}</div>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                @if ($message_tiketing->status == 3)
                                <p><i class="fas fa-check text-success"></i> Keluhan Telah Ditutup</p>
                                @else
                                <a href="{{ route('close.tiketing', $message_tiketing->id) }}" class="btn btn-danger btn-sm"><i class="fas fa-power-off"></i> Tutup Keluhan</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-box" id="mychatbox">
                    <div class="card-body chat-content" tabindex="2" style="overflow: hidden; outline: none;">
                        <div class="chat-item chat-left" style=""><img src="{{ ($foto_profile['foto_user'] == null) ? getAWS_BUCKET('images/admin/user-default.png') : getAWS_BUCKET('images/foto_user/' .$foto_profile['foto_user'])}}" alt="{{$foto_profile['foto_user']}}" alt="avatar">
                            <div class="chat-details">
                                <div class="chat-text">{{ $message_tiketing->message }}</div>
                                <div class="chat-time">{{ $message_tiketing->created_at }}</div>
                            </div>
                        </div>
                        @foreach($message_tiketing['reply'] as $p)
                        @if ($p->user_id == auth()->user()->id)
                        <div class="chat-item chat-right" style=""><img src="{{ getAWS_BUCKET('images/admin/user-default.png') }}">
                            <div class="chat-details">
                                <div class="chat-text">{{ $p->pesan }}</div>
                                <div class="chat-time">{{ $p->created_at }}</div>
                            </div>
                        </div>
                        @elseif($message_tiketing->status == 3)
                        <hr>
                        <div class="chat-item text-center" style="">
                            <div class="chat-details">
                                <div class="chat-text">Keluhan Telah Berakhir</div>
                                {{-- <div class="chat-time">{{ $p->created_at }}
                            </div> --}}
                        </div>
                    </div>
                    <hr class="mt-1">
                    @else
                    <div class="chat-item chat-left" style=""><img src="{{ ($foto_profile['foto_user'] == null) ? getAWS_BUCKET('images/admin/user-default.png') : getAWS_BUCKET('images/foto_user/' .$foto_profile['foto_user'])}}" alt="{{$foto_profile['foto_user']}}" alt="avatar">
                        <div class="chat-details">
                            <div class="chat-text">{{ $p->pesan }}</div>
                            <div class="chat-time">{{ $p->created_at }}</div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
                <div class="card-footer chat-form">
                    @if ($message_tiketing->status == 3)
                    <form id="chat-form" action="#" method="POST">
                        <input type="text" class="form-control" name="pesan" placeholder="Balas ..." disabled>
                        <button class="btn btn-success" disabled>
                            <i class="far fa-paper-plane"></i>
                        </button>
                    </form>
                    @else
                    <form id="chat-form" action="{{ route('reply.tiketing', $message_tiketing->id) }}" method="POST">
                        @csrf
                        <input type="text" class="form-control" name="pesan" placeholder="Balas ...">
                        <button class="btn btn-primary">
                            <i class="far fa-paper-plane"></i>
                        </button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    {{-- <h2 class="section-title">{{ $message_tiketing->created_at }}</h2>
    <div class="row">
        <div class="col-12">
            <div class="activities">
                <div class="activity">
                    <div class="activity-icon bg-primary text-white">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="activity-detail">
                        <div class="mb-2">
                            <span class="text-job">{{ !empty($message_tiketing->user) ? $message_tiketing->user->name : '' }}</span>
                        </div>
                        <span class="text-job">SUBJECT : {{ $message_tiketing->judul }}</span>
                        <p>{{ $message_tiketing->message }}</p>
                    </div>
                </div>
                @foreach($message_tiketing['reply'] as $p)
                <div class="activity">
                    <div class="activity-icon bg-primary text-white">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="activity-detail">
                        <div class="mb-2">
                            <span class="text-job">{{ $p->created_at }}</span>
                            <span class="bullet"></span>
                            <span class="text-job">{{ !empty($p->user) ? $p->user->name : '' }}</span>
                            <!-- <div class="float-right dropdown">
                                    <a href="#" data-toggle="dropdown"><i class="fas fa-ellipsis-h"></i></a>
                                    <div class="dropdown-menu">
                                        <div class="dropdown-title">Options</div>
                                        <a href="#" class="dropdown-item has-icon"><i class="fas fa-eye"></i> View</a>
                                        <a href="#" class="dropdown-item has-icon"><i class="fas fa-list"></i> Detail</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item has-icon text-danger" data-confirm="Wait, wait, wait...|This action can't be undone. Want to take risks?" data-confirm-text-yes="Yes, IDC"><i class="fas fa-trash-alt"></i> Archive</a>
                                    </div>
                                </div> -->
                        </div>
                        <p style="font-family:Monaco;">{{ $p->pesan }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @if($message_tiketing->status == 3)
    <section class="section">
        <div class="container mt-5">
            <div class="page-error">
                <div class="page-inner">
                    <h5 style="font-family:Monaco;">Tiket Berhasil DiTutup</h5>
                </div>
            </div>
        </div>
    </section>
    @else
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" style="font-family:Monaco;">Balas Keluhan</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('reply.tiketing', $message_tiketing->id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Pesan</label>
                            <textarea class="form-control" name="pesan" style="font-family:Monaco;"></textarea>
                        </div>
                        <button class="btn btn-primary btn-sm">Balas</button>
                        <a href="{{ route('close.tiketing', $message_tiketing->id) }}" class="btn btn-danger btn-sm">Tutup Keluhan</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endif --}}
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/js/page/chat.js') }}"></script>
@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>

@endif

@endsection