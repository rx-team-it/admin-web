@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

<div class="card-header">
    <h5>Daftar Produk</h5>
</div>
<div class="card-body">
    <form action="{{ route('flash.tambah') }}" method="POST" class="needs-validation">
        @csrf
        <div class="table-responsive">
            <table id="example" class="display" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th><input type="checkbox" class="select-all checkbox" name="select-all"></th>
                        <th>Foto Produk</th>
                        <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product as $item)
                    <tr>
                        <td>
                            <input type="checkbox" class="select-item checkbox" name="product_id[]" value="{{$item->id}}" />
                        </td>
                        <td><img src="{{!empty($item->product_images_thumbnail->img_product) ? Storage::disk('s3')->url('public/images/product/' . $item->product_images_thumbnail->img_product) : ''}}" alt="" height="50px" width="50px"></td>
                        <td>{{$item->name}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <table id="table_group" class="table table-hover table-bordered table-sm">
            <thead>
                <th>Pilih Grup</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <select class="form-control" name="group">
                            @foreach ($group_product as $item)
                            <option value="{{$item->id}}">{{$item->nm_group}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <button type="submit" class="btn btn-sm btn-primary">
            <i class="fa fa-check"></i> Simpan
        </button>
    </form>
</div>


@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('table.display').DataTable();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tbl').DataTable({

        });
        //button select all or cancel
        $("#select-all").click(function() {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function(index, item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function() {
            $("input.select-item").each(function(index, item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function() {
            var items = [];
            $("input.select-item:checked:checked").each(function(index, item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            } else {
                var values = items.join(',');
                console.log(values);
                var html = $("<div></div>");
                html.html("selected:" + values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function() {
            var checked = this.checked;
            $("input.select-item").each(function(index, item) {
                item.checked = checked;
            });
        });

        //check selected items
        $("input.select-item").click(function() {
            var checked = this.checked;
            console.log(checked);
            checkSelected();
        });

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            console.log("total:" + total);
            console.log("len:" + len);
            all.checked = len === total;
        }
    });
</script>
@endsection
