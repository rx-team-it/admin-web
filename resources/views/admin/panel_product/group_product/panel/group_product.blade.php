<div class="card-header">
    <h4>Daftar Grup Produk</h4>
    <div class="card-header-action">
        <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-default"><i class="fas fa-plus-circle"></i>
            Grup Baru</a>
    </div>
</div>
<div class="card-body">
    <div id="accordion">
        @foreach ($group_product as $f)
        <div class="accordion">
            <div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-{{$f->id}}" aria-expanded="false">
                <div class="row">
                    <div class="col-6">
                        <h4>
                            {{ $f->nm_group }} @if ($f->type == 'FlashSale')
                            &nbsp; <span class="badge badge-danger" id="flashsale"></span>
                            @else
                                
                            @endif 
                        </h4>
                    </div>
                    <div class="col-6 text-right">
                        <i class="fa fa-arrow-alt-circle-down" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detail"></i> &nbsp;
                        <a href="{{ route('panel-grup.delete', $f->id) }}"><i class="far fa-trash-alt text-danger" data-toggle="tooltip" data-placement="top" title="" data-original-title="Hapus Grup"></i></a>
                    </div>
                </div>
            </div>
            <div class="accordion-body collapse" id="panel-body-{{$f->id}}" data-parent="#accordion">
                <form action="{{route('panel-produk.delete')}}" method="post">
                    @csrf
                    @if ($f->image == null)
                    <div class="row">
                        <div class="col-6">
                            <h6>Tambah Banner</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="" class="btn btn-sm btn-primary" role="button" data-toggle="modal" data-id="{{$f->id}}" data-target="#modal-tambahBanner"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <div class="col-6">
                            <h6>Foto Banner</h6>
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{route('banner.delete', $f->id)}}" onclick="alert('hapus banner?')" class="btn btn-sm btn-danger" role="button"><i class="fa fa-trash"></i></a>
                            <a href="" class="btn btn-sm btn-warning" role="button" data-toggle="modal" data-id="{{$f->id}}" data-target="#modal-editBanner"><i class="fa fa-edit"></i></a>
                        </div>
                        <img alt="image" src="{{Storage::disk('s3')->url('public/images/BannerImage/' . $f->image)}}" width="100%" height="60%">
                    </div>
                    <hr>
                    @endif
                    <table id="tbl-grup" class="display" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>
                                    Pilih
                                </th>
                                <th>Foto Produk</th>
                                <th>Nama</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($f['flash_sale'] as $item)
                            <tr>
                                <td>
                                    <input type="checkbox" class="select-item1 checkbox" name="product_id[]" value="{{$item->id}}" />
                                </td>
                                <td><img src="{{!empty($item->product->product_images_thumbnail->img_product) ? Storage::disk('s3')->url('public/images/product/' . $item->product->product_images_thumbnail->img_product) : ''}}" alt="" height="50px" width="50px"></td>
                                <td>{{!empty($item->product->name) ? $item->product->name: 'Produk telah dihapus'}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr>
                    <div class="col-12 text-right">
                        <button type="submit" class="btn btn-block btn-danger">
                            Hapus Produk Terpilih
                        </button>
                    </div>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</div>

@section('js')
<script>
    var cd = @json($count_down);
    console.log(cd);
    if (cd == null) {
        // console.log("ga ada flashsalenya");
    } else {
        var id_event = cd.id;
        var set_event = cd.event;
        var sesi_awal = cd.sesi_awal;
        var sesi_akhir = cd.sesi_akhir;
        // var countDownDate = new Date("2022-07-07 11:19:00").getTime();
        // console.log(id_event);
        var countDownDate = new Date(set_event +" "+ sesi_akhir).getTime();
        
        var x = setInterval(function() {
        
          // Get today's date and time
          var now = new Date().getTime();
        
          // Find the distance between now and the count down date
          var distance = countDownDate - now;
        
          // Time calculations for days, hours, minutes and seconds
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
          // Display the result in the element with id="demo"
          document.getElementById("flashsale").innerHTML = hours + "Jam "
          + minutes + "Menit " + seconds + "Detik ";
        
          // If the count down is finished, write some text
          if (distance < 0) {
              clearInterval(x);
            $.ajax({
                    url: "{{ url('/product/panel/delete-panel') }}"+ '/' + id_event,
                    type: 'GET',
                    success: function(response){
                        console.log(response);
                        window.location.reload();
                    }
                })
            // document.getElementById("flashsale").innerHTML = "EXPIRED";
          }
        }, 1000);
    }
    </script>

<script>
    $('#modal-tambahBanner').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var modal = $(this)
        modal.find('#id').val(id)
    })

    $('#modal-editBanner').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var modal = $(this)
        modal.find('#id').val(id)
    })
</script>
@endsection