@extends('admin.layouts.admin.index')
@section('admin.css')
    <style>
        #hidden_div {
            display: none;
        }
    </style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection
@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Panel Produk</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item">Panel Produk</div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <p class="text-center">Pilih Produknya terlebih dahulu kak</p>
                @endforeach
            </div>
            @endif
            @if(Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-check-circle"></i> {{ session('message') }}</strong>
            </div>
            @endif
            @if(Session::has('gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-times"></i> {{ session('gagal') }}</strong>
            </div>
            @endif
        </div>
        <div class="col-md-6">
            <div class="card card-primary">
                @include('admin.panel_product.group_product.panel.product_tambah')
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-primary">
                @include('admin.panel_product.group_product.panel.group_product')
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('group.tambah') }}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Buat Produk Grup</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nm_group">Nama Grup</label>
                                <input type="text" name="nm_group" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="type">Tipe Grup</label>
                                <select id="test" class="form-control" name="type" onchange="showDiv(this)" required>
                                    <option value="Produk">Produk</option>
                                    <option value="Rekomendasi">Rekomendasi</option>
                                    @if ($cd == "null")
                                        <option value="FlashSale">FlashSale</option>
                                    @endif
                                    {{-- diskon hidden dulu --}}
                                    {{-- <option value="Diskon">Diskon</option> --}}
                                    <!-- <option value="Gratis ongkir">Gratis ongkir</option> -->
                                </select>
                            </div>
                            <div id="event" style="display: none">
                                <div class="form-group">
                                    <div class="input-daterange">
                                        <label for="date">tanggal flashsale</label>
                                        <input type="text" name="min" id="min" class="form-control" placeholder="Tgl. Mulai" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="html">sesi</label>
                                    <select class="form-control" name="sesi">
                                        <option value="" selected disabled>== PILIH ==</option>
                                        <option value="00:00:00-02:00:00">00:00:00 - 02:00:00</option>
                                        <option value="02:00:00-04:00:00">02:00:00 - 04:00:00</option>
                                        <option value="04:00:00-06:00:00">04:00:00 - 06:00:00</option>
                                        <option value="06:00:00-08:00:00">06:00:00 - 08:00:00</option>
                                        <option value="08:00:00-10:00:00">08:00:00 - 10:00:00</option>
                                        <option value="10:00:00-12:00:00">10:00:00 - 12:00:00</option>
                                        <option value="12:00:00-14:00:00">12:00:00 - 14:00:00</option>
                                        <option value="14:00:00-16:00:00">14:00:00 - 16:00:00</option>
                                        <option value="16:00:00-18:00:00">16:00:00 - 18:00:00</option>
                                        <option value="18:00:00-20:00:00">18:00:00 - 20:00:00</option>
                                        <option value="20:00:00-22:00:00">20:00:00 - 22:00:00</option>
                                        <option value="22:00:00-24:00:00">22:00:00 - 24:00:00</option>
                                    </select>
                                </div>
                            </div>
                            <div id="hidden_div" style="display:none;">
                                <div class="form-group">
                                    <label class="showing" for="diskon">Diskon %</label>
                                    <input type="number" min="0" name="diskon" class="form-control showing">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Foto Banner : </label>
                                <div class="custom-file">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                    <input type="file" name="image" class="custom-file-input" id="customFile">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- modal tambah banner --}}
<div class="modal fade" id="modal-tambahBanner">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('group.tambah.banner') }}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah foto banner</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto Banner : </label>
                        <input type="text" name="id" class="form-control" id="id" hidden>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="image" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- modal edit banner --}}
<div class="modal fade" id="modal-editBanner">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('group.edit.banner') }}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit foto banner</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto Banner : </label>
                        <input type="text" name="id" class="form-control" id="id" hidden>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="image" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection
<script>
    function showDiv(select){
        if(select.value=="FlashSale"){
            document.getElementById('event').style.display = "block";
        } else{
            $('#min').val('');
            $('select[name="sesi"]').val('');
            document.getElementById('event').style.display = "none";
        }
    }
</script>