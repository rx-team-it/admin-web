<nav class="navbar navbar-expand-lg main-navbar">
    <div class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg collapse-btn"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
                    <i class="fas fa-expand"></i>
                </a>
            </li>
            <li>
                <center>
                    <h3 style="font-family:Monaco;" class="text-navbar-admin">Panel Vendor</h3>
                </center>
            </li>
        </ul>
    </div>
    <ul class="navbar-nav navbar-right">
        <li><a href="" class="nav-link nav-link-lg"><i class="fas fa-sync"></i></a></li>
        {{-- <li>
            <a href="https://chat.{{session()->get('sites')->domain}}/app/accounts/2/dashboard" target="_blank" class="nav-link nav-link-lg">
                    <i class="far fa-envelope"></i>
                </a>
            </li> --}}
        {{-- <li><a href="#" class="nav-link nav-link-lg message-toggle beep"><i class="far fa-envelope"></i></a>
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
                <div class="dropdown-header">Messages
                    <div class="float-right">
                        <a href="#">Mark All As Read</a>
                    </div>
                </div>
                <div class="dropdown-list-content dropdown-list-message">
                    <a href="#" class="dropdown-item">
                        <span class="dropdown-item-avatar text-white">
                            <img alt="image" src="assets/img/users/user-1.png" class="rounded-circle">
                        </span>
                        <span class="dropdown-item-desc">
                            <span class="message-user">John Deo</span>
                            <span class="time messege-text">Please check your mail !!</span>
                            <span class="time text-primary">2 Min Ago</span>
                        </span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <span class="dropdown-item-avatar text-white">
                            <img alt="image" src="assets/img/users/user-2.png" class="rounded-circle">
                        </span>
                        <span class="dropdown-item-desc">
                            <span class="message-user">Sarah Smith</span>
                            <span class="time messege-text">Request for leave application</span>
                            <span class="time text-primary">5 Min Ago</span>
                        </span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <span class="dropdown-item-avatar text-white">
                            <img alt="image" src="assets/img/users/user-5.png" class="rounded-circle">
                        </span>
                        <span class="dropdown-item-desc">
                            <span class="message-user">Jacob Ryan</span>
                            <span class="time messege-text">Your payment invoice is generated.</span>
                            <span class="time text-primary">12 Min Ago</span>
                        </span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <span class="dropdown-item-avatar text-white">
                            <img alt="image" src="assets/img/users/user-4.png" class="rounded-circle">
                        </span>
                        <span class="dropdown-item-desc">
                            <span class="message-user">Lina Smith</span>
                            <span class="time messege-text">hii John, I have upload doc related to task.</span>
                            <span class="time text-primary">30 Min Ago</span>
                        </span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <span class="dropdown-item-avatar text-white">
                            <img alt="image" src="assets/img/users/user-3.png" class="rounded-circle">
                        </span>
                        <span class="dropdown-item-desc">
                            <span class="message-user">Jalpa Joshi</span>
                            <span class="time messege-text">Please do as specify. Let me know if you have any query.</span>
                            <span class="time text-primary">1 Days Ago</span>
                        </span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <span class="dropdown-item-avatar text-white">
                            <img alt="image" src="assets/img/users/user-2.png" class="rounded-circle">
                        </span>
                        <span class="dropdown-item-desc">
                            <span class="message-user">Sarah Smith</span>
                            <span class="time messege-text">Client Requirements</span>
                            <span class="time text-primary">2 Days Ago</span>
                        </span>
                    </a>
                </div>
                <div class="dropdown-footer text-center">
                    <a href="#">View All <i class="fas fa-chevron-right"></i></a>
                </div>
            </div>
        </li> --}}
        <li class="dropdown dropdown-list-toggle">
            @php
                $beep = DB::select("SELECT COUNT(status) AS reseller FROM master_store where status = 0")[0];
                if ($beep->reseller == '0') {
                    print_r('<a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg">
                                <i class="far fa-bell"></i>
                            </a>');
                } else {
                    print_r('<a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep">
                        <i class="far fa-bell"></i>
                    </a>');
                }
            @endphp
            <div class="dropdown-menu dropdown-list dropdown-menu-right">
                <div class="dropdown-header">Notifikasi
                    <div class="float-right">
                        {{-- <a href="#">Mark All As Read</a> --}}
                    </div>
                </div>
                <div class="dropdown-list-content dropdown-list-icons">
                    {{-- <a href="#" class="dropdown-item dropdown-item-unread">
                        <span class="dropdown-item-icon bg-primary text-white">
                            <i class="fas fa-code"></i>
                        </span>
                        <span class="dropdown-item-desc">
                            Template update is available now!
                            <span class="time text-primary">2 Min Ago</span>
                        </span>
                    </a> --}}
                    <a href="{{route('toko.reseller')}}" class="dropdown-item">
                        <span class="dropdown-item-icon bg-success text-white">
                            <i class="far fa-user"></i>
                        </span>
                        <span class="dropdown-item-desc">
                            @php
                                $nu = DB::select("SELECT COUNT(status) AS reseller FROM master_store where status = 0")[0];
                                if ($nu->reseller == '0') {
                                    print_r('Belum ada notifikasi baru');
                                } else {
                                    print_r('pengajuan reseller baru');
                                    print_r('<span class="time">'. $nu->reseller .' Akun pending</span>');
                                }
                            @endphp
                        </span>
                    </a>
                </div>
                <div class="dropdown-footer text-center">
                    {{-- <a href="#">View All <i class="fas fa-chevron-right"></i></a> --}}
                </div>
            </div>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ asset('assets/img/user.png') }}" class="user-img-radious-style">
                <span class="d-sm-none d-lg-inline-block"></span></a>
            <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-title">Hello Admin</div>
                <!-- <a href="profile.html" class="dropdown-item has-icon">
                    <i class="far fa-user"></i> Profile
                </a>
                <a href="timeline.html" class="dropdown-item has-icon">
                    <i class="fas fa-bolt"></i> Activities
                </a> -->
                <a href="{{route('password.admin')}}" class="dropdown-item has-icon">
                    <i class="fas fa-cog"></i> Ubah Password
                </a> 
                <div class="dropdown-divider"></div>
                <a href="{{ route('logout.admin') }}" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
