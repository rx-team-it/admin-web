<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="{{ route('home.admin') }}">
            <img alt="image" src="{{ getAWS_BUCKET('assets')}}/{{session()->get('sites')->mini_logo}}"
                class="header-logo" />
        </a>
    </div>
    <ul class="sidebar-menu">
        @foreach ( session()->get('navigasi') as $grup)
            <li class="menu-header">{{ $grup->name }}</li>
            @foreach ($grup->parent_model as $parent)
                @if (!empty($parent->route))
                        <li class="dropdown">
                            <a href="{{ $parent->status == 0 ? '#' : route($parent->route) }}" class="nav-link">
                                <i class="{{ $parent->icon }}"></i><span>{{ $parent->name }} <i class="{{ $parent->status == 0 ? $parent->label : '' }}"></i></span>
                            </a>
                        </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="nav-link has-dropdown"> 
                            <i class="{{ $parent->icon }}"></i><span>{{ $parent->name }} <i class="{{ $parent->status == 0 ? $parent->label : '' }}"></i> </span>
                        </a>
                        <ul class="dropdown-menu">
                            @foreach ($parent->child_of_parent as $cop)
                                    @if (!empty($cop->route))
                                        <li><a class="nav-link" href="{{ $cop->status == 0 ? '#' : route($cop->route) }}">{{ $cop->name }} <i class="{{ $cop->status == 0 ? $cop->label : '' }}"></i></a></li>
                                    @else
                                        <li class="dropdown">
                                            <a class="nav-link has-dropdown" href="#"> {{ $cop->name }} <i class="{{ $cop->status == 0 ? $cop->label : '' }}"></i></a>
                                            <ul class="dropdown-menu">
                                                @foreach ($cop->child_of_child as $coc)
                                                    <li class="dropdown"><a class="nav-link" href="{{ $coc->status == 0 ? '#' : route($coc->route) }}">{{ $coc->name }} <i class="{{ $coc->status == 0 ? $coc->label : '' }}"></i></a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                            @endforeach
                        </ul>
                    </li>
                @endif
            @endforeach
        @endforeach
    </ul>
    {{-- @if (session()->get('status_membership') == 'premium')
        <ul class="sidebar-menu">
            @foreach ( session()->get('navigasi') as $grup)
                <li class="menu-header">{{ $grup->name }}</li>
                @foreach ($grup->parent_model as $parent)
                    @if (!empty($parent->route))
                            <li class="dropdown">
                                <a href="{{ $parent->premium ? '#' : route($parent->route) }}" class="nav-link">
                                    <i class="{{ $parent->icon }}"></i><span>{{ $parent->name }} <i class="{{ $parent->premium ? $parent->label : '' }}"></i></span>
                                </a>
                            </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"> 
                                <i class="{{ $parent->icon }}"></i><span>{{ $parent->name }} <i class="{{ $parent->premium ? $parent->label : '' }}"></i> </span>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach ($parent->child_of_parent as $cop)
                                        @if (!empty($cop->route))
                                            <li><a class="nav-link" href="{{ $cop->premium ? '#' : route($cop->route) }}">{{ $cop->name }} <i class="{{ $cop->premium ? $cop->label : '' }}"></i></a></li>
                                        @else
                                            <li class="dropdown">
                                                <a class="nav-link has-dropdown" href="#"> {{ $cop->name }} <i class="{{ $cop->premium ? $cop->label : '' }}"></i></a>
                                                <ul class="dropdown-menu">
                                                    @foreach ($cop->child_of_child as $coc)
                                                        <li class="dropdown"><a class="nav-link" href="{{ $coc->premium ? '#' : route($coc->route) }}">{{ $coc->name }} <i class="{{ $coc->premium ? $coc->label : '' }}"></i></a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            @endforeach
        </ul>
    @elseif(session()->get('status_membership') == 'trial')
        <ul class="sidebar-menu">
            @foreach ( session()->get('navigasi') as $grup)
                <li class="menu-header">{{ $grup->name }}</li>
                @foreach ($grup->parent_model as $parent)
                    @if (!empty($parent->route))
                            <li class="dropdown">
                                <a href="{{ $parent->trial ? '#' : route($parent->route) }}" class="nav-link">
                                    <i class="{{ $parent->icon }}"></i><span>{{ $parent->name }} <i class="{{ $parent->trial ? $parent->label : '' }}"></i></span>
                                </a>
                            </li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"> 
                                <i class="{{ $parent->icon }}"></i><span>{{ $parent->name }} <i class="{{ $parent->trial ? $parent->label : '' }}"></i> </span>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach ($parent->child_of_parent as $cop)
                                        @if (!empty($cop->route))
                                            <li><a class="nav-link" href="{{ $cop->trial ? '#' : route($cop->route) }}">{{ $cop->name }} <i class="{{ $cop->trial ? $cop->label : '' }}"></i></a></li>
                                        @else
                                            <li class="dropdown">
                                                <a class="nav-link has-dropdown" href="#"> {{ $cop->name }} <i class="{{ $cop->trial ? $cop->label : '' }}"></i></a>
                                                <ul class="dropdown-menu">
                                                    @foreach ($cop->child_of_child as $coc)
                                                        <li class="dropdown"><a class="nav-link" href="{{ $coc->trial ? '#' : route($coc->route) }}">{{ $coc->name }} <i class="{{ $coc->trial ? $coc->label : '' }}"></i></a></li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                @endforeach
                            </ul>
                        </li>
                    @endif
                @endforeach
            @endforeach
        </ul>
    @endif --}}
</aside>
