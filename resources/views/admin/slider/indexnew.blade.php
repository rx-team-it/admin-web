@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Banner</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Gambar</div>
        </div>
    </div>
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">                            
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('success') }}</strong>
        </div>    
    @endif
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">                            
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        @foreach ($errors->all() as $error)
        <strong><i class="fa fa-check-times"></i>&nbsp; Gagal, silahkan cek format kembali! ( {{ $error }} )</strong> <br>
        @endforeach      
    </div>  
  @endif
    <div class="row mt-4">
        <div class="col-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>List Banner</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#infobanner"><i class="fas fa-question-circle"></i>
                            Info</a>
                        @if ($cb[0]->banner >= 4)
                        @else
                        <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addbanner"><i class="fas fa-plus-circle"></i>
                            Tambah Banner</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- {{dd($b)}} --}}
                                @if (empty($b))
                                    <tr class="text-center">
                                        <td colspan="4">Tidak ada data</td>
                                    </tr>
                                @else
                                    @foreach ($b as $item1)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item1->type_content }}</td>
                                        <td><img src="{{Storage::disk('s3')->url('public/assets/' . $item1->image)}}" width="150px" alt="{{ $item1->alt }}"></td>
                                        <td><a href="{{ route('slider.delete',$item1->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>List Slider Carousel</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#infoslider"><i class="fas fa-question-circle"></i>
                            Info</a>
                        @if ($cs[0]->slider >= 3)
                        @else
                        <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addslider"><i class="fas fa-plus-circle"></i>
                            Tambah Slider</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($s))
                                <tr class="text-center">
                                    <td colspan="4">Tidak ada data</td>
                                </tr>
                                @else
                                @foreach ($s as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->type_content }}</td>
                                    <td><img src="{{Storage::disk('s3')->url('public/assets/' . $item->image)}}" width="150px" alt="{{ $item->alt }}"></td>
                                    <td><a href="{{ route('slider.delete',$item->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Banner Promo</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#infopromo"><i class="fas fa-question-circle"></i>
                            Info</a>
                        @if ($cbp[0]->bp >= 1)
                        @else
                        <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addbp"><i class="fas fa-plus-circle"></i>
                            Tambah Banner Promo</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($bp))
                                    <tr class="text-center">
                                        <td colspan="4"> Tidak ada data</td>
                                    </tr>
                                @else
                                @foreach ($bp as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->type_content }}</td>
                                    <td><img src="{{Storage::disk('s3')->url('public/assets/' . $item->image)}}" width="150px" alt="{{ $item->alt }}"></td>
                                    <td><a href="{{ route('slider.delete',$item->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Banner Promo</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#infopromo1"><i class="fas fa-question-circle"></i>
                            Info</a>
                        @if ($cbp1[0]->bp >= 1)
                        @else
                        <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addbp1"><i class="fas fa-plus-circle"></i>
                            Tambah Banner Promo</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($bp1))
                                    <tr class="text-center">
                                        <td colspan="4">Tidak ada data</td>
                                    </tr>
                                @else
                                @foreach ($bp1 as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->type_content }}</td>
                                    <td><img src="{{Storage::disk('s3')->url('public/assets/' . $item->image)}}" width="150px" alt="{{ $item->alt }}"></td>
                                    <td><a href="{{ route('slider.delete',$item->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Banner Wide</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-info btn-sm" data-toggle="modal" data-target="#infowide"><i class="fas fa-question-circle"></i>
                            Info</a>
                        @if ($cbw[0]->bw >= 1)
                        @else
                        <a href="" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#addbw"><i class="fas fa-plus-circle"></i>
                            Tambah Banner Promo</a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (empty($bw))
                                    <tr class="text-center">
                                        <td colspan="4">Tidak ada data</td>
                                    </tr>
                                @else
                                    
                                @foreach ($bw as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->type_content }}</td>
                                    <td><img src="{{Storage::disk('s3')->url('public/assets/' . $item->image)}}" width="150px" alt="{{ $item->alt }}"></td>
                                    <td><a href="{{ route('slider.delete',$item->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="infobanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <img src="{{asset('assets/img/banner.png')}}" alt="">
        </div>
    </div>
</div>

<div class="modal fade" id="infoslider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <img src="{{asset('assets/img/slider-carousel.png')}}" alt="">
        </div>
    </div>
</div>

<div class="modal fade" id="infopromo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <img src="{{asset('assets/img/banner-promo.png')}}" alt="">
        </div>
    </div>
</div>

<div class="modal fade" id="infopromo1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <img src="{{asset('assets/img/banner-promo1.png')}}" alt="">
        </div>
    </div>
</div>

<div class="modal fade" id="infowide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <img src="{{asset('assets/img/banner-wide.png')}}" alt="">
        </div>
    </div>
</div>

{{-- add banner --}}
<div class="modal fade" id="addbanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('slider.add') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Banner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Urutan banner</label>
                        <select class="form-control" name="urutan_banner">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                        <small class="text-muted">* Hanya ada 4 Urutan untuk banner</small>
                    </div> --}}
                    {{-- <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Deskripsi : </label>
                        <textarea name='description' class='form-control' placeholder="gambar banner.." required></textarea>
                    </div> --}}
                    <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Gambar : (format: jpg, jpeg, png)</label>
                        <input type='file' name='file' class='form-control' placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- add slider --}}
<div class="modal fade" id="addslider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('sliders.add') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Slider</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Deskripsi : </label>
                        <textarea name='description' class='form-control' placeholder="gambar slider.." required></textarea>
                    </div> --}}
                    <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Gambar : (format: jpg, jpeg, png)</label>
                        <input type='file' name='file' class='form-control' placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- add banner promo --}}
<div class="modal fade" id="addbp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('bp.add') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Banner Promo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Deskripsi : </label>
                        <textarea name='description' class='form-control' placeholder="gambar slider.." required></textarea>
                    </div> --}}
                    <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Gambar : (format: jpg, jpeg, png)</label>
                        <input type='file' name='file' class='form-control' placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- add banner promo --}}
<div class="modal fade" id="addbp1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('bp1.add') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Banner Promo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Deskripsi : </label>
                        <textarea name='description' class='form-control' placeholder="gambar banner.." required></textarea>
                    </div> --}}
                    <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Gambar : (format: jpg, jpeg, png)</label>
                        <input type='file' name='file' class='form-control' placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- add banner wide --}}
<div class="modal fade" id="addbw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('bw.add') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Banner Wide</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Deskripsi : </label>
                        <textarea name='description' class='form-control' placeholder="gambar slider.." required></textarea>
                    </div> --}}
                    <div class="form-group col-12">
                        <label for="message-text" class="col-form-label">Gambar : (format: jpg, jpeg, png)</label>
                        <input type='file' name='file' class='form-control' placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection