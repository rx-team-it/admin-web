@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Banner</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Gambar</div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4>List Banner</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>No</th>
                                <th>Type</th>
                                <th>Gambar</th>
                                <th>Aksi</th>
                            </tr>
                            @foreach($banner as $key => $row)
                            <tr>
                                <td>{{ $key + $banner->firstitem()  }}</td>
                                <td>{{ $row->description }}</td>
                                <td>
                                    <img src="{{Storage::disk('s3')->url('public/assets/' . $row->image)}}" width="100px" alt="{{ $row->alt }}">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-icon btn-primary" data-toggle="modal" data-target="#EditGambar" data-whatever="{{$row->id}}" data-type_content="{{$row->type_content}}" data-description="{{$row->description}}"><i class="far fa-edit"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4>List Slidder</h4>
                </div>
                <div class="card-body">
                    {{-- <a href="#" class="btn btn-icon icon-left btn-primary @if ($slidder->count() > 8) disabled @endif "><i class="fas fa-plus"></i> Tambah Slider</a> --}}
                    {{-- <p style="font-size:10px;">Max 10 images</p> --}}
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>No</th>
                                <th>Deskripsi</th>
                                <th>Gambar</th>
                                <th>Aksi</th>
                            </tr>
                            @foreach($slidder as $key => $row)
                            <tr>
                                <td>{{ $key + $slidder->firstitem()  }}</td>
                                <td>{{ $row->type_content }}</td>
                                <td>
                                    <img src="{{Storage::disk('s3')->url('public/assets/' . $row->image)}}" width="150px" alt="{{ $row->alt }}">
                                </td>
                                <td>
                                    <form action="#" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="button" class="btn btn-sm btn-icon btn-primary" data-toggle="modal" data-target="#EditGambar" data-whatever="{{$row->id}}" data-type_content="{{$row->type_content}}" data-description="{{$row->description}}"><i class="far fa-edit"></i></button>
                                        {{-- <button class="btn btn-icon btn-sm btn-danger"><i class="fas fa-times"></i></button> --}}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>


{{-- MODAL UBAH GAMBAR --}}
<div class="modal fade" id="EditGambar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form method='post' action="{{route('slider.up')}}" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Gambar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                        <input type="text" name="type_content" class="form-control" id="type_content">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Deskripsi : </label>
                        <textarea name='description' id='description' class='form-control' placeholder="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Gambar : </label>
                        <input type='file' name='file' id='file' class='form-control' placeholder="Gambar">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </form>
</div>


@endsection

@section('admin.js')
<script>
    $('#EditGambar').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var description = button.data('description') // Extract info from data-* attributes
        var type_content = button.data('type_content') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Ubah Gambar ')
        modal.find('#recipient-name').val(recipient)
        modal.find('#description').val(description)
        modal.find('#type_content').val(type_content)
    })
</script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

@endsection