@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('payment.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Permintaan Withdraw</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('order.index') }}">List Withdraw</a></div>
            <div class="breadcrumb-item">Detail Pembayaran</div>
        </div>
    </div>
    <main class="main">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Detail Permintaan
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <!-- BLOCK UNTUK MENAMPILKAN DATA PELANGGAN -->
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pelanggan</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pelanggan</th>
                                                <td>{{ !empty($user->user) ?  $user->user->name : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{ !empty($user->user) ?  $user->user->email : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Telp</th>
                                                <td>{{ !empty($user->user) ?  $user->user->phone : '' }}</td>
                                            </tr>
                                            {{-- <tr>
                                                <th>Alamat</th>
                                                <td>{{ !empty($history->user) ?  $history->user->address : '' }}<br>
                                            {{ !empty($history->user->countrys) ?  $history->user->countrys->name : '' }}<br>
                                            {{ !empty($history->user->states) ?  $history->user->states->name : '' }}<br>
                                            {{ !empty($history->user->citys) ?  $history->user->citys->name : '' }}
                                            </td>
                                            </tr> --}}
                                            <!-- FORM INPUT RESI HANYA AKAN TAMPIL JIKA STATUS LEBIH BESAR 1 -->
                                            <!-- <tr>
                                            <th>Nomor Resi</th>
                                            <td>
                                                <form action="#" method="post">
                                                    @csrf
                                                    <div class="input-group">
                                                        <input type="hidden" name="order_id" value="#">
                                                        <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" required>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-secondary" type="submit">Kirim</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr> -->
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Toko</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Toko</th>
                                                <td>{{ !empty($history->store) ? $history->store->name_site : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>
                                                    {{ !empty($history->store->address) ? $history->store->address->address : '' }}<br>
                                                    {{ !empty($history->store->address->countrys) ? $history->store->address->countrys->province_name : '' }}<br>
                                                    {{ !empty($history->store->address->states) ? $history->store->address->states->city_name : '' }} <br>
                                                    {{ !empty($history->store->address->citys) ? $history->store->address->citys->subdistrict_name : '' }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Status Toko</th>
                                                <td>{!! !empty($history->store) ? $history->store->status_label : '' !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Total Saldo</th>
                                                <td>@currency($history_payment)</td>
                                            </tr>
                                        </table>
                                        @if($history->status == 0)
                                        <h5 style="font-family:Monaco;" class="text-center">Belum Konfirmasi Penarikan</h5>
                                        @else
                                        <h5 style="font-family:Monaco;" class="text-center">Penarikan Berhasil</h5>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="font-family:Monaco;">Detail Rekening</h4>
                                        <table class="table table-borderd table-hover">
                                            <tr>
                                                <th>No Rekening</th>
                                                <th>Withdraw</th>
                                                <th>Tanggal</th>
                                                <th>Status</th>
                                                @if ($history->status == 0)
                                                <th>Aksi</th>
                                                @else
                                                @endif
                                            </tr>
                                            <tr>
                                                <td>{{ !empty($history) ? $history->no_rekening : '' }}</td>
                                                <td>@currency(!empty($history) ? $history->total_harga : '')</td>
                                                <td>{{ !empty($history) ? $history->created_at : '' }}</td>
                                                <td>{!! !empty($history) ? $history->status_label : '' !!}</td>
                                                <td>
                                                    @if ($history->status == 0)
                                                    <div class="dropdown d-inline mr-2">
                                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Pilih Aksi
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="{{ route('payment.selesai',$history->id) }}">Selesai</a>
                                                            <a class="dropdown-item" href="{{ route('payment.tolak',$history->id) }}">Ditolak</a>
                                                        </div>

                                                    </div>
                                                    @else

                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</section>
@endsection

@section('admin.js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>

@endif

@endsection