@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
    </div>
    <div class="card card-primary">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card-header">
                        <h4>Transaksi Terakhir</h4>
                    </div>
                    <div class="col-md-6">
                        <div class="row input-daterange">
                            <div class="col-md-4">
                                <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                            </div>
                            <div class="col-md-4">
                                <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                            </div>
                        </div>
                    </div>
                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Semua</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="true">Penarikan Berhasil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pemesanan_gagal-tab2" data-toggle="tab" href="#pemesanan_gagal" role="tab" aria-selected="true">Penarikan Gagal</a>
                            </li>
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="row">
                                    <div class="table-responsive">
                                        <table id="saldo" class="table table-hover table-bordered table-sm">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Rekening Bank</th>
                                                    <th>Jumlah Penarikan</th>
                                                    <th>Tanggal</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="sudah_bayar" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="pemesanan_gagal" role="tabpanel" aria-labelledby="pemesanan_gagal-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="gagal" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#saldo').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#saldo').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#saldo').DataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">frtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('payment.index') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'no_rekening',
                        name: 'no_rekening',
                    },
                    {
                        data: 'total_harga',
                        render: $.fn.dataTable.render.number('.', '.', 0),
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label',
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#sudah_bayar').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#sudah_bayar').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#sudah_bayar').DataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">frtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('payment.berhasil') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'no_rekening',
                        name: 'no_rekening',
                    },
                    {
                        data: 'total_harga',
                        render: $.fn.dataTable.render.number('.', '.', 0),
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label',
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#gagal').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#gagal').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#gagal').DataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">frtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('payment.gagal') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'no_rekening',
                        name: 'no_rekening',
                    },
                    {
                        data: 'total_harga',
                        render: $.fn.dataTable.render.number('.', '.', 0),
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label',
                    },
                    {
                        data: 'action',
                        name: 'action',
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });
</script>

@endsection