@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <h1>Kelola toko reseller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item">List Toko reseller</div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h4>Table Toko Reseller</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <div role="tabpanel" class="tab-pane fade in show active" id="semua-reseller">
                    <table id="semua-reseller-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                        <thead>
                            <th>No</th>
                            <th>Nama Toko</th>
                            <th>Status</th>
                            <th class="text-center">Aksi</th>
                        </thead>
                        <tbody>
                            @foreach ($storee as $item)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td><strong>{{$item->name_site}}</strong> <br> <small>Tgl. Daftar: {{$item->created_at}}</small></td>
                                <td>{!! $item->status_label !!}</td>
                                <td>
                                    @if ($item->status == 0)
                                    <div class="dropdown d-inline mr-2">
                                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilih Aksi
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ route('terima.toko', $item->id) }}">Terima</a>
                                            <a class="dropdown-item" href="{{ route('delete', $item->id) }}">Hapus</a>
                                        </div>
                                        <a href="{{route("detail.verifikasi", $item->id)}}" class="btn btn-sm btn-primary">Detail</a>
                                    </div>
                                    @elseif ($item->status == 1)
                                    <div class="dropdown d-inline mr-2">
                                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilih Aksi
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ route('blok', $item->id) }}">Blok</a>
                                            <a class="dropdown-item" href="{{ route('delete', $item->id) }}">Hapus</a>
                                        </div>
                                        <a href="{{route("detail.verifikasi", $item->id)}}" class="btn btn-sm btn-primary">Detail</a>
                                    </div>
                                    @elseif($item->status == 3)
                                        <div class="dropdown d-inline mr-2">
                                            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Pilih Aksi
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{ route('reactive', $item->id) }}">Aktif</a>
                                            </div>
                                            <a href="{{route("detail.verifikasi", $item->id)}}" class="btn btn-sm btn-primary">Detail</a>
                                        </div>
                                    @else
                                    <div class="dropdown d-inline mr-2">
                                        <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Pilih Aksi
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{ route('approve', $item->id) }}">Aktif</a>
                                            <a class="dropdown-item" href="{{ route('delete', $item->id) }}">Hapus</a>
                                        </div>
                                        <a href="{{route("detail.verifikasi", $item->id)}}" class="btn btn-sm btn-primary">Detail</a>
                                    </div>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#semua-reseller-dt').DataTable({
            scrollX: true
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });
    });
</script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@endsection