@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <h1>List Toko Reseller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item">List Toko Pending</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-4 col-md-4 col-lg-4">
                <!-- <div class="form-group">
                    <label>Search</label>
                    <form action="{{ route('cari.pending') }}" method="GET">
                        <input type="text" class="form-control" name="cari" placeholder="Cari Toko ..">
                    </form>
                </div> -->
                <div class="form-group">
                    <form action="{{ route('cari.aktif') }}" method="GET">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="" aria-label="">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($storee as $store)
            <div class="col-12 col-md-4 col-lg-4">
                <article class="article article-style-c">
                    <div class="article-details">
                        <div class="article-category"><a href="#">Mendaftar</a>
                            <div class="bullet"></div> <a href="#">{{ !empty($store) ? $store->created_at : '' }}</a>
                        </div>
                        <div class="article-title float-left">
                            <h2><a href="{{ route('toko.detail', $store->id) }}">{{ !empty($store) ? $store->name_site : '' }}</a>
                            </h2>
                        </div>
                        <div class="article-title float-right">
                            {!! $store->status_label !!}
                            </h2>
                        </div>
                        <!-- <p>Ini adalah Deskripsi</p> -->
                        <div class="article-user">
                            @if ($store->logo == null)
                            <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}">
                            @else
                            <img alt="image" src="{{Storage::disk('s3')->url('public/images/store/' . $store->logo)}}">
                            @endif
                            <div class="article-user-details float-left">
                                <div class="user-detail-name">
                                    <a href="#">{{ !empty($store->user_profile) ? $store->user_profile->name : '' }}</a>
                                </div>
                                <div class="text-job">{{ !empty($store->user_profile) ? $store->user_profile->nm_role : '' }}</div>
                            </div>
                            <div class="article-user-details float-right">
                                <div class="dropdown d-inline mr-2">
                                    <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Pilih Aksi
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ route('terima.toko', $store->id) }}">Terima</a>
                                        <a class="dropdown-item" href="{{ route('delete', $store->id) }}">Delete</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            @endforeach
        </div>
        {{ $storee->links() }}
    </div>
</section>
@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@endsection