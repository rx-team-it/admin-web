@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{route('toko.pending')}}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>{{ !empty($store) ? $store->name_site : '' }}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{route('home.admin')}}">Beranda</a></div>
            <div class="breadcrumb-item">Profil Toko</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Sampul Toko</h4>
                    </div>
                    <div class="card-body">
                        <div class="owl-carousel owl-theme slider" id="slider2">
                            @foreach($datas as $keys => $row)
                            <div>
                                @if ($row->image == null)
                                <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}" width="300px" height="320px">
                                @else
                                <img alt="image" src="{{Storage::disk('s3')->url('public/images/BannerImage/' . $row->image)}}" width="300px" height="320px">
                                @endif
                                <div class="slider-caption">
                                    <div class="slider-title">{{ $row->type_content }}</div>
                                    <div class="slider-description">{{ $row->description }}</div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4">
                <div class="card author-box">
                    <div class="card-body">
                        <h6>Foto Toko</h6>
                        <div class="author-box-center">
                            @if ($store->logo == null)
                            <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}" width="80px" height="80px" class="rounded-circle author-box-picture">
                            @else
                            <img alt="image" src="{{Storage::disk('s3')->url('public/images/store/' . $store->logo)}}" width="80px" height="80px" class="rounded-circle author-box-picture">
                            @endif
                            <!-- <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}" class="rounded-circle author-box-picture"> -->
                            <div class="clearfix"></div>
                            <div class="author-box-name">
                                <a href="#">{{ $store->name_site }}</a>
                            </div>
                            <div class="author-box-job">Seller</div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4>Detail Alamat</h4>
                    </div>
                    <div class="card-body">
                        <div class="py-1">
                            <p class="clearfix">
                                <span class="float-left">
                                    Negara
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->countrys->name) ? $address->countrys->name : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Provinsi
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->states->name) ? $address->states->name : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Kota
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->citys->name) ? $address->citys->name : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Mulai Mendaftar
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($store) ? $store->created_at : '' }}
                                </span>
                            </p>
                            @if($store->status == 0)
                            <a class="btn btn-primary btn-icon icon-left" href="{{ route('terima.toko', $store->id) }}"><i class="fas fa-check"></i> Terima</a>
                            @else
                            <a class="btn btn-danger btn-icon icon-left" href="{{ route('blok', $store->id) }}"><i class="fas fa-times"></i> Blok</a>
                            <a class="btn btn-danger btn-icon icon-left" href="{{ route('delete', $store->id) }}"><i class="fas fa-times"></i> Delete</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Tentang</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Edit</a>
                            </li> -->
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="row">
                                    <div class="col-md-3 col-6 b-r">
                                        <strong>Nama Toko</strong>
                                        <br>
                                        <p class="text-muted">{{ !empty($store) ? $store->name_site : '' }}</p>
                                    </div>
                                    <div class="col-md-3 col-6 b-r">
                                        <strong>No Hp</strong>
                                        <br>
                                        <p class="text-muted">{{ !empty($store) ? $store->contact_phone : '' }}</p>
                                    </div>
                                    <div class="col-md-3 col-6 b-r">
                                        <strong>Email</strong>
                                        <br>
                                        <p class="text-muted">{{ !empty($store) ? $store->contact_email : '' }}</p>
                                    </div>
                                    <div class="col-md-3 col-6">
                                        <strong>Alamat</strong>
                                        <br>
                                        <p class="text-muted">{{ !empty($store) ? $store->contact_address : '' }}</p>
                                    </div>
                                </div>
                                <strong>Tentang Toko</strong>
                                <p class="m-t-10">{!! !empty($store) ? $store->description : '' !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/page/owl-carousel.js') }}"></script>
<script src="{{ asset('assets/bundles/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/page/ckeditor.js') }}"></script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection