<section class="section">
    <link rel="stylesheet" href="{{asset('assets/css/app.min.css')}}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/components.css')}}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

    <div class="section-body">
        <div class="invoice">
            <div class="invoice-print" id="printableArea">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="invoice-title">
                            <h2>Invoice</h2>
                            <h5>{{ !empty($order) ? $order->invoice : '' }}</h5>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Pengirim:</strong><br>
                                    {{ !empty($store) ? $store->name_site: '' }}<br>
                                    {{ !empty($store) ? $store->phone : '' }}<br>
                                    {{ !empty($store) ? $store->address : '' }}<br>
                                    {{ !empty($store) ? $store->urban : '' }}<br>
                                    {{ !empty($store) ? $store->district : '' }}<br>
                                    {{ !empty($store) ? $store->city : '' }}<br>
                                    {{ !empty($store->countrys->province_name) ? $store->countrys->province_name : '' }}
                                    Kodepos {{ !empty($store) ? $store->postal_code: '' }}<br>
                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Penerima:</strong><br>
                                    {{ !empty($order) ? $order->name : '' }}<br>
                                    {{ !empty($order) ? $order->phone : '' }}<br>
                                    {{ !empty($order) ? $order->address : '' }}<br>
                                    {{ !empty($order) ? $order->state : '' }}<br>
                                    {{ !empty($order) ? $order->city : '' }}<br>
                                    {{ !empty($order) ? $order->province : '' }},
                                    {{ !empty($order) ? $order->postal_code : '' }}<br>
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Pembayaran:</strong>
                                    <br>{{$order->bank}}<br>
                                    <strong>Pengantaran:</strong>
                                    <br>{{$order->courrier}} - {{$order->service}}
                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Tanggal Pemesanan:</strong><br>
                                    {{ $order->created_at }}<br><br>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="section-title">Ringkasan Pesanan</div>
                        <p class="section-lead">Semua Produk di sini tidak dapat dihapus.</p>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-md">
                                <tr>
                                    <th data-width="40">No</th>
                                    <th>Produk</th>
                                    <th class="text-center">Harga Awal</th>
                                    <th class="text-center">Diskon</th>
                                    <th class="text-center">Komisi</th>
                                    <th class="text-center">Total Harga</th>
                                    <th class="text-center">Quantity</th>
                                    <th class="text-center">Total</th>
                                </tr>
                                @php
                                $subtotal = 0;

                                @endphp

                                @foreach($order['detail'] as $keys => $rows)

                                @php $total = $rows->price * $rows->qty; @endphp
                                <tr>
                                    <td>{{ $loop->iteration}}</td>
                                    <td><span style="font-size:13px;">{{ Str::limit ($rows->product->name,50) }}</span><br>
                                        <span style="font-size:10px;">{{ !empty($rows->product_detail->varian_option->varian) ? $rows->product_detail->varian_option->varian->name_varian : '' }} {{ !empty($rows->product_detail->varian_option) ? $rows->product_detail->varian_option->name : '' }}</span>
                                        <br>
                                        <span style="font-size:10px;">
                                            {{ !empty($rows->sub_variasi) ? $rows->sub_variasi->name_varian : '' }}
                                            {{ !empty($rows->parent_option) ? $rows->parent_option->name : '' }}
                                        </span>
                                    </td>
                                    <td class="text-center"><span style="font-size:13px;">@currency($rows->price)</span></td>
                                    @if ($rows->type_diskon == 'persen')
                                        <td class="text-center"><span style="font-size:13px;">{{ $rows->diskon }}%</span></td>
                                        @if ($rows->type_komisi == 'persentase')
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->nilai_komisi }}%</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @elseif($rows->type_komisi == 'harga')
                                            <td class="text-center"><span style="font-size:13px;">@currency(($rows->nilai_komisi))</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @else
                                            <td class="text-center"><span style="font-size:13px;">@currency(($rows->nilai_komisi))</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @endif
                                    @elseif ($rows->type_diskon == 'nominal')
                                        <td class="text-center"><span style="font-size:13px;">Rp. {{ number_format($rows->diskon) }}</span></td>
                                        @if ($rows->type_komisi == 'persentase')
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->nilai_komisi }}%</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @elseif($rows->type_komisi == 'harga')
                                            <td class="text-center"><span style="font-size:13px;">@currency(($rows->nilai_komisi))</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @else
                                            <td class="text-center"><span style="font-size:13px;">{{ number_format($rows->nilai_komisi) }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @endif
                                    @else
                                        <td class="text-center"><span style="font-size:13px;">Rp. {{ number_format($rows->diskon) }}</span></td>
                                        @if ($rows->type_komisi == 'persentase')
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->nilai_komisi }}%</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>

                                        @elseif($rows->type_komisi == 'harga')
                                            <td class="text-center"><span style="font-size:13px;">@currency(($rows->nilai_komisi))</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ number_format($rows->fix_price) }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>
                                        @else
                                            <td class="text-center"><span style="font-size:13px;">@currency(($rows->nilai_komisi))</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price)</span></td>
                                            <td class="text-center"><span style="font-size:13px;">{{ $rows->qty }}</span></td>
                                            <td class="text-center"><span style="font-size:13px;">@currency($rows->fix_price * $rows->qty)</span></td>

                                        @endif
                                    @endif
                                    

                                </tr>
                                {{-- @php $subtotal = ($rows->harga_diskon + ($rows->harga_diskon*$rows->nilai_komisi) / 100) * $rows->qty; @endphp --}}
                                @php $subtotal = $rows->fix_price * $rows->qty; @endphp

                                @endforeach
                            </table>
                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-12 text-right">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Cashback</div>
                                    <div class="invoice-detail-value">@currency( $order->cashback )</div>
                                </div>
                                {{-- <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Subtotal</div>
                                    <div class="invoice-detail-value">@currency( $subtotal )</div>
                                </div> --}}
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Ongkos Kirim</div>
                                    @if ( $order->cashback != 'null' && $order->ongkos_kirim == 0)
                                        <div class="invoice-detail-value">Gratis Ongkir (<span style="color: #FC4F4F">@currency( $order->ongkos_kirim_fix)</span>)</div>
                                    @else
                                        <div class="invoice-detail-value">@currency( !empty($order) ? $order->ongkos_kirim : '' )</div>
                                    @endif
                                </div>
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Biaya Layanan </div>
                                    <div class="invoice-detail-value">@currency( !empty($order) ? $order->kode_unik + $order->biaya_layanan : '' )</div>
                                </div>

                                {{-- @php $grandtotal = $subtotal + $order->ongkos_kirim + $order->kode_unik ; @endphp --}}
                                <hr class="mt-2 mb-2">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Total</div>
                                    <div class="invoice-detail-value invoice-detail-value-lg">@currency( $order->total )</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <!-- {!! $order->status_label !!} -->
            <br>
            <br>
            <div class="text-md-right">
                <div class="float-lg-left mb-lg-0 mb-3">
                </div>
                <!-- <a class="btn btn-warning btn-icon icon-left" target="_blank" rel="noopener" type="button" onclick="printDiv('printableArea')"><i class="fas fa-print"></i> Print</a> -->
            </div>
        </div>
    </div>
    <script src="{{asset('assets/js/app.min.js')}}"></script>
    <!-- JS Libraies -->
    <!-- Page Specific JS File -->
    <!-- Template JS File -->
    <script src="{{asset('assets/js/scripts.js')}}"></script>
    <!-- Custom JS File -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script>
        window.addEventListener("load", window.print());
    </script>

</section>