<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>PDF</title>
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css'), '/' }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}, '/'">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css'), '/' }}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}, '/'">
    <link rel='shortcut icon' type='image/x-icon' href='assets/img/favicon.ico' />
</head>

<body>
    <div class="loader"></div>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <!-- Main Content -->
            <br />
            <br />
            <div class="container">
                <section class="section">
                    <div class="section-body">
                        <div class="invoice">
                            <div class="invoice-print">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="invoice-title">
                                            <h2>Invoice</h2>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <address>
                                                    <strong>Pengirim :</strong><br>
                                                    {{ !empty($order->store) ? $order->store->name_site : '' }}<br>
                                                    {{ !empty($order->store) ? $order->store->contact_address : '' }}<br>
                                                </address>
                                                <address>
                                                    <strong>Penerima :</strong><br>
                                                    {{ !empty($order) ?  $order->address : '' }}<br>
                                                    {{ !empty($order) ? $order->province : '' }}<br>
                                                    {{ !empty($order) ? $order->state : '' }}<br>
                                                    {{ !empty($order) ? $order->city : '' }}
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-md">
                                                <tr>
                                                    <th data-width="40">#</th>
                                                    <th>Item</th>
                                                    <th class="text-center">Price</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-right">Totals</th>
                                                </tr>
                                                @php
                                                $subtotal = 0;

                                                @endphp

                                                @foreach($detail as $rows)

                                                @php $total = $rows->price * $rows->qty; @endphp
                                                <tr>
                                                    <td>1</td>
                                                    <td>{{ $rows->product->name }} <br>
                                                        <span style="font-size:10px;">{{ !empty($rows->product_detail->varian_option->varian) ? $rows->product_detail->varian_option->varian->name_varian : '' }} - {{ !empty($rows->product_detail->varian_option) ? $rows->product_detail->varian_option->name : '' }}</span>
                                                        <br>
                                                        <span style="font-size:10px;">
                                                            {{ !empty($rows->sub_variasi) ? $rows->sub_variasi->name_varian : '' }} -
                                                            {{ !empty($rows->parent_option) ? $rows->parent_option->name : '' }}
                                                        </span>
                                                    </td>
                                                    <td class="text-center">@currency( $rows->price )</td>
                                                    <td class="text-center">{{ $rows->qty }}</td>
                                                    <td class="text-right">@currency( $total )</td>
                                                </tr>
                                                @php $subtotal = $subtotal + $total; @endphp

                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="row mt-4">
                                            <div class="col-lg-12 text-right">
                                                <div class="invoice-detail-item">
                                                    <div class="invoice-detail-name">Subtotal</div>
                                                    <div class="invoice-detail-value">@currency( $subtotal )</div>
                                                </div>
                                                <!-- <div class="invoice-detail-item">
                                                    <div class="invoice-detail-name">Shipping</div>
                                                    <div class="invoice-detail-value">-</div>
                                                </div> -->
                                                <hr class="mt-2 mb-2">
                                                <div class="invoice-detail-item">
                                                    <div class="invoice-detail-name">Total</div>
                                                    <div class="invoice-detail-value invoice-detail-value-lg">@currency($subtotal)</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</body>

</html>