@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('list.order.new') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Pesanan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('list.order.new') }}">List Pesanan</a></div>
            <div class="breadcrumb-item">Detail Pesanan</div>
        </div>
    </div>
    <main class="main">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Detail pesanan {{ $order->invoice }}
                                </h4>
                                <div class="card-header-action">
                                    <!-- <a href="{{ route('pesanan.detail', $order->invoice) }}" class="btn btn-sm btn-primary">
                                        <i class="fas fa-file-invoice-dollar"></i> Invoice
                                    </a> -->
                                    <a href="{{ route('print.invoice', $order->invoice) }}" rel="noopener" target="_blank" class="btn btn-info btn-icon icon-left" title="Lihat Invoice"><i class="fas fa-print"></i></a>
                                    @if ($order->courrier != 'pickup' && $order->status !== 0)
                                    <a href="{{route("print.Resi", $order->invoice)}}" target="_blank" class="btn btn-sm btn-warning" title="Lihat Resi" >
                                        <i class="fas fa-receipt"></i>
                                    </a>
                                    @else
                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <!-- BLOCK UNTUK MENAMPILKAN DATA PELANGGAN -->
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pelanggan</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pelanggan</th>
                                                <td>{{ $order->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Telp</th>
                                                <td>{{ $order->phone }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>{{ $order->address }}</td>
                                            </tr>
                                            <tr>
                                                <th>Order Status</th>
                                                <td>{!! $order->status_label !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Pesan</th>
                                                <td>{{ $order->message }}</td>
                                            </tr>
                                            <tr>
                                                <th>Nomor Resi</th>
                                                
                                                <td>
                                                    {{ !empty($order) ? $order->tracking_number : '' }}
                                                    @if ($order->courrier != 'pickup' && $order->status == 1)
                                                    <form action="{{route("cetak.Resi", $order->invoice)}}" method="POST">
                                                        @csrf
                                                        <div class="row">
                                                            &nbsp;&nbsp;
                                                            <select style="background-color:pink;" id="pilih_ekspedisi_{{$order->id}}" class="form-control col-7" required name="admin_courrier">
                                                                <option value="">Pilih Ekspedisi</option>
                                                            </select>
                                                            <input type="hidden" id="admin_courrier" name="admin_courrier" value="">
                                                            <input type="hidden" id="admin_service" name="admin_service" value="">
                                                            <input type="hidden" id="admin_service_code" name="admin_service_code" value="">
                                                            <input type="hidden" id="admin_ongkos_kirim" name="admin_ongkos_kirim" value="">
                                                            &nbsp;
                                                                <button class="btn btn-sm btn-warning">
                                                                    <i class="fas fa-print"> Request Resi</i> 
                                                                </button>
                                                        </div>
                                                    </form>
                                                    @elseif($order->courrier != 'pickup' && $order->ready_to_print == true)
                                                    <a href="{{route("print.Resi", $order->invoice)}}" target="_blank" class="btn btn-sm btn-success">
                                                            <i class="fas fa-print"> Cetak Resi</i> 
                                                    </a>
                                                    @if ($order->register_awb == 0)
                                                        <a href="{{route('regist.awb', $order->invoice)}}" class="btn btn-sm btn-primary">Register awb</a>
                                                    @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pembayaran</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pengirim</th>
                                                <td>{{ $order->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Bank Tujuan</th>
                                                <td>{{ $order->bank }}</td>
                                            </tr>
                                            <tr>
                                                <th>Tanggal Transfer</th>
                                                <td>{{ $order->date }}</td>
                                            </tr>
                                            <tr>
                                                @if ($order->status == 1 && $order->bank != 'QRIS')
                                                <th>Bukti Pembayaran</th>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. ( isset($order->confirm_payment->bukti_transfer)? $order->confirm_payment->bukti_transfer:''))}}" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat</a>
                                                    {{-- &nbsp;
                                                        <a href="{{ route('order.reupload', $order->invoice) }}" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i> Upload Ulang</a> --}}
                                                </td>
                                                @elseif ($order->status == 7 && $order->bank != 'QRIS')
                                                <th>Bukti Pembayaran</th>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. ( isset($order->confirm_payment->bukti_transfer)? $order->confirm_payment->bukti_transfer:''))}}" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat</a>
                                                    &nbsp;
                                                    <a href="{{ route('order.reupload', $order->invoice) }}" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i> Upload Ulang</a>
                                                    &nbsp;
                                                    <a href="{{ route('acc.bukti', $order->invoice) }}" class="btn btn-sm btn-success"><i class="fas fa-check"></i> Setujui</a>
                                                </td>
                                                @elseif ($order->status == 8 && $order->bank != 'QRIS')
                                                <th>Bukti Pembayaran</th>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. ( isset($order->confirm_payment->bukti_transfer)? $order->confirm_payment->bukti_transfer:''))}}" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Bukti</a>
                                                </td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <th>Pengiriman</th>
                                                <td>
                                                    <p style="font-family:Monaco;">{{ Str::upper($order->courrier)}} - {{$order->service}}</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Kurir admin</th>
                                                <td><p style="font-family:Monaco;">{{ Str::upper($order->admin_courrier)}} - {{$order->admin_service}}</p></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        @if ($order->status == 0)
                                        <a class="btn btn-warning btn-lg" href="" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $order->id }}"><i class="fa fa-ban"></i> Batalkan Pesanan</a>
                                        @elseif($order->status == 5)
                                        <p><button class="btn btn-md btn-success">Pesanan Selesai</button></p>
                                        @elseif($order->status == 1 && $order->courrier == 'pickup')
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                Pilih Aksi
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{ route('tombol.dikemas', $order->invoice) }}">Dikemas</a>
                                            </div>
                                        </div>
                                        @elseif($order->status == 3 && $order->courrier == 'pickup')
                                        <div class="dropdown d-inline mr-2">
                                            <a class="btn btn-primary btn-md" href="{{ route('order.selesai', $order->invoice) }}">Pesanan Telah di ambil & Selesai</a>
                                        </div>
                                        @endif
                                    </div>
                                    <br>
                                    <div class="col-md-12">
                                        <h4 style="font-family:Monaco;">Detail Produk</h4>
                                        <table class="table table-borderd table-hover">
                                            <thead style="background-color: #353c48">
                                                <tr style="">
                                                    <th style="color: white">Produk</th>
                                                    <th style="color: white">Varian</th>
                                                    <th style="color: white">Quantity</th>
                                                    <th style="color: white">Komisi</th>
                                                    <th style="color: white">Harga</th>
                                                    <th style="color: white">Subtotal</th>
                                                </tr>
                                            </thead>
                                            @foreach($order['product'] as $product)
                                            <tr>
                                                <td>{{ !empty($product) ? $product->name_product : 'Kosong' }}</td>
                                                <td>
                                                    <span style="font-size:13px;">{{ !empty($product->varian) ? $product->varian : '---' }}
                                                    </span>
                                                    <br>
                                                    <span style="font-size:13px;">
                                                        {{ !empty($product->sub_varian) ? $product->sub_varian : '---' }}
                                                    </span>
                                                </td>
                                                <td>{{ !empty($product) ? $product->qty : 'Kosong' }}</td>
                                                @if($product->type_komisi == 'persentase')
                                                    <td>{{ $product->nilai_komisi }}%</td>
                                                @elseif($product->type_komisi == 'harga')
                                                    <td>@currency($product->nilai_komisi)</td>
                                                @else
                                                    <td>0</td>
                                                @endif
                                                <td>{{ !empty($product) ? 'Rp. '.number_format($product->fix_price) : 'Kosong' }}</td>
                                                <td>{{ !empty($product) ? 'Rp. '.number_format ($product->fix_price * $product->qty) : 'Kosong' }}</td>
                                            </tr>
                                            @endforeach
                                        </table>
                                        <div class="row mr-3">
                                            <div class="col-lg-12 text-right">
                                                <div class="mt-3">
                                                    @if ($order->cashback == 0 )
                                                        {{-- kosong --}}
                                                    @else
                                                    <h6>Cashback</h6>
                                                    <p>@currency( $order->cashback )</p>
                                                    @endif
                                                </div>
                                                {{-- <div class="mt-3">
                                                    <h6>Subtotal</h6>
                                                    <p>@currency( $subtotal )</p>
                                                </div> --}}
                                                <div class="mt-3">
                                                    @if ( $order->total_komisi_produk == 0)
                                                        <h6>Ongkos Kirim</h6>
                                                        <div class="invoice-detail-value"><p>@currency( !empty($order) ? $order->ongkos_kirim : '' )</p> </s></div>
                                                        {{-- <p>Gratis Ongkir</p> --}}
                                                    @else
                                                        <h6>Ongkos Kirim</h6>
                                                        <div class="invoice-detail-value"><s style="color: #FC4F4F; font-size:12px" > @currency( !empty($order) ? $order->ongkos_kirim_fix : '' ) </s></div>
                                                        <p>@currency( !empty($order) ? $order->ongkos_kirim : '' )</p>
                                                    @endif
                                                </div>
                                                <div class="mt-3">
                                                    <h6>Biaya Layanan </h6>
                                                    <p>@currency( !empty($order) ? $order->kode_unik + $order->biaya_layanan : '' )</p>
                                                </div>
                
                                                {{-- @php $grandtotal = $subtotal + $order->ongkos_kirim + $order->kode_unik ; @endphp --}}
                                                <hr class="mt-2 mb-2">
                                                <div class="mt-3">
                                                    <h6>Total</h6>
                                                    <div class="invoice-detail-value invoice-detail-value-lg">@currency( $order->total )</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- di hide dulu --}}
        {{-- jika ada respon dari resinya roby --}}
        {{-- @if (!empty($response))
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Lacak Pengiriman ({{ !empty($order) ? $order->tracking_number : '' }}) {{ $order->invoice }}
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="activities">
                                        @foreach ($response as $res)
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white">
                                            <i class="fas fa-truck"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <p>{{$res['desc']}} - Tgl.{{ date("d F Y", strtotime($res['date']) )}} | Jam {{ date("H:i:s", strtotime($res['date']) )}}</p>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif --}}
    </main>
</section>

<div class="modal fade" id="editpesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('order.gagal', $order->invoice) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Batalkan Orderan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Pesan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-success">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('admin.js')
@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>
@endif

@if(Session::has('error'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'error',
        title: "{!!Session::get('error')!!}"
    })
</script>
@endif
<script>
    //function rupiah
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join(',').split('').reverse().join('');
        return ribuan;
    }
    //alamat pengirim
    var postal_code_origin = "{{$default_store->postal_code}}";
    var sub_district_origin = "{{$default_store->district}}";
    var city_origin = "{{$default_store->city}}";

    //group_service
    var group_service = "{{$order->group_service}}";

    //alamat penerima
    var kelurahan =  "{{$order->urban}}";
    var postal_code_destination = "{{$order->postal_code}}";
    var sub_district_destination = "{{$order->city}}";
    var city_destination = "{{$order->state}}";

    //berat_pengiriman
    var weight = "{{$order->berat_pengiriman}}";
    berat_totals = weight / 1000;
    if(berat_totals < 1)
    {
        berat_totals = 1;
    }

    //courrier
    var type = "{{$order->courrier}}";
    if(type !== "pickup")
    {
        $.ajax({
            url: "{{url('api/check-service-admin/')}}/" + postal_code_origin + '/' + sub_district_origin + '/' + city_origin + '/' + postal_code_destination + '/' + sub_district_destination + '/' + city_destination + '/' + berat_totals + '/' + group_service,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var check_service_api = response.data.price;
                var object = check_service_api;
                var select = $("#pilih_ekspedisi_{{$order->id}}");
                select.empty();
                select.append('<option value="">Pilih Pengiriman</option>');
            
                for (const property in object) {
                    var obj2 = object[property];
                    select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="JNE" data-code_service="' + object[property].service_code + '">' + object[property].service_display + '('+ object[property].etd_from +'-'+ object[property].etd_thru +' Hari)' + ' - ' + rubah(object[property].price) + '</option>');
                }
    
            }
        });
    }

    $('#pilih_ekspedisi_{{$order->id}}').on('change', function(e) {
        let service_code = ($(this).find(':selected').data('code_service'));
        let kurir = ($(this).find(':selected').data('kurir'));
        let servis = ($(this).find(':selected').data('servis'));
        let ongkir = ($(this).find(':selected').data('ongkir'));

        $('#admin_courrier').val(kurir);
        $('#admin_service').val(servis);
        $('#admin_service_code').val(service_code);
        $('#admin_ongkos_kirim').val(ongkir);

        $('#ongkir_penjual_{{$order->id}}').text('Ongkir Penjual: Rp. '+ rubah(ongkir) +'');
        let ongkos_kirim = "{{$order->ongkos_kirim}}";
        let keuntungan = ongkos_kirim - ongkir;
        if(keuntungan < 0 ){
            $('#keuntungan_{{$order->id}}').html(`<small style="color:red; font-size:12px;">Hasil : Anda Rugi<b> Rp. `+ rubah(keuntungan) +`</b></small>`);
        }else{
            $('#keuntungan_{{$order->id}}').html(`<small style="color:green; font-size:12px;">Hasil : Anda Profit :<b> Rp. `+ rubah(keuntungan) +`</b></small>`);
        }

    });
</script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

@endsection