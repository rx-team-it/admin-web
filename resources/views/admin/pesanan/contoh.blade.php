@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')

<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Order</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Order</div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Order Vendor</h4>
                    </div>
                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Semua Pesanan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="true">Sudah Bayar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#settings1" role="tab" aria-selected="true">Belum Bayar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pemesanan_gagal-tab2" data-toggle="tab" href="#pemesanan_gagal" role="tab" aria-selected="true">Pemesanan Gagal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="dikemas-tab2" data-toggle="tab" href="#dikemas" role="tab" aria-selected="true">Dikemas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="dikirim-tab2" data-toggle="tab" href="#dikirim" role="tab" aria-selected="true">Dikirim</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="order_selesai-tab2" data-toggle="tab" href="#order_selesai" role="tab" aria-selected="true">Selesai</a>
                            </li>
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="table_produk" class="table table-striped table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($order as $item)
                                                    <tr>
                                                        <td>{{$no++}}</td>
                                                        <td>{{ $item->reseller->user->name }}</td>
                                                        <td>
                                                            {{ !empty($item) ? $item->name : '' }}
                                                            <br>
                                                            <small><strong>invoice: {{ !empty($item) ? $item->invoice : ''}} </strong>
                                                                <br>
                                                                alamat: {{ !empty($item) ? $item->address : ''}}
                                                            </small>
                                                        </td>
                                                        <td>{!! $item->status_label !!}</td>
                                                        <td>{{ !empty($item) ? $item->created_at : ''}}</td>
                                                        <td>{{ !empty($item->komisi) ? $item->komisi->komisi : 'Blm Selesai' }}</td>
                                                        <td>
                                                            <a href="{{ route('pesanan.detail.reseller', $item->invoice) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                                            <a href="{{ route('pesanan.invoice.reseller', $item->invoice) }}" class="btn btn-primary btn-sm"><i class="fas fa-file-invoice-dollar"></i></a>
                                                            @if($item->status == 6)
                                                            @elseif($item->status == 5)
                                                            @elseif($item->status == 4)
                                                            @elseif($item->status == 3)
                                                            @elseif($item->status == 2)
                                                            @else
                                                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $item->id }}">Cancel</button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="sudah_bayar" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Pengiriman</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="settings1" role="tabpanel" aria-labelledby="profile-tab3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="belum_bayar" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Pengiriman</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="pemesanan_gagal" role="tabpanel" aria-labelledby="pemesanan_gagal-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="order_pemesanan_gagal" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Pengiriman</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="dikemas" role="tabpanel" aria-labelledby="dikemas-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="order_dikemas" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Pengiriman</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="dikirim" role="tabpanel" aria-labelledby="dikirim-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="order_dikirim" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Pengiriman</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="order_selesai" role="tabpanel" aria-labelledby="order_selesai-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="selesai" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Pengiriman</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                        <th>Detail Order</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<script>
    $(document).ready(function() {
        $('#semua-pesanan-dt, #sudah-bayar-dt, #gagal-dt, #dikirim-dt, #dikemas-dt, #selesai-dt').DataTable({
            scrollX: true
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });
    });

    $('#editpesan').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Batalkan Orderan')
        modal.find('#recipient-name').val(recipient)
    })
</script>

@endsection