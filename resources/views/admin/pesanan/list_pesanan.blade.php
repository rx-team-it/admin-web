@extends('admin.layouts.admin.index')

@section('admin.css')
<style>
    .tab-content {
        padding: 10px;
        border-left: 1px solid #DDD;
        border-bottom: 1px solid #DDD;
        border-right: 1px solid #DDD;
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#{{ route('home.admin') }}" class=" btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1 id="beranda">Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Order</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('error') }}</strong>
                </div>
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Order Vendor</h4>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="row input-daterange ml-2 mt-4 mb-2">
                            <div class="col-md-4">
                                <input type="text" name="from_date" id="from_date" class="form-control form-control-sm" placeholder="From Date" readonly />
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="to_date" id="to_date" class="form-control form-control-sm" placeholder="To Date" readonly />
                            </div>
                            <div class="col-md-4">
                                <button type="button" name="filter" id="filter" class="btn btn-primary btn-sm">Filter</button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-secondary btn-sm">Refresh</button>
                            </div>
                        </div>
                    </div> -->
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#semua-pesanan" aria-controls="semua-pesanan" role="tab" aria-selected="true">Semua Pesanan <span class="badge badge-transparent" style="color: red;">{{$hitung_order}}</span></a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#belum-bayar" aria-controls="belum-bayar" role="tab">Belum Bayar <span class="badge badge-transparent" style="color: red;">{{$hitung_belum_bayar}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#cek-pembayaran" aria-controls="cek-pembayaran" role="tab">Cek Pembayaran <span class="badge badge-transparent" style="color: red;">{{$hitung_cek_pembayaran}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#upload-ulang" aria-controls="upload-ulang" role="tab">Pembayaran Bermasalah <span class="badge badge-transparent" style="color: red;">{{$hitung_upload_ulang}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#sudah-bayar" aria-controls="sudah-bayar" role="tab">Sudah Bayar <span class="badge badge-transparent" style="color: red;">{{$hitung_udah_bayar}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#dikemas" aria-controls="dikemas" role="tab">Dikemas <span class="badge badge-transparent" style="color: red;">{{$hitung_dikemas}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#dikirim" aria-controls="dikirim" role="tab">Dikirim <span class="badge badge-transparent" style="color: red;">{{$hitung_dikirim}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#selesai" aria-controls="selesai" role="tab">Selesai <span class="badge badge-transparent" style="color: red;">{{$hitung_selesai}}</span>
                                </a>
                            </li>
                            {{-- <li>
                                <a class="nav-link" data-toggle="tab" href="#gagal" aria-controls="gagal" role="tab">Gagal <span class="badge badge-transparent" style="color: red;">{{$hitung_gagal}}</span>
                                </a>
                            </li> --}}
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#batal" aria-controls="batal" role="tab">Pembatalan Transaksi <span class="badge badge-transparent" style="color: red;">{{$hitung_batal}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#ready_to_print" aria-controls="batal" role="tab">Siap dicetak <span class="badge badge-transparent" style="color: red;">{{$hitung_ready}}</span>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in show active" id="semua-pesanan">
                               {{--  <button onclick="printSelectedInvoice()" id="button-print" class="btn btn-primary mb-4"> <i class="fas fa-print"></i><span> Print Invoice</span></button> --}}
                               <div class="row mb-3">
                                   <div class="col-4">
                                        <div class="form-group">
                                        <!-- Get opsi Pengirim -->
                                            <label for="filter">Filter Pengiriman</label>
                                            <select class="form-control form-control-sm" id="filter" data='x' onchange="byPengirim(this.value, this.data)" autocomplete="off">
                                                <option value="" selected>--Semua--</option>
                                                <option value="grab" selected>GRAB</option>
                                                <option value="anter_aja" selected>Anter Aja</option>
                                                <option value="pickup" selected>Pickup</option>
                                                <option value="jne" selected>JNE</option>
                                                <option value="jnt" selected>JNT</option>
                                            </select>
                                        </div>
                                        <a href="" id="btn_refresh" class="btn btn-success d-none"> Refresh</a>
                                    </div>
                               </div>
                                <table id="semua-pesanan-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            {{-- <th><input type="checkbox" class="select-all checkbox" name="select-all"></th> --}}
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="semua_pesanan_b">
                                        @foreach($order as $data)
                                        <tr class="semua_pesanan_tr">
                                            {{-- <td><input type="checkbox" class="select-item checkbox" name="idorder[]" value="{{$data->id}}" /></td> --}}
                                            <td>{{!empty($data) ? $data->invoice : ''}} </a>
                                                <br>
                                                <small> Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 5)
                                                @elseif($data->status == 4)
                                                @elseif($data->request_cancel == 1)
                                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#requestpembatalan{{ $data->id }}"><i class="fas fa-comment-dots" data-toggle="tooltip" data-placement="top" title="Request Pembatalan"></i></button>
                                                @elseif($data->status == 2)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $data->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="belum-bayar">
                                <table id="belum-bayar-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($belumbayar as $data)
                                        <tr>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $data->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="cek-pembayaran">
                                <form action="{{ route('acc.bukti.multi') }}" method="POST" class="needs-validation">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-primary" id="tombol" disabled>
                                        <i class="fa fa-check"></i> Setujui masal
                                    </button>
                                    <br>
                                    <br>
                                    <table id="cek-pembayaran-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" class="select-all-cek-pembayaran checkbox" id="cekb1" name="select-all"></th>
                                                <th>Invoice</th>
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>Status</th>
                                                <th>Tanggal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cekpembayaran as $data)
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="select-item checkbox" id="cekb" name="idorder[]" value="{{$data->id}}" />
                                                </td>

                                                <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                    <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                                </td>
                                                <td>{{ !empty($data) ? $data->name : '' }}</td>
                                                <td>{{ !empty($data) ? $data->address : ''}}</td>
                                                <td>{!! $data->status_label !!}</td>
                                                <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                                <td>
                                                    <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                    <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                        <i class="fas fa-receipt"></i>
                                                    </a>
                                                    @if($data->status == 6)
                                                    @elseif($data->status == 3)
                                                    @else
                                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $data->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="upload-ulang">
                                <table id="upload-ulang-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($uploadulang as $data)
                                        <tr>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $data->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="sudah-bayar">
                                <div class="row mb-3">
                                    <div class="col-4">
                                         <div class="form-group">
                                         <!-- Get opsi Pengirim -->
                                             <label for="filter">Filter Servis</label>
                                             <select class="form-control form-control-sm" id="filter" data='x' onchange="byService(this.value, this.data)" autocomplete="off">
                                                 <option value="" selected>--Semua--</option>
                                                 <option value="regular">Reguler</option>
                                                 <option value="sameday">Sameday</option>
                                                 <option value="nextday">Nextday</option>
                                                 <option value="kargo">Kargo</option>
                                                 <option value="self_pickup">Self Pickup</option>
                                             </select>
                                         </div>
                                         <a href="" id="btn_refresh2" class="btn btn-success d-none"> Refresh</a>
                                     </div>
                                </div>
                                <span role="button"class="btn btn-outline-info mb-2" id="jne_masal"> <i class="fas fa-file-signature"></i> Buat JOB masal</span>
                                <form action="{{route('multi.print.Resi.sudah.bayar')}}"  method="POST" >
                                    @csrf
                                {{-- <button id="button-print" class="btn btn-primary mb-4 btn-print" disabled> <i class="fas fa-print"></i><span> Request Resi</span></button>  --}}
                                <!-- <button onclick="printSelected()" id="button-print" class="btn btn-primary mb-4"> <i class="fas fa-print"></i><span> Print Resi</span></button> -->
                                <table id="sudah-bayar-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" class="select-all-sudah-bayar checkbox" id="cek0" name="select-all"></th>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Pilih Ekspedisi</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sudahbayar as $data)
                                        <tr class="sudah_bayar_tr">
                                            <td>
                                                @if ($data->courrier == 'pickup')

                                                @else
                                                <input type="checkbox" id="select-item-sudah-bayar_{{$data->id}}" class="select-item-sudah-bayar select-item-sudah-bayar_{{$data->id}}  checkbox" data-service_code_="{{$data->service_code}}" data-ongkir="{{$data->ongkos_kirim}}" data-kurir="" name="idorder[]" value="{{$data->id}}" data-code_service=""/> 
                                                @endif
                                            </td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}}
                                                <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->service : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>                                         
                                            <td>                                                
                                                @if ($data->courrier !== 'pickup')
                                                {{-- <small>Ongkir Pembeli : {{'Rp. '.number_format($data->ongkos_kirim)}}</small>
                                                <br>
                                                <small id="ongkir_penjual_{{$data->id}}">Ongkir Penjual : -</small>
                                                <br>
                                                <small id="keuntungan_{{$data->id}}">Keuntungan : -</small>
                                                <input type="hidden" id="admin_courrier_{{$data->id}}" name="admin_courrier[]" value="" readonly>
                                                <input type="hidden" id="admin_service_{{$data->id}}" name="admin_service[]" value="" readonly>
                                                <input type="hidden" id="admin_service_code_{{$data->id}}" name="admin_service_code[]" value="" readonly>
                                                <input type="hidden" id="admin_ongkos_kirim_{{$data->id}}" name="admin_ongkos_kirim[]" value="" readonly> --}}
                                                 <select id="pilih_ekspedisi_{{$data->id}}" class="form-control" name="admin_courrier[]" readonly>
                                                    <option value="">Pilih Ekspedisi</option>
                                                </select> 
                                                @else
                                                  <center>  <strong> PICKUP </strong> </center>
                                                @endif
                                            </td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $data->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                                @if ($data->request_cancel == 1)
                                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#requestpembatalan{{ $data->id }}"><i class="fas fa-comment-dots" data-toggle="tooltip" data-placement="top" title="Request Pembatalan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="gagal">
                                <table id="gagal-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($gagal as $data)
                                        <tr>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->service : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else

                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="batal">
                                <table id="batal-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($batal as $data)
                                        <tr>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else

                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="dikemas">

                                {{-- <button onclick="register_awb_masal()">awb masal</button> --}}
                                <form action="{{route('register.awb.masal')}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-primary mb-2">register awb masal</button>
                                    <table id="dikemas-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Invoice</th>
                                                <th>Nama</th>
                                                <th>Alamat</th>
                                                <th>Status</th>
                                                <th>Tanggal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($dikemas as $data)
                                            <tr>
                                                @if ($data->courrier != 'pickup' && $data->register_awb == 0)
                                                    <td>
                                                        <input type="checkbox" name="invoice[]" id="invoice[]" value="{{ $data->invoice }}">
                                                        {{-- <input type="checkbox" id="select-item-dikemas_{{$data->id}}" class="select-item-dikemas select-item-dikemas_{{$data->id}}  checkbox" data-kode_boking="{{$data->kode_boking}}" name="awb[]" value="{{$data->invoice}}" />  --}}
                                                    </td>
                                                @elseif($data->register_awb == 1)
                                                    <td><i class="fas fa-check"></i></td>

                                                @else
                                                    <td></td>
                                                @endif
                                                <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                    <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                                </td>
                                                <td>{{ !empty($data) ? $data->name : '' }}</td>
                                                <td>{{ !empty($data) ? $data->address : ''}}</td>
                                                <td>{!! $data->status_label !!}</td>
                                                <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                                <td>
                                                    <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                    <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                        <i class="fas fa-receipt"></i>
                                                    </a>
                                                    @if($data->courrier == 'pickup')
                                                    {{--<button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $data->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>--}}
                                                    @else

                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="dikirim">
                                <table id="dikirim-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dikirim as $data)
                                        <tr>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else

                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div role="tabpanel" class="tab-pane fade in show" id="selesai">
                                <table id="selesai-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($selesai as $data)
                                        <tr>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else

                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="ready_to_print">
                            <form action="{{route('readytoprint')}}" method="post" target="_blank">
                                @csrf
                            <button id="button-print" class="btn btn-primary mb-4 oys" disabled> <i class="fas fa-print"></i><span> Cetak Resi</span></button> 
                                <table id="ready" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>                                      
                                            <th><input type="checkbox" class="select-all-ready_to_print checkbox" id="cekprint0" name="select-all"></th>
                                            <th>Invoice</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ready_to_print as $data)
                                        <tr>
                                        <td><input type="checkbox" id="select-item-sudah-bayar_{{$data->id}}" class="select-item-ready_to_print checkbox oy" name="ready_to_print[]" value="{{$data->id}}"/></td>
                                        <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                            <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                        </td>
                                        <td>{{ !empty($data) ? $data->name : '' }}</td>
                                        <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $data->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $data->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($data->status == 6)
                                                @elseif($data->status == 3)
                                                @else

                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- MODAL BATALKAN ORDER !-->
<div class="modal fade" id="editpesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('cancel.order') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Batalkan Orderan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Pesan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Batalkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL REQUEST PEMBATALAN ORDER DARI BUYER !-->
@foreach($request_pembatalan as $data)
<div class="modal fade" id="requestpembatalan{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class=" modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request Pembatalan Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Invoice :</label>
                    <span class="form-control">{{$data->invoice}}</span>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Pesan : </label>
                    <span class="form-control">{{$data->message_request_cancel}}</span>
                </div>
                <form action="{{route('tolak.pembatalan')}}" method="POST">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Balasan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <button type="submit" class="btn btn-danger"> Tolak</button>
                </form>
                <form action="{{route('acc.pembatalan')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <button type="submit" class="btn btn-success"> Setujui</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

<script>
    window.onload = function(){
        $('#filter').val('')
    }
    // get by pengirim
    function byPengirim(slug, data){
            $.ajax({
                url: "{{ route('list.order.new') }}",
                type: 'GET',
                data:{
                    slug: slug
                },
                success: function(response){
                    $('.semua_pesanan_tr').remove();
                    $('.pagination').remove();
                    $('.dataTables_info').remove();
                    // $('.dataTables_length').replaceWith(`<p class="dataTables_length">Show all by ${ slug ? slug : "all"}</p>`);
                    $('.dataTables_length').remove();
                    $('#btn_refresh').removeClass('d-none');
                    response.forEach(data => {
                        if (data.status == 6 || data.status == 5 || data.status == 4 || data.status == 2) {
                            $('#semua_pesanan_b').append(`<tr class="semua_pesanan_tr"> <td>${data.invoice} <br> <small> Pengiriman: ${data.courrier}</small> </td> <td>${data.name}</td> <td>${data.address}</td> <td>${data.status_label}</td> <td>${data.date}</td> <td>
                            <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                <i class="fas fa-eye"></i>
                            </a> 
                            <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                <i class="fas fa-receipt"></i>
                            </a>

                        </td> </tr> `);
                        }else if(data.request_cancel == 1){
                            $('#semua_pesanan_b').append(`<tr class="semua_pesanan_tr"> <td>${data.invoice} <br> <small> Pengiriman: ${data.courrier}</small> </td> <td>${data.name}</td> <td>${data.address}</td> <td>${data.status_label}</td> <td>${data.date}</td> <td>
                            <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                <i class="fas fa-eye"></i>
                            </a> 
                            <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                <i class="fas fa-receipt"></i>
                            </a>
                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#requestpembatalan${data.id}"><i class="fas fa-comment-dots" data-toggle="tooltip" data-placement="top" title="Request Pembatalan"></i></button>
                        </td> </tr> `);
                        }else{
                            $('#semua_pesanan_b').append(`<tr class="semua_pesanan_tr"> <td>${data.invoice} <br> <small> Pengiriman: ${data.courrier}</small> </td> <td>${data.name}</td> <td>${data.address}</td> <td>${data.status_label}</td> <td>${data.date}</td> <td>
                            <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                <i class="fas fa-eye"></i>
                            </a> 
                            <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                <i class="fas fa-receipt"></i>
                            </a>
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever=" ${data.id}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                        </td> </tr> `);
                        }
                    
                    });
                    
                }
            })
    }

        function byService(service, data){
            $.ajax({
                url: "{{ route('list.order.new') }}",
                type: 'GET',
                data:{
                    service: service,
                    slug: 'no'
                },
                beforeSend: function(){
                    console.log('loading');
                },
                success: function(res){
                    console.log('success');
                    $('.sudah_bayar_tr').remove();
                    $('.pagination').remove();
                    $('.dataTables_info').remove();
                    $('.dataTables_length').remove();
                    $('#btn_refresh2').removeClass('d-none');
                    res.forEach(data => {
                        if (data.courrier != 'pickup') {
                            $('#sudah-bayar-dt').append(`<tr class="sudah_bayar_tr"> <td><input type="checkbox" id="select-item-sudah-bayar" class="select-item-sudah-bayar checkbox" name="idorder[]" value="${data.id}" /> </td> <td>${data.invoice} <br> <small> Pengiriman: ${data.courrier}</small> </td> <td>${data.name}</td>   <td>${data.address}</td> <td>  <small>Ongkir Pembeli : {{'Rp. '.number_format($data->ongkos_kirim)}}</small>
                                                <br>
                                                <small id="ongkir_penjual_${data.id}">Ongkir Penjual : -</small>
                                                <br>
                                                <small id="keuntungan_${data.id}">Keuntungan : -</small>
                                                <input type="hidden" id="admin_courrier_${data.id}" name="admin_courrier[]" value="" readonly>
                                                <input type="hidden" id="admin_service_${data.id}" name="admin_service[]" value="" readonly>
                                                <input type="hidden" id="admin_service_code_${data.id}" name="admin_service_code[]" value="" readonly>
                                                <input type="hidden" id="admin_ongkos_kirim_${data.id}" name="admin_ongkos_kirim[]" value="" readonly>
                                                 <select style="background-color:pink;" id="pilih_ekspedisi_${data.id}" class="form-control" name="admin_courrier[]" readonly>
                                                    <option value="">Pilih Ekspedisi</option>
                                                </select>  </td> <td>
                                                    ${data.status_label}
                                </td> <td>${data.date}</td> <td>
                                <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fas fa-eye"></i>
                                </a> 
                                <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                    <i class="fas fa-receipt"></i>
                                </a>
                            </td> </tr>`);
                        }else{
                            $('#sudah-bayar-dt').append(`<tr class="sudah_bayar_tr"> <td></td> <td>${data.invoice} <br> <small> Pengiriman: ${data.courrier}</small> </td> <td>${data.name}</td>   <td>${data.address}</td> <td>${data.status_label}</td> <td><center>  <strong> PICKUP </strong> </center></td> <td>${data.date}</td> <td>
                                <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fas fa-eye"></i>
                                </a> 
                                <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                    <i class="fas fa-receipt"></i>
                                </a>
                            </td> </tr>`);

                        }
                    });
                    //alamat pengirim
                    var postal_code_origin = "{{$default_store->postal_code}}";
                    var sub_district_origin = "{{$default_store->district}}";
                    var city_origin = "{{$default_store->city}}";

                    res.forEach(data => {
                        var type = data.courrier;
                        
                        //alamat_penerima
                        var postal_code_destination = data.postal_code;
                        var sub_district_destination = data.city;
                        var city_destination = data.state;
                        var group_service = data.group_service;
                        // console.log(group_service)
                        var weight = data.berat_pengiriman;
                        var bt = weight / 1000;
                        if(type !== "pickup"){
                            $.ajax({
                                    url: "{{url('api/check-service-admin/')}}/" + postal_code_origin + '/' + sub_district_origin + '/' + city_origin + '/' + postal_code_destination + '/' + sub_district_destination + '/' + city_destination + '/' + bt + '/' + group_service,
                                    type: 'get',
                                    dataType: 'json',
                                    success: function(response) {
                                        var check_service_api = response.data.price;
                                        // console.log(check_service_api);
                                        var object = check_service_api;
                                        var select = $(`#pilih_ekspedisi_${data.id}`);
                                        select.empty();
                                        select.append('<option value="">Pilih Pengiriman</option>');
                        
                                        for (const property in object) {
                                            var obj2 = object[property];                       
                                            select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="JNE" data-code_service="' + object[property].service_code + '">' + object[property].service_display + '('+ object[property].etd_from +'-'+ object[property].etd_thru +' Hari)' + ' - ' + rubah(object[property].price) + '</option>');
                                        }
    
                                    }
                                });
    
                                $(`#pilih_ekspedisi_${data.id}`).on('change', function(e) {
                                let service_code = ($(this).find(':selected').data('code_service'));
                                let kurir = ($(this).find(':selected').data('kurir'));
                                let servis = ($(this).find(':selected').data('servis'));
                                let ongkir = ($(this).find(':selected').data('ongkir'));
    
                                $(`#admin_courrier_${data.id}`).val(kurir);
                                $(`#admin_service_${data.id}`).val(servis);
                                $(`#admin_service_code_${data.id}`).val(service_code);
                                $(`#admin_ongkos_kirim_${data.id}`).val(ongkir);
    
                                $(`#ongkir_penjual_${data.id}`).text('Ongkir Penjual : Rp. '+ rubah(ongkir) +'');
                                let ongkos_kirim = data.ongkos_kirim;
                                let keuntungan = ongkos_kirim - ongkir;
                                if(keuntungan < 0 ){
                                    $(`#keuntungan_${data.id}`).html(`<small style="color:red; font-size:12px;">Anda Rugi :<b> Rp. `+ rubah(keuntungan) +`</b></small>`);
                                }else{
                                    $(`#keuntungan_${data.id}`).html(`<small style="color:green; font-size:12px;">Anda Profit :<b> Rp. `+ rubah(keuntungan) +`</b></small>`);
                                }
                                console.log(keuntungan);    
                                
                                console.log(kurir);
                                console.log(service_code);
                                console.log(servis);
                                console.log(ongkir);
                            });
                        }   
                    })
                    // @foreach($sudahbayar as $data)
                // @endforeach
                console.log(res);
                    
                }
            })
        } 
</script>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>
@endif

<script>
    var checker = document.getElementById('cekb');
    var checker1 = document.getElementById('cekb1');
    var sendbtn = document.getElementById('tombol');
    // when unchecked or checked, run the function
    checker.onchange = function() {
        if (this.checked) {
            sendbtn.disabled = false;
        } else {
            sendbtn.disabled = true;
        }
    }
    checker1.onchange = function() {
        if (this.checked) {
            sendbtn.disabled = false;
        } else {
            sendbtn.disabled = true;
        }
    }

</script>

<script>
    var cek0 = document.getElementById('cek0');
    var cek1 = document.getElementById('select-item-sudah-bayar');
    var btn = document.getElementById('button-print');
    // when unchecked or checked, run the function
    cek0.onchange = function() {
        if (this.checked) {
            btn.disabled = false;
        } else {
            btn.disabled = true;
        }
    }
    cek1.onchange = function() {
        if (this.checked) {
            btn.disabled = false;
        } else {
            btn.disabled = true;
        }
    }
</script>

<script>
    var cekprint = document.getElementById('cekprint0');
    var cekb = document.getElementsByClassName('oy')[0];
    var btn_print = document.getElementsByClassName('oys')[0];
    // when unchecked or checked, run the function
    cekprint.onchange = function() {
        if (this.checked) {
            btn_print.disabled = false;
        } else {
            btn_print.disabled = true;
        }
    }
    cekb.onchange = function() {
        if (this.checked) {
            btn_print.disabled = false;
        } else {
            btn_print.disabled = true;
        }
    }
</script>

<script>
    $(document).ready(function() {
        $('#semua-pesanan-dt, #sudah-bayar-dt, #gagal-dt, #batal-dt, #dikirim-dt, #selesai-dt, #belum-bayar-dt, #dikemas-dt, #cek-pembayaran-dt, #upload-ulang-dt, #ready').DataTable({
            scrollX: true
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });
    });

    function printSelected() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];

        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });

        window.open('{{route("multi.print.Resi")}}?datas=' + items, '_blank');
    };

    function printSelectedInvoice() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];

        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.open('{{route("multi.print.Invoice")}}?datas=' + items, '_blank');
    };

    function accSelected() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];
        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.location.href = "{{ route('acc.bukti.multi')}}";
    };

    $('#editpesan').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Batalkan Orderan')
        modal.find('#recipient-name').val(recipient)
    })
</script>

<script type="text/javascript">
    let select_id;
    let register;
    $(document).ready(function() {
       
        // $(".check_all_register").click(function() {
        //     $("#check_register input[type='checkbox']").prop('checked', this.checked);
        //     console.log('all');
        // });

        // $(".check_register").click(function() {
        //     var test = [];
        //     $("input.check_register:checked:checked").each(function(index, item) {
        //         test[index] = item.value;
        //     });
        //     register = test;
        //     console.log(register);
        // });


        $(".select-all-sudah-bayar").click(function() {
            $("#sudah-bayar-dt input[type='checkbox']").prop('checked', this.checked);
        });

        $(".select-item-sudah-bayar").click(function() {
            var items = [];
            $("input.select-item-sudah-bayar:checked:checked").each(function(index, item) {
                // items = item.value;
                items[index] = {id:item.value, ongkir:item.getAttribute('data-ongkir'), service_code_:item.getAttribute('data-service_code_'), code_service:item.getAttribute('data-code_service'), kurir:item.getAttribute('data-kurir')}
            });
            select_id = items;
            console.log(select_id);
        });
        
        //checkbox all cek pembayaran
        $(".select-all-cek-pembayaran").click(function() {
            $("#cek-pembayaran-dt input[type='checkbox']").prop('checked', this.checked);
            console.log('cek');
        });

        //checkbox all siap di cetak
        $(".select-all-ready_to_print").click(function() {
            $("#ready input[type='checkbox']").prop('checked', this.checked);
        });


        // select-all-cek-pembayaran

        //button select all or cancel
        // $("#select-all").click(function() {
        //     var all = $("input.select-all")[0];
        //     all.checked = !all.checked
        //     var checked = all.checked;
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = checked;
        //     });
        // });

        //button select invert
        // $("#select-invert").click(function() {
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = !item.checked;
        //     });
        //     checkSelected();
        // });

        //button get selected info
        // $("#selected").click(function() {
        //     var items = [];
        //     $("input.select-item:checked:checked").each(function(index, item) {
        //         items[index] = item.value;
        //     });
        //     if (items.length < 1) {
        //         alert("no selected items!!!");
        //     } else {
        //         var values = items.join(',');
        //         var html = $("<div></div>");
        //         html.html("selected:" + values);
        //         html.appendTo("body");
        //     }
        // });

        //column checkbox select all or cancel
        // $("input.select-all").click(function() {
        //     var checked = this.checked;
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = checked;
        //     });
        // });

        //check selected items
        // $("input.select-item").click(function() {
        //     var checked = this.checked;
        //     console.log(checked);
        //     checkSelected();
        // });

        //check is all selected
        // function Selected() {
        //     var all = $("input.select-all")[0];
        //     var total = $("input.select-item").length;
        //     var len = $("input.select-item:checked:checked").length;
        //     console.log("total:" + total);
        //     console.log("len:" + len);
        //     all.checked = len === total;
        // }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#saldo').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#saldo').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#saldo').DataTable({
                ajax: {
                    url: "{{ route('list.order.new') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
            });
        }
    });
</script>

<script>

    //RUPIAH
    const rupiah = (number) => {
        return new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR"
        }).format(number);
    }

    //function rupiah
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join(',').split('').reverse().join('');
        return ribuan;
    }

    // //alamat pengirim
    var postal_code_origin = "{{$default_store->postal_code}}";
    var sub_district_origin = "{{$default_store->district}}";
    var city_origin = "{{$default_store->city}}";

    @foreach($sudahbayar as $data)
    var type = "{{$data->courrier}}";
    
    //alamat_penerima
    var postal_code_destination = "{{$data->postal_code}}";
    var sub_district_destination = "{{$data->city}}";
    var city_destination = "{{$data->state}}";
    var group_service = "{{$data->group_service}}";
    var weight = "{{$data->berat_pengiriman}}";
    var bt = weight / 1000;
    if(type !== "pickup"){
        $.ajax({
                url: "{{url('api/check-service-admin/')}}/" + postal_code_origin + '/' + sub_district_origin + '/' + city_origin + '/' + postal_code_destination + '/' + sub_district_destination + '/' + city_destination + '/' + bt + '/' + group_service,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var check_service_api = response.data.price;
                    var object = check_service_api;
                    var select = $("#pilih_ekspedisi_{{$data->id}}");
                    select.empty();
                    select.append('<option value="">Pilih Pengiriman</option>');
                    for (const property in object) {
                        var obj2 = object[property];                       
                        select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="JNE" data-code_service="' + object[property].service_code + '">' + object[property].service_display + '('+ object[property].etd_from +'-'+ object[property].etd_thru +' Hari)' + ' - ' + rubah(object[property].price) + '</option>');
                    }
                }
            });

            $('#pilih_ekspedisi_{{$data->id}}').on('change', function(e) {
            let service_code = ($(this).find(':selected').data('code_service'));
            let kurir = ($(this).find(':selected').data('kurir'));
            let servis = ($(this).find(':selected').data('servis'));
            let ongkir = ($(this).find(':selected').data('ongkir'));

            $('#admin_courrier_{{$data->id}}').val(kurir);
            $('#admin_service_{{$data->id}}').val(servis);
            $('#admin_service_code_{{$data->id}}').val(service_code);
            $('#admin_ongkos_kirim_{{$data->id}}').val(ongkir);

            $('#ongkir_penjual_{{$data->id}}').text('Ongkir Penjual : Rp. '+ rubah(ongkir) +'');
            let ongkos_kirim = "{{$data->ongkos_kirim}}";
            let keuntungan = ongkos_kirim - ongkir;
            if(keuntungan < 0 ){
                $('#keuntungan_{{$data->id}}').html(`<small style="color:red; font-size:12px;">Anda Rugi :<b> Rp. `+ rubah(keuntungan) +`</b></small>`);
            }else{
                $('#keuntungan_{{$data->id}}').html(`<small style="color:green; font-size:12px;">Anda Profit :<b> Rp. `+ rubah(keuntungan) +`</b></small>`);
            }
            console.log(keuntungan);    
            
            console.log(kurir);
            console.log(service_code);
            console.log(servis);
            console.log(ongkir);
    });
    }   
    @endforeach
</script>
    
<script>
    //creaete job jne masal
    let berat_totals;
    let kode_pos;
    let kelurahan;
    let type_jne;
    let type_service;
    @foreach($sudahbayar as $data)
            type_jne = "{{$data->courrier}}";
            type_service = "{{$data->group_service}}";
            berat_totals = "{{$data->berat_pengiriman}}";
            bt = berat_totals / 1000;
            if(bt < 1)
            {
                bt = 1;
            }
            kode_pos =  "{{$data->postal_code}}";
            kelurahan =  "{{$data->urban}}";
            if(type_jne !== "pickup")
            {
                $.ajax({
                        url: "{{url('api/get_thru')}}/" + kode_pos + '/' + kelurahan,
                        type: 'get',
                        dataType: 'json',
                        success: function(response) {
                            console.log(response);
                            let thru = response.data.DEST;
                            let from = "CGK10000";
                            let weights = berat_totals;
                            // $.ajax({
                            //     url: "{{url('api/api-tarif-jne')}}/" + from + '/' + thru + '/' + weights,
                            //     type: 'get',
                            //     dataType: 'json',
                            //     success: function(response) {

                            //         // console.log(response.data);
                            //         var check_service_api = response.data;
                            //         var object = check_service_api;
                            //         var select = $("#pilih_ekspedisi_{{$data->id}}");
                            //         select.empty();
                            //         select.append('<option value="">Pilih Pengiriman</option>');
                                    
                            //         for (const property in object) {
                            //             let type_service = "{{$data->group_service}}";
                            //             if(type_service == "regular")
                            //             {
                            //                 if(object[property].service_code == "OKE19" || object[property].service_code == "REG19"){
                            //                     select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="jne" data-code_service="' + object[property].service_code + '">' + object[property].service_display + ' - ' + rubah(object[property].price) + '</option>');
                            //                 }else{
                            //                     continue;
                            //                 }
                            //             }else if(type_service == "nextday"){
                            //                 if(object[property].service_code == "YES19"){
                            //                     select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="jne" data-code_service="' + object[property].service_code + '">' + object[property].service_display + ' - ' + rubah(object[property].price) + '</option>');
                            //                 }else{
                            //                     continue;
                            //                 }
                            //             }else if(type_service == "sameday"){
                            //                 if(object[property].service_code == "JTR18"){
                            //                     select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="jne" data-code_service="' + object[property].service_code + '">' + object[property].service_display + ' - ' + rubah(object[property].price) + '</option>');
                            //                 }else{
                            //                     continue;
                            //                 }
                            //             }

                            //         }
                            //     }
                            // });
                            $.ajax({
                            url: "{{url('api/check-service-admin/')}}/" + postal_code_origin + '/' + sub_district_origin + '/' + city_origin + '/' + postal_code_destination + '/' + sub_district_destination + '/' + city_destination + '/' + bt + '/' + group_service,
                            type: 'get',
                            dataType: 'json',
                            success: function(response) {
                                var check_service_api = response.data.price;
                                var object = check_service_api;
                                var select = $("#pilih_ekspedisi_{{$data->id}}");
                                select.empty();
                                select.append('<option value="">Pilih Pengiriman</option>');
                                for (const property in object) {
                                    var obj2 = object[property];                       
                                    select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="JNE" data-code_service="' + object[property].service_code + '">' + object[property].service_display + '('+ object[property].etd_from +'-'+ object[property].etd_thru +' Hari)' + ' - ' + rubah(object[property].price) + '</option>');
                                }
                            }
                        });
                        
                        }
                    });
                    }
                            $('#pilih_ekspedisi_{{$data->id}}').on('change', function(e) {
                                let service_code_ = ($(this).find(':selected').data('servis'));
                                let ongkir = ($(this).find(':selected').data('ongkir'));
                                let code_service = ($(this).find(':selected').data('code_service'));
                                let kurir = ($(this).find(':selected').data('kurir'));

                                $('#select-item-sudah-bayar_{{$data->id}}').attr('data-kurir' , kurir);
                                $('#select-item-sudah-bayar_{{$data->id}}').attr('data-code_service' , code_service);
                                $('#select-item-sudah-bayar_{{$data->id}}').attr('data-service_code_' , service_code_);
                                $('#select-item-sudah-bayar_{{$data->id}}').attr('data-ongkir' , ongkir);

                                console.log(kurir);
                                console.log(service_code_);
                            });
    @endforeach

</script>

<script>
$("#jne_masal").click(function() {

   
            let data = {
                datas: select_id,
                _token: "{{ csrf_token() }}",
            }

            console.log(data);
            // $.ajax({
            //             url: "{{url('api/create_job_masal')}}/" + select_id + '/'  + kurir+ '/' + service_code_ + '/' + ongkir + '/' + code_service,
            //             type: 'post',
            //             dataType: 'json',
            //             success: function(response) {
            //                 console.log(response);
            //             }
            //         });

            $.ajax({
                type: "post",
                url:"{{url('api/create_job_masal_post')}}",
                data: JSON.stringify(data),
                contentType: "application/json",
                dataType: "json",
                success: function(response) {
                    console.log(response);
                    // location.reload();
                }
            });

            // if(select_id !== undefined)
            // {

            //     for (const property in select_id) {
            //     let id = select_id[property].id;
            //     let ongkir = select_id[property].ongkir;
            //     let service_code_ = select_id[property].service_code_;
            //     let code_service = select_id[property].code_service;
            //     let kurir = select_id[property].kurir;
            //     $.ajax({
            //             url: "{{url('api/create_job_masal')}}/" + id + '/'  + kurir+ '/' + service_code_ + '/' + ongkir + '/' + code_service,
            //             type: 'get',
            //             dataType: 'json',
            //             success: function(response) {
            //                 console.log(response);
            //             }
            //         });
            //     }
            // }else{
            //     alert('Pilih Invoice !')
            // }
        
});
</script>
@endsection