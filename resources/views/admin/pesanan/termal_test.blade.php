@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <h1>Resi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
            <div class="breadcrumb-item">Resi</div>
        </div>
    </div>
    <div class="section-body">
        <div class="invoice">
            <div class="invoice-print">
                <hr style="border: none; border-bottom: 1px solid black;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="invoice-title">
                            <div class="row">
                                <div class="col-lg-6">
                                    <table class="table table-bordered" style="border:2px solid black;">
                                        <tr>
                                            <th>SUB-SUB21-SUB05</th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-lg-6 text-md-right">
                                    <table class="table table-bordered" style="border:2px solid black;">
                                        <tr>
                                            <th>No. Resi: AB00789753468373849738</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-lg-center">
                                    <tr>
                                        <th><?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG('ini adalah invoice', 'C39', 3, 100) . '" alt="barcode" />'; ?></th>
                                    </tr>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Penerima:</strong><br>
                                    Aldiansyah<br>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <table class="table table-bordered" style="border:2px solid black;">
                                                <tr>
                                                    <th>HOME</th>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Pengirim:</strong><br>
                                    Abbabill<br>
                                    <br>
                                    089630132793
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <address>
                                    <!-- <strong>Payment Method:</strong><br> -->
                                    Nusa indah loka he 5/29 Pakujaya Serpong Utara<br>
                                    Kota Tangerang Selatan<br>
                                    Indonesia
                                </address>
                            </div>
                            <div class="col-md-4 text-md-right">
                                <address>
                                    <!-- <strong>Order Date:</strong><br> -->
                                    Kota Jakarta Pusat<br><br>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <table class="table table-bordered" style="border:2px solid black;">
                            <tr>
                                <th>Kota Jakarta Pusat</th>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-6 text-md-right">
                        <table class="table table-bordered" style="border:2px solid black;">
                            <tr>
                                <th>Pakujaya, Serpong Utara</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <table class="table table-bordered" style="border:1px solid black;">
                            <tr>
                                <th>Cashless</th>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-8 text-md-center">
                        <table class="table table-bordered" style="border:1px solid black;">
                            <tr>
                                <th>Penjual Tidak Perlu Bayar Ongkir Ke Kurir</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <address>
                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Berat:</strong> 250gr
                                    <br>
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <strong>COD:</strong> Rp0
                                    <br>
                                </div>
                            </div>
                            <strong>Batas Kirim:</strong> 29-09-2021
                            <br>
                            <strong>Nomor Pesanan:</strong> AB298834949
                            <!-- <div class="row">
                                <div class="col-md-4">
                                    <table class="table table-bordered" style="border:2px solid black;">
                                        <tr>
                                            <th>HOME</th>
                                        </tr>
                                    </table>
                                </div>
                            </div> -->
                        </address>
                    </div>
                    <div class="col-lg-8 text-md-right">
                        <address>
                            <?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG('ini adalah resi termal test', 'C39', 1, 50) . '" alt="barcode" />'; ?>
                        </address>
                    </div>
                </div>
                <hr style="border: none; border-bottom: 1px solid black;">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-sm">
                                <tr>
                                    <th>Nama Produk</th>
                                    <th class="text-center">Qty</th>
                                    <th class="text-right">Variasi</th>
                                    <th class="text-right">Sku</th>
                                </tr>
                                <tr>
                                    <td>Mouse Wireless</td>
                                    <td class="text-center">1</td>
                                    <td class="text-right">Hitam</td>
                                    <td class="text-right">M199</td>
                                </tr>
                                <tr>
                                    <td>Keyboard Wireless</td>
                                    <td class="text-center">3</td>
                                    <td class="text-right">Gaming</td>
                                    <td class="text-right">K2367</td>
                                </tr>
                                <tr>
                                    <td>Headphone Blitz TDR-3000</td>
                                    <td class="text-center">1</td>
                                    <td class="text-right">Hitam</td>
                                    <td class="text-right">HBlitz1</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class="text-md-right">
                    <div class="float-lg-left mb-lg-0 mb-3">
                    </div>
                    <!-- <a class="btn btn-warning btn-icon icon-left" target="_blank" rel="noopener" type="button" onclick="printDiv('printableArea')"><i class="fas fa-print"></i> Print</a> -->
                    <a href="#" rel="noopener" target="_blank" class="btn btn-warning btn-icon icon-left"><i class="fas fa-print"></i> Print</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection