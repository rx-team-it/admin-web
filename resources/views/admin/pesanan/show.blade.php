@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('pesanan.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Toko Aktif</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Toko Aktif</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Pesanan Toko - {{ $store->name_site }}</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <a class="btn btn-success btn-icon icon-left" href="{{ route('pesanan.show', $store->id) }}"><i class="fas fa-print"></i> Belum Bayar</a>
                        <a class="btn btn-primary btn-icon icon-left" href="{{ route('pesanan.sudah_bayar', $store->id) }}"><i class="fas fa-print"></i> Sudah Bayar</a>
                        <a class="btn btn-danger btn-icon icon-left" href="{{ route('pesanan.pembayaran_gagal', $store->id) }}"><i class="fas fa-print"></i> Pembayaran Gagal</a>
                        <a class="btn btn-secondary btn-icon icon-left" href="{{ route('pesanan.dikemas', $store->id) }}"><i class="fas fa-print"></i> Dikemas</a>
                        <a class="btn btn-info btn-icon icon-left" href="{{ route('pesanan.dikirim', $store->id) }}"><i class="fas fa-print"></i> Dikirim</a>
                        <a class="btn btn-primary btn-icon icon-left" href="{{ route('pesanan.selesai', $store->id) }}"><i class="fas fa-print"></i> Selesai</a>
                        <br>
                        <br>
                        <div class="card-body">
                            <div class="row input-daterange">
                                <div class="col-md-4">
                                    <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                                </div>
                                <div class="col-md-4">
                                    <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                                </div>
                                <div class="col-md-4">
                                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                                </div>
                            </div>
                            <table id="table_produk" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Invoice</th>
                                    <th>No Hp</th>
                                    <th>Alamat</th>
                                    <th>Status</th>
                                    <th>Tanggal</th>
                                    <th>Detail</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#table_produk').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#table_produk').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#table_produk').DataTable({
                pageLength: 25,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">lBfrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order Sudah Bayar'
                }, {
                    extend: 'excel',
                    title: 'List Order Sudah Bayar'
                }, {
                    extend: 'print',
                    title: 'List Order Sudah Bayar'
                }, ],
                ajax: {
                    url: "{{ route('pesanan.show', $store->id) }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                    },
                    {
                        data: 'address',
                        name: 'address'
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });
</script>
@endsection