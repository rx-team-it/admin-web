<section class="section">
    <link rel="stylesheet" href="{{asset('assets/css/app.min.css')}}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/components.css')}}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

    <div class="section-body">
        @foreach($invoice as $order)
        <div class="invoice">
            <div class="invoice-print" id="printableArea">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="invoice-title">
                            <h2>Invoice</h2>
                            <h5>{{ !empty($order) ? $order->invoice : '' }}</h5>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Pengirim:</strong><br>
                                    {{ !empty($address) ? $address->name: '' }}<br>
                                    {{ !empty($address) ? $address->phone : '' }}<br>
                                    {{ !empty($address) ? $address->address : '' }}<br>
                                    {{ !empty($address) ? $address->urban : '' }}<br>
                                    {{ !empty($address) ? $address->district : '' }}<br>
                                    {{ !empty($address) ? $address->city : '' }}<br>
                                    {{ !empty($address->countrys->province_name) ? $address->countrys->province_name : '' }},
                                    {{ !empty($address) ? $address->postal_code: '' }}<br>
                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Penerima:</strong><br>
                                    {{ !empty($order) ? $order->name : '' }}<br>
                                    {{ !empty($order) ? $order->phone : '' }}<br>
                                    {{ !empty($order) ? $order->address : '' }}<br>
                                    {{ !empty($order) ? $order->state : '' }}<br>
                                    {{ !empty($order) ? $order->city : '' }}<br>
                                    {{ !empty($order) ? $order->province : '' }},
                                    {{ !empty($order) ? $order->postal_code : '' }}<br>
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Pembayaran:</strong>
                                    <br>{{$order->bank}}<br>
                                    <strong>Pengantaran:</strong>
                                    <br>{{$order->courrier}} - {{$order->service}}
                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Tanggal Pemesanan:</strong><br>
                                    {{ $order->created_at }}<br><br>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="section-title">Ringkasan Pesanan</div>
                        <p class="section-lead">Semua Produk di sini tidak dapat dihapus.</p>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-md">
                                <tr>
                                    <th data-width="40">No</th>
                                    <th>Produk</th>
                                    <th class="text-center">Kode Produk</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                @php
                                $subtotal = 0;

                                @endphp

                                @foreach($order['detail'] as $keys => $rows)

                                @php $total = $rows->price * $rows->qty; @endphp
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $rows->product->name }} <br>
                                        <span style="font-size:10px;">{{ !empty($rows->product_detail->varian_option->varian) ? $rows->product_detail->varian_option->varian->name_varian : '' }} {{ !empty($rows->product_detail->varian_option) ? $rows->product_detail->varian_option->name : '' }}</span>
                                        <br>
                                        <span style="font-size:10px;">
                                            {{ !empty($rows->sub_variasi) ? $rows->sub_variasi->name_varian : '' }}
                                            {{ !empty($rows->parent_option) ? $rows->parent_option->name : '' }}
                                        </span>
                                    </td>
                                    <td class="text-center">@currency($rows->price)</td>
                                    <td class="text-center">{{ $rows->qty }}</td>
                                    <td class="text-right">@currency( $total )</td>
                                </tr>
                                @php $subtotal = $subtotal + $total; @endphp

                                @endforeach
                            </table>
                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-12 text-right">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Subtotal</div>
                                    <div class="invoice-detail-value">@currency( $subtotal )</div>
                                </div>
                                <hr class="mt-2 mb-2">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Total</div>
                                    <div class="invoice-detail-value invoice-detail-value-lg">@currency( $subtotal )</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <!-- {!! $order->status_label !!} -->
            <br>
            <br>
            <div class="text-md-right">
                <div class="float-lg-left mb-lg-0 mb-3">
                </div>
                <!-- <a class="btn btn-warning btn-icon icon-left" target="_blank" rel="noopener" type="button" onclick="printDiv('printableArea')"><i class="fas fa-print"></i> Print</a> -->
            </div>
        </div>
        @endforeach
    </div>
    <script src="{{asset('assets/js/app.min.js')}}"></script>
    <!-- JS Libraies -->
    <!-- Page Specific JS File -->
    <!-- Template JS File -->
    <script src="{{asset('assets/js/scripts.js')}}"></script>
    <!-- Custom JS File -->
    <script src="{{asset('assets/js/custom.js')}}"></script>
    <script>
        window.addEventListener("load", window.print());
    </script>

</section>