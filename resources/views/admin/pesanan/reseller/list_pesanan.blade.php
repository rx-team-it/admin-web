@extends('admin.layouts.admin.index')

@section('admin.css')
<style>
    .tab-content {
        padding: 10px;
        border-left: 1px solid #DDD;
        border-bottom: 1px solid #DDD;
        border-right: 1px solid #DDD;
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Order</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('error') }}</strong>
                </div>
                @endif
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Order Reseller</h4>
                    </div>
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#semua-pesanan" aria-controls="semua-pesanan" role="tab" aria-selected="true">Semua Pesanan <span class="badge badge-transparent" style="color: red;">{{$hitung_order}}</span></a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#belum-bayar" aria-controls="belum-bayar" role="tab">Belum Bayar <span class="badge badge-transparent" style="color: red;">{{$hitung_belum_bayar}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#cek-pembayaran" aria-controls="cek-pembayaran" role="tab">Cek Pembayaran <span class="badge badge-transparent" style="color: red;">{{$hitung_cek_pembayaran}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#upload-ulang" aria-controls="upload-ulang" role="tab">Pembayaran Bermasalah <span class="badge badge-transparent" style="color: red;">{{$hitung_upload_ulang}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#sudah-bayar" aria-controls="sudah-bayar" role="tab">Sudah Bayar <span class="badge badge-transparent" style="color: red;">{{$hitung_udah_bayar}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#dikemas" aria-controls="dikemas" role="tab">Dikemas <span class="badge badge-transparent" style="color: red;">{{$hitung_dikemas}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#dikirim" aria-controls="dikirim" role="tab">Dikirim <span class="badge badge-transparent" style="color: red;">{{$hitung_dikirim}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#selesai" aria-controls="selesai" role="tab">Selesai <span class="badge badge-transparent" style="color: red;">{{$hitung_selesai}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#batal" aria-controls="batal" role="tab">Pembatalan Transaksi <span class="badge badge-transparent" style="color: red;"> {{$hitung_batal}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#siapcetak" aria-controls="siapcetak" role="tab">Siap Dicetak <span class="badge badge-transparent" style="color: red;"> {{$hitungcetak}}</span>
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in show active" id="semua-pesanan">
                                {{-- <button onclick="printSelectedInvoice()" id="button-print" class="btn btn-primary mb-4"> <i class="fas fa-print"></i><span> Print Invoice</span></button> --}}
                                <div class="row mb-3">
                                   <div class="col-4">
                                        <div class="form-group">
                                        <!-- Get opsi Pengirim -->
                                            <label for="filter">Filter Pengiriman</label>
                                            <select class="form-control form-control-sm" id="filter" data='x' onchange="byPengirim(this.value, this.data)" autocomplete="off">
                                                <option value="" selected>--Semua--</option>
                                                <option value="grab" selected>GRAB</option>
                                                <option value="anter_aja" selected>Anter Aja</option>
                                                <option value="pickup" selected>Pickup</option>
                                                <option value="jne" selected>JNE</option>
                                                <option value="jnt" selected>JNT</option>
                                            </select>
                                        </div>
                                        <a href="" id="btn_refresh" class="btn btn-success d-none"> Refresh</a>
                                    </div>
                               </div>
                                <table id="semua-pesanan-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            {{-- <th><input type="checkbox" class="select-all checkbox" name="select-all"></th> --}}
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="semua_pesanan_b">
                                        @foreach ($order as $item)
                                        <tr class="semua_pesanan_tr">
                                            {{-- <td><input type="checkbox" class="select-item checkbox" name="idorder[]" value="{{$item->id}}" /></td> --}}
                                            <td>
                                                {{ !empty($item) ? $item->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($item) ? $item->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($item) ? $item->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($item) ? $item->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $item->reseller->user->name }}</td>
                                            <td>{!! $item->status_label !!}</td>
                                            <td>{{ !empty($item) ? $item->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($item->komisi))
                                                    {{'Rp. ' . number_format($item->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $item->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $item->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($item->status == 6)
                                                @elseif($item->status == 5)
                                                @elseif($item->status == 4)
                                                @elseif($item->request_cancel == 1)
                                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#requestpembatalan{{ $item->id }}"><i class="fas fa-comment-dots" data-toggle="tooltip" data-placement="top" title="Request Pembatalan"></i></button>
                                                @elseif($item->status == 2)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $item->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="sudah-bayar">
                                <span role="button"class="btn btn-outline-info" id="jne_masal"> <i class="fas fa-file-signature"></i> jne masal</span>
                                <button onclick="printSelected()" id="button-print" class="btn btn-primary mb-4" disabled> <i class="fas fa-print"></i><span> Print Resi</span></button>
                                <table id="sudah-bayar-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" class="select-all checkbox" id="cekAllPrint" name="select-all"></th>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Pilih Ekspedisi</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sudahbayar as $dah)
                                        <tr>
                                            <td>
                                                @if ($dah->courrier == 'pickup')

                                                @else
                                                
                                                <input type="checkbox" class="select-item select-item-sudah-bayar_{{$dah->id}} checkbox" id="cekIdPrint" data-service_code_="{{$dah->service_code}}" data-ongkir="{{$dah->ongkos_kirim}}" data-kurir="" name="idorder[]" value="{{$dah->id}}" data-code_service=""/>
                                                @endif
                                            </td>
                                            <td>
                                                {{ !empty($dah) ? $dah->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($dah) ? $dah->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($dah) ? $dah->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($dah) ? $dah->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $dah->reseller->user->name }}</td>
                                            <td>
                                                @if ($dah->courrier !== 'pickup')
                                                 <select id="pilih_ekspedisi_{{$dah->id}}" class="form-control" name="admin_courrier[]" readonly>
                                                    <option value="">Pilih Ekspedisi</option>
                                                </select> 
                                                @else
                                                  <center>  <strong> PICKUP </strong> </center>
                                                @endif
                                            </td>
                                            <td>{!! $dah->status_label !!}</td>
                                            <td>{{ !empty($dah) ? $dah->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($dah->komisi))
                                                    {{'Rp. ' . number_format($dah->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $dah->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $dah->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>

                                                @if($dah->status == 6)
                                                @elseif($dah->status == 5)
                                                @elseif($dah->status == 4)
                                                @elseif($dah->status == 3)
                                                @elseif($dah->status == 2)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $dah->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                                @if ($dah->request_cancel == 1)
                                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#requestpembatalan{{ $dah->id }}"><i class="fas fa-comment-dots" data-toggle="tooltip" data-placement="top" title="Request Pembatalan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="cek-pembayaran">
                                <form action="{{ route('acc.bukti.multi') }}" method="POST" class="needs-validation">
                                    @csrf
                                    <button id="tombolSetuju" type="submit" class="btn btn-sm btn-primary" disabled>
                                        <i class="fa fa-check"></i> Setujui masal
                                    </button>
                                    <br>
                                    <br>
                                    <table id="cek-pembayaran-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" class="select-all checkbox" id="cekAllPembayaran" name="select-all"></th>
                                                <th>Pembeli</th>
                                                <th>Nama Reseller</th>
                                                <th>Status</th>
                                                <th>Tanggal</th>
                                                <th>Komisi</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($cekpembayaran as $cekspem)
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="select-item checkbox" id="cekIdPembayaran" name="idorder[]" value="{{$cekspem->id}}" />
                                                </td>

                                                <td>
                                                    {{ !empty($cekspem) ? $cekspem->name : '' }} <br>
                                                    <small>
                                                        <strong>invoice: {{ !empty($cekspem) ? $cekspem->invoice : ''}} </strong>
                                                        <br>
                                                        Alamat: {{ !empty($cekspem) ? $cekspem->address : ''}}
                                                        <br>
                                                        Pengiriman: {{ !empty($cekspem) ? $cekspem->courrier : ''}}
                                                    </small>
                                                </td>
                                                <td>{{ $cekspem->reseller->user->name }}</td>
                                                <td>{!! $cekspem->status_label !!}</td>
                                                <td>{{ !empty($cekspem) ? $cekspem->created_at : ''}}</td>
                                                <td>
                                                    @if (!empty($cekspem->komisi))
                                                        {{'Rp. ' . number_format($cekspem->komisi->komisi)}}
                                                    @else
                                                        blm selesai
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="{{ route('order.pembayaran', $cekspem->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                    <a href="{{ route('pesanan.detail', $cekspem->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                        <i class="fas fa-receipt"></i>
                                                    </a>

                                                    @if($cekspem->status == 6)
                                                    @elseif($cekspem->status == 3)
                                                    @else
                                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $cekspem->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="upload-ulang">
                                <table id="upload-ulang-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($uploadulang as $item1)
                                        <tr>
                                            <td>
                                                {{ !empty($item1) ? $item1->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($item1) ? $item1->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($item1) ? $item1->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($item1) ? $item1->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $item1->reseller->user->name }}</td>
                                            <td>{!! $item1->status_label !!}</td>
                                            <td>{{ !empty($item1) ? $item1->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($item1->komisi))
                                                    {{'Rp. ' . number_format($item1->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $item1->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $item1->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>

                                                @if($item1->status == 6)
                                                @elseif($item1->status == 3)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $item1->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="belum-bayar">
                                <table id="belum-bayar-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($belumbayar as $belar)
                                        <tr>
                                            <td>
                                                {{ !empty($belar) ? $belar->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($belar) ? $belar->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($belar) ? $belar->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($belar) ? $belar->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $belar->reseller->user->name }}</td>
                                            <td>{!! $belar->status_label !!}</td>
                                            <td>{{ !empty($belar) ? $belar->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($belar->komisi))
                                                    {{'Rp. ' . number_format($belar->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $belar->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $belar->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>

                                                @if($belar->status == 6)
                                                @elseif($belar->status == 5)
                                                @elseif($belar->status == 4)
                                                @elseif($belar->status == 3)
                                                @elseif($belar->status == 2)
                                                @else
                                                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $belar->id }}"><i class="fas fa-window-close" data-toggle="tooltip" data-placement="top" title="Batalkan"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="batal">
                                <table id="batal-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($batal as $bats)
                                        <tr>
                                            <td>
                                                {{ !empty($bats) ? $bats->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($bats) ? $bats->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($bats) ? $bats->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($bats) ? $bats->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $bats->reseller->user->name }}</td>
                                            <td>{!! $bats->status_label !!}</td>
                                            <td>{{ !empty($bats) ? $bats->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($bats->komisi))
                                                    {{'Rp. ' . number_format($bats->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $bats->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $bats->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>

                                                @if($bats->status == 6)
                                                @elseif($bats->status == 3)
                                                @else
                                                {{-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $gag->id }}">Cancel</button> --}}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="dikemas">
                            <form action="{{route('register.awb.masal')}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-primary">register awb masal</button>
                                <table id="dikemas-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($dikemas as $kem)
                                        <tr>
                                            @if ($kem->courrier != 'pickup' && $kem->register_awb == 0)
                                            <td>
                                                <input type="checkbox" name="invoice[]" id="invoice[]" value="{{ $kem->invoice }}">
                                               
                                            </td>
                                            @elseif($kem->register_awb == 1)
                                                <td><i class="fas fa-check"></i></td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>
                                                {{ !empty($kem) ? $kem->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($kem) ? $kem->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($kem) ? $kem->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($kem) ? $kem->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $kem->reseller->user->name }}</td>
                                            <td>{!! $kem->status_label !!}</td>
                                            <td>{{ !empty($kem) ? $kem->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($kem->komisi))
                                                    {{'Rp. ' . number_format($kem->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $kem->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $kem->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                                @if($kem->status == 6)
                                                @elseif($kem->status == 3)
                                                @else
                                                {{-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $kem->id }}">Cancel</button> --}}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="dikirim">
                                <table id="dikirim-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($dikirim as $kir)
                                        <tr>
                                            
                                            <td>
                                                {{ !empty($kir) ? $kir->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($kir) ? $kir->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($kir) ? $kir->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($kir) ? $kir->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $kir->reseller->user->name }}</td>
                                            <td>{!! $kir->status_label !!}</td>
                                            <td>{{ !empty($kir) ? $kir->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($kir->komisi))
                                                    {{'Rp. ' . number_format($kir->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $kir->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $kir->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>

                                                @if($kir->status == 6)
                                                @elseif($kir->status == 3)
                                                @else
                                                {{-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $kir->id }}">Cancel</button> --}}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="selesai">
                                <table id="selesai-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($selesai as $sel)
                                        <tr>
                                            <td>
                                                {{ !empty($sel) ? $sel->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($sel) ? $sel->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($sel) ? $sel->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($sel) ? $sel->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $sel->reseller->user->name }}</td>
                                            <td>{!! $sel->status_label !!}</td>
                                            <td>{{ !empty($sel) ? $sel->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($sel->komisi))
                                                    {{'Rp. ' . number_format($sel->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $sel->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $sel->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="siapcetak">
                                <table id="siapcetak-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Pembeli</th>
                                            <th>Nama Reseller</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Komisi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($siapcetak as $sc)
                                        <tr>
                                            <td>
                                                {{ !empty($sc) ? $sc->name : '' }} <br>
                                                <small>
                                                    <strong>invoice: {{ !empty($sc) ? $sc->invoice : ''}} </strong>
                                                    <br>
                                                    Alamat: {{ !empty($sc) ? $sc->address : ''}}
                                                    <br>
                                                    Pengiriman: {{ !empty($sc) ? $sc->courrier : ''}}
                                                </small>
                                            </td>
                                            <td>{{ $sc->reseller->user->name }}</td>
                                            <td>{!! $sc->status_label !!}</td>
                                            <td>{{ !empty($sc) ? $sc->created_at : ''}}</td>
                                            <td>
                                                @if (!empty($sc->komisi))
                                                    {{'Rp. ' . number_format($sc->komisi->komisi)}}
                                                @else
                                                    blm selesai
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('order.pembayaran', $sc->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                                <a href="{{ route('pesanan.detail', $sc->invoice) }}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                                    <i class="fas fa-receipt"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- MODAL PEMBATALAN ORDER SEPIHAK !-->
<div class="modal fade" id="editpesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('cancel.order') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Batalkan Orderan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Pesan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Batalkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL REQUEST PEMBATALAN ORDER DARI BUYER !-->
@foreach($request_pembatalan as $data)
<div class="modal fade" id="requestpembatalan{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class=" modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request Pembatalan Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Invoice :</label>
                    <span class="form-control">{{$data->invoice}}</span>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Pesan : </label>
                    <span class="form-control">{{$data->message_request_cancel}}</span>
                </div>
                <form action="{{route('tolak.pembatalan')}}" method="POST">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Balasan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <button type="submit" class="btn btn-danger"> Tolak</button>
                </form>
                <form action="{{route('acc.pembatalan')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <button type="submit" class="btn btn-success"> Setujui</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach

@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>
@endif

<script>
    // disable tombol kalo belum di pilih checkboxnya tab cek pembayaran
    var c = document.getElementById('cekAllPembayaran');
    var c1 = document.getElementById('cekIdPembayaran');
    var tombol = document.getElementById('tombolSetuju');
    c.onchange = function() {
        if (this.checked) {
            tombol.disabled = false;
        } else {
            tombol.disabled = true;
        }
    }
    c1.onchange = function() {
        if (this.checked) {
            tombol.disabled = false;
        } else {
            tombol.disabled = true;
        }
    }

    // tab sudah-bayar
    var ca = document.getElementById('cekAllPrint');
    var ca1 = document.getElementById('cekIdPrint');
    var tombolPrint = document.getElementById('button-print');
    // when unchecked or checked, run the function
    ca.onchange = function() {
        if (this.checked) {
            tombolPrint.disabled = false;
        } else {
            tombolPrint.disabled = true;
        }
    }
    ca1.onchange = function() {
        if (this.checked) {
            tombolPrint.disabled = false;
        } else {
            tombolPrint.disabled = true;
        }
    }
</script>

<script>
    $(document).ready(function() {
        $('#semua-pesanan-dt, #sudah-bayar-dt, #belum-bayar-dt, #gagal-dt, #dikirim-dt, #selesai-dt, #dikemas-dt, #batal-dt, #cek-pembayaran-dt, #upload-ulang-dt','#siapcetak-dt').DataTable({
            scrollX: true
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });
    });

    function printSelected() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];

        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.open('{{route("multi.print.Resi")}}?datas=' + items, '_blank');
    };

    function printSelectedInvoice() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];

        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.open('{{route("multi.print.Invoice")}}?datas=' + items, '_blank');
    };

    function accSelected() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];
        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.location.href = "{{ route('acc.bukti.multi')}}";
    };

    $('#editpesan').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Batalkan Orderan')
        modal.find('#recipient-name').val(recipient)
    })
</script>

<script type="text/javascript">
    $(document).ready(function() {
        //button select all or cancel
        // $("#select-all").click(function() {
        //     var all = $("input.select-all")[0];
        //     all.checked = !all.checked
        //     var checked = all.checked;
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = checked;
        //     });
        // });

        // //button select invert
        // $("#select-invert").click(function() {
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = !item.checked;
        //     });
        //     checkSelected();
        // });

        // //button get selected info
        // $("#selected").click(function() {
        //     var items = [];
        //     $("input.select-item:checked:checked").each(function(index, item) {
        //         items[index] = item.value;
        //     });
        //     if (items.length < 1) {
        //         alert("no selected items!!!");
        //     } else {
        //         var values = items.join(',');
        //         console.log(values);
        //         var html = $("<div></div>");
        //         html.html("selected:" + values);
        //         html.appendTo("body");
        //     }
        // });

        // //column checkbox select all or cancel
        // $("input.select-all").click(function() {
        //     var checked = this.checked;
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = checked;
        //     });
        // });

        // //check selected items
        // $("input.select-item").click(function() {
        //     var checked = this.checked;
        //     console.log(checked);
        //     checkSelected();
        // });

        // //check is all selected
        // function checkSelected() {
        //     var all = $("input.select-all")[0];
        //     var total = $("input.select-item").length;
        //     var len = $("input.select-item:checked:checked").length;
        //     console.log("total:" + total);
        //     console.log("len:" + len);
        //     all.checked = len === total;
        // }
    });
</script>
<script>
    window.onload = function(){
        $('#filter').val('')
    }
    // get by pengirim
    function byPengirim(slug, data){
            $.ajax({
                url: "{{ route('pesanan.reseller') }}",
                type: 'GET',
                data:{
                    slug: slug
                },
                success: function(response){
                    $('.semua_pesanan_tr').remove();
                    $('.pagination').remove();
                    $('.dataTables_info').remove();
                    $('.dataTables_length').replaceWith(`<p class="dataTables_length">Show all by ${ slug ? slug : "all"}</p>`);
                    $('#btn_refresh').removeClass('d-none');
                    response.forEach(data => {
                        if (data.komisi === null) {
                            $('#semua_pesanan_b').append(`<tr class="semua_pesanan_tr"> 
                            <td>${data.name} 
                                <br> 
                                <small> Invoice: ${data.invoice}
                                <br>
                                Alamat: ${data.address}
                                <br>
                                Pengiriman: ${data.courrier}
                                </small>
                            </td> 
                            <td>${data.reseller.user.name}</td> 
                            <td>${data.status_label}</td> 
                            <td>${data.created_at}</td>
                            <td>
                                Pesanan Belum Selesai
                            </td>
                            <td>
                                <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                    <i class="fas fa-eye"></i>
                                </a> 
                                <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                    <i class="fas fa-receipt"></i>
                                </a>
                            </td> 
                        </tr> `);
                        } else {
                            $('#semua_pesanan_b').append(`<tr class="semua_pesanan_tr"> 
                                <td>${data.name} 
                                    <br> 
                                    <small> Invoice: ${data.invoice}
                                    <br>
                                    Alamat: ${data.address}
                                    <br>
                                    Pengiriman: ${data.courrier}
                                    </small>
                                </td> 
                                <td>${data.reseller.user.name}</td> 
                                <td>${data.status_label}</td> 
                                <td>${data.created_at}</td>
                                <td>
                                    ${data.komisi}
                                </td>
                                <td>
                                    <a href="detail/${data.invoice}"class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail">
                                        <i class="fas fa-eye"></i>
                                    </a> 
                                    <a href="order/detail/${data.invoice}" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Invoice">
                                        <i class="fas fa-receipt"></i>
                                    </a>
                                </td> 
                            </tr> `);
                        }
                    });
                    
                }
            })
        } 
</script>

<script>
    //RUPIAH
    const rupiah = (number) => {
            return new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR"
            }).format(number);
        }

        //function rupiah
        function rubah(angka) {
            var reverse = angka.toString().split('').reverse().join(''),
                ribuan = reverse.match(/\d{1,3}/g);
            ribuan = ribuan.join(',').split('').reverse().join('');
            return ribuan;
        }

        let select_id;

        $(".select-item").click(function() {
            var items = [];
            $("input.select-item:checked:checked").each(function(index, item) {
                // items = item.value;
                items[index] = {id:item.value, ongkir:item.getAttribute('data-ongkir'), service_code_:item.getAttribute('data-service_code_'), code_service:item.getAttribute('data-code_service'), kurir:item.getAttribute('data-kurir')}
            });
            select_id = items;
            console.log(select_id);
        });

    //creaete job jne masal
    let berat_totals;
    let kode_pos;
    let kelurahan;
    let type_jne;
    let type_service;
    @foreach($sudahbayar as $data)
            type_jne = "{{$data->courrier}}";
            type_service = "{{$data->group_service}}";
            berat_totals = "{{$data->berat_pengiriman}}";
            berat_totals = berat_totals / 1000;
            if(berat_totals < 1)
            {
                berat_totals = 1;
            }
            kode_pos =  "{{$data->postal_code}}";
            kelurahan =  "{{$data->urban}}";
            if(type_jne !== "pickup")
            {
                $.ajax({
                        url: "{{url('api/get_thru')}}/" + kode_pos + '/' + kelurahan,
                        type: 'get',
                        dataType: 'json',
                        success: function(response) {
                            console.log(response);
                            let thru = response.data.DEST;
                            let from = "CGK10000";
                            let weights = berat_totals;
                            $.ajax({
                                url: "{{url('api/api-tarif-jne')}}/" + from + '/' + thru + '/' + weights,
                                type: 'get',
                                dataType: 'json',
                                success: function(response) {

                                    var check_service_api = response.data;
                                    var object = check_service_api;
                                    var select = $("#pilih_ekspedisi_{{$data->id}}");
                                    select.empty();
                                    select.append('<option value="">Pilih Pengiriman</option>');
                                    
                                    for (const property in object) {
                                        let type_service = "{{$data->group_service}}";
                                        if(type_service == "regular")
                                        {
                                            if(object[property].service_code == "OKE19" || object[property].service_code == "REG19"){
                                                select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="jne" data-code_service="' + object[property].service_code + '">' + object[property].service_display + ' - ' + rubah(object[property].price) + '</option>');
                                            }else{
                                                continue;
                                            }
                                        }else if(type_service == "nextday"){
                                            if(object[property].service_code == "YES19"){
                                                select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="jne" data-code_service="' + object[property].service_code + '">' + object[property].service_display + ' - ' + rubah(object[property].price) + '</option>');
                                            }else{
                                                continue;
                                            }
                                        }else if(type_service == "sameday"){
                                            if(object[property].service_code == "JTR18"){
                                                select.append('<option value="JNE" data-servis="' + object[property].service_display + '" data-estimasi="" data-ongkir="' + object[property].price + '" data-kurir="jne" data-code_service="' + object[property].service_code + '">' + object[property].service_display + ' - ' + rubah(object[property].price) + '</option>');
                                            }else{
                                                continue;
                                            }
                                        }

                                    }
                                }
                            });

                        
                        }
                    });
                    }
                            $('#pilih_ekspedisi_{{$data->id}}').on('change', function(e) {
                                let service_code_ = ($(this).find(':selected').data('servis'));
                                let ongkir = ($(this).find(':selected').data('ongkir'));
                                let code_service = ($(this).find(':selected').data('code_service'));
                                let kurir = ($(this).find(':selected').data('kurir'));

                                $('.select-item-sudah-bayar_{{$data->id}}').attr('data-kurir' , kurir);
                                $('.select-item-sudah-bayar_{{$data->id}}').attr('data-code_service' , code_service);
                                $('.select-item-sudah-bayar_{{$data->id}}').attr('data-service_code_' , service_code_);
                                $('.select-item-sudah-bayar_{{$data->id}}').attr('data-ongkir' , ongkir);

                                console.log(kurir);
                                console.log(service_code_);
                            });
    @endforeach

</script>

<script>
    $("#jne_masal").click(function() {
                let data = {
                    datas: select_id,
                    _token: "{{ csrf_token() }}",
                }
    
                console.log(data);
                $.ajax({
                    type: "post",
                    url:"{{url('api/create_job_masal_post')}}",
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    dataType: "json",
                    success: function(response) {
                        console.log(response);
                    //    location.reload();
                    }
                });
    
               
            
    });
    </script>
@endsection