@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#{{ route('home.admin') }} class=" btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Reseller</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Pesanan Reseller {{ $user->referral_code }}</h4>
                    </div>
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">Order Manual</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">Order Midtrans</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div class="card-body table-responsive">
                                <table id="example1" class="table table-bordered table-striped table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>No Hp</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Detail Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($pesanan as $data)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}}</td>
                                            <td>{{ !empty($data) ? $data->phone : ''}}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('pesanan.detail.reseller', $data->invoice) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                            <div class="card-body table-responsive">
                                <table id="example2" class="table table-bordered table-striped table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>No Hp</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Detail Order</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($pesanan_midtrans as $data)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}}</td>
                                            <td>{{ !empty($data) ? $data->phone : ''}}</td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            <td>
                                                <a href="{{ route('pesanan.detail.reseller', $data->invoice) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<script>
    $(function() {
        $("#example1").DataTable({
            pageLength: 15
        });
    });

    $(function() {
        $("#example2").DataTable({
            pageLength: 15
        });
    });
</script>
@endsection