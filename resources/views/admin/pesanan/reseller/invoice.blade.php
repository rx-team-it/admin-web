@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('pesanan.reseller') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Pesanan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('list.order.new') }}">List Pesanan</a></div>
            <div class="breadcrumb-item">Invoice</div>
        </div>
    </div>
    <div class="section-body">
        <div class="invoice">
            <div class="invoice-print" id="printableArea">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="invoice-title">
                            <h2>Invoice</h2>
                            <h5>{{ !empty($order) ? $order->invoice : '' }}</h5>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <address>
                                    <strong>Pengirim:</strong><br>
                                    {{ !empty($address) ? $address->name: '' }}<br>
                                    {{ !empty($address) ? $address->phone : '' }}<br>
                                    {{ !empty($address) ? $address->address : '' }}<br>
                                    {{ !empty($address) ? $address->urban : '' }}<br>
                                    {{ !empty($address) ? $address->district : '' }}<br>
                                    {{ !empty($address) ? $address->city : '' }}<br>
                                    {{ !empty($address->countrys->province_name) ? $address->countrys->province_name : '' }},
                                    {{ !empty($address) ? $address->postal_code: '' }}<br>
                                </address>
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Dikirim Ke:</strong><br>
                                    {{ !empty($order) ? $order->name : '' }}<br>
                                    {{ !empty($order) ? $order->phone : '' }}<br>
                                    {{ !empty($order) ? $order->address : '' }}<br>
                                    {{ !empty($order) ? $order->state : '' }}<br>
                                    {{ !empty($order) ? $order->city : '' }}<br>
                                    {{ !empty($order) ? $order->province : '' }},
                                    {{ !empty($order) ? $order->postal_code : '' }}<br>
                                </address>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- <address>
                                    <strong>Cara Pembayaran:</strong><br>
                                </address> -->
                            </div>
                            <div class="col-md-6 text-md-right">
                                <address>
                                    <strong>Tanggal Pemesanan:</strong><br>
                                    {{ $order->created_at }}<br><br>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="section-title">Ringkasan Pesanan</div>
                        <p class="section-lead">Semua Produk di sini tidak dapat dihapus.</p>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-md">
                                <tr>
                                    <th data-width="40">No</th>
                                    <th>Produk</th>
                                    <th class="text-center">Harga</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                @php
                                $subtotal = 0;

                                @endphp

                                @foreach($order['detail'] as $keys => $rows)

                                @php $total = $rows->price * $rows->qty; @endphp
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ $rows->product->name }} <br>
                                        <span style="font-size:10px;">{{ !empty($rows->product_detail->varian_option->varian) ? $rows->product_detail->varian_option->varian->name_varian : '' }} {{ !empty($rows->product_detail->varian_option) ? $rows->product_detail->varian_option->name : '' }}</span>
                                        <br>
                                        <span style="font-size:10px;">
                                            {{ !empty($rows->sub_variasi) ? $rows->sub_variasi->name_varian : '' }}
                                            {{ !empty($rows->parent_option) ? $rows->parent_option->name : '' }}
                                        </span>
                                    </td>
                                    <td class="text-center">@currency($rows->price)</td>
                                    <td class="text-center">{{ $rows->qty }}</td>
                                    <td class="text-right">@currency( $total )</td>
                                </tr>
                                @php $subtotal = $subtotal + $total; @endphp

                                @endforeach
                            </table>
                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-12 text-right">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Subtotal</div>
                                    <div class="invoice-detail-value">@currency( $subtotal )</div>
                                </div>
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Ongkos Kirim</div>
                                    <div class="invoice-detail-value">@currency( !empty($order) ? $order->ongkos_kirim : '' )</div>
                                </div>
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Kode Unik</div>
                                    <div class="invoice-detail-value">@currency( !empty($order) ? $order->kode_unik : '' )</div>
                                </div>

                                @php $grandtotal = $subtotal + $order->ongkos_kirim + $order->kode_unik ; @endphp
                                <hr class="mt-2 mb-2">
                                <div class="invoice-detail-item">
                                    <div class="invoice-detail-name">Total</div>
                                    <div class="invoice-detail-value invoice-detail-value-lg">@currency( $grandtotal )</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <!-- {!! $order->status_label !!} -->
            <br>
            <br>
            <div class="text-md-right">
                <div class="float-lg-left mb-lg-0 mb-3">
                </div>
                <!-- <a class="btn btn-warning btn-icon icon-left" target="_blank" rel="noopener" type="button" onclick="printDiv('printableArea')"><i class="fas fa-print"></i> Print</a> -->
                <a href="{{ route('print.invoice', $order->invoice) }}" rel="noopener" target="_blank" class="btn btn-warning btn-icon icon-left"><i class="fas fa-print"></i> Print</a>
            </div>
        </div>
    </div>
</section>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        window.print();
    });
</script> -->
@endsection

@section('admin.js')
<script>
    function printDiv(divName) {
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
    }
</script>
@endsection