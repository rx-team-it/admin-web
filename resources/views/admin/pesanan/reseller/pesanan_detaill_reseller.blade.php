@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('pesanan.reseller') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Pesanan</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('pesanan.reseller') }}">List Pesanan</a></div>
            <div class="breadcrumb-item">Detail Pembayaran</div>
        </div>
    </div>
    <main class="main">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Detail pesanan {{ $order->invoice }}
                                </h4>
                                <div class="card-header-action">
                                    <a href="{{ route('pesanan.invoice.reseller', $order->invoice) }}" class="btn btn-sm btn-primary">
                                        <i class="fas fa-file-invoice-dollar"></i> Invoice
                                    </a>
                                    @if ($order->courrier != 'pickup' && $order->status == 1)
                                    <a href="{{route("print.Resi", $order->invoice)}}" target="_blank" class="btn btn-sm btn-warning">
                                        <i class="fas fa-print"></i> Cetak resi
                                    </a>
                                    @elseif ($order->courrier != 'pickup' && $order->status != 0 && $order->status != 2 && $order->status != 6 && $order->status != 7 && $order->status != 8)
                                    <a href="{{route("print.Resi", $order->invoice)}}" target="_blank" class="btn btn-sm btn-warning">
                                        <i class="fas fa-eye"></i> Lihat resi
                                    </a>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <!-- BLOCK UNTUK MENAMPILKAN DATA PELANGGAN -->
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pelanggan</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pelanggan</th>
                                                <td>{{ $order->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Telp</th>
                                                <td>{{ $order->phone }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>{{ $order->address }}</td>
                                            </tr>
                                            <tr>
                                                <th>Order Status</th>
                                                <td>{!! $order->status_label !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Pesan</th>
                                                <td>{{ $order->message }}</td>
                                            </tr>
                                            <!-- FORM INPUT RESI HANYA AKAN TAMPIL JIKA STATUS LEBIH BESAR 1 -->
                                            <tr>
                                                <th>Nomor Resi</th>
                                                <td>
                                                    {{ !empty($order) ? $order->tracking_number : '' }}
                                                </td>
                                                {{-- @if($order->status == 3 and $order->courrier == "jne")
                                                <th>Nomor Resi</th>
                                                <td>
                                                    <form action="{{ route('input.resi', $order->id) }}" method="post">
                                                @csrf
                                                <div class="input-group">
                                                    <input type="hidden" name="order_id" value="#">
                                                    <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" value="{{$order->tracking_number}}" required disabled>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-primary" type="submit">Kirim</button>
                                                    </div>
                                                </div>
                                                </form>
                                                </td>
                                                @elseif($order->status == 3 and $order->courrier == "tiki")
                                                <th>Nomor Resi</th>
                                                <td>
                                                    <form action="{{ route('input.resi', $order->id) }}" method="post">
                                                        @csrf
                                                        <div class="input-group">
                                                            <input type="hidden" name="order_id" value="#">
                                                            <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" value="{{$order->tracking_number}}" required disabled>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">Kirim</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                                @elseif($order->status == 3 and $order->courrier == "pos")
                                                <th>Nomor Resi</th>
                                                <td>
                                                    <form action="{{ route('input.resi', $order->id) }}" method="post">
                                                        @csrf
                                                        <div class="input-group">
                                                            <input type="hidden" name="order_id" value="#">
                                                            <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" value="{{$order->tracking_number}}" required disabled>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">Kirim</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                                @elseif($order->status == 3 and $order->courrier == "wahana")
                                                <th>Nomor Resi</th>
                                                <td>
                                                    <form action="{{ route('input.resi', $order->id) }}" method="post">
                                                        @csrf
                                                        <div class="input-group">
                                                            <input type="hidden" name="order_id" value="#">
                                                            <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" value="{{$order->tracking_number}}" required disabled>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">Kirim</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                                @elseif($order->status == 3 and $order->courrier == "ide")
                                                <th>Nomor Resi</th>
                                                <td>
                                                    <form action="{{ route('input.resi', $order->id) }}" method="post">
                                                        @csrf
                                                        <div class="input-group">
                                                            <input type="hidden" name="order_id" value="#">
                                                            <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" value="{{$order->tracking_number}}" required disabled>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">Kirim</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                                @elseif($order->status == 3 and $order->courrier == "pickup")

                                                @else
                                                <th>Nomor Resi</th>
                                                <td>
                                                    {{ !empty($order) ? $order->tracking_number : '' }}
                                                </td>
                                                @endif --}}
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pembayaran</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pengirim</th>
                                                <td>{{ $order->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Bank Tujuan</th>
                                                <td>{{ $order->bank }}</td>
                                            </tr>
                                            <tr>
                                                <th>Tanggal Transfer</th>
                                                <td>{{ $order->date }}</td>
                                            </tr>
                                            <tr>
                                                @if ($order->status == 1 && $order->bank != 'QRIS')
                                                <th>Bukti Pembayaran</th>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. $order->confirm_payment->bukti_transfer)}}" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat</a>
                                                    {{-- &nbsp;
                                                        <a href="{{ route('order.reupload', $order->invoice) }}" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i> Upload Ulang</a> --}}
                                                </td>
                                                @elseif ($order->status == 7 && $order->bank != 'QRIS')
                                                <th>Bukti Pembayaran</th>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. $order->confirm_payment->bukti_transfer)}}" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat</a>
                                                    &nbsp;
                                                    <a href="{{ route('order.reupload', $order->invoice) }}" class="btn btn-sm btn-danger"><i class="fas fa-undo"></i> Upload Ulang</a>
                                                    &nbsp;
                                                    <a href="{{ route('acc.bukti', $order->invoice) }}" class="btn btn-sm btn-success"><i class="fas fa-check"></i> Setujui</a>
                                                </td>
                                                @elseif ($order->status == 8 && $order->bank != 'QRIS')
                                                <th>Bukti Pembayaran</th>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. $order->confirm_payment->bukti_transfer)}}" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-eye"></i> Lihat</a>
                                                </td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    @if ($order->status == 0)
                                                    <p style="font-family:Monaco;">Belum Melakukan Pembayaran</p>
                                                    @else
                                                    <p style="font-family:Monaco;">Sudah Bayar</p>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Pengiriman</th>
                                                <td>
                                                    <p style="font-family:Monaco;">{{ $order->courrier }} - {{ $order->service }}</p>
                                                </td>
                                            </tr>
                                        </table>
                                        @if ($order->status == 0)
                                        <h5 class="text-center" style="font-family:Monaco;">Belum Konfirmasi Pembayaran</h5>
                                        @else
                                        <h5 class="text-center" style="font-family:Monaco;">Sudah Bayar</h5>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="font-family:Monaco;">Detail Produk</h4>
                                        <table class="table table-borderd table-hover">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>Produk</th>
                                                    <th>Varian</th>
                                                    <th>Quantity</th>
                                                    <th>Harga</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                            </thead>
                                            @foreach($order['product'] as $product)
                                            <tr>
                                                <td>{{ !empty($product) ? $product->name_product : 'Kosong' }}</td>
                                                <td>
                                                    <span style="font-size:13px;">{{ !empty($product->varian) ? $product->varian : '' }}
                                                    </span>
                                                    <br>
                                                    <span style="font-size:13px;">
                                                        {{ !empty($product->sub_varian) ? $product->sub_varian : '' }}
                                                    </span>
                                                </td>
                                                <td>{{ !empty($product) ? $product->qty : 'Kosong' }}</td>
                                                <td>{{ !empty ($product) ? 'Rp. '.number_format($product->price) : 'Kosong' }}</td>
                                                <td>{{ !empty($product) ? 'Rp. '.number_format ($product->price * $product->qty) : 'Kosong' }}</td>
                                            </tr>
                                            @endforeach
                                            <td>
                                                @if ($order->status == 0)
                                                <a class="btn btn-warning btn-sm" href="" data-toggle="modal" data-target="#editpesan" data-whatever="{{ $order->id }}">Batalkan</a>
                                                @elseif($order->status == 5)
                                                <p><button class="btn btn-sm btn-success">Pesanan Selesai</button></p>
                                                @elseif($order->status == 3 && $order->courrier != 'pickup')
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Pilih Aksi
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="{{ route('tombol.dikirim', $order->invoice) }}">Dikirim</a>
                                                    </div>
                                                </div>
                                                @elseif( $order->status == 8)

                                                @elseif($order->status == 1 && $order->courrier == 'pickup')
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        Pilih Aksi
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="{{ route('tombol.dikemas', $order->invoice) }}">Dikemas</a>
                                                    </div>
                                                </div>
                                                @elseif($order->status == 3 && $order->courrier == 'pickup')
                                                <div class="dropdown d-inline mr-2">
                                                    <a class="btn btn-primary btn-sm" href="{{ route('order.selesai', $order->invoice) }}">Pesanan Telah di ambil & Selesai</a>
                                                </div>
                                                @elseif($order->status == 1 && $order->courrier != 'pickup')

                                                @endif
                                            </td>
                                        </table>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- jika ada respon dari resinya roby --}}
        @if (!empty($response))
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Lacak Pengiriman ({{ !empty($order) ? $order->tracking_number : '' }}) {{ $order->invoice }}
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="activities">
                                        @foreach ($response as $res)
                                        <div class="activity">
                                            <div class="activity-icon bg-primary text-white">
                                            <i class="fas fa-truck"></i>
                                            </div>
                                            <div class="activity-detail">
                                                <p>{{$res['manifest_description']}} - Tgl.{{ date("H:i:s", strtotime($res['manifest_date']) )}} </p>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </main>
</section>

<div class="modal fade" id="editpesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('cancel.order') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Batalkan Pesanan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Pesan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Batalkan</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('admin.js')

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script>
    $('#editpesan').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Batalkan Orderan')
        modal.find('#recipient-name').val(recipient)
    })
</script>

@endsection