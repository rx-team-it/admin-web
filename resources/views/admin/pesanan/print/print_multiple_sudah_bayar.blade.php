<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>receipt</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
    <link rel="stylesheet" href="../paper.css">
    <style>
        @page {
            size: 120mm 150mm
        }

        /* output size */
        body.receipt .sheet {
            width: 120mm;
            height: 150mm
        }

        /* sheet size */
        @media print {
            body.receipt {
                width: 120mm
            }
        }

        /* fix for Chrome */


        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 0.5cm;
            }
        }

        table {
            border: 0.5px solid black;
            width: 95%;
            font-size: 14px;
        }

        table td,
        table tr,
        table tr td {
            border: 0.2px solid rgba(39, 39, 39, 0.452);
            padding: 5px;
        }

        .img_kurir {
            align-items: center;
        }

        .content_addr {
            font-size: 12px;
        }

        .content_expedisi,
        .content_prod {
            font-size: 11px;
        }

        .content_notes {
            font-size: 11px;
            border: solid 1px #000;
            border-radius: 10px;
            padding: 10px;
            margin-top: 10px;
            width: 90%;
        }
    </style>
    <script type="text/javascript">
        window.print();
    </script>
</head>
<body class="receipt">
    @foreach($items as $items)
    <section class="sheet padding-10mm">
        <table border="0">
            <tr class="">
                <td>
                    {{session()->get('sites')->domain}}
                </td>
                <td class=" content_expedisi">
                    Tgl Cetak :
                    {{ $items->updated_at }}
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="4" class="img_kurir">
                    <center><img width="200px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG('0217307', 'C128', 1, 33) }}" alt="no resi" />
                        <br>
                        <b>No. Resi : {{$items['tracking_number']}} </b>
                    </center> 
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center>
                        @if ($order->courrier == 'jne')
                        <img src="{{asset('assets/img/ekspedisi/jne.png')}}" width="80">
                        <br>
                        {{$order->service}}
                        @elseif ($order->courrier == 'anter_aja')
                        <img src="{{asset('assets/img/ekspedisi/anteraja.png')}}" width="120">
                        <br>
                        {{$order->service}}
                        @elseif ($order->courrier == 'jnt')
                            <img src="{{asset('assets/img/ekspedisi/jnt.png')}}" width="80">
                        <br>
                        {{$order->service}}
                        @elseif ($order->courrier == 'seryu')
                        <img src="{{asset('assets/img/ekspedisi/seryu.png')}}" width="120">
                        <br>
                        {{$order->service}}
                        @elseif ($order->courrier == 'grab')
                        <img src="{{asset('assets/img/ekspedisi/grab.png')}}" width="100">
                        <br>
                        {{$order->service}}
                        @endif
                    </center>
                </td>
                <td class="content_expedisi" style="width: 350px;" colspan="2">

                    <b>Berat</b> : {{ !empty($items) ? $items->berat_pengiriman : '' }}gr <br>
                    <b>Batas Kirim</b> : {{$exp}} <br>
                    <b>Ongkir</b> : @currency($items->ongkos_kirim) <br>
                    <b>No.Order</b> : {{ !empty($items) ? $items->invoice : '' }}<br><br>
                    <center>
                        <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG('123', 'C128', 1, 33) }}" alt="no resi" width="100" />
                        <br>
                    </center> 
                </td>
            </tr>
            <tr>
                <td class="content_addr" colspan="2">
                    <b>Penerima : </b><br>
                    {{ !empty($items['default_store']['name']) ? $items['default_store']['name'] : '' }}<br>
                    {{ !empty($items['default_store']['phone']) ? $items['default_store']['phone'] : '' }}<br>
                    {{ !empty($items['default_store']['address']) ? $items['default_store']['address'] : '' }}<br>
                    Kel. {{ !empty($items['default_store']['urban']) ? $items['default_store']['urban'] : '' }} <br>
                    Kec. {{ !empty($items['default_store']['district']) ? $items['default_store']['district'] : '' }} <br>
                    Kota {{ !empty($items['default_store']['city']) ? $items['default_store']['city'] : '' }} <br>
                   
                    {{ !empty($items['default_store']->get_province->province_name) ? $items['default_store']->get_province->province_name : '' }} <br>
                    Kodepos {{ !empty($items['default_store']['porstal_code']) ? $items['default_store']['postal_code'] : '' }} <br>
                    @if($items->dest_code !== null)
                    <center><h2 style="font-size: 20px;" >{{substr($items->dest_code,0,3)}}</h2></center>
                    @endif
                </td>
                <td class="content_addr" colspan="2">
                    <b>Pengirim : </b><br>
                    {{ !empty($items->name) ? $items->name: '' }} <br>
                    {!! !empty($items->contact_phone) ? $items->contact_phone : '' !!} <br>
                    {{ !empty($items->address) ? $items->address : '' }} <br>
                    Kel. {{ !empty($items->urban) ? $items->urban : '' }} <br>
                    Kec. {{ !empty($items->city) ? $items->city : '' }} <br>
                    Kota {{ !empty($items->state) ? $items->state : '' }} <br>
                    {{ !empty($items->province) ? $items->province : '' }} <br>
                    Kodepos {{ !empty($items->postal_code) ? $items->postal_code : '' }}<br>
                    @if($items->dest_code !== null)
                    <center><h2 style="font-size: 20px; color:white;" >{{$items->dest_code}}</h2></center>
                    @endif
                </td>
            </tr>
            <tr class="content_prod">
                <td colspan="4">
                    <b>Pesanan</b>
                </td>
            </tr>
            <tr class="content_prod">
                <td>Produk</td>
                <td>Varian</td>
                <td>SKU</td>
                <td>Qty</td>
            </tr>
            @foreach ($items['detail'] as $keys => $rows)
            <tr class="content_prod">
                <td>{{ Str::limit(!empty ($rows->product->name)? $rows->product->name:'',30) }}</td>
                <td><span style="font-size:10px;">{{ !empty($rows->product_detail->varian_option->varian) ?$rows->product_detail->varian_option->varian->name_varian : '' }}
                        {{ !empty($rows->product_detail->varian_option) ? $rows->product_detail->varian_option->name : '' }}</span>
                    <br>
                    <span style="font-size:10px;">
                        {{ !empty($rows->sub_variasi) ? $rows->sub_variasi->name_varian : '' }}
                        {{ !empty($rows->parent_option) ? $rows->parent_option->name : '' }}
                    </span>
                </td>
                <td>{{ !empty($rows->product->sku_induk)? $rows->product->sku_induk:'' }}</td>
                <td>{{ !empty($rows->qty)? $rows->qty:'' }}</td>
            </tr>
            @endforeach
        </table>
        @if($items->sorting_code !== null)
        <center><div class="content_notes" style="background: black; color:white; border-radius:5px; padding:2px; font-size:20px; margin-left:-15px;" >{{$items->sorting_code}}</div></center>
        @endif
        <div class="content_notes">
            Penjual tidak perlu bayar apapun ke kurir, Sudah dibayarkan otomatis
        </div>
        <br>
        <strong>{{ !empty($items) ? $items->invoice : '' }}</strong>
    </section>
    @endforeach
</body>

</html>