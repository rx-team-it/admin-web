<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>receipt</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
    <link rel="stylesheet" href="../paper.css">
    <style>
        @page {
            size: 120mm 150mm
        }

        /* output size */
        body.receipt .sheet {
            width: 120mm;
            height: 150mm
        }

        /* sheet size */
        @media print {
            body.receipt {
                width: 120mm
            }
        }

        /* fix for Chrome */


        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 0.5cm;
            }
        }

        table {
            border: 0.5px solid black;
            width: 95%;
            font-size: 14px;
        }

        table td,
        table tr,
        table tr td {
            border: 0.2px solid rgba(39, 39, 39, 0.452);
            padding: 5px;
        }

        .img_kurir {
            align-items: center;
        }

        .content_addr {
            font-size: 12px;
        }

        .content_expedisi,
        .content_prod {
            font-size: 11px;
        }

        .content_notes {
            font-size: 11px;
            border: solid 1px #000;
            border-radius: 10px;
            padding: 10px;
            margin-top: 10px;
            width: 90%;
        }
    </style>
    {{-- <script type="text/javascript">
        window.print();
    </script> --}}
</head>
<body class="receipt">
    <section class="sheet padding-10mm">
        <table border="0">
            <tr class="">
                <td>
                    abbabill-dev.site
                </td>
                <td class=" content_expedisi">
                    Tgl Cetak :
                    02-feb-2022
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="1" class="img_kurir">
                        <img src="{{asset('assets/img/ekspedisi/idx.jpg')}}" width="100">
                        <br>
                        <b>21 Juli 2021</b>
                </td>
                <td colspan="3" class="img_kurir">
                    <center>
                        <img src="{{asset('assets/img/ekspedisi/barcode.jpg')}}" alt="no resi" width="200" />
                    </center>
                </td>
            </tr>
            <tr>
                <td rowspan="4">
                    Tujuan: <b>CIHAMPELAS</b>
                    <br>
                    <br>
                    Penerima: <br>
                    <b>Kevin</b> <br>
                    <b>082131xxxxx</b>
                    <br>
                    jln. cihampelas
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center>
                        <b>PERIODIK</b>
                    </center>
                </td>
                <td colspan="2">
                    <center>
                        <b>IDlite</b>
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <center>
                        <b>COD</b>
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <center>

                        <b>Total: &nbsp; &nbsp; 120.000</b>
                    </center>
                </td>
            </tr>
            <tr>
                <td rowspan="4">
                    Pengirim: <br>
                    <b>Kenny</b> <br>
                    <b>081200000000</b> <br>
                    <b>JAKARTA UTARA</b>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <center>

                        <b>Waktu Antar: Normal</b>
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <center>

                        <b>Percobaan Pengantaran</b>
                    </center>
                </td>
            </tr>
            <tr>
                <td>
                    1 <center></center><br>
                </td>
                <td>
                    2 <center></center><br>
                </td>
                <td>
                    3 <center></center><br>
                </td>
            </tr>
            <tr>
                <td>
                    <b>botol kaca 1L X 12</b>
                </td>
                <td>
                    <b>QTY: 1</b>
                </td>
                <td colspan="2">12 KG</td>
            </tr>
            <tr style="background-color:black;">
                <td colspan="4">
                    <center>
                        <b style="color:white;">
                            BDO-BDO46-BDOB02
                        </b>
                    </center>
                </td>
            </tr>
        </table>
        <br>
    </section>
</body>

</html>