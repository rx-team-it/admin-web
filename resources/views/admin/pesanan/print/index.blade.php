@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Laporan Order</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Laporan Order</div>
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12 col-sm-12">

                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Semua Pesanan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="true">Sudah Bayar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab3" data-toggle="tab" href="#settings1" role="tab" aria-selected="true">Belum Bayar</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pemesanan_gagal-tab2" data-toggle="tab" href="#pemesanan_gagal" role="tab" aria-selected="true">Pemesanan Gagal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="dikemas-tab2" data-toggle="tab" href="#dikemas" role="tab" aria-selected="true">Dikemas</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="dikirim-tab2" data-toggle="tab" href="#dikirim" role="tab" aria-selected="true">Dikirim</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="order_selesai-tab2" data-toggle="tab" href="#order_selesai" role="tab" aria-selected="true">Selesai</a>
                            </li>
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="table_produk" class="table table-striped table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>

                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="sudah_bayar" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="settings1" role="tabpanel" aria-labelledby="profile-tab3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="belum_bayar" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="pemesanan_gagal" role="tabpanel" aria-labelledby="pemesanan_gagal-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="order_pemesanan_gagal" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="dikemas" role="tabpanel" aria-labelledby="dikemas-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="order_dikemas" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="dikirim" role="tabpanel" aria-labelledby="dikirim-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="order_dikirim" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade active" id="order_selesai" role="tabpanel" aria-labelledby="order_selesai-tab2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="selesai" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Invoice</th>
                                                        <th>Alamat</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        function load_data(from_date = '', to_date = '') {
            $('#table_produk').DataTable({
                pageLength: 10,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('indexprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    }
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });


    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#sudah_bayar').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#sudah_bayar').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#sudah_bayar').DataTable({
                pageLength: 30,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('sudahbayarprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });

    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#belum_bayar').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#belum_bayar').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#belum_bayar').DataTable({
                pageLength: 30,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('belumbayarprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });


    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#selesai').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#selesai').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#selesai').DataTable({
                pageLength: 30,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('selesaiprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });


    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#order_pemesanan_gagal').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#order_pemesanan_gagal').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#order_pemesanan_gagal').DataTable({
                pageLength: 30,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('pemesanangagalprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });


    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#order_dikemas').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#order_dikemas').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#order_dikemas').DataTable({
                pageLength: 30,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('dikemasprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });


    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#order_dikirim').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#order_dikirim').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#order_dikirim').DataTable({
                pageLength: 30,
                processing: true,
                serverSide: true,
                dom: '<"html5buttons">Bflrtip',
                buttons: [{
                    extend: 'csv'
                }, {
                    extend: 'pdf',
                    title: 'List Order'
                }, {
                    extend: 'excel',
                    title: 'List Order'
                }, {
                    extend: 'print',
                    title: 'List Order'
                }, ],
                ajax: {
                    url: "{{ route('dikirimprint') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
                columns: [{
                        "data": 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    }, {
                        data: 'name',
                        name: 'name',
                    },
                    {
                        data: 'invoice',
                        name: 'invoice',
                    },
                    {
                        data: 'address',
                        name: 'address',
                    },
                    {
                        data: 'created_at',
                        name: 'created_at',
                    },
                    {
                        data: 'status_label',
                        name: 'status_label'
                    },
                ],
                order: [
                    [0, 'asc']
                ]
            });
        }
    });
</script>
@endsection