<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Print Resi</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
    <link rel="stylesheet" href="../paper.css">
    <style>
        @page {
            size: 100mm 150mm
        }

        /* output size */
        body.receipt .sheet {
            width: 100mm;
            height: 150mm
        }

        /* sheet size */
        @media print {
            body.receipt {
                width: 100mm
            }
        }

        /* fix for Chrome */


        @media print {
            @page {
                margin: 0;
            }

            body {
                margin: 0.5cm;
            }
        }

        table {
            border: 0.5px solid black;
            width: 95%;
            font-size: 14px;
        }

        table td,
        table tr,
        table tr td {
            border: 0.2px solid rgba(39, 39, 39, 0.452);
            padding: 5px;
        }

        .img_kurir {
            align-items: center;
        }

        .content_addr {
            font-size: 12px;
        }

        .content_expedisi,
        .content_prod {
            font-size: 11px;
        }

        .content_notes {
            font-size: 11px;
            border: solid 1px #000;
            border-radius: 10px;
            padding: 10px;
            margin-top: 10px;
            width: 90%;
        }
    </style>
    <script type="text/javascript">
        window.print();
    </script>
</head>

<body class="receipt">
    <section class="sheet padding-10mm">
        <table border="0">
            <tr class="">
                <td>
                    {{session()->get('sites')->domain}}
                </td>
                <td class=" content_expedisi">
                    Tgl Cetak :
                    {{ $order->updated_at }}
                </td>
            </tr>
        </table>


        <table>
            <tr>
                <td colspan="4" class="img_kurir">
                    <center><img width="200" src="data:image/png;base64,{{DNS1D::getBarcodePNG(!empty($order->tracking_number) ? $order->tracking_number : 'kosong', 'C128')}}" alt="barcode" />
                    {{-- <center><img src="data:image/png;base64,{{ DNS1D::getBarcodePNG(!empty($order->tracking_number) ? $order->tracking_number : '', 'C128', 1, 33) }}" alt="no resi" width="300" /> --}}
                        <br>
                        <b>No. Resi : {{$order->tracking_number}} </b>
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center>
                        @if ($order->admin_courrier == 'JNE')
                        <img src="{{asset('assets/img/ekspedisi/jne.png')}}" width="80">
                        <br>
                        {{$order->group_service}}
                        @elseif ($order->admin_courrier == 'anter_aja')
                        <img src="{{asset('assets/img/ekspedisi/anteraja.png')}}" width="120">
                        <br>
                        {{$order->group_service}}
                        @elseif ($order->admin_courrier == 'jnt')
                            <img src="{{asset('assets/img/ekspedisi/jnt.png')}}" width="80">
                        <br>
                        {{$order->group_service}}
                        @elseif ($order->admin_courrier == 'seryu')
                        <img src="{{asset('assets/img/ekspedisi/seryu.png')}}" width="120">
                        <br>
                        {{$order->group_service}}
                        @elseif ($order->admin_courrier == 'grab')
                        <img src="{{asset('assets/img/ekspedisi/grab.png')}}" width="100">
                        <br>
                        {{$order->group_service}}
                        @endif
                    </center>
                </td>
                <td class="content_expedisi" style="width: 350px;" colspan="2">

                    <b>Berat</b> : {{ !empty($order) ? $order->berat_pengiriman : '' }}gr <br>
                    <b>Batas Kirim</b> : {{$exp}} <br>
                    <b>Ongkir</b> : @currency($order->ongkos_kirim) <br>
                    <b>No.Order</b> : {{ !empty($order) ? $order->invoice : '' }}<br><br>
                    <center>
                        <img width="100" src="data:image/png;base64,{{DNS1D::getBarcodePNG(!empty($order->tracking_number) ? $order->tracking_number : 'gagal', 'C128')}}" alt="barcode" />
                        {{-- <img src="data:image/png;base64,{{ DNS1D::getBarcodePNG(!empty($order->tracking_number) ? $order->tracking_number : '', 'C128', 1, 33) }}" alt="no resi" width="150" /> --}}
                        <br>
                    </center>
                </td>
            </tr>
            <tr>
                <td class="content_addr" colspan="2">
                    <b>Penerima : </b><br>
                    {{ !empty($order) ? $order->name : '' }}<br>
                    {{ !empty($order) ? $order->phone : '' }}<br>
                    {{ !empty($order) ? $order->address : '' }}<br>
                    Kel. {{ !empty($order) ? $order->urban : '' }} <br>
                    Kec. {{ !empty($order) ? $order->city : '' }} <br>
                    {{ !empty($order) ? $order->state : '' }} <br>
                    {{ !empty($order) ? $order->province : '' }} <br>
                    Kodepos {{ !empty($order) ? $order->postal_code : '' }} <br>
                    @if($order->dest_code !== null)
                    <center><h2 style="font-size: 20px;" >{{substr($order->dest_code,0,3)}}</h2></center>
                    @endif
                </td>
                <td class="content_addr" colspan="2">
                    <b>Pengirim : </b><br>
                    {{ !empty($address->name) ? $address->name_site: '' }} <br>
                    {!! !empty($store) ? $store->contact_phone : '' !!} <br>
                    {{ !empty($address->address) ? $address->address : '' }} <br>
                    Kel. {{ !empty($address->urban) ? $address->urban : '' }} <br>
                    Kec. {{ !empty($address->district) ? $address->district : '' }} <br>
                    {{ !empty($address->city) ? $address->city : '' }} <br>
                    {{ !empty($address->countrys->province_name) ? $address->countrys->province_name : '' }} <br>
                    Kodepos {{ !empty($address->postal_code) ? $address->postal_code : '' }}<br>
                    @if($order->dest_code !== null)
                    <center><h2 style="font-size: 20px; color:white;" >{{$order->dest_code}}</h2></center>
                    @endif
                </td>
                </td>
            </tr>
            <tr class="content_prod">
                <td colspan="4">
                    <b>Pesanan</b>
                </td>
            </tr>
            <tr class="content_prod">
                <td>Produk</td>
                <td>Varian</td>
                <td>SKU</td>
                <td>Qty</td>
            </tr>
            @foreach ($order['detail'] as $rows)
            <tr class="content_prod">
                <td>{{ Str::limit($rows->product->name,30) }}</td>
                <td><span style="font-size:10px;">{{ !empty($rows->product_detail->varian_option->varian) ? $rows->product_detail->varian_option->varian->name_varian : '' }}
                        {{ !empty($rows->product_detail->varian_option) ? $rows->product_detail->varian_option->name : '' }}</span>
                    <br>
                    <span style="font-size:10px;">
                        {{ !empty($rows->sub_variasi) ? $rows->sub_variasi->name_varian : '' }}
                        {{ !empty($rows->parent_option) ? $rows->parent_option->name : '' }}
                    </span>
                </td>
                <td>{{ $rows->product->sku_induk }}</td>
                <td>{{ $rows->qty }}</td>
            </tr>
            @endforeach
        </table>
        @if($order->sorting_code !== null)
        <center><div class="content_notes" style="background: black; color:white; border-radius:5px; padding:2px; font-size:20px; margin-left:-15px;" >{{$order->sorting_code}}</div></center>
        @endif
        <div class="content_notes">
            Penjual tidak perlu bayar apapun ke kurir, Sudah dibayarkan otomatis
        </div>
        <strong>{{ !empty($order) ? $order->invoice : '' }}</strong>
    </section>
</body>

</html>