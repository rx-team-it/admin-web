@extends('admin.layouts.admin.index')

@section('admin.css')
<style>
    .tab-content {
        padding: 10px;
        border-left: 1px solid #DDD;
        border-bottom: 1px solid #DDD;
        border-right: 1px solid #DDD;
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')

<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#{{ route('home.admin') }}" class=" btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Order</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Order Vendor</h4>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="row input-daterange ml-2 mt-4 mb-2">
                            <div class="col-md-4">
                                <input type="text" name="from_date" id="from_date" class="form-control form-control-sm" placeholder="From Date" readonly />
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="to_date" id="to_date" class="form-control form-control-sm" placeholder="To Date" readonly />
                            </div>
                            <div class="col-md-4">
                                <button type="button" name="filter" id="filter" class="btn btn-primary btn-sm">Filter</button>
                                <button type="button" name="refresh" id="refresh" class="btn btn-secondary btn-sm">Refresh</button>
                            </div>
                        </div>
                    </div> -->
                    <div class="card-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#semua-pesanan" aria-controls="semua-pesanan" role="tab" aria-selected="true">Semua Pesanan <span class="badge badge-transparent" style="color: red;">{{$hitung_order}}</span></a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#belum-bayar" aria-controls="belum-bayar" role="tab">Belum Bayar <span class="badge badge-transparent" style="color: red;">{{$hitung_belum_bayar}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#cek-pembayaran" aria-controls="cek-pembayaran" role="tab">Cek Pembayaran <span class="badge badge-transparent" style="color: red;">{{$hitung_cek_pembayaran}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#upload-ulang" aria-controls="upload-ulang" role="tab">Upload Ulang <span class="badge badge-transparent" style="color: red;">{{$hitung_upload_ulang}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#sudah-bayar" aria-controls="sudah-bayar" role="tab">Sudah Bayar <span class="badge badge-transparent" style="color: red;">{{$hitung_udah_bayar}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#dikemas" aria-controls="dikemas" role="tab">Dikemas <span class="badge badge-transparent" style="color: red;">{{$hitung_dikemas}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#dikirim" aria-controls="dikirim" role="tab">Dikirim <span class="badge badge-transparent" style="color: red;">{{$hitung_dikirim}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#selesai" aria-controls="selesai" role="tab">Selesai <span class="badge badge-transparent" style="color: red;">{{$hitung_selesai}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#gagal" aria-controls="gagal" role="tab">Gagal <span class="badge badge-transparent" style="color: red;">{{$hitung_gagal}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#batal" aria-controls="batal" role="tab">Batal <span class="badge badge-transparent" style="color: red;">{{$hitung_batal}}</span>
                                </a>
                            </li>
                            <li>
                                <a class="nav-link" data-toggle="tab" href="#ready_to_print" aria-controls="batal" role="tab">Siap dicetak <span class="badge badge-transparent" style="color: red;">{{$hitung_ready}}</span>
                                </a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in show active" id="semua-pesanan">
                                <div class="row mb-3">
                                    <div class="col-4">
                                        <!-- Get opsi Pengirim -->
                                        <div class="form-group">
                                            <label for="filter">Filter Pengiriman</label>
                                            <select class="form-control form-control-sm" id="filter" data='x' onchange="byPengirim(this.value, this.data)" autocomplete="off">
                                                <option value="" selected>--Semua--</option>
                                                <option value="anter_aja">Anter Aja</option>
                                                <option value="pickup">Pickup</option>
                                                <option value="jne">JNE</option>
                                                <option value="jnt">JNT</option>
                                            </select>
                                        </div>
                                        <a href="" id="btn_refresh" class="btn btn-success d-none"> <i class="fas fa-rotate-right"></i> Refresh</a>
                                    </div>
                                </div>
                               {{--  <button onclick="printSelectedInvoice()" id="button-print" class="btn btn-primary mb-4"> <i class="fas fa-print"></i><span> Print Invoice</span></button> --}}
                                <table id="semua-pesanan-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            {{-- <th><input type="checkbox" class="select-all checkbox" name="select-all"></th> --}}
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody id="semua_pesanan_b">
                                        @foreach($order as $data)
                                        <tr class="semua_pesanan_tr">
                                            {{-- <td><input type="checkbox" class="select-item checkbox" name="idorder[]" value="{{$data->id}}" /></td> --}}
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{!empty($data) ? $data->invoice : ''}} </a>
                                                <br>
                                                <small> Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="belum-bayar">
                                <table id="belum-bayar-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($belumbayar as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="cek-pembayaran">
                                <form action="{{ route('acc.bukti.multi') }}" method="POST" class="needs-validation">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        <i class="fa fa-check"></i> Setujui masal
                                    </button>
                                    <br>
                                    <br>
                                    <table id="cek-pembayaran-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox" class="select-all checkbox" name="select-all"></th>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Invoice</th>
                                                <th>Alamat</th>
                                                <th>Status</th>
                                                <th>Tanggal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cekpembayaran as $data)
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="select-item checkbox" name="idorder[]" value="{{$data->id}}" />
                                                </td>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{ !empty($data) ? $data->name : '' }}</td>
                                                <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                    <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                                </td>
                                                <td>{{ !empty($data) ? $data->address : ''}}</td>
                                                <td>{!! $data->status_label !!}</td>
                                                <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="upload-ulang">
                                <table id="upload-ulang-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($uploadulang as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="sudah-bayar">
                                <form action="{{route('multi.print.Resi.sudah.bayar')}}"  method="POST" >
                                    @csrf
                           <button id="button-print" class="btn btn-primary mb-4"> <i class="fas fa-print"></i><span> Request Resi</span></button> 
                                <!-- <button onclick="printSelected()" id="button-print" class="btn btn-primary mb-4"> <i class="fas fa-print"></i><span> Print Resi</span></button> -->
                                <table id="sudah-bayar-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" class="select-all-sudah-bayar checkbox" name="select-all"></th>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sudahbayar as $data)
                                        <tr>
                                            <td>
                                                @if ($data->courrier == 'pickup')

                                                @else
                                                <input type="checkbox" id="select-item-sudah-bayar" class="select-item-sudah-bayar checkbox" name="idorder[]" value="{{$data->id}}" />
                                                @endif
                                            </td>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}}
                                                <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="gagal">
                                <table id="gagal-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($gagal as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="batal">
                                <table id="batal-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($batal as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="dikemas">
                                <table id="dikemas-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dikemas as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="dikirim">
                                <table id="dikirim-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($dikirim as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="selesai">
                                <table id="selesai-dt" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($selesai as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in show" id="ready_to_print">
                                <table id="ready" class="table table-striped table-bordered table-condensed" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Invoice</th>
                                            <th>Alamat</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($ready_to_print as $data)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{ !empty($data) ? $data->name : '' }}</td>
                                            <td>{{ !empty($data) ? $data->invoice : ''}} <br>
                                                <small>Pengiriman: {{ !empty($data) ? $data->courrier : ''}}</small>
                                            </td>
                                            <td>{{ !empty($data) ? $data->address : ''}}</td>
                                            <td>{!! $data->status_label !!}</td>
                                            <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- MODAL BATALKAN ORDER !-->
<div class="modal fade" id="editpesan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('cancel.order') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Batalkan Orderan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Pesan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Batalkan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL REQUEST PEMBATALAN ORDER DARI BUYER !-->
@foreach($request_pembatalan as $data)
<div class="modal fade" id="requestpembatalan{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class=" modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Request Pembatalan Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">Invoice :</label>
                    <span class="form-control">{{$data->invoice}}</span>
                </div>
                <div class="form-group">
                    <label class="col-form-label">Pesan : </label>
                    <span class="form-control">{{$data->message_request_cancel}}</span>
                </div>
                <form action="{{route('tolak.pembatalan')}}" method="POST">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Balasan : </label>
                        <textarea class="form-control" name="reply_message" id="message-text"></textarea>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <button type="submit" class="btn btn-danger"> Tolak</button>
                </form>
                <form action="{{route('acc.pembatalan')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <button type="submit" class="btn btn-success"> Setujui</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
<script>
    // tampil berdasarkan jenis pengiriman
    function byPengirim(slug, data){
            $.ajax({
                url: "{{ route('indexprint') }}",
                type: 'GET',
                data:{
                    slug: slug
                },
                success: function(response){
                    $('.semua_pesanan_tr').remove();
                    $('.pagination').remove();
                    $('.dataTables_info').remove();
                    $('.dataTables_length').replaceWith(`<p class="dataTables_length">Show all by ${ slug ? slug : "all"}</p>`);
                    $('#btn_refresh').removeClass('d-none');
                    let i = 1;
                    response.forEach(data => {
                        $('#semua_pesanan_b').append(`<tr class="semua_pesanan_tr"> <td> <input type="checkbox" class="select-item checkbox" name="idorder[]" value="${data.id}" </td> <td>${i++}</td> <td>${data.name}</td> <td>${data.invoice} <br> <small>Pengirim: ${data.courrier}</small> </td> <td>${data.address}</td> <td>${data.status_label}</td> <td>${data.created_at}</td> </tr>`)
                    });
                    
                }
            })
    } 
</script>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>

@endif

<script>
    $(document).ready(function() {
        $('#semua-pesanan-dt, #sudah-bayar-dt, #gagal-dt, #batal-dt, #dikirim-dt, #selesai-dt, #belum-bayar-dt, #dikemas-dt, #cek-pembayaran-dt, #upload-ulang-dt, #ready').DataTable({
            scrollX: true,
            dom: 'Bfrtip',
            buttons: [
                'pdfHtml5',
                'excelHtml5',
                'csvHtml5'
            ]
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });
    });

    function printSelected() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];

        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });

        window.open('{{route("multi.print.Resi")}}?datas=' + items, '_blank');
    };

    function printSelectedInvoice() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];

        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.open('{{route("multi.print.Invoice")}}?datas=' + items, '_blank');
    };

    function accSelected() {
        var len = $("input.select-item:checked:checked").length;
        let all_id = [];
        var items = [];
        $("input.select-item:checked:checked").each(function(index, item) {
            items[index] = item.value;
        });
        window.location.href = "{{ route('acc.bukti.multi')}}";
    };

    $('#editpesan').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Batalkan Orderan')
        modal.find('#recipient-name').val(recipient)
    })
</script>

<script type="text/javascript">
    $(document).ready(function() {

        $(".select-all-sudah-bayar").click(function() {
            $("#sudah-bayar-dt input[type='checkbox']").prop('checked', this.checked);
            console.log('aalll');
        });

        $(".select-item-sudah-bayar").click(function() {
            var items = [];
            $("input.select-item-sudah-bayar:checked:checked").each(function(index, item) {
                items[index] = item.value;
            });
            console.log(items);
        });


        //button select all or cancel
        // $("#select-all").click(function() {
        //     var all = $("input.select-all")[0];
        //     all.checked = !all.checked
        //     var checked = all.checked;
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = checked;
        //     });
        // });

        //button select invert
        // $("#select-invert").click(function() {
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = !item.checked;
        //     });
        //     checkSelected();
        // });

        //button get selected info
        // $("#selected").click(function() {
        //     var items = [];
        //     $("input.select-item:checked:checked").each(function(index, item) {
        //         items[index] = item.value;
        //     });
        //     if (items.length < 1) {
        //         alert("no selected items!!!");
        //     } else {
        //         var values = items.join(',');
        //         var html = $("<div></div>");
        //         html.html("selected:" + values);
        //         html.appendTo("body");
        //     }
        // });

        //column checkbox select all or cancel
        // $("input.select-all").click(function() {
        //     var checked = this.checked;
        //     $("input.select-item").each(function(index, item) {
        //         item.checked = checked;
        //     });
        // });

        //check selected items
        // $("input.select-item").click(function() {
        //     var checked = this.checked;
        //     console.log(checked);
        //     checkSelected();
        // });

        //check is all selected
        // function Selected() {
        //     var all = $("input.select-all")[0];
        //     var total = $("input.select-item").length;
        //     var len = $("input.select-item:checked:checked").length;
        //     console.log("total:" + total);
        //     console.log("len:" + len);
        //     all.checked = len === total;
        // }
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        load_data();
        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('#filter').click(function() {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if (from_date != '' && to_date != '') {
                $('#saldo').DataTable().destroy();
                load_data(from_date, to_date);
            } else {
                alert('Both Date is required');
            }
        });

        $('#refresh').click(function() {
            $('#from_date').val('');
            $('#to_date').val('');
            $('#saldo').DataTable().destroy();
            load_data();
        });

        function load_data(from_date = '', to_date = '') {
            $('#saldo').DataTable({
                ajax: {
                    url: "{{ route('list.order.new') }}",
                    type: 'GET',
                    data: {
                        from_date: from_date,
                        to_date: to_date
                    }
                },
            });
        }
    });
</script>
@endsection