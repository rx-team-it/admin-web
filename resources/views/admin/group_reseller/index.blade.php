@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Grup Reseller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Grup Reseller</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-check-circle"></i>&nbsp; Nama grup tidak boleh sama</strong>
            </div>
            @endif
        </div>
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>List Grup Reseller</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#add">
                            <i class="fa fa-plus-circle"></i> Tambah Grup & Komisi
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_reseller" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>Nama Grup</th>
                                <th>Komisi</th>
                                <th>Satuan</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($r as $items)
                                <tr>
                                    <td>{{$items->nm_grup}}</td>
                                    @if ($items->satuan == 'persentase')
                                        <td>{{ $items->nominal }}%</td>
                                    @else
                                        <td>@currency($items->nominal)</td>
                                    @endif
                                    <td>{{ $items->satuan }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('show.reseller',$items->id) }}" class="edit btn btn-primary btn-sm edit-post" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-eye"></i></a>
                                        <a href="" class=" edit btn btn-sm btn-warning" data-toggle="modal" data-target="#edit" data-id="{{$items->id}}" data-nm_grup="{{$items->nm_grup}}" data-nominal="{{$items->nominal}}" data-satuan="{{$items->satuan}}" data-placement="top" title="Edit">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('reseller.delete',$items->id) }}" class="edit btn btn-danger btn-sm edit-post" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>Tambah Reseller ke Grup</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('daftar.reseller') }}" method="POST" class="needs-validation">
                        @csrf
                        <div class="table-responsive">
                            <table id="example" class="display" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" class="select-all checkbox" name="select-all"></th>
                                        <th>Nama Reseller</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($reseller as $item)
                                    <tr>
                                        <td>
                                            <input type="checkbox" class="select-item checkbox" name="id_reseller[]" value="{{$item->id}}" />
                                        </td>
                                        <td>{{$item->name}} - ({{$item->email}})</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <table id="table_group" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>Pilih Grup</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control" name="group" required>
                                            @foreach ($r as $item)
                                            <option value="{{$item->id}}">{{$item->nm_grup}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-sm btn-primary">
                            <i class="fa fa-check"></i> Simpan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('daftar.grup') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Grup Reseller</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-12">
                        <label for="nm_group">Masukkan Nama grup</label>
                        <input type="text" name="nm_grup" class="form-control" required>
                    </div>
                    <div class="form-group col-12">
                        <label for="satuan">Satuan :</label>
                        <select class="form-control" id="satuan" name="satuan" required>
                            <option value="harga">Harga</option>
                            <option value="persentase">Persentase (%)</option>
                        </select>
                    </div>
                    <div class="form-group col-12" id="nominal">
                        <label class="col-form-label">Nominal : </label>
                        <input type="number" onkeypress="if(this.value.length==6) return false;" name="nominal" class="form-control" placeholder="harga">
                    </div>
                    <div class="form-group col-12" id="nominal1" style="display:  none;">
                        <label class="col-form-label">Nominal : </label>
                        <input type="number" maxlength="3" onchange="changeHandler(this)" name="nominal1" class="form-control" placeholder="persentase">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('edit.grup') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="id" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="id">
                    </div>
                    <div class="form-group col-12">
                        <label for="nm_group">Masukkan Nama grup</label>
                        <input type="text" name="nm_grup" id="nm_grup" class="form-control" required>
                    </div>
                    <div class="form-group col-12">
                        <label for="satuan">Satuan :</label>
                        <select class="form-control" id="satuan" name="satuan" required>
                            <option value="harga">Harga</option>
                            <option value="persentase">Persentase (%)</option>
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <label class="col-form-label">Nominal : </label>
                        <input type="text" name="nominal" class="form-control" id="nominal" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable({


        });
    });
    // inputan persentase/harga
    const satuan = document.getElementById("satuan");
    const inputHarga = document.getElementById("nominal");
    const inputPersen = document.getElementById("nominal1");

    satuan.addEventListener("click", function() {
        if (this.value === "harga") {
            $('input[name="nominal1"]').val('');
            inputHarga.style.display = "block";
            inputPersen.style.display = "none";
        } else if (this.value === "persentase") {
            $('input[name="nominal"]').val('');
            inputHarga.style.display = "none";
            inputPersen.style.display = "block";
        }
    });
</script>

<script>
    $('#edit').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('id') // Extract info from data-* attributes
        var nm_grup = button.data('nm_grup') // Extract info from data-* attributes
        var nominal = button.data('nominal') // Extract info from data-* attributes
        var satuan = button.data('satuan') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Edit')
        modal.find('#id').val(recipient)
        modal.find('#nm_grup').val(nm_grup)
        modal.find('#nominal').val(nominal)
        modal.find('#satuan').val(satuan)
    })

    // inputan persen
    function changeHandler(val)
    {
        if (Number(val.value) > 100)
        {
        val.value = 100
        }
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table_reseller').DataTable({

        });
        //button select all or cancel
        $("#select-all").click(function() {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function(index, item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function() {
            $("input.select-item").each(function(index, item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function() {
            var items = [];
            $("input.select-item:checked:checked").each(function(index, item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            } else {
                var values = items.join(',');
                console.log(values);
                var html = $("<div></div>");
                html.html("selected:" + values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function() {
            var checked = this.checked;
            $("input.select-item").each(function(index, item) {
                item.checked = checked;
            });
        });

        //check selected items
        $("input.select-item").click(function() {
            var checked = this.checked;
            console.log(checked);
            checkSelected();
        });

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            console.log("total:" + total);
            console.log("len:" + len);
            all.checked = len === total;
        }
    });
</script>

@endsection