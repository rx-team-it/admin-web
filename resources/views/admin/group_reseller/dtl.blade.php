@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('index.GroupReseller') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Grup Reseller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('index.GroupReseller') }}">Grup Reseller</a></div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>List Grup</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table_reseller" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <th>Nama Reseller</th>
                                    <th class="text-center">Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ($g1 as $items)
                                    <tr>
                                        <td>{{$items->name}}</td>
                                        <td class="text-center">
                                            <a href="{{ route('GroupReseller.delete',$items->id_reseller) }}" class="edit btn btn-danger btn-md edit-post" data-toggle="tooltip" data-placement="top" title="Hapus"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#table_reseller').DataTable({     
        });
    });
</script>
@endsection