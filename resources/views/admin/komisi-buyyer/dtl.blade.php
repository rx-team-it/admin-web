@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{route('komisi.buyyer')}}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Grup {{!empty($dtl[0]->grup)}}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item"><a href="#">Grup Komisi</a></div>
            <div class="breadcrumb-item active"><a href="#">Detail Grup Komisi</a></div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Daftar Produk {{!empty($dtl[0]->grup)}}</h4>
                    </div>
                    <div class="row ml-5">
                        <div class="col-4">
                            <div class="form-group col-12">
                                <p> Nama grup: <span class="badge badge-light">{{ $komisi->grup }}</span></p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group col-12">
                                <p>Satuan :  <span class="badge badge-light">{{ $komisi->satuan }}</span> </p>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group col-12">
                                <p class="col-form-label">Nominal :  <span class="badge badge-light">{{ $komisi->satuan == 'persentase' ? $komisi->nominal .'%': 'Rp.' .$komisi->nominal  }}</span> </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('komisi.dtl.delete.multiple')}}" method="post">
                            @csrf
                            <button class="btn btn-sm btn-danger float-left"><i class="fas fa-ban"></i>Hapus masal</button>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table id="table_reseller" class="table table-hover table-sm" style="border-collapse: collapse;">
                                    <thead style="border-top: 2px solid #eeeeee; border-left: 1px solid #eeeeee">
                                        <th><input type="checkbox" class="select-all checkbox" name="select-all"></th>
                                        <th style="text-align: center border-right: 2px solid #eeeeee; border-left: 1px solid #eeeeee">Nama Produk</th>
                                        <th style="text-align: center border-right: 2px solid #eeeeee; border-left: 1px solid #eeeeee">Varian</th>
                                        <th style="text-align: center border-right: 2px solid #eeeeee; border-left: 1px solid #eeeeee">Harga Fix</th>
                                    </thead>
                                    @foreach ($x as $items)
                                        <tbody>
                                            <tr>
                                                <td style="border-bottom: 1px solid #eeeeee; border-left: 1px solid #eeeeee; " rowspan="0">
                                                    <input type="checkbox" class="select-item checkbox" name="product_id[]"
                                                        value="{{$items->product_id}}" />
                                                </td>
                                                <td style="border-bottom: 1px solid #eeeeee; border-left: 1px solid #eeeeee; " rowspan="0">
                                                    {{ $items->product->name }}
                                                </td>
                                            </tr>
                                            @foreach ($items->product_detail as $item)
                                                <tr>
                                                    @if (empty($item->product_varian_options[0]))
                                                        <td style="border-bottom: 1px solid #eeeeee; border-left: 1px solid #eeeeee; ">No varian</td>
                                                        <td style="border-bottom: 1px solid #eeeeee; border-left: 1px solid #eeeeee; ">{{'Rp. '.number_format( $item->fix_price) }}</td>
                                                    @else
                                                        <td style="border-bottom: 1px solid #eeeeee; border-left: 1px solid #eeeeee; "> {{ $item->product_varian_options[0]->parent_options['name'] }} ({{ $item->product_varian_options[0]->name }})</td>
                                                        <td style="border-bottom: 1px solid #eeeeee; border-left: 1px solid #eeeeee; ">{{'Rp. '.number_format( $item->fix_price) }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    @endforeach
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        // $('#table_reseller').DataTable({     
        //     pageLength: 10,
        //     order: [
        //         [0, 'asc']
        //     ]
        // });

        //button select all or cancel
        $("#select-all").click(function () {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function (index, item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function () {
            $("input.select-item").each(function (index, item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function () {
            var items = [];
            $("input.select-item:checked:checked").each(function (index, item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            } else {
                var values = items.join(',');
                console.log(values);
                var html = $("<div></div>");
                html.html("selected:" + values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function () {
            var checked = this.checked;
            $("input.select-item").each(function (index, item) {
                item.checked = checked;
            });
        });

        //check selected items
        $("input.select-item").click(function () {
            var checked = this.checked;
            console.log(checked);
            checkSelected();
        });

        $(".checkbox").click(function(){
            console.log('ssas');
        })

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            console.log("total:" + total);
            console.log("len:" + len);
            all.checked = len === total;
        }
    });
</script>
@endsection