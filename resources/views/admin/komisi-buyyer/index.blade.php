@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Grup Komisi Produk</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Grup Komisi Produk</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('status') }}</strong>
            </div>
            @endif
            @if(Session::has('status_gagal'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('status_gagal') }}</strong>
            </div>
            @endif
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong><i class="fa fa-check-circle"></i>&nbsp; Nama grup tidak boleh sama</strong>
            </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h4>List Grup Komisi Produk</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#add">
                            <i class="fa fa-plus-circle"></i> Tambah Grup & Komisi
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_reseller" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>Nama Grup</th>
                                <th>Komisi</th>
                                <th>Tipe Komisi</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($a as $items)
                                <tr>
                                    <td>{{$items->grup}}</td>
                                    <td>
                                        @if ($items->satuan == 'harga')
                                        @currency($items->nominal)
                                        @else
                                        {{$items->nominal}}%
                                        @endif
                                    </td>
                                    <td>{{ $items->satuan }}</td>
                                    <td class="text-center">
                                        <a href="{{route('detail.komisi.produk', $items->id)}}" class="edit btn btn-primary btn-md edit-post"><i class="fa fa-eye"></i></a>

                                        <a href="{{route('preview.edit.komisi.buyyer', $items->id)}}" class="edit btn btn-success btn-md edit-post"><i class="fas fa-edit"></i></a>

                                            {{-- <a href="" class="btn btn-primary" data-toggle="modal" data-target="#edit" data-id="{{$items->id}}" data-grup="{{$items->grup}}" data-nominal="{{$items->nominal}}" data-satuan="{{$items->satuan}}" >
                                                <i class="fas fa-edit"></i> --}}
                                            </a>
                                        <a href="{{ route('komisi.produk.delete',$items->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>List Produk</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('grup.komisi.buyyer')}}" method="POST" class="needs-validation">
                        @csrf
                        <div class="table-responsive">
                            <table id="example" class="display" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Foto Produk</th>
                                        <th style="width: 30%" >Produk</th>
                                        <th>Harga</th>
                                        <th>Diskon</th>
                                        <th style="text-align: center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($product as $item)
                                    {{-- @php
                                        var_dump($item->product_details[0]->price)
                                    @endphp --}}
                                    <tr>
                                        <td>
                                            @if($item->grup_komisi_buyyer == null)
                                            <input type="checkbox" class="select-item checkbox" name="product_id[]"
                                            value="{{$item->id}}" />
                                            @else
                                            <i class="fas fa-ban text-danger"></i>
                                            @endif
                                        </td>
                                        <td><img src="{{!empty($item->product_images_thumbnail->img_product) ? Storage::disk('s3')->url('public/images/product/' . $item->product_images_thumbnail->img_product) : ''}}"
                                                alt="" height="50px" width="50px"></td>
                                        <td>{{$item->name}}</td>
                                        
                                        
                                        @php
                                            $type_diskon = $item->type_diskon;
                                            $diskon = $item->diskon;
                                        @endphp
                                        {{-- buat yg ada variannya --}}
                                        @if (count($item->product_details) > 1)
                                            <td>
                                                
                                                @php $prices = []; @endphp
                                                @foreach ($item->product_details as $item)
                                                    @php
                                                        array_push($prices, $item->price);
                                                    @endphp
                                                @endforeach
                                                @if (min($prices) != max($prices))
                                                    @currency(min($prices)) - @currency(max($prices))
                                                @else
                                                    @currency($prices[0])
                                                @endif
                                            </td>
                                            @if ($type_diskon == 'persen')
                                                <td>{{!empty($diskon) ? $diskon : '0'}}% {{ $item->type_diskon }}</td>
                                                @if (min($prices) != max($prices))
                                                    <td style="background-color: aquamarine; text-align: center"> @currency(min($prices) - ($diskon*min($prices) / 100)) - @currency(max($prices) - ($diskon*max($prices) / 100))</td>
                                                @else
                                                    <td style="background-color: aquamarine; text-align: center"> @currency($prices[0] - ($diskon*$prices[0] / 100))</td>
                                                @endif
                                            @else
                                                <td> Rp. {{!empty($diskon) ? number_format($diskon) : '0'}}</td>
                                                @if (min($prices) != max($prices))
                                                    <td style="background-color: aquamarine; text-align: center"> @currency(min($prices) - $diskon) - @currency(max($prices) - $diskon) </td>
                                                @else
                                                    <td style="background-color: aquamarine; text-align: center"> @currency($prices[0] - $diskon) </td>
                                                @endif
                                            @endif
                                        {{-- buat yg no varian --}}
                                        @else
                                            <td>
                                                @if (!empty($item->product_details[0]->price))
                                                @currency($item->product_details[0]->price)
                                                @endif
                                            </td>
                                            @if ($type_diskon == 'persen')
                                                <td>{{!empty($diskon) ? $diskon : '0'}} %</td>
                                                <?php $total = $item->product_detail['price'] - ($diskon*$item->product_detail['price'])/100; ?>
                                                <td style="background-color: aquamarine; text-align: center"> @currency($total)</td>
                                            @else
                                                <td> Rp. {{!empty($diskon) ? number_format($diskon) : '0'}}</td>
                                                <?php $total = $item->product_detail['price'] - $diskon; ?>
                                                <td style="background-color: aquamarine; text-align: center"> @currency($total)</td>
                                            @endif

                                        @endif
                                        
                                        
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <table id="table_group" class="table table-hover table-bordered table-sm">
                            <div class="row mt-4">
                                <div class="col-6"></div>
                                <div class="col-6">
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Perhatian!</strong> Komisi produk yang diberikan tidak boleh melebihi dari harga produk.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                </div>
                            </div>
                            <thead>
                                <th>Pilih Grup</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control" name="grup" id="grup" disabled>
                                            <option value="" selected disabled>-- PILIH -- </option>
                                            @foreach ($a as $item)
                                            <option value="{{$item->id}}">{{$item->grup}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row d-none" id="preview">
                            <div class="col-12">
                                <div class="card card-primary">
                                    <div class="card-header">
                                        <h4>Preview</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table id="table_reseller" class="table table-hover table-bordered table-sm">
                                                <thead>
                                                    <th style="text-align: center">Nama</th>
                                                    <th style="text-align: center">Total Harga Produk | (Varian)</th>
                                                    <th style="text-align: center">Komisi</th>
                                                    <th style="text-align: center">Total Komisi</th>
                                                    <th style="text-align: center">Keterangan</th>
                                                </thead>
                                                <tbody id="bodyPreview">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button onclick="" type="submit" class="btn btn-sm btn-primary" id="btn_simpan">
                            <i class="fa fa-check"></i> Simpan
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{route('simpan.komisi.buyyer')}}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Grup Buyyer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-12">
                        <label for="nm_group">Masukkan Nama grup</label>
                        <input type="text" name="grup" class="form-control" required>
                    </div>
                    <div class="form-group col-12">
                        <label for="satuan">Satuan :</label>
                        <select class="form-control" id="satuan" name="satuan" required>
                            <option value="harga">Harga</option>
                            <option value="persentase">Persentase (%)</option>
                        </select>
                    </div>
                    <div class="form-group col-12">
                        <label class="col-form-label">Nominal : </label>
                        <input type="number" onKeyPress="if(this.value.length==10) return false;" name="nominal"
                            class="form-control" id="nominal" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({


        });
    });

</script>

<script>
    $('#edit').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('id') // Extract info from data-* attributes
        var grup = button.data('grup') // Extract info from data-* attributes
        var nominal = button.data('nominal') // Extract info from data-* attributes
        var satuan = button.data('satuan') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Edit')
        modal.find('#recipient-name').val(recipient)
        modal.find('#grup').val(grup)
        modal.find('#nominal').val(nominal)
        modal.find('#satuan').val(satuan)
    })
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#table_reseller').DataTable({

        });
        //button select all or cancel
        $("#select-all").click(function () {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function (index, item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function () {
            $("input.select-item").each(function (index, item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function () {
            var items = [];
            $("input.select-item:checked:checked").each(function (index, item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            } else {
                var values = items.join(',');
                // console.log(values);
                var html = $("<div></div>");
                html.html("selected:" + values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function () {
            var checked = this.checked;
            $("input.select-item").each(function (index, item) {
                item.checked = checked;
            });
        });

        //check selected items
        //     var ids = [];
        // $("input.select-item").change(function () {
        //     var checked = this.checked;
        //     // console.log(checked);
        //     checkSelected();
        //     // $.each($("input[name='product_id[]']:checked"), function(){
        //     //     favorite.push($(this).val());
        //     // });
        //     let x = this.value;
        //     if(checked){
        //     ids.push(x);
        //     }else{
        //         ids.pop(x)
        //     }
        //     console.log(x);  
        //     console.log(ids);
        // });

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            // console.log("total:" + total);
            // console.log("len:" + len);
            all.checked = len === total;
        }
    // $('input.select-item').change(function(){
    // //    var favorite = [];
    // //         $.each($("input[name='idP']:checked"), function(){
    // //             favorite.push($(this).val());
    // //         });
    // console.log('ok');
    // })
    });
    // var ids = [];
    
    $("input.select-item").change(function () {
        let allPid = [];
            // var checked = this.checked;
            // let x = this.value;
            // if(checked){
            // ids.push(x);
            // }else{
            //     ids.pop(x)
            // }
            // console.log(x);  
            // console.log(ids);

            $("input:checkbox[name='product_id[]']:checked").each(function(){
                allPid.push($(this).val());
            });
            
            let p = allPid.length;

            if(p < 1){
                $('#grup').prop('disabled', true);
            }else{
                $('#grup').prop('disabled', false);

            }

            // console.log(p);
            // console.log('oke');
            
        });
        $('#grup').change(function(){
            let id_komisiBuyyer = this.value;
            let ids = [];

            $("input:checkbox[name='product_id[]']:checked").each(function(){
                ids.push($(this).val());
            });
            
            $('#preview').removeClass('d-none');
            // console.log(ids);
            $.ajax({
                type: "GET",
                url: "preview",
                data: {
                    ids: ids,
                    id_komisiBuyyer: id_komisiBuyyer
                },
                success: function(res){
                    // console.log(res);
                    let datas = res.data;
                    $('.tr_preview').remove();
                    
                    if(res.komisi_buyyer.satuan == 'harga'){
                        datas.forEach(e => {
                            // console.log(e.nameVarian);
                            // console.log(e.product_varian_option_id);
                            // console.log(e);
                            if(e.type_diskon == 'persen'){
                                let total = e.price - (e.diskon*e.price)/100;
                                    let total_komisi = total + res.komisi_buyyer.nominal;
                                $('#bodyPreview').append(`<tr class="tr_preview"> <td style="text-align: center"> <input type="hidden" name="${e.product_varian_option_id ? 'id_varian[]' : 'id_produk[]' }" value="${e.product_varian_option_id ? e.product_varian_option_id : e.id}"> </input> ${e.name}</td> <td style="text-align: center">Rp. ${total} | ${e.nameVarian ? e.nameVarian : ''} (${e.varian ? e.varian: 'no varian'})</td> <td style="text-align: center">Rp. ${res.komisi_buyyer.nominal}</td> <td style="text-align: center"><input type="hidden" name="${e.product_varian_option_id ? 'fix_price_varian[]' : 'fix_price_byId[]'}" value="${total_komisi}"> </input>Rp. ${total_komisi}</td>${total <= res.komisi_buyyer.nominal ? '<td style="background-color:#FCEDDA;text-align:center;font-weight:bold"><input type="hidden" value="failed" name="failed" />komisi melebihi / sama dengan harga produk</td>' : '<td style="background-color:#DAEBEE; text-align:center;font-weight:bold">syarat komisi terpenuhi</td>'}</td></tr>`)
                            }else{
                                let total = e.price - e.diskon;
                                let total_komisi = total + res.komisi_buyyer.nominal;
                                $('#bodyPreview').append(`<tr class="tr_preview"> <td style="text-align: center"> <input type="hidden" name="${e.product_varian_option_id ? 'id_varian[]' : 'id_produk[]' }" value="${e.product_varian_option_id ? e.product_varian_option_id : e.id}"> </input> ${e.name}</td> <td style="text-align: center">Rp. ${total} | ${e.nameVarian ? e.nameVarian : ''} (${e.varian ? e.varian: 'no varian'})</td> <td style="text-align: center">Rp. ${res.komisi_buyyer.nominal}</td> <td style="text-align: center"><input type="hidden" name="${e.product_varian_option_id ? 'fix_price_varian[]' : 'fix_price_byId[]'}" value="${total_komisi}"> </input>Rp. ${total_komisi}</td>${total <= res.komisi_buyyer.nominal ? '<td style="background-color:#FCEDDA;text-align:center;font-weight:bold"><input type="hidden" value="failed" name="failed" />komisi melebihi / sama dengan harga produk</td>' : '<td style="background-color:#DAEBEE; text-align:center;font-weight:bold">syarat komisi terpenuhi</td>'}</td></tr>`)
                            }
                        });
                    }else {

                        datas.forEach(e => {
                        // console.log(e.nameVarian);
                            if(e.type_diskon == 'persen'){
                                let total = e.price - (e.diskon*e.price)/100;
                                let total_komisi = total + ((total*res.komisi_buyyer.nominal) / 100);
                                $('#bodyPreview').append(`<tr class="tr_preview"> <td style="text-align: center"> <input type="hidden" name="${e.product_varian_option_id ? 'id_varian[]' : 'id_produk[]' }" value="${e.product_varian_option_id ? e.product_varian_option_id : e.id}"> </input> ${e.name}</td> <td style="text-align: center">Rp. ${total} | ${e.nameVarian ? e.nameVarian : ''} (${e.varian ? e.varian: 'no varian'})</td> <td style="text-align: center">${res.komisi_buyyer.nominal}%</td> <td style="text-align: center"><input type="hidden" name="${e.product_varian_option_id ? 'fix_price_varian[]' : 'fix_price_byId[]'}" value="${total_komisi}"> </input>Rp. ${total_komisi}</td> <td style="background-color:#DAEBEE; text-align:center;font-weight:bold">syarat komisi terpenuhi</td></tr>`)
                            }else{
                                let total = e.price - e.diskon;
                                let total_komisi = total + ((total*res.komisi_buyyer.nominal) / 100);
                                $('#bodyPreview').append(`<tr class="tr_preview"> <td style="text-align: center"> <input type="hidden" name="${e.product_varian_option_id ? 'id_varian[]' : 'id_produk[]' }" value="${e.product_varian_option_id ? e.product_varian_option_id : e.id}"> </input> ${e.name} xxx</td> <td style="text-align: center">Rp. ${total} | ${e.nameVarian ? e.nameVarian : ''} (${e.varian ? e.varian: 'no varian'})</td> <td style="text-align: center">${res.komisi_buyyer.nominal}%</td> <td style="text-align: center"><input type="hidden" name="${e.product_varian_option_id ? 'fix_price_varian[]' : 'fix_price_byId[]'}" value="${total_komisi}"> </input>Rp. ${total_komisi}</td> <td style="background-color:#DAEBEE; text-align:center;font-weight:bold">syarat komisi terpenuhi</td></tr>`)
                            }
                        });

                    }
                }
            })
        })


</script>

@endsection
