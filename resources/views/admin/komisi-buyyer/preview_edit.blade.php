@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{route('komisi.buyyer')}}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Grup {{!empty($dtl[0]->grup)}}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item"><a href="#">Grup Komisi</a></div>
            <div class="breadcrumb-item active"><a href="#">Detail Grup Komisi</a></div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('edit.komisi.buyyer') }}" method="POST">
                    @csrf
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Edit Grup {{!empty($dtl[0]->grup)}}</h4>
                    </div>
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name" value="{{ $komisi->id }}">
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group col-12">
                                <label for="nm_group">Masukkan Nama grup</label>
                                <input type="text" name="grup" id="grup" class="form-control" value="{{ $komisi->grup }}" required>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group col-12">
                            <label for="satuan">Satuan :</label>
                            <p>{{ $komisi->satuan }}</p>
                            <input type="hidden" name="satuan" id="satuan" value="{{ $komisi->satuan }}">
                        </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group col-12">
                                <label class="col-form-label">Nominal : {{ $komisi->satuan == 'persentase' ? '%': 'Rp' }} </label>
                                @if ($komisi->satuan == 'persentase')
                                    <input type="number" min="0" onkeyup="cekPersen(this.value)" name="nominal" class="form-control" id="nominal" value="{{ $komisi->nominal }}" required>

                                @elseif($komisi->satuan == 'harga')
                                    <input type="number" min="0" onkeyup="cekHarga(this.value)" name="nominal" class="form-control" id="nominal" value="{{ $komisi->nominal }}" required>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_reseller" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <th style="width: 40%">Nama Produk</th>
                                        <th>Varian</th>
                                        <th style="width: 15%">Price</th>
                                        <th style="width: 15%">Komisi</th>
                                        <th style="width: 15%">Total</th>
                                    </thead>
                                    <tbody>
                                        <?php $id_komisi = 1 ?>
                                        <?php $fix_price_id = 1 ?>
                                        <?php $price_id = 1 ?>
                                        @foreach ($dtl as $items)
                                        <tr>
                                            <td>{{$items->name}} <input type="hidden" name="{{ $items->product_varian_option_id ? 'product_varian_id[]' : 'product_id[]' }}" value="{{ $items->product_varian_option_id ? $items->product_varian_option_id : $items->id_noVarian }}"></td>
                                            <td>{{ $items->nameVarian ? $items->nameVarian : '' }} ({{ $items->varian ? $items->varian : 'tidak ada varian' }})</td>

                                            @if($items->type_diskon == 'nominal')
                                                <?php $total = $items->price - $items->diskon ?>
                                                <td>Rp. <input type="text" style="width: 70%" name="price[]" class="price" id="price<?= $price_id++ ?>" value="{{ $total }}" disabled> </td>
                                            @else
                                                <?php $total = $items->price - (($items->price*$items->diskon)/100) ?>
                                                <td>Rp. <input type="text" style="width: 70%" name="price[]" class="price" id="price<?= $price_id++ ?>" value="{{ $total }}" disabled> </td>
                                            @endif

                                            @if ($items->satuan == 'harga')
                                                <td>Rp. <input type="text" style="width: 70%" name="nominal_komisi[]" class="nominal_komisi" id="nominal_komisi<?= $id_komisi++ ?>" class="nominal_komisi" value=" {{ $komisi->nominal }}" disabled></td>
                                                <td>Rp. <input type="text" style="width: 70%" name="{{ $items->product_varian_option_id ? 'fix_price_varian[]' : 'fix_price[]' }}" class="fix_price" id="fix_price<?= $fix_price_id++ ?>"value="{{ $items->fix_price }}"readonly ></td>
                                            @else
                                                <td> <input type="text" style="width: 70%" name="nominal_komisi[]" class="nominal_komisi" id="nominal_komisi" class="nominal_komisi" value="{{ $komisi->nominal }}" disabled> %</td>
                                                <?php $total = $items->price + (($items->price*$items->nominal) / 100) ?>
                                                <td>Rp. <input type="text" style="width: 70%" name="{{ $items->product_varian_option_id ? 'fix_price_varian[]' : 'fix_price[]' }}" class="fix_price" id="fix_price<?= $fix_price_id++ ?>" value="{{ $items->fix_price }}" readonly></td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {

        //button select all or cancel
        $("#select-all").click(function () {
            var all = $("input.select-all")[0];
            all.checked = !all.checked
            var checked = all.checked;
            $("input.select-item").each(function (index, item) {
                item.checked = checked;
            });
        });

        //button select invert
        $("#select-invert").click(function () {
            $("input.select-item").each(function (index, item) {
                item.checked = !item.checked;
            });
            checkSelected();
        });

        //button get selected info
        $("#selected").click(function () {
            var items = [];
            $("input.select-item:checked:checked").each(function (index, item) {
                items[index] = item.value;
            });
            if (items.length < 1) {
                alert("no selected items!!!");
            } else {
                var values = items.join(',');
                console.log(values);
                var html = $("<div></div>");
                html.html("selected:" + values);
                html.appendTo("body");
            }
        });

        //column checkbox select all or cancel
        $("input.select-all").click(function () {
            var checked = this.checked;
            $("input.select-item").each(function (index, item) {
                item.checked = checked;
            });
        });

        //check selected items
        $("input.select-item").click(function () {
            var checked = this.checked;
            console.log(checked);
            checkSelected();
        });

        //check is all selected
        function checkSelected() {
            var all = $("input.select-all")[0];
            var total = $("input.select-item").length;
            var len = $("input.select-item:checked:checked").length;
            console.log("total:" + total);
            console.log("len:" + len);
            all.checked = len === total;
        }
    });
  
    // buat ganti nominal persen
    function cekPersen(x){
        // nominal persen ga boleh lebi dari 99
        if(x > 99){
            $('#nominal').val('99');
            $('.nominal_komisi').val('99');
            kalkulasiPersen(99);

        }else{
            $('.nominal_komisi').val(x);

            kalkulasiPersen(x);
        }

        function kalkulasiPersen(param) {
            var rowCount = $("#table_reseller tr").length;  


            for (let i = 1; i < rowCount; i++) {

                let price =  $(`#price${i}`).val();
                let cal = (price*param) / 100;
                let fix_price = parseInt(price)+parseInt(cal);

                $(`#fix_price${i}`).val(fix_price);

            } 
        }


    }    

    // buat ganti nominal
    function cekHarga(x){

        var rowCount = $("#table_reseller tr").length;  
        $('.nominal_komisi').val(x);
        for (let i = 1; i < rowCount; i++) {

            let price = $(`#price${i}`).val();
            let nominal_komisi = $(`#nominal_komisi${i}`).val();
           if(parseInt(nominal_komisi) > parseInt(price)){
                alert(`perhatian!! terdapat produk yang berharga ${price}, silahkan kalkulasi lagi!`);
                location.reload();
            }else{
                let fix_price = parseInt(price) + parseInt(x);
                $(`#fix_price${i}`).val(fix_price);
            }
       }

       
    }             


</script>
@endsection