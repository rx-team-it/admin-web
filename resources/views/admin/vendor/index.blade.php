@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-social/bootstrap-social.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Management Vendor</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item">Management Vendor</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-12">
                <div class="card card-danger">
                    <div class="card-header">
                        <h4>Daftar Vendor</h4>
                        <div class="card-header-action">
                            <a href="" class="btn btn-primary btn-icon icon-right" data-toggle="modal"
                                data-target="#modal-vendor"><i class="fas fa-plus-circle"></i> Tambah vendor</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="owl-carousel owl-theme" id="users-carousel">
                            @foreach ($user as $item)
                            <div>
                                <div class="user-item">
                                    <img alt="image" src="{{asset('assets/img/users/user-1.png')}}" class="img-fluid">
                                    <div class="user-details">
                                        <div class="user-name">{{ !empty($item->user) ? $item->user->name : '' }}</div>
                                        <div class="text-job text-muted">{{ !empty($item) ? $item->created_at : '' }}</div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- modal tambah vendor --}}
<div class="modal fade" id="modal-vendor">
    <div class="modal-dialog">
        <form method="POST" action="{{route('vendor.tambah')}}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Vendor Baru</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-6">
                            <label>Nama</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Lengkap">
                            <span class="text-danger error-text name_error"></span>
                        </div>
                        <div class="form-group col-6">
                            <label>No Hp</label>
                            <input type="number" class="form-control" id="phone" name="phone" placeholder="Nomor Hp">
                            <span class="text-danger error-text phone_error"></span>
                        </div>
                        <div class="form-group col-12">
                            <label>Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            <span class="text-danger error-text email_error"></span>
                        </div>
                        <div class="form-group col-6">
                            <label class="d-block">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <span class="text-danger error-text password_error"></span>
                        </div>
                        <div class="form-group col-6">
                            <label class="d-block">Konfirmasi Password</label>
                            <input type="password" class="form-control" name="password_confirmation"
                                placeholder="Konfirmasi Password">
                            <span class="text-danger error-text password_error"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/page/widget-data.js') }}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(function() {
        $("#regist_form").on('submit', function(e) {
            e.preventDefault();

            $.ajax({
                url: $(this).attr('action'),
                method: $(this).attr('method'),
                data: new FormData(this),
                processData: false,
                dataType: 'json',
                contentType: false,
                beforeSend: function() {
                    $(document).find('span.error-text').text('');
                },
                success: function(data) {
                    if (data.status == 0) {
                        $.each(data.error, function(prefix, val) {
                            $('span.' + prefix + '_error').text(val[0]);
                        });
                    } else {
                        document.regist_form.reset();
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Vendor Berhasil Dibuat'
                        })
                    }
                }
            })
        });
    });
</script>

@if(Session::has('success'))
<script>
    Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: "{!!Session::get('success')!!}",
        showConfirmButton: false,
        timer: 1500
    })
</script>
@endif

@endsection