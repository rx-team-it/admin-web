@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <div class="col-md-4">
                <h4>Total Produk terjual</h4>
            </div>
            <div class="col-md-8">
                <div class="row input-daterange">
                    <div class="col-md-4">
                        <input type="text" name="min" id="min" class="form-control" placeholder="From Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="max" id="max" class="form-control" placeholder="To Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                        <a href="" class="btn btn-sm btn-secondary">Refresh</a>
                        {{-- <button type="button" name="refresh" id="refresh" class="btn btn-secondary">Refresh</button> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                        {{-- <div class="col-md-6 mb-3">
                            <table border="0" cellspacing="5" cellpadding="5">
                                <tbody>
                                    <tr>
                                        <td>Dari Tgl:</td>
                                        <td><input name="min" id="min" type="date" value=""></td>
                                    </tr>
                                    <tr>
                                        <td>Ke Tgl:</td>
                                        <td>
                                            <input name="max" id="max" type="date" value=""> 
                                            <button id="filter" class="btn btn-sm btn-success">Filter</button>
                                            <a href="" class="btn btn-sm btn-primary">Refresh</a>
                                        </td>
                                    </tr>
                                    <tr>
                                    </tr>
                                </tbody>
                            </table>
                        </div> --}}
                        <div class="table-responsive">
                            {{-- <table id="saldo" class="table table-hover table-bordered table-sm cell-border"> --}}
                            <table id="saldo" class="table table-bordered table-striped table-sm">
                                <thead>
                                    <tr>
                                        {{-- <th>No</th> --}}
                                        <th>Tanggal</th>
                                        <th>Invoice</th>
                                        <th>Harga Awal</th>
                                        <th>Komisi</th>
                                        <th>Harga jual@</th>
                                        <th>QTY</th>
                                        <th>Biaya Layanan</th>
                                        <th>Cashback</th>
                                        <th>Ongkir</th>
                                        <th>Gross</th>
                                        <th>Pendapatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($penjualan as $items)
                                        <tr>
                                            {{-- <td>{{ $loop->iteration }}</td> --}}
                                            <td>{{ date('Y-m-d', strtotime($items->created_at)) }}</td>
                                            <td>
                                                <a href={{ route("order.pembayaran", $items->invoice) }}>{{ $items->invoice }}</a>
                                                {{-- <br>
                                                <small>Tgl. {{ date('Y-m-d', strtotime($items->created_at)) }}</small> --}}
                                            </td>
                                            <td>
                                                <?php foreach ($items->order_details as $item) {
                                                        if (!empty($item->harga_diskon)) {
                                                            echo 'Rp. ' .number_format($item->harga_diskon);
                                                            echo '<br>';
                                                        }
                                                        else {
                                                            echo 'Rp. ' . number_format($item->price);
                                                            echo '<br>';
                                                        }
                                                    };
                                                ?>

                                            </td>
                                            <td>
                                                <?php 
                                                    foreach ($items->order_details as $item) {
                                                        if ($item->type_komisi == 'persentase') {
                                                            if (!empty( $item->harga_diskon)) {
                                                                echo 'Rp. ' . number_format($item->harga_diskon * $item->nilai_komisi / 100);
                                                            } else {
                                                                echo 'Rp. ' . number_format($item->price * $item->nilai_komisi / 100);
                                                            }
                                                            echo '<br>';
                                                        }else{
                                                            echo 'Rp. ' . number_format($item->nilai_komisi);
                                                            echo '<br>';
                                                        }
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    foreach ($items->order_details as $item) {
                                                       echo 'Rp. ' . number_format($item->fix_price);
                                                       echo '<br>';
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    foreach ($items->order_details as $item) {
                                                        echo $item->qty . ' pcs';
                                                        echo '<br>';
                                                    }
                                                ?>
                                            </td>
                                            <td>Rp. {{ number_format($items->biaya_layanan + $items->kode_unik) }}</td>
                                            <td>Rp. {{ number_format($items->cashback) }}</td>
                                            <td>Rp. {{ number_format($items->ongkos_kirim_fix) }}</td>
                                            {{-- <td>Rp. {{ number_format($items->laba_bersih + ($items->kode_unik + $items->biaya_layanan + $items->ongkos_kirim_fix) ) }}</td> --}}
                                            <td>
                                                Rp. {{ number_format($items->total + $items->total_komisi_produk ) }}
                                            </td>
                                            <td>Rp. {{ number_format($items->laba_bersih) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot id="footer">
                                    <tr id="trFooter">
                                        <th colspan="2" style="text-align:center">Total</th>
                                        <th id="harga_awal">{{'Rp. ' . number_format($harga_awal)}}</th>
                                        <th id="komisi">{{'Rp. ' . number_format($komisi)}}</th>
                                        <th id="fix_price">{{'Rp. ' . number_format($fix_price)}}</th>
                                        <th id="qty">{{$qty}} pcs</th>
                                        <th id="biaya_layanan">{{'Rp. ' . number_format($biaya_layanan)}}</th>
                                        <th id="cashback">{{'Rp. ' . number_format($cashback)}}</th>
                                        <th id="ongkir">{{'Rp. ' . number_format($ongkir)}}</th>
                                        <th id="gross">
                                            {{'Rp. ' . number_format($gross)}}
                                        </th>
                                        <th id="pendapatan">{{'Rp. ' . number_format($pendapatan)}}</th>
                                        
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" referrerpolicy="no-referrer"></script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif


<script>
    $(document).ready(function () {
        $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = $('#min').val();
                var max = $('#max').val();
                var startDate = data[0];
                if (min == '' && max == '') {
                    return true;
                }
                if (min == '' && startDate <= max) {
                    return true;
                }
                if (max == '' && startDate >= min) {
                    return true;
                }
                if (startDate <= max && startDate >= min) {
                    return true;
                }
                return false;
            }
        );

        $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });


        // $("#min").datepicker({
        //     format: 'dd-mm-yyyy',
        //     onSelect: function () {
        //         table.draw();
        //     },
        //     changeMonth: false,
        //     changeYear: false
        // });
        // $("#max").datepicker({
        //     format: 'dd-mm-yyyy',
        //     onSelect: function () {
        //         table.draw();
        //     },
        //     changeMonth: false,
        //     changeYear: false
        // });
        var table = $('#saldo').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5'
            ]
        });

        // Event listener to the two range filtering inputs to redraw on input
        $('#min, #max').change(function () {
            var from_date = $('#min').val();
            var to_date = $('#max').val();
            // console.log([from_date, to_date]);
        });
        $('#filter').click(function(){

            var from_date = $('#min').val();
            var to_date = $('#max').val();

            if (from_date != '' && to_date != '') {
                //parse the input date
                function dateFormat(inputDate, format) {
                    const date = new Date(inputDate);
    
                    //extract the parts of the date
                    const day = date.getDate();
                    const month = date.getMonth() + 1;
                    const year = date.getFullYear();    
    
                    //replace the month
                    format = format.replace("MM", month.toString().padStart(2,"0"));        
    
                    //replace the year
                    if (format.indexOf("yyyy") > -1) {
                        format = format.replace("yyyy", year.toString());
                    } else if (format.indexOf("yy") > -1) {
                        format = format.replace("yy", year.toString().substr(2,2));
                    }
    
                    //replace the day
                    format = format.replace("dd", day.toString().padStart(2,"0"));
    
                    //a simple date formatting function
                    return format;
                }
    
                table.draw();
                load_total(dateFormat(from_date, 'yyyy-MM-dd'), dateFormat(to_date, 'yyyy-MM-dd'));
            } else {
                alert('Both Date is required');
            }

        })

        // total otomatis
        function load_total(from_date, to_date) {
            $.ajax({
                type: "GET",
                url: "{{ route('histori-vendor.total-penjualan') }}",
                data: {
                    from_date: from_date,
                    to_date: to_date
                },
                success: function(res){
                    // console.log(res);
                    function uang(param){
                        var	reverse = param.toString().split('').reverse().join(''),
                            ribuan 	= reverse.match(/\d{1,3}/g);
                            ribuan	= ribuan.join('.').split('').reverse().join('');
                            return ribuan;
                    }
                    $('#harga_awal').text(`Rp. ${uang(res[0])}`);
                    $('#komisi').text(`Rp. ${uang(res[1])}`);
                    $('#fix_price').text(`Rp. ${uang(res[2])}`);
                    $('#qty').text(`${res[3]} pcs`);
                    $('#biaya_layanan').text(`Rp. ${uang(res[4])}`);
                    $('#cashback').text(`Rp. ${uang(res[5])}`);
                    $('#ongkir').text(`Rp. ${uang(res[6])}`);
                    $('#gross').text(`Rp. ${uang(res[7])}`);
                    $('#pendapatan').text(`Rp. ${uang(res[8])}`);
                }
            })
        }
    });


    $('#editpesan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Batalkan Orderan')
        modal.find('#recipient-name').val(recipient)
    })

</script>


@endsection