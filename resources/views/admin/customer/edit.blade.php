@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('customer.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail User</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Detail User</div>
        </div>
    </div>
    <div class="section-body">
        <div class="invoice">
            <div class="invoice-print">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="invoice-title">
                            <h2>{{ $datas->fullname }}</h2>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="section-title">Data User</div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-md">
                                <tr>
                                    <th>Nama</th>
                                    <th>Gender</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Alamat</th>
                                    <th>Bio</th>
                                </tr>
                                <tr>
                                    <td>{{ !empty($datas) ? $datas->fullname : '' }}</td>
                                    <td>{{ !empty($datas) ? $datas->gender : '' }}</td>
                                    <td>{{ !empty($datas) ? $datas->phone : '' }}</td>
                                    <td>{{ !empty($datas->user) ? $datas->user->email : '' }}</td>
                                    <td>{{ !empty($datas) ? $datas->address : '' }}<br>
                                        {{ !empty($datas->countrys) ? $datas->countrys->name : '' }}<br>
                                        {{ !empty($datas->states) ?  $datas->states->name : '' }}<br>
                                        {{ !empty($datas->citys) ?  $datas->citys->name : '' }}
                                    </td>
                                    <td>{!! substr($datas->bio,0, 150) !!} ...</td>
                                </tr>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="images">
                                    <table>
                                        <td>
                                            <div class="section-title">Foto User</div>
                                            <img alt="image" src="{{Storage::disk('s3')->url('public/images/foto_user/' . $datas->foto_user)}}" width="180px" height="180px">
                                        </td>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <p class="section-lead"><strong>Pilih Status</strong></p>
            <div class="text-md-right">
                <div class="float-lg-left mb-lg-0 mb-3">
                    <form action="{{ route('customer.blok', $datas->id) }}" method="post">
                        @csrf
                        <button class="btn btn-danger btn-icon icon-left"><i class="fas fa-times"></i> Blok</a></button>
                    </form>
                </div>
                <form action="{{ route('customer.delete', $datas->id) }}" method="post">
                    @csrf
                    <button class="btn btn-danger btn-icon icon-left"><i class="fas fa-times"></i> Delete Permanen</a></button>
                </form>
                <br>
            </div>
        </div>
    </div>
</section>
@endsection