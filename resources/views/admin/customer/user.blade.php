@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Customer</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Buyyer Aktif</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <table id="user" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama User</th>
                                    <th>Email</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                ?>
                                @foreach($user as $u)
                                @if($u['role_nm'] == "buyyer")
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{ $u->name }}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->created_at}}</td>
                                    <td>
                                        <a href="{{ route('customer.delete', $u->id) }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                        <a href="{{ route('customer.blok', $u->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-lock"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $no++;
                                ?>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">List Buyyer Di Blok</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <table id="user_blok" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama User</th>
                                    <th>Email</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $nomor = 1;
                                ?>
                                @foreach($user_blok as $u)
                                <tr>
                                    <td>{{$nomor}}</td>
                                    <td>{{ $u->name }}</td>
                                    <td>{{$u->email}}</td>
                                    <td>{{$u->created_at}}</td>
                                    <td>
                                        <a href="{{ route('customer.unblock', $u->id) }}" class="btn btn-primary btn-sm" onclick="return confirm('Are you sure?')"><i class="fa fa-unlock"></i></a>
                                    </td>
                                </tr>
                                <?php
                                $nomor++;
                                ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif


<script>
    $(function() {
        $("#user").DataTable({
            pageLength: 5
        });
    });
</script>

<script>
    $(function() {
        $("#user_blok").DataTable({
            pageLength: 5
        });
    });
</script>

<script>
    $('.swal-confirm').click(function(e) {
        id = e.target.dataset.id;
        swal({
                title: "Are you sure?",
                text: "Once block, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // swal("Poof! Your imaginary file has been deleted!", {
                    //     icon: "success",
                    // });
                    $(`#delete${id}`).submit();
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    });
</script>
@endsection