@extends('admin.layouts.admin.index')


@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('histori-saldo.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Saldo Reseller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Histori Saldo Reseller</div>
            <div class="breadcrumb-item">Detail</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>Table Saldo</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tbl" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>No</th>
                                <th>Invoice</th>
                                <th>Tgl </th>
                                <th>Nominal</th>
                                <th class="text-center">Keterangan</th>
                            </thead>
                            <tbody>
                                @if (empty($c))
                                
                                @else
                                    @foreach($c as $item)
                                    <tr>
                                        <td> {{$loop -> iteration}} </td>
                                        @foreach($item->user_id as $item2)
                                            <td>
                                                <a href="{{ route('pesanan.detail.reseller', $item2->invoice) }}">
                                                    {{$item2->invoice}}</a>
                                            </td>
                                        @endforeach
                                        @if ($item->keterangan == 'Pengurangan saldo')
                                            <td>Withdraw</td>
                                        @endif
                                        <td> {{$item->created_at}} </td>
                                        <td>@currency($item->nominal)
                                            @if ($item->keterangan == 'Pengurangan saldo')
                                            <i class="fa fa-arrow-circle-down" style="color: red"></i>
                                            @else
                                            <i class="fa fa-arrow-circle-up" style="color: green"></i>
                                        </td>
                                        @endif
                                        <td>{{$item->keterangan}}</td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#tbl').DataTable({});
    });
</script>

@endsection