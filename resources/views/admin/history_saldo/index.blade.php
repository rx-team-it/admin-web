@extends('admin.layouts.admin.index')


@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Histori Saldo Reseller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Histori Saldo Reseller</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h5>Table Saldo Reseller</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tbl" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>No </th>
                                <th>Nama</th>
                                <th>Saldo</th>
                                <th class="text-center">Aksi</th>
                            </thead>
                            <tbody>
                                {{-- {{dd($xx)}} --}}
                                @foreach ($xx as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td><strong>{{$item->name}}</strong> <br> <small>{{$item->email}} <br> Tgl. Daftar - {{$item->created_at}}</small></td>
                                    @if ($item->komisi == null)
                                    <td>Rp. 0,-</td>
                                    @else
                                    <td>@currency($item->komisi),-</td>
                                    @endif
                                    <td class="text-center">
                                        <a href="{{route('histori-saldo.dtl', $item->user_referral_id)}}" class="edit btn btn-primary btn-md edit-post"><i class="fa fa-eye"></i> Detail</a>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
<script type="text/javascript">
    $(document).ready(function() {
        $('#tbl').DataTable({});
    });
</script>

@endsection