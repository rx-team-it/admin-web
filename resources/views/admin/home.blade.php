@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/Tour/tour-default.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<div class="row">
    <div class="col-md-6 kedua">
        <div class="card card-primary">
            <div class="card-header">
                <h4 style="font-family:Monaco;">Pendapatan Kotor</h4>
                <select name="filter_kotor_3v card-icon" id="filter_kotor_3v" class="form-control form-control-sm" style="width: 30%; margin-right:5px">
                    <option value="all">--Semua--</option>
                    <option value="today" selected>Hari ini</option>
                    <option value="week">Minggu ini</option>
                    <option value="month">Bulan ini</option>
                    <option value="year">Tahun ini</option>
                </select>
                
                <div style="margin-left: auto">
                    <a href="{{ route('histori-vendor.penjualan') }}" class="btn btn-primary">Lihat semua</a>
                </div>
            </div>
            <div class="card-body justify-content-cente">
                <h2 id="kotor" class="text-center">@currency($gross)</h2>
                <img src="{{ asset('assets\img\loading2.gif') }}" alt="" class="img-load d-none" style="margin-left: 30%">
            </div>
            <div class="card-footer bg-whitesmoke">
                <div class="row input-daterange">
                    <div class="col-md-4">
                        <input type="text" name="min" id="min" class="form-control" placeholder="From Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="max" id="max" class="form-control" placeholder="To Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <button type="button" name="filter" id="filter_kotor" class="btn btn-md btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 kedua">
        <div class="card card-primary">
            <div class="card-header">
                <h4 style="font-family:Monaco;">Pendapatan Bersih</h4>
                <select name="filter_bersih_3v card-icon" id="filter_bersih_3v" class="form-control form-control-sm" style="width: 30%; margin-right:5px">
                    <option value="all">--Semua--</option>
                    <option value="today" selected>Hari ini</option>
                    <option value="week">Minggu ini</option>
                    <option value="month">Bulan ini</option>
                    <option value="year">Tahun ini</option>
                </select>
                <div style="margin-left: auto">
                    <a href="{{ route('histori-vendor.penjualan') }}" class="btn btn-primary">Lihat semua</a>
                </div>
            </div>
            <div class="card-body justify-content-cente">
                <h2 id="bersih" class="text-center">@currency($pendapatan)</h2>
                <img src="{{ asset('assets\img\loading2.gif') }}" alt="" class="img-load-bersih d-none" style="margin-left: 30%">
            </div>
            <div class="card-footer bg-whitesmoke">
                <div class="row input-daterange">
                    <div class="col-md-4">
                        <input type="text" name="min" id="min1" class="form-control" placeholder="From Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="max" id="max1" class="form-control" placeholder="To Date" readonly />
                    </div>
                    <div class="col-md-4">
                        <button type="button" name="filter" id="filter_bersih" class="btn btn-md btn-primary">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1 card-primary">
            <i class="fas fa-shopping-bag card-icon col-green"></i>
            <div class="card-wrap">
                <div class="padding-20">
                    <div class="text-right">
                        <h3 class="font-light mb-0">
                            <i class="ti-arrow-up text-success"></i> {{ $pesananbaru }}
                        </h3>
                        <span class="text-muted" style="font-family:Monaco;">Pesanan baru</span>
                    </div>
                    <p style="font-family:Monaco;" class="mb-3 text-muted pull-left text-sm">
                        <!-- <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 10%</span> -->
                        <a href="{{ route('list.order.new') }}" class="text-nowrap align-right pertama">Detail</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1 card-primary">
            <i class="fas fa-truck-loading card-icon col-green"></i>
            <div class="card-wrap">
                <div class="padding-20">
                    <div class="text-right">
                        <h3 class="font-light mb-0">
                            <i class="ti-arrow-up text-success"></i> {{ $hitung_dikemas }}
                        </h3>
                        <span class="text-muted" style="font-family:Monaco;">Siap dikirim </span>
                    </div>
                    <p style="font-family:Monaco;" class="mb-3 text-muted pull-left text-sm">
                        <!-- <span class="col-orange mr-2"><i class="fa fa-arrow-down"></i> 5%</span> -->
                        <a href="{{ route('list.order.new') }}" class="text-nowrap align-right">Detail</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1 card-primary">
            <i class="fas fa-truck-moving card-icon col-green"></i>
            <div class="card-wrap">
                <div class="padding-20">
                    <div class="text-right">
                        <h3 class="font-light mb-0">
                            <i class="ti-arrow-up text-success"></i> {{ $hitung_dikirim }}
                        </h3>
                        <span class="text-muted" style="font-family:Monaco;">Sedang dikirim</span>
                    </div>
                    <p style="font-family:Monaco;" class="mb-3 text-muted pull-left text-sm">
                        <!-- <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 24%</span> -->
                        <a href="{{ route('list.order.new') }}" class="text-nowrap align-right">Detail</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1 card-primary">
            <i class="fas fa-check-circle card-icon col-green"></i>
            <div class="card-wrap">
                <div class="padding-20">
                    <div class="text-right">
                        <h3 class="font-light mb-0">
                            <i class="ti-arrow-up text-success"></i> {{ $total }}
                        </h3>
                        <span class="text-muted" style="font-family:Monaco;">Pesanan Selesai</span>
                    </div>
                    <p style="font-family:Monaco;" class="mb-3 text-muted pull-left text-sm">
                        <!-- <span class="col-orange mr-2"><i class="fa fa-arrow-down"></i> 7.5%</span> -->
                        <a href="{{ route('list.order.new') }}" class="text-nowrap align-right">Detail</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-8 kedua">
        <div class="card card-primary">
            <div class="card-header">
                {{-- <h4 style="font-family:Monaco;">Statistik </h4> --}}
                <div class="col-6">
                    <div class="form-group">
                        <label for="filter">Filter Tahun</label>
                        <select class="form-control form-control-sm" id="filter" data='x' onchange="byTahun(this.value, this.data)" autocomplete="off">
                            <option value="" selected>--Semua--</option>
                            @foreach ($test as $tahun)
                                <option value="{{$tahun->tahun}}">{{$tahun->tahun}}</option>
                            @endforeach
                        </select>
                    </div>
                    <a href="" id="btn_refresh" class="btn btn-success d-none"> <i class="fas fa-rotate-right"></i> Refresh</a>
                </div>
            </div>
            <div class="card-body">
                <img src="{{ asset('assets\img\loading2.gif') }}" alt="" class="img-load-bersih d-none" style="margin-left: 30%">
                <div id="statis" style="width: 100%;height:350%;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4 kedua">
        <div class="card card-primary">
            <div class="card-header">
                <h4 style="font-family:Monaco;">Produk terlaris</h4>
            </div>
            <div class="card-body">
                @foreach ($browsers as $pt)
                <div class="m-b-20">
                    <div class="text-small float-right font-weight-bold text-muted">{{$pt->value}}</div>
                    <div class="font-weight-bold">{{$pt->name}}</div>
                    <div class="progress" data-height="5" style="height: 5px;">
                        <div class="progress-bar l-bg-purple" role="progressbar" data-width="{{$pt->value}}%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                    </div>
                </div>
                @endforeach
                {{-- <div id="produk" style="width: 100%;height:350%;"></div> --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('admin.js')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="{{ asset('assets/bundles/chartjs/chart.min.js') }}"></script>
<script src="{{ asset('assets/bundles/echart/echarts.js') }}"></script>
<script src="{{ asset('assets/bundles/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/Tour/tour.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script>
    const rupiah = (number) => {
        return new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR"
        }).format(number);
    }

    //function rupiah
    function rubah(angka) {
        var reverse = angka.toString().split('').reverse().join(''),
            ribuan = reverse.match(/\d{1,3}/g);
        ribuan = ribuan.join(',').split('').reverse().join('');
        return ribuan;
    }

    // filter pendapatan
    $('.input-daterange').datepicker({
            todayBtn: 'linked',
            format: 'yyyy-mm-dd',
            autoclose: true
        });

    $('#min, #max').change(function () {
        var from_date = $('#min').val();
        var to_date = $('#max').val();
    });

    $('#filter_kotor').click(function(){
        var from_date = $('#min').val();
        var to_date = $('#max').val();
        if (from_date != '' && to_date != '') {
            p_kotor(from_date, to_date);
        } else {
            alert('Masukkan tanggal!');
        }
    });
    
    $('#filter_bersih').click(function(){
        var from_date = $('#min1').val();
        var to_date = $('#max1').val();
        if (from_date != '' && to_date != '') {
            p_bersih(from_date, to_date);
        } else {
            alert('Masukkan tanggal!');
        }
    });

    $('#filter_kotor_3v').change(function(){
        var x = $('#filter_kotor_3v').val();
        if (x == 'today') {
            f_day_kotor();
        } else if(x == 'week') {
            f_week_kotor();
        } else if(x == 'month'){
            f_month_kotor();
        } else if(x == 'year'){
            f_year_kotor();
        }else if(x == 'all'){
            f_all_kotor();
        }
    })

    $('#filter_bersih_3v').change(function(){
        var x = $('#filter_bersih_3v').val();
        if (x == 'today') {
            f_day_bersih();
        } else if(x == 'week') {
            f_week_bersih();
        } else if(x == 'month'){
            f_month_bersih();
        } else if(x == 'year'){
            f_year_bersih();
        } else if(x == 'all'){
            f_all_bersih();
        }
    })

    function p_kotor(from_date, to_date) {
            $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    from_date: from_date,
                    to_date: to_date
                },
                beforeSend: function(){
                    $('#kotor').text('loading...');
                },
                success: function(res){
                    // console.log(res);
                    $("#kotor").text("Rp." + rubah(res[0]));
                }
            })
        }
    function p_bersih(from_date, to_date) {
            $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    from_date: from_date,
                    to_date: to_date
                },
                beforeSend: function(){
                    $('#bersih').text('loading...');
                },
                success: function(res){
                    console.log(res);
                    $("#bersih").text("Rp." + rubah(res[1]));
                }
            })
        }

    // filter grafik penjualan bersih
    function byTahun(tahun, data){
            $.ajax({
                url: "{{ route('home.admin') }}",
                type: 'GET',
                data:{
                    tahun: tahun
                },
                beforeSend: function(){
                    $('#statis').addClass('d-none');
                    $('.img-load-bersih').removeClass('d-none');
                },
                success: function(response){
                    var myChart = echarts.init(document.getElementById('statis'));
                    var data = response;
                    var option = {
                    color: ['#80FFA5', '#00DDFF', '#37A2FF', '#FF0087', '#FFBF00'],
                    title: {
                        text: 'Grafik Penjualan Bersih',
                        subtext: 'data perbulan',
                        left: 'center'
                    },
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                        }
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                        type: 'category',
                        boundaryGap: false,
                        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                        }
                    ],
                    yAxis: [
                        {
                        type: 'value'
                        }
                    ],
                    series: [
                        {
                        name: 'Rp',
                        type: 'line',
                        stack: 'Total',
                        smooth: true,
                        lineStyle: {
                            width: 0
                        },
                        showSymbol: false,
                        areaStyle: {
                            opacity: 0.8,
                            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
                            {
                                offset: 0,
                                color: 'rgba(128, 255, 165)'
                            },
                            {
                                offset: 1,
                                color: 'rgba(1, 191, 236)'
                            }
                            ])
                        },
                        emphasis: {
                            focus: 'series'
                        },
                        data: data
                        },
                    ]
                    };

                    // Display the chart using the configuration items and data just specified.
                    myChart.setOption(option);
                    $('#statis').removeClass('d-none');
                    $('.img-load-bersih').addClass('d-none');
                }
            })
    }

    // -- kotor
    function f_day_kotor() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    today: 'today',
                },
                beforeSend: function(){
                    $('#kotor').addClass('d-none');
                    $('.img-load').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].gros == null) {
                        res[0].gros = 0;
                    }
                    $('.img-load').addClass('d-none');
                    $('#kotor').removeClass('d-none');
                    $("#kotor").text("Rp." + rubah(res[0].gros));
                }
        })
    }

    function f_week_kotor() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    week: 'week',
                },
                beforeSend: function(){
                    $('#kotor').addClass('d-none');
                    $('.img-load').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].gros == null) {
                        res[0].gros = 0;
                    }
                    $('.img-load').addClass('d-none');
                    $('#kotor').removeClass('d-none');
                    $("#kotor").text("Rp." + rubah(res[0].gros));
                }
            })
    }

    function f_month_kotor() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    month: 'month',
                },
                beforeSend: function(){
                    $('#kotor').addClass('d-none');
                    $('.img-load').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].gros == null) {
                        res[0].gros = 0;
                    }
                    $('.img-load').addClass('d-none');
                    $('#kotor').removeClass('d-none');
                    $("#kotor").text("Rp." + rubah(res[0].gros));
                }
            })
    }

    function f_year_kotor(params) {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    year: 'year',
                },
                beforeSend: function(){
                    $('#kotor').addClass('d-none');
                    $('.img-load').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].gros == null) {
                        res[0].gros = 0;
                    }
                    $('.img-load').addClass('d-none');
                    $('#kotor').removeClass('d-none');
                    $("#kotor").text("Rp." + rubah(res[0].gros));
                }
            }) 
    }

    function f_all_kotor() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    all: 'all',
                },
                beforeSend: function(){
                    $('#kotor').addClass('d-none');
                    $('.img-load').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].gros == null) {
                        res[0].gros = 0;
                    }
                    $('.img-load').addClass('d-none');
                    $('#kotor').removeClass('d-none');
                    $("#kotor").text("Rp." + rubah(res[0].gros));
                }
            })
    }

    // --berish
    function f_day_bersih() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    today: 'today',
                },
                beforeSend: function(){
                    $('#bersih').addClass('d-none');
                    $('.img-load-bersih').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].total == null) {
                        res[0].total = 0;
                    }
                    $('.img-load-bersih').addClass('d-none');
                    $('#bersih').removeClass('d-none');
                    $("#bersih").text("Rp." + rubah(res[0].total));
                }
            })
    }

    function f_week_bersih() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    week: 'week',
                },
                beforeSend: function(){
                    $('#bersih').addClass('d-none');
                    $('.img-load-bersih').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].total == null) {
                        res[0].total = 0;
                    }
                    $('.img-load-bersih').addClass('d-none');
                    $('#bersih').removeClass('d-none');
                    $("#bersih").text("Rp." + rubah(res[0].total));
                }
            })
    }

    function f_month_bersih() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    month: 'month',
                },
                beforeSend: function(){
                    $('#bersih').addClass('d-none');
                    $('.img-load-bersih').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].total == null) {
                        res[0].total = 0;
                    }
                    $('.img-load-bersih').addClass('d-none');
                    $('#bersih').removeClass('d-none');
                    $("#bersih").text("Rp." + rubah(res[0].total));
                }
            })
    }

    function f_year_bersih() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    year: 'year',
                },
                beforeSend: function(){
                    $('#bersih').addClass('d-none');
                    $('.img-load-bersih').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].total == null) {
                        res[0].total = 0;
                    }
                    $('.img-load-bersih').addClass('d-none');
                    $('#bersih').removeClass('d-none');
                    $("#bersih").text("Rp." + rubah(res[0].total));
                }
            })
    }

    function f_all_bersih() {
        $.ajax({
                type: "GET",
                url: "{{ route('home.admin') }}",
                data: {
                    all: 'all',
                },
                beforeSend: function(){
                    $('#bersih').addClass('d-none');
                    $('.img-load-bersih').removeClass('d-none')
                },
                success: function(res){
                    console.log('ok');
                    if (res[0].total == null) {
                        res[0].total = 0;
                    }
                    $('.img-load-bersih').addClass('d-none');
                    $('#bersih').removeClass('d-none');
                    $("#bersih").text("Rp." + rubah(res[0].total));
                }
            })
    }
</script>
<script type="text/javascript">
    var myChart = echarts.init(document.getElementById('statis'));
    var data = {!! $d !!};
    var option = {
    color: ['#80FFA5', '#00DDFF', '#37A2FF', '#FF0087', '#FFBF00'],
    title: {
        text: 'Grafik Penjualan Bersih',
        subtext: 'data perbulan',
        left: 'center'
      },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
        type: 'cross',
        label: {
            backgroundColor: '#6a7985'
        }
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: [
        {
        type: 'category',
        boundaryGap: false,
        data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        }
    ],
    yAxis: [
        {
        type: 'value'
        }
    ],
    series: [
        {
        name: 'Rp',
        type: 'line',
        stack: 'Total',
        smooth: true,
        lineStyle: {
            width: 0
        },
        showSymbol: false,
        areaStyle: {
            opacity: 0.8,
            color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [
            {
                offset: 0,
                color: 'rgba(128, 255, 165)'
            },
            {
                offset: 1,
                color: 'rgba(1, 191, 236)'
            }
            ])
        },
        emphasis: {
            focus: 'series'
        },
        data: data
        },
    ]
    };
    // Display the chart using the configuration items and data just specified.
    myChart.setOption(option);
</script>
{{-- <script type="text/javascript">
    var myChart = echarts.init(document.getElementById('produk'));
    var e = {!! $dataPoints !!};
    // console.log(e)
    var option = {
    tooltip: {
        trigger: 'item'
    },
    legend: {
        orient: 'horizontal',
        left: 'center'
    },
    series: [
        {
        name: 'Access From',
        type: 'pie',
        radius: ['40%', '67%'],
        avoidLabelOverlap: false,
        label: {
            show: false,
            position: 'center'
        },
        data: e
        }
    ]
    };

    // Display the chart using the configuration items and data just specified.
    myChart.setOption(option);
</script> --}}

{{-- <script>
    const tour = new Tour("demo");
    tour.addStep('step1', {
        title: "Detail Informasi",
        text: "Klik untuk melihat detail masing masing card",
        hook: ".pertama",
        timer: 5000,
        onShow: function() {
            // Custom Function
        },
        onHide: function() {
            // Custom Function
        },
        buttons: [{
            text: "Selanjutnya",
            action: "tour.next()"
        }],
        links: [
            // {
            //     text: "More info?",
            //     href: "#"
            // }
        ]
    });
    tour.addStep('step2', {
        title: "Informasi Grafik",
        text: "Total Pengguna Reseller selama Sebulan",
        hook: ".kedua",
        timer: 5000,
        onShow: function() {
            // Custom Function
        },
        onHide: function() {
            // Custom Function
        },
        buttons: [{
                text: "Kembali",
                action: "tour.previous()"
            },
            {
                text: "Selanjutnya",
                action: "tour.next()"
            }
        ],
        links: [
            // {
            //     text: "More info?",
            //     href: "#"
            // }
        ]
    });
    tour.addStep('step3', {
        title: "Informasi Produk Terjual",
        text: "Daftar Produk yang terjual bisa di lihat di daftar ini",
        hook: ".ketiga",
        timer: 5000,
        onHide: function() {
            // Custom Function
        },
        buttons: [{
                text: "Kembali",
                action: "tour.previous()"
            },
            {
                text: "Selesai",
                action: "tour.stop()"
            }
        ],
        links: [
            // {
            //     text: "More info?",
            //     href: "#"
            // }
        ]
    });
    tour.start();
</script> --}}

@if(Session::has('success'))

<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>

@endif

<script>
    $(function() {
        $("#produk_terjual").DataTable({
            pageLength: 5
        });
    });
</script>

<script>
    var datase = <?php echo json_encode($datase); ?>;
    Highcharts.chart('totaluser', {
        chart: {
            type: 'column'
        },
        title: {
            text: '<p style="font-family:Monaco;">Chart Pengguna</p>'
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: '<p style="font-family:Monaco;"></p>'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} Pengguna</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'User',
            data: datase

        }]
    });
</script>
@endsection