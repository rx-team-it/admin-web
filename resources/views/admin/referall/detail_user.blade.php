@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('index.referall') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail User</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('order.index') }}">List Withdraw</a></div>
            <div class="breadcrumb-item">Detail User Referral</div>
        </div>
    </div>
    <main class="main">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Detail User Referral {{ !empty($user) ? $user->referral_code : '' }}
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <!-- BLOCK UNTUK MENAMPILKAN DATA PELANGGAN -->
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail User</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama User</th>
                                                <td>{{ !empty($user->user) ? $user->user->name : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{ !empty($user->user) ? $user->user->email : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Telp</th>
                                                <td>{{ !empty($user->user) ? $user->user->phone : '' }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Saldo</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Total Saldo</th>
                                                <td>{{ !empty($user->payment->komisi) ? 'Rp. '.number_format ($user->payment->komisi) : '' }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</section>
@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@endsection