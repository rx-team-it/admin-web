@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#{{ route('home.admin') }} class=" btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Kode Referall Saya</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-4">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">Kode Referall Saya</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <center>
                                    <!-- <h3 style="font-family:Monaco;">http://abbabill.com/register/reff/{{ $user->referral_code }}</h3> -->
                                    <div class="input-group">
                                        <input type="text" id="referral_code" name="referral_code" class="form-control" readonly value="{{ config('app.url')}}/register/reff/reseller/{{ $user->referral_code }}">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-default" onclick="copyToClipboard()"><i class="fa fa-copy"></i></button>
                                        </div>
                                    </div>
                                    <hr>
                                    <h6 style="font-family:Monaco;">Gunakan Referral Tersebut Untuk Menhubungkan Reseller Dengan Toko Anda</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-8">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title" style="font-family:Monaco;">Referall Reseller Yang Terhubung</h4>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama User</th>
                                    <th>Referral</th>
                                    <th>Tanggal Daftar</th>
                                    <th>Detail User</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                ?>
                                @foreach($user['get'] as $data)
                                <tr>
                                    <td>{{$no}}</td>
                                    <td>{{ !empty($data->user) ? $data->user->name : '' }}</td>
                                    <td>{{ !empty($data) ? $data->referral_code : ''}}</td>
                                    <td>{{ !empty($data) ? $data->created_at : ''}}</td>
                                    <td><a href="{{ route('detail.user.referral', $data->referral_code) }}" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a></td>
                                </tr>
                                <?php
                                $no++;
                                ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('admin.js')

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<script>
    $(function() {
        $("#example1").DataTable({
            pageLength: 5
        });
    });
</script>

<script>
    function copyToClipboard() {
        let copyText = document.getElementById('referral_code')
        document.execCommand('copy');
        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert("Copied the text: " + copyText.value);
    }
</script>
@endsection