@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#{{ route('home.admin') }}" class=" btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1 id="beranda">Kredit Top Up</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Kredit Top Up</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="pull-left">
                            Riwayat Top Up
                        </h4>
                        <div class="card-header-action">
                            <a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modal-topup"><i class="fa fa-plus"></i> Top Up</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p class="text-left">
                                    Saldo Sekarang:
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p class="text-right">
                                    @if ($data_saldo == null)
                                        Rp. 0,-
                                    @else
                                        @currency($data_saldo)
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-12">
                                <table id="riwayat-saldo" class="table table-bordered table-striped table-sm">
                                    <thead>
                                        <tr>
                                            <td>Total</td>
                                            <td>Tgl</td>
                                            <td>Status</td>
                                            <td>Aksi</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data_topup as $item)
                                            <tr>
                                                <td>@currency($item->nominal)</td>
                                                <td>{{$item->created_at}}</td>
                                                <td>
                                                    @if ($item->status == 0)
                                                        <span class="badge badge-warning">Proses</span>
                                                    @elseif ($item->status == 1)
                                                        <span class="badge badge-success">Berhasil</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->status == 0)
                                                        <a href="{{$item->url}}" class="btn btn-sm btn-warning">Bayar Sekarang</a>
                                                    @else
                                                        
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="pull-left">Riwayat Transaksi</h4>
                        <div class="card-header-action">
                            <input name="min" id="min" type="date" value=""> &nbsp;
                            <input name="max" id="max" type="date" value=""> 
                            <button id="filter" class="btn btn-sm btn-success">filter</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="saldo" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <td>Invoice</td>
                                    <td>Total</td>
                                    <td>Saldo</td>
                                    <td>Status</td>
                                    <td>Aksi</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data_order as $order)
                                <tr>
                                    <td><a href="">{{$order->invoice}}</a><br><small>{{$order->created_at}}</small></td>
                                    <td>@currency($order->total)</td>
                                    <td></td>
                                    <td>
                                        @if ($order->status == 5)
                                            <span class="badge badge-success">Pesanan Selesai</span>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>

{{-- modal topup --}}
<div class="modal fade" id="modal-topup">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('topup.saldo') }}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Top Up Saldo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="selectgroup selectgroup-pills">
                                <label class="selectgroup-item">
                                <input type="radio" name="nominal" id="nomi" value="2500000" class="selectgroup-input">
                                <span class="selectgroup-button">Rp. 2.500.000</span>
                                </label>
                                <label class="selectgroup-item">
                                <input type="radio" name="nominal" id="nomi" value="3000000" class="selectgroup-input">
                                <span class="selectgroup-button">Rp. 3.000.000</span>
                                </label>
                                <label class="selectgroup-item">
                                <input type="radio" name="nominal" id="nomi" value="5000000" class="selectgroup-input">
                                <span class="selectgroup-button">Rp. 5.000.000</span>
                                </label>
                                <input type="text" name="id_user" style="display: none;" value="{{Auth::user()->id}}">
                            </div>
                            <label class="selectgroup-item">
                                <label class="col-form-label">Isi Manual</label>
                                <input type="checkbox" class="" id="cekbok" />
                            </label>
                            <div id="manual1">
                                <label for="nominal" class="col-form-label">Nominal</label>
                                <input type="number" name="nominal1" onkeypress="if(this.value.length==8) return false;" onchange="changeHandler(this)" class="form-control" placeholder="Rp 2.500.000 s/d xxxxxx">
                            </div>
                            <br>
                            <div class="alert alert-primary alert-has-icon">
                                <div class="alert-icon"><i class="fa fa-info-circle"></i></div>
                                <div class="alert-body">
                                    <div class="alert-title">Info</div>
                                    Minimal Top Up Rp. 2.500.000 Untuk bulan pertama gabung, saldo akan berkurang dari transaksi berhasil.
                                </div>
                            </div>
                            {{-- <div class="form-group">
                                <label for="pembayaran">Pembayaran</label>
                                <select class="form-select">
                                    <option selected>== Pilih Pembayaran ==</option>
                                    <option id="jenis_pembayaran" value="QRIS">QRIS</option>
                                </select>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" id="bayar" class="btn btn-primary">Lanjut Pembayaran<i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('admin.js')
<script type="text/javascript">
// bikin minimal input topup 2,5jt
function changeHandler(val) {
    if (Number(val.value) < 2500000) {
        val.value = 2500000
    }
}

window.onload = function() {
    $("#manual1").hide();
    $(".selectgroup-pills").show();
}

$(document).ready(function() {
    $('#cekbok').click(function() {
        if( $(this).is(':checked')) {
            $("#manual1").show();
            $(".selectgroup-pills").hide();
        } else {
            $(".selectgroup-pills").show();
            $('input[name="nominal1"]').val('');
            $("#manual1").hide();
        }
    });
});

</script>

@endsection