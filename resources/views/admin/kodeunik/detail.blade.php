@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('index.kodeunik') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Riwayat Kode Unik</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Daftar Riwayat Kode Unik</div>
        </div>
    </div>
    <main class="main">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Detail pesanan {{ $order->invoice }}

                                    <!-- TOMBOL UNTUK MENERIMA PEMBAYARAN -->
                                    <!-- <div class="float-right"> -->
                                    <!-- TOMBOL INI HANYA TAMPIL JIKA STATUSNYA 1 DARI ORDER DAN 0 DARI PEMBAYARAN -->
                                    <!-- <a href="#" class="btn btn-primary btn-sm">Terima Pembayaran</a>
                                    </div> -->
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <!-- BLOCK UNTUK MENAMPILKAN DATA PELANGGAN -->
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pelanggan</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pelanggan</th>
                                                <td>{{ $order->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Telp</th>
                                                <td>{{ $order->phone }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>{{ $order->address }}</td>
                                            </tr>
                                            <tr>
                                                <th>Order Status</th>
                                                <td>{!! $order->status_label !!}</td>
                                            </tr>
                                            <tr>
                                                <th>Pesan</th>
                                                <td>{{ $order->message }}</td>
                                            </tr>
                                            <!-- FORM INPUT RESI HANYA AKAN TAMPIL JIKA STATUS LEBIH BESAR 1 -->
                                            <tr>
                                                <th>Nomor Resi</th>
                                                @if($order->status == 3)
                                                <td>
                                                    <form action="{{ route('input.resi', $order->id) }}" method="post">
                                                        @csrf
                                                        <div class="input-group">
                                                            <input type="hidden" name="order_id" value="#">
                                                            <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" required>
                                                            <div class="input-group-append">
                                                                <button class="btn btn-primary" type="submit">Kirim</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                                @else
                                                <td>
                                                    {{ !empty($order) ? $order->tracking_number : '' }}
                                                </td>
                                                @endif
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Pembayaran</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Pengirim</th>
                                                <td>{{ $order->name }}</td>
                                            </tr>
                                            <tr>
                                                <th>Bank Tujuan</th>
                                                <td>{{ $order->bank }}</td>
                                            </tr>
                                            <tr>
                                                <th>Tanggal Transfer</th>
                                                <td>{{ $order->date }}</td>
                                            </tr>
                                            <tr>
                                                <th>Bukti Pembayaran</th>
                                                @if($order->status == null)
                                                <td>
                                                    <p style="font-family:Monaco;">Belum Melakukan Pembayaran</p>
                                                </td>
                                                @else
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/transfer/'. $order->confirm_payment->bukti_transfer)}}" target="_blank">Lihat</a>
                                                </td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>
                                                    @if ($order->status == 0)
                                                    <p style="font-family:Monaco;">Belum Melakukan Pembayaran</p>
                                                    @else
                                                    <p style="font-family:Monaco;">Sudah Bayar</p>
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                        @if ($order->status == 0)
                                        <h5 class="text-center" style="font-family:Monaco;">Belum Konfirmasi Pembayaran</h5>
                                        @else
                                        <h5 class="text-center" style="font-family:Monaco;">Sudah Bayar</h5>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="font-family:Monaco;">Detail Produk</h4>
                                        <table class="table table-borderd table-hover">
                                            <tr>
                                                <th>Produk</th>
                                                <th>Quantity</th>
                                                <th>Harga</th>
                                                <th>Subtotal</th>
                                                <th>Aksi</th>
                                            </tr>
                                            @foreach($order['product'] as $product)
                                            <tr>
                                                <td>{{ !empty($product) ? $product->name_product : 'Kosong' }}</td>
                                                <td>{{ !empty($product) ? $product->qty : 'Kosong' }}</td>
                                                <td>{{ !empty($product) ? $product->price : 'Kosong' }}</td>
                                                <td>{{ !empty($product) ? $product->price * $product->qty : 'Kosong' }}</td>
                                                <td>
                                                    @if ($order->status == 0)
                                                    <a class="btn btn-warning btn-sm" href="{{ route('order.gagal', $order->invoice) }}"><i class="fas fa-exclamation-circle"></i> Tolak Orderan</a>
                                                    @elseif($order->status == 5)
                                                    <p>SELESAI</p>
                                                    @elseif($order->status == 1)
                                                    <div class="dropdown d-inline mr-2">
                                                        <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            @if($order->status == 2)
                                                            Dikemas
                                                            @elseif($order->status == 3)
                                                            Dikirim
                                                            @else
                                                            Select Action
                                                            @endif
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            @if($order->status == 1)
                                                            <a class="dropdown-item" href="{{ route('order.dikemas', $order->invoice) }}">Dikemas</a>
                                                            <a class="dropdown-item" href="{{ route('order.gagal', $order->invoice) }}">Tolak Order</a>
                                                            @else
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="col-md-12">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</section>
@endsection

@section('admin.js')

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

@endsection