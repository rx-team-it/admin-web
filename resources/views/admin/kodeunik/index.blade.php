@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Beranda</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Daftar Riwayat Kode Unik</div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">Daftar Riwayat Kode Unik</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tbl" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>No</th>
                                <th>Kode Unik</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                                @foreach ($r as $item)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td> {{ 'Rp. '.number_format($item->kode_unik ) }},- <br> <small><strong>invoice: {{ $item->invoice }}</strong> tgl.buat {{ $item->created_at }}</small></td>
                                    <td>
                                        {{--<a href="{{ url('kodeunik/detail/'.$item->invoice) }}" class="edit btn btn-secondary btn-md edit-post" data-toggle="tooltip" data-placement="top" title="Lihat"><i class="fa fa-eye"></i></a>--}}
                                        <a href="{{ route('order.pembayaran', $item->invoice) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Detail"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            <tfoot>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <hr>

                                        <strong>Total Kode Unik: {{ 'Rp. '.number_format($tb->kode)}},-</strong>

                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tfoot>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Grafik Jumlah Kode Unik</h4>
                </div>
                <div class="card-body">
                    <div id="chart" class="chartsh chart-shadow2"></div>
                </div>
            </div>
        </div> --}}
    </div>
</section>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<!-- JS Libraies -->
<script src="{{asset('assets/bundles/echart/echarts.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tbl').DataTable({
            "pageLength": 5,
            "lengthMenu": [5, 20, 50, 75, 100]
        });
    });
</script>
<script type="text/javascript">
    // based on prepared DOM, initialize echarts instance
    var myChart = echarts.init(document.getElementById('chart'));
    var e = <?php echo json_encode($datase); ?>;
    var tb = <?php echo json_encode($tb->kode); ?>;
    // specify chart configuration item and data
    var option = {
        title: {
            text: "Jumlah: Rp." + tb
        },
        tooltip: {},
        legend: {
            data: ['Kode Unik']
        },
        xAxis: {
            data: ['Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ]
        },
        yAxis: {},
        series: [{
            name: 'Kode Unik',
            type: 'bar',
            data: e
        }]
    };

    // use configuration item and data specified to show chart
    myChart.setOption(option);
</script>
@endsection