@extends('admin.layouts.admin.index')

@section('admin.content')
<style>
    .img-thumbnail:hover{
        cursor: pointer;
    }
</style>
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('index.verifikasi') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Permintaan Verifikasi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('index.verifikasi') }}">List Verifikasi</a></div>
            <div class="breadcrumb-item">Detail Verifikasi</div>
        </div>
    </div>
    <main class="main">
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="font-family:Monaco;">
                                    Detail Permintaan
                                </h4>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <!-- BLOCK UNTUK MENAMPILKAN DATA PELANGGAN -->
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail User</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama</th>
                                                <td>{{ !empty($history->user) ?  $history->user->name : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td>{{ !empty($history->user) ?  $history->user->email : '' }}</td>
                                            </tr>
                                            {{--<tr>
                                                <th>Telp</th>
                                                <td>{{ !empty($history->user) ?  $history->user->phone : '' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>{{ !empty($history->user) ?  $history->user->address : '' }}<br>
                                                    {{ !empty($history->user->countrys) ?  $history->user->countrys->name : '' }}<br>
                                                    {{ !empty($history->user->states) ?  $history->user->states->name : '' }}<br>
                                                    {{ !empty($history->user->citys) ?  $history->user->citys->name : '' }}
                                                </td>
                                            </tr>--}}
                                            <!-- FORM INPUT RESI HANYA AKAN TAMPIL JIKA STATUS LEBIH BESAR 1 -->
                                            <!-- <tr>
                                            <th>Nomor Resi</th>
                                            <td>
                                                <form action="#" method="post">
                                                    @csrf
                                                    <div class="input-group">
                                                        <input type="hidden" name="order_id" value="#">
                                                        <input type="text" name="tracking_number" placeholder="Masukkan Nomor Resi" class="form-control" required>
                                                        <div class="input-group-append">
                                                            <button class="btn btn-secondary" type="submit">Kirim</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr> -->
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h4 style="font-family:Monaco;">Detail Toko</h4>
                                        <table class="table table-bordered">
                                            <tr>
                                                <th width="30%">Nama Toko</th>
                                                <td>{{ !empty($history) ? $history->name_site : '' }}</td>
                                            </tr>
                                            {{-- <tr>
                                                <th>Alamat</th>
                                                <td>{{ !empty($history->address) ? $history->address->address : '' }}<br>
                                            {{ !empty($history->address->countrys) ? $history->address->countrys->name : '' }}<br>
                                            {{ !empty($history->address->states) ? $history->address->states->name : '' }}<br>
                                            {{ !empty($history->address->citys) ? $history->address->citys->name : '' }}
                                            </td>
                                            </tr>
                                            <tr>
                                                <th>Status Toko</th>
                                                <td>{!! !empty($history) ? $history->status_label_verifikasi : '' !!}</td>
                                            </tr> --}}
                                        </table>
                                        @if($history->status_verifikasi == 0|2)
                                        <h5 style="font-family:Monaco;" class="text-center">Belum Konfirmasi Verifikasi</h5>
                                        @else
                                        <h5 style="font-family:Monaco;" class="text-center">Verikasi Diterima</h5>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="font-family:Monaco;">Detail Foto</h4>
                                        <table class="table table-borderd table-hover">
                                            <tr>
                                                <th>Nik Ktp</th>
                                                <th>Foto Ktp</th>
                                                <th>Foto Selfie</th>
                                                <th>Pas Foto</th>
                                            </tr>
                                            <tr>
                                                <td>{{ !empty($history) ? $history->nik_ktp : '' }}</td>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/verifikasi/foto_ktp/' . $history->foto_ktp)}}" target="_blank">
                                                        <img alt="image" src="{{Storage::disk('s3')->url('public/images/verifikasi/foto_ktp/' . $history->foto_ktp)}}" width="220px" height="200px" class="img-thumbnail">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{Storage::disk('s3')->url('public/images/verifikasi/pas_foto/' . $history->pas_foto)}}" target="_blank">
                                                        <img alt="image" src="{{Storage::disk('s3')->url('public/images/verifikasi/pas_foto/' . $history->pas_foto)}}" width="220px" height="200px" class="img-thumbnail">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="{{ Storage::disk('s3')->url('public/images/verifikasi/foto_selfie/'. $history->foto_selfie) }}" target="_blank">
                                                        <img alt="image" src="{{Storage::disk('s3')->url('public/images/verifikasi/foto_selfie/' . $history->foto_selfie)}}" width="220px" height="200px" class="img-thumbnail">
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <h4 style="font-family:Monaco;">Detail Permintaan</h4>
                                        <table class="table table-borderd table-hover">
                                            <tr>
                                                <th>Nama Toko</th>
                                                <th>Tanggal</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                            <tr>
                                                <td>{{ !empty($history) ? $history->name_site : '' }}</td>
                                                <td>{{ !empty($history) ? $history->created_at : '' }}</td>
                                                <td>{!! !empty($history) ? $history->status_label_verifikasi : '' !!}</td>
                                                @if($history->status_verifikasi == 1)
                                                @else
                                                <td>
                                                    <div class="dropdown d-inline mr-2">
                                                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Pilih Aksi
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="{{ route('diterima.verifikasi',$history->id) }}">Diterima</a>
                                                            <a class="dropdown-item" href="{{ route('ditolak.verifikasi',$history->id) }}">Ditolak</a>
                                                        </div>
                                                    </div>
                                                </td>
                                                @endif
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <img alt="image" src="{{Storage::disk('s3')->url('public/images/verifikasi/foto_ktp/' . $history->foto_ktp)}}" width="90%">
    
</div>
@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@endsection