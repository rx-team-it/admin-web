@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Tiketing</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Tiketing</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">List Keluhan</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">Semua</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">Verifikasi Berhasil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">Verifikasi Di Tolak</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            <div class="table-responsive">
                                <table id="semua" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Toko</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($verifikasi as $store)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ $store->name_site }}</td>
                                            <td>{{ $store->created_at }}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div class="table-responsive">
                                <table id="dibalas" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Toko</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($verifikasi_diterima as $store)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ $store->name_site }}</td>
                                            <td>{{ $store->created_at }}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
                            <div class="table-responsive">
                                <table id="close" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Toko</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($verifikasi_ditolak as $store)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{ $store->name_site }}</td>
                                            <td>{{ $store->created_at }}</td>
                                            <td>
                                                <a href="#" class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

@if(Session::has('error'))
<script>
    swal("Gagal", "{!!Session::get('error')!!}", "error", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script>
    $(function() {
        $("#close").DataTable({
            pageLength: 4,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });
        $('#semua').DataTable({
            pageLength: 4,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });
        $('#dibalas').DataTable({
            pageLength: 4,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
        });
    });
</script>

@endsection