@extends('admin.layouts.admin.index')

@section('admin.css')

@endsection

@section('admin.content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h4 style="font-family:Monaco;">Ubah Password</h4>
                {{-- <div class="card-header-action">
                    <a href="{{ route('histori-vendor.penjualan') }}" class="btn btn-primary">Lihat</a>
                </div> --}}
            </div>
            <div class="card-body justify-content-center">
                <form action="{{route('update.ubah.password')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Password lama</label>
                            <input type="password" class="form-control" name="password_lama" id="password_lama" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Password baru</label>
                            <input type="password" class="form-control" name="password" id="baru" required minlength="6">
                            {{-- <span id="errorMsg" style="display:none;">minimal 6 karakter ya kak</span> --}}
                            <br>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Konfirmasi password baru</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required id="baru_1" minlength="6">
                            <input type="checkbox" onclick="myFunction()" class="mt-3">Lihat
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary" type="submit" id="simpan">Simpan</button>
                            {{-- <button class="btn btn-secondary" type="reset">Reset</button> --}}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('admin.js')

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if(Session::has('success'))
<script>
  swal("Berhasil", "{!!Session::get('success')!!}", "success", {
    button: "OK",
  });
</script>
@endif
@if(Session::has('failed'))
<script>
  swal("Gagal", "{!!Session::get('failed')!!}", "error", {
    button: "OK",
  });
</script>
@endif

<script>
function myFunction() {
    var x = document.getElementById("baru");
    var x2 = document.getElementById("password_lama")
    var x3 = document.getElementById("password_confirmation");
    if (x.type === "password") {
        x.type = "text";
        x2.type = "text";
        x3.type = "text";
    } else {
        x.type = "password";
        x2.type = "password";
        x3.type = "password";
    }
}
// $("#baru" ).keyup(function() {
//     if($('#baru').val() > 6 ){
//         $('#errorMsg').show();
//         $('#simpan').prop( "disabled", false );
//     } else{
//         $('#errorMsg').hide();
//         $('#simpan').prop( "disabled", true );

//     }
// });
</script>

@endsection