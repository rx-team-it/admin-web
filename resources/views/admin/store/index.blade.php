@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/owlcarousel2/dist/assets/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Profile Toko</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
            <div class="breadcrumb-item">Profil Toko</div>
        </div>
    </div>
    <div class="section-body">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{ $error }}</div>
        @endforeach
        @endif
        <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Sampul Toko</h4>
                    </div>
                    <div class="card-body">
                        <div class="owl-carousel owl-theme slider" id="slider2">
                            @if(count($slider) == 0)
                            <div>
                                <img alt="image" width="300px" height="320px" src="{{ asset('assets/img/image-gallery/9.png') }}">
                                <div class="slider-caption">
                                    <div class="slider-title">Kosong</div>
                                    <div class="slider-description">Kosong</div>
                                </div>
                            </div>
                            @else
                            @foreach($slider as $slider)
                            <div>
                                <img alt="image" src="{{Storage::disk('s3')->url('public/images/BannerImage/' . $slider->image)}}" width="300px" height="320px">
                                <div class="slider-caption">
                                    <div class="slider-title">{{ $slider->type_content }}</div>
                                    <div class="slider-description">{{ $slider->description }}</div>
                                    <div class="float-right">
                                        <a href="#" data-id="{{ $slider->id }}" class="btn btn-warning btn-sm swal-confirm">
                                            <form action="{{ route('banner.toko', $slider->id) }}" id="delete{{ $slider->id }}" method="POST">
                                                @csrf
                                                @method('delete')
                                            </form>
                                            Hapus Gambar
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                        <a href="{{ route('slider.create') }}" data-toggle="modal" data-target="#tambahslider" class="btn btn-icon icon-left btn-sm btn-primary "><i class="fas fa-plus"></i> Foto sampul profil toko</a>
                        @if ($banner_produk > 0)
                            @foreach ($get_banner_produk as $item)
                                <a href="" data-toggle="modal" data-target="#ubahBanner" data-id="{{$item->id}}" class="btn btn-icon icon-left btn-sm btn-primary "><i class="fa fa-edit"></i> Ubah banner produk</a>
                            @endforeach
                        @else
                            <a href="" data-toggle="modal" data-target="#tambahBanner" class="btn btn-icon icon-left btn-sm btn-primary "><i class="fas fa-plus"></i> Banner produk</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-4">
                <div class="card author-box card-primary">
                    <div class="card-body">
                        <h6>Foto Toko</h6>
                        <div class="author-box-center">
                            @if($store->logo == null)
                            <img alt="image" width="80px" height="80px" src="{{ asset('assets/img/users/user-1.png') }}" class="rounded-circle author-box-picture">
                            @else
                            <img alt="image" src="{{Storage::disk('s3')->url('public/images/profiletoko/' . $store->logo)}}" width="80px" height="80px" class="rounded-circle author-box-picture">
                            @endif
                            <div class="clearfix"></div>
                            <br>
                            <div class="author-box-name">
                                <a href="#">{{ !empty($store) ? $store->name_site : '' }}</a>
                            </div>
                            <br>
                            {{-- <div class="author-box-job">Vendor</div> --}}
                        </div>
                        <br>
                        <div class="text-center">
                            <button type="button" class="form-control btn btn-primary btn-sm" data-toggle="modal" data-target="#edittoko" data-whatever="{{ $store->id }}" data-name_site="{{ $store->name_site }}" data-description="{{ $store->description }}" data-contact_phone="{{ $store->contact_phone }}" data-contact_email="{{ $store->contact_email }}" style="border-radius: 22px;">Ubah Profil Toko</button>
                            @if($address == null)
                            <a href="#" data-toggle="modal" data-target="#tambahalamat" class="btn btn-icon icon-left btn-sm btn-primary ml-2"><i class="fas fa-plus"></i> Tambah Alamat</a>
                            @else
                            {{--<a href="{{ route('alamat.edit', $address->id) }}" class="btn btn-primary btn-sm">Ubah Alamat Pengambilan barang</a>--}}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Alamat Pengambilan Barang</h4>
                        <div class="card-header-action">
                            <a href="{{ route('alamat.edit', $address->id) }}" class="btn btn-primary btn-sm">Ubah</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="py-1">
                            <p class="clearfix">
                                <span class="float-left">
                                    Nama Toko
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->name) ? $address->name: '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Alamat
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->address) ? $address->address : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Provinsi
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->countrys->province_name) ? $address->countrys->province_name : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Kota
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->city) ? $address->city : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Kecamatan
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->district) ? $address->district : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Kelurahan
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->urban) ? $address->urban : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Kodepos
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($address->postal_code) ? $address->postal_code : '' }}
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Mulai Mendaftar
                                </span>
                                <span class="float-right text-muted">
                                    {{ !empty($store) ? $store->created_at : '' }}
                                </span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-8">
                <div class="card card-primary">
                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">Profile Toko</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Edit</a>
                            </li> -->
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="row">
                                    <div class="col-md-4 col-6 b-r">
                                        <strong>Nama Toko</strong>
                                        <br>
                                        <p class="text-muted">
                                            {{ !empty($store) ? $store->name_site : '' }}
                                        </p>
                                    </div>
                                    <div class="col-md-4 col-6 b-r">
                                        <strong>No Hp</strong>
                                        <br>
                                        <p class="text-muted">
                                            {!! !empty($store) ? $store->contact_phone : '' !!}</p>
                                    </div>
                                    <div class="col-md-4 col-6 b-r">
                                        <strong>Email</strong>
                                        <br>
                                        <p class="text-muted">
                                            {!! !empty($store) ? $store->contact_email : '' !!}</p>
                                    </div>
                                </div>
                                <strong>Tentang Toko</strong>
                                <p class="m-t-10">
                                    {!! !empty($store) ? $store->description : '' !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="edittoko" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('update.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Profile Toko</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama Toko : </label>
                        <input type="text" name="name_site" class="form-control" id="name_site">
                    </div>
                    {{-- <div class="form-group">
                        <p>Note: Anda Harus Menambahkan Alamat Terlebih Dahulu</p>
                        <label>Alamat</label>
                        @foreach($alamat as $alamat)
                        <input type="hidden" name="id_address" id="id_address" value="{{ $alamat->id }}">
                        <textarea class="form-control" name="address_id" id="address_id" rows="3">{{ $alamat->address }}</textarea>
                        @endforeach
                    </div> --}}
                    <div class="form-group">
                        <label for="contact_phone" class="col-form-label">Phone : </label>
                        {{-- <select class="form-control" name="contact_phone">
                            <option value="{{ $address->phone }}">{{ $address->phone }}</option>
                        </select> --}}
                        <input type="number" class="form-control" name="contact_phone" id="contact_phone" value="{{ $store->contact_phone }}">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email Toko : </label>
                        <input type="text" name="contact_email" class="form-control" id="contact_email">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Bio : </label>
                        <textarea name="description" id="ckeditor">{{ $store->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto : </label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="logo" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tambahslider" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method='post' action="{{ route('slider.create') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Gambar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Judul Banner : </label>
                        <input type="text" name="type_content" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Deskripsi Banner : </label>
                        <input type="text" name="description" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto Banner : </label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="image" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- modal tambah banner --}}
<div class="modal fade" id="tambahBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method='post' action="{{ route('banner.create') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Banner Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto Banner : </label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="image" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- modal ubah banner --}}
<div class="modal fade" id="ubahBanner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            @foreach ($get_banner_produk as $item)
            <form method='post' action="{{ route('banner.produk.ubah', $item->id) }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Banner Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img alt="image" src="{{Storage::disk('s3')->url('public/images/BannerImage/' . $item->image)}}" width="100%" height="60%">
                    @endforeach
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Ubah Foto Banner : </label>
                        <div class="custom-file">
                            <input type="text" id="id" name="id" hidden>
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="image" class="custom-file-input" id="customFile" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/owlcarousel2/dist/owl.carousel.min.js') }}"></script>
<script src="{{ asset('assets/js/page/owl-carousel.js') }}"></script>
<script src="{{ asset('assets/bundles/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/page/ckeditor.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif

<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('.swal-confirm').click(function(e) {
        id = e.target.dataset.id;
        swal({
                title: "Apakah Anda Yakin?",
                text: "Sekali Anda Hapus, Banner Akan Terhapus Permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // swal("Poof! Your imaginary file has been deleted!", {
                    //     icon: "success",
                    // });
                    $(`#delete${id}`).submit();
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    });
</script>

<script>
    $('#edittoko').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var name_site = button.data('name_site') // Extract info from data-* attributes
        var logo = button.data('logo') // Extract info from data-* attributes
        var contact_email = button.data('contact_email') // Extract info from data-* attributes
        var description = button.data('description') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Edit Profile Toko')
        modal.find('#recipient-name').val(recipient)
        modal.find('#name_site').val(name_site)
        modal.find('#logo').val(logo)
        modal.find('#contact_email').val(contact_email)
        modal.find('#description').val(description)
    })

    $('#ubahBanner').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')

        var modal = $(this)
        modal.find('#id').val(id)
    })
</script>

@endsection