@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('index.GroupReseller') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Komisi</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item active"><a href="{{ route('index.GroupReseller') }}">List Komisi Reseller</a></div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>List Master Komisi</h4>
                    </div>
                    <div class="card-body">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"> Tambah Master Komisi</i></button>
                        {{-- <a href="{{ route('add.komisi') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Master Komisi</a> --}}
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="table_reseller" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <th>Group</th>
                                    <th>Satuan</th>
                                    <th>Nominal Komisi</th>
                                    <th class="text-center">Aksi</th>
                                </thead>
                                <tbody>
                                    @foreach ($r as $items)
                                    <tr>
                                        <td>{{$items->id_group}}</td>
                                        <td>{{$items->satuan}}</td>
                                        <td>
                                            @if ($items->satuan == 'persentase')
                                                {{$items->nominal}}%
                                            @elseif ($items->satuan == 'harga')
                                                @currency($items->nominal)
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {{-- <button class="btn btn-primary btn-md edit-post" data-toggle="modal" data-target="#ubah" data-whatever="{{$items->id}}" data-name="{{$items->id}}">
                                                <i class="far fa-edit"> </i>
                                            </button> --}}
                                            <a href="{{ route('komisi.delete',$items->id) }}" class="btn btn-danger edit-post"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal add -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('add.komisi') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Master Komisi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sel1">Grup</label>
                        <select class="form-control" id="group" name="group">
                          <option>--- Pilih ---</option>
                          @foreach ($g as $item)    
                            <option value="{{ $item->nm_grup }}">{{ $item->nm_grup }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="satuan">Satuan :</label>
                        <select class="form-control" id="satuan" name="satuan">
                          <option value="harga">Harga</option>
                          <option value="persentase">Persentase (%)</option>
                        </select>
                      </div>
                    <div class="form-group">
                        <label class="col-form-label">Nominal : </label>
                        <input type="text" name="nominal" class="form-control" id="nominal">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal edit -->
<div class="modal fade" id="ubah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('edit.komisi',$r[0]->id) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Master Komisi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sel1">Grup</label>
                        <select class="form-control" id="group" name="group">
                          <option>--- Pilih ---</option>
                          @foreach ($g as $item)    
                            <option value="{{ $item->nm_grup }}">{{ $item->nm_grup }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="satuan">Satuan :</label>
                        <select class="form-control" id="satuan" name="satuan">
                            @foreach ($r as $item)
                            <option value="{{ $item->harga }}">{{ $item->harga }}</option>
                            @endforeach
                        </select>
                      </div>
                    <div class="form-group">
                        <label class="col-form-label">Nominal : </label>
                        <input type="text" name="nominal" class="form-control" id="nominal">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('admin.js')

<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@if(Session::has('error'))
<script>
    swal("Gagal!", "{!!Session::get('error')!!}", "error", {
        button: "OK",
    });
</script>
@endif

<script type="text/javascript">
    $('#table_reseller').DataTable({

    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#example').DataTable({
        });

        // Child # 1
        $("#subcat-id").depdrop({
            url: '/server/getSubcat.php',
            depends: ['cat-id']
        });
    
        // Child # 2
        $("#prod-id").depdrop({
            url: '/server/getProd.php',
            depends: ['cat-id', 'subcat-id']
        });
    });
</script>


@endsection