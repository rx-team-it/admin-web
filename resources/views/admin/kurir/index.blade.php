@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{asset('assets/bundles/pretty-checkbox/pretty-checkbox.min.css')}}">
<link rel="stylesheet"
    href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<style type="text/css">
#map {
  height: 400px;
  width: 450px;
}
</style>
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Daftar Kurir</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Daftar Kurir</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Daftar Kurir</h4>
                    {{-- @if (empty($kur)) --}}
                    {{-- <div class="card-header-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modal-kurir">
                            <i class="fa fa-plus-circle"></i> Kurir Baru
                        </a>
                    </div> --}}
                    {{-- @else --}}

                    {{-- @endif --}}
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="tbl" class="table table-hover table-bordered table-sm">
                            <thead>
                                <th>Nama Kurir</th>
                                <th class="text-center">Aktif</th>
                                {{-- <th class="text-right">Aksi</th> --}}
                            </thead>
                            <tbody>
                                @foreach ($kur as $item)
                                @csrf
                                <tr>
                                    <td>
                                        {{$item->capital}}
                                    </td>
                                    <td class="text-center">
                                        @if ($item->status_supersystem == null || $item->status_supersystem == "Aktif")
                                        <div class="pretty p-switch p-fill">
                                            <input type="hidden" name="nama" value="{{$item->nama}}">
                                            <input data-nama="{{$item->nama}}" onclick="myFunction()" class="toggle-class" type="checkbox"
                                                data-onstyle="success" data-offstyle="danger" data-toggle="toggle"
                                                data-on="Active" data-off="InActive"
                                                {{ $item->status ? 'checked' : '' }}>
                                            <div class="state p-success">
                                                <label></label>
                                            </div>
                                        </div>
                                        {{-- @if ($item->capital == 'GRAB')
                                            <br>
                                            <div id="pin">
                                                <a href="#" data-toggle="modal" data-target="#modalpin" data-id="{{$address->id}}" data-latitude="{{$address->latitude}}" data-longtitude="{{$address->longtitude}}" data-latdefault="-6.275521" data-longdefault="106.878732" data-pin="{{$address->pin_point}}" type="button"> <i class="fa fa-map-pin"></i> ATUR PIN POINT</a>
                                            </div>
                                        @endif --}}
                                        @elseif ($item->status_supersystem == "Non-Aktif")
                                            <div class="badge badge-danger">Non-Aktif</div>
                                        @else
                                            <div class="badge badge-warning">Maintenance</div>
                                        @endif
                                    </td>
                                    {{-- <td class="text-right"> --}}
                                        {{-- <a href="{{ route('hapus.kurir',$item->id) }}" class="edit btn btn-danger btn-md edit-post"><i class="fa fa-trash"></i></a> --}}
                                    {{-- </td> --}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{-- modal tambah kurir --}}
<div class="modal fade" id="modal-kurir">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('kurir.tambah') }}" autocomplete="off" enctype='multipart/form-data'>
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Buat Kurir Baru</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="nama">Nama Kurir</label>
                                <input type="text" name="nama" class="form-control" placeholder="jne, tiki, pos, wahana, jet, ide(ID Express)" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{-- modal --}}
<div class="modal fade" id="modalpin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Atur Pin Point</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{route('pinpoint.address')}}" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
                <input type="text" name="id" class="form-control" id="id" name="id" style="display: none;">
                <input id="location_latitude" name="lat" hidden>
                <input id="location_longitude" name="long" hidden>
                <input id="latdefault" name="latdefault" hidden>
                <input id="longdefault" name="longdefault" hidden>
                <div id="map"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection


@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl').DataTable({

        });
    });

</script>
<script>
    $(function () {
        $('.toggle-class').change(function () {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var nama = $(this).data('nama');
            let _token = "{{ csrf_token() }}"
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{ route('update.kurir') }}",
                data: {
                    'status': status,
                    'nama': nama,
                    _token: _token
                },
                success: function (data) {
                    
                }
            });
        })
    })

</script>
<script type="text/javascript">
function myFunction() {
    var x = document.getElementById("pin");

    if (x.style.display === "none") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGOg7oN2JcV2HFpsD0cTo7H1tVwNpM2Dw&callback=initMap&v=weekly&channel=2" async></script>
<script>
    //get_country
    $(document).ready(function() {

        var country_id = "{{$address->province}}";
        var state_id = "{{$address->city}}";
        var city_id = "{{$address->district}}";
        var urban_id = "{{$address->urban}}";
        var postal_code_id = "{{$address->postal_code}}";

        //GET STATE FROM DB
        $.ajax({
            url: "{{url('api/get-state')}}/" + country_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var state = response.data;
                console.log(state)
                var select = $("#get_state");
                select.empty();
                for (var i = 0; i < state.length; i++) {
                    if (state_id == state[i].value) {
                        select.append('<option selected value="' + state[i].value + '">' + state[i].label + '</option>');
                    } else {
                        select.append('<option value="' + state[i].value + '">' + state[i].label + '</option>');
                    }
                }
            }
        });

        //GET CITY
        $.ajax({
            url: "{{url('api/get-city')}}/" + state_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var city = response.data;
                console.log(city)
                var select = $("#get_city");
                select.empty();
                for (var i = 0; i < city.length; i++) {
                    if (city_id == city[i].value) {
                        select.append('<option selected value="' + city[i].value + '">' + city[i].label + '</option>');
                    } else {
                        select.append('<option value="' + city[i].value + '">' + city[i].label + '</option>');
                    }
                }
            }
        });

        //GET URBAN
        $.ajax({
            url: "{{url('api/get-urban')}}/" + city_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var urban = response.data;
                console.log(urban)
                var select = $("#get_urban");
                select.empty();
                for (var i = 0; i < urban.length; i++) {
                    if (urban_id == urban[i].value) {
                        select.append('<option selected value="' + urban[i].value + '">' + urban[i].label + '</option>');
                    } else {
                        select.append('<option value="' + urban[i].value + '">' + urban[i].label + '</option>');
                    }
                }
            }
        });

        //GET POSTAL CODE
        $.ajax({
            url: "{{url('api/get-postal')}}/" + urban_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var postal_code = response.data;
                console.log(postal_code)
                var select = $("#get_postal_code");
                select.empty();
                for (var i = 0; i < postal_code.length; i++) {
                    if (postal_code_id == postal_code[i].value) {
                        select.append('<option selected value="' + postal_code[i].value + '">' + postal_code[i].label + '</option>');
                    } else {
                        select.append('<option value="' + postal_code[i].value + '">' + postal_code[i].label + '</option>');
                    }
                }
            }
        });

    }); //document ready

    $('#get_country').on('change', function(e) {
        var country_id = $('#get_country').val();
        $.ajax({
            url: "{{url('api/get-state')}}/" + country_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var state = response.data;
                console.log(state);
                var select = $("#get_state");
                select.empty();
                select.append('<option value="">== Pilih Kota ==</option>');
                for (var i = 0; i < state.length; i++) {
                    select.append('<option value="' + state[i].value + '">' + state[i].label + '</option>');
                }
            }

        });
    });

    $('#get_state').on('change', function(e) {
        var state_id = $('#get_state').val();
        $.ajax({
            url: "{{url('api/get-city')}}/" + state_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var city = response.data;
                console.log(city)
                var select = $("#get_city");
                select.empty();
                select.append('<option value="">== Pilih Kabupaten ==</option>');
                for (var i = 0; i < city.length; i++) {
                    select.append('<option value="' + city[i].value + '">' + city[i].label + '</option>');
                }
            }
        });
    });

    $('#get_city').on('change', function(e) {
        var city_id = $('#get_city').val();
        $.ajax({
            url: "{{url('api/get-urban')}}/" + city_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var urban = response.data;
                console.log(urban)
                var select = $("#get_urban");
                select.empty();
                select.append('<option value="">== Pilih Kabupaten ==</option>');
                for (var i = 0; i < urban.length; i++) {
                    select.append('<option value="' + urban[i].value + '">' + urban[i].label + '</option>');
                }
            }
        });
    });

    $('#get_urban').on('change', function(e) {
        var urban_id = $('#get_urban').val();
        $.ajax({
            url: "{{url('api/get-postal')}}/" + urban_id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                var postal_code = response.data;
                console.log(postal_code)
                var select = $("#get_postal_code");
                select.empty();
                select.append('<option value="">== Pilih Kode Pos ==</option>');
                for (var i = 0; i < postal_code.length; i++) {
                    select.append('<option value="' + postal_code[i].value + '">' + postal_code[i].label + '</option>');
                }
            }
        });
    });

    // maps
    $('#modalpin').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var id = button.data('id')
        var pin = button.data('pin')
        var location_latitude = button.data('location_latitude')
        var location_longitude = button.data('location_longitude')
        // LONG LAT DARI DB
        var latitude = button.data('latitude')
        var longtitude = button.data('longtitude')
        // LONG LAT DEFAULT KALO BLM DI PIN
        var latdefault = button.data('latdefault')
        var longdefault = button.data('longdefault')
        var modal = $(this)

        modal.find('#id').val(id)
        modal.find('#pin').val(pin)

        if(pin != 0 ){
            modal.find('#location_latitude').val(latitude)
            modal.find('#location_longitude').val(longtitude)
        } else {
            modal.find('#latdefault').val(latdefault)
            modal.find('#longdefault').val(longdefault)
            
        }

        // LAT LONG DARI DB
        latitude = $('#location_latitude').val();
        longtitude = $('#location_longitude').val();
        // DEFAULT LAT LONG
        deflatitude = $('#latdefault').val();
        deflongtitude = $('#longdefault').val();
        

        if(pin != 0 ){
            Myzoom = 15;
            myLatLng = { lat: parseFloat(latitude), lng: parseFloat(longtitude) };
        } else {
            Myzoom = 6;
            myLatLng = { lat: parseFloat(deflatitude), lng: parseFloat(deflongtitude) };
        }
        
        map = new google.maps.Map(document.getElementById("map"), {
            zoom: Myzoom,
            center: myLatLng,
            streetViewControl: false,
            mapTypeControl: false,
        });

        new google.maps.Marker({
            position: myLatLng,
            map,
            title: "Your pin location",
        });

        geocoder = new google.maps.Geocoder();

        const submitButton = document.createElement("input");

        submitButton.type = "button";
        submitButton.value = "Cari";
        submitButton.classList.add("button", "button-primary");

        response = document.createElement("pre");
        response.id = "response";
        response.innerText = "";
        responseDiv = document.createElement("div");
        responseDiv.id = "response-container";
        responseDiv.appendChild(response);

        marker = new google.maps.Marker({
            position: myLatLng,
            map,
            draggable: true,
            animation: google.maps.Animation.DROP,
        });

        marker.addListener("dragend", (e) => {
            geocode({
                location: e.latLng
            });
            langsLoct = e.latLng.toJSON();
            $('#location_latitude').val(langsLoct.lat);
            $('#location_longitude').val(langsLoct.lng);
        });
        map.addListener("click", (e) => {
            geocode({
                location: e.latLng
            });
            langsLoct = e.latLng.toJSON();

            $('#location_latitude').val(langsLoct.lat);
            $('#location_longitude').val(langsLoct.lng);
        });
        clear();
    });
    
    function clear() {
        marker.setMap(null);
        responseDiv.style.display = "none";
    }

    function geocode(request) {
        clear();
        geocoder
            .geocode(request)
            .then((result) => {
            const {
                results
            } = result;

            map.setCenter(results[0].geometry.location);
            marker.setPosition(results[0].geometry.location);
            marker.setMap(map);
            responseDiv.style.display = "block";
            response.innerText = JSON.stringify(result, null, 1);
            return results;
            })
            .catch((e) => {
            alert("Alamat tidak di temukan!");
            });
    }
</script>

@endsection
