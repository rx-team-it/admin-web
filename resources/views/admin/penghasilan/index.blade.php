@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet"
    href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Penghasilan Saya</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Penghasilan Saya</div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>Informasi Penghasilan Saya</h4>
                </div>
                <div class="card-body">
                    <div class="summary">
                        <div class="summary-info active" data-tab-group="summary-tab" id="summary-text">
                            <div class="text-muted">Total Pendapatan Bersih</div>
                            <h4>{{'Rp. ' . number_format($grand)}}</h4>
                            <div class="d-block mt-2">
                                <a href="{{route('histori-vendor.penjualan')}}">Lihat Semua Detail Pendapatan <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                        <h6 class="mt-3">Riwayat Transaksi Terbaru<span class="text-muted"></span></h6>
                        <div class="summary-item">
                            <div class="table-responsive">
                                <table id="tbl" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <th>Nama Pemesan</th>
                                        <th>invoice</th>
                                        <th>Total laba bersih</th>
                                    </thead>
                                    <tbody>
                                        @foreach ($penjualan as $item)
                                            <tr>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->invoice}} <br><small>{{$item->created_at}}</small></td>
                                                <td>{{'Rp. ' . number_format($item->laba_bersih)}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{-- <ul class="list-unstyled list-unstyled-border">
                                @foreach ($penjualan as $item)   
                                <li class="media">
                                    <img alt="image" src="{{asset('assets/img/users/user-1.png')}}"
                                        class="mr-3 user-img-radious-style user-list-img" width="40">
                                    <div class="media-body">
                                        <div class="media-right">{{'Rp. ' . number_format($item->laba_bersih)}}</div>
                                        <div class="media-title"><a href="#">{{$item->name}}</a></div>
                                        <div class="text-small text-muted"><a href="{{route('order.pembayaran',$item->invoice)}}">{{$item->invoice}}</a>
                                            <div class="bullet"></div> {{$item->created_at}}
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                                {{ $penjualan->links() }}
                            </ul> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4>Informasi Penghasilan saya</h4>
                </div>
                <div class="card-body card-type-3">
                    <div class="row">
                        <div class="col">
                            <h6 class="text-muted mb-0">Total</h6>
                            <span class="font-weight-bold mb-0">{{'Rp. ' . number_format($grand)}}</span>
                        </div>
                        <div class="col-auto">
                            <div class="card-circle l-bg-green text-white">
                                <i class="fas fa-money-bill"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 mx-auto">
            <div class="card">
                <div class="card-header">
                    <h4>Riwayat Transaksi Saya</h4>
                    <div class="card-header-action">
                        <a href="{{route('histori-vendor.penjualan')}}" class="btn btn-primary">
                            <i class="fa fa-arrow-right"></i> Detail
                        </a>
                    </div>
                </div>
                <div class="card-body card-type-3">
                    <div class="table-responsive">
                        <table id="tbl" class="table table-hover table-bordered table-sm">
                            <thead>
                                <tr>
                                    <th>Invoice</th>
                                    <th>Total</th>
                                    <th>Tgl</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($penjualan as $item)
                                <tr>
                                    <td><a href="{{route('order.pembayaran',$item->invoice)}}">{{$item->invoice}}</a>
                                    </td>
                                    <td>{{'Rp. ' . number_format($item->price)}}</td>
                                    <td>{{$item->created_at}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</section>
@endsection

@section('admin.js')
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tbl').DataTable({

        });
    });

</script>
{{-- <script type="text/javascript">
    $('ul.pagination').hide();
    $(function() {
        $('.scrolling-pagination').jscroll({
            autoTrigger: true,
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.scrolling-pagination',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });
</script> --}}

@endsection
