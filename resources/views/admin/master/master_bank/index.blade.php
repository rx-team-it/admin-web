@extends('admin.layouts.admin.index')

@section('admin.css')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">
@endsection

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Master Bank</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Master Bank</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Tambah Bank</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">Rekening</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            <form action="{{ route('mb.uploadbank') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="nm_group">Nama Rekening</label>
                                    <input type="text" name="nama" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleDataList" class="form-label">Pilih Bank</label>
                                    <select name="nama_bank" class="form-control">
                                        <option value="Bank Mandiri">Bank Mandiri</option>
                                        <option value="Bank Bukopin">Bank Bukopin</option>
                                        <option value="Bank Danamon">Bank Danamon</option>
                                        <option value="Bank Mega">Bank Mega</option>
                                        <option value="Bank CIMB Niaga">Bank CIMB Niaga</option>
                                        <option value="Bank Permata">Bank Permata</option>
                                        <option value="Bank Sinarmas">Bank Sinarmas</option>
                                        <option value="Bank QNB">Bank QNB</option>
                                        <option value="Bank Lippo">Bank Lippo</option>
                                        <option value="Bank UOB">Bank UOB</option>
                                        <option value="Panin Bank">Panin Bank</option>
                                        <option value="Citibank">Citibank</option>
                                        <option value="Bank ANZ">Bank ANZ</option>
                                        <option value="Bank Commonwealth">Bank Commonwealth</option>
                                        <option value="Bank Maybank">Bank Maybank</option>
                                        <option value="Bank Maspion">Bank Maspion</option>
                                        <option value="Bank J Trust">Bank J Trust</option>
                                        <option value="Bank QNB">Bank QNB</option>
                                        <option value="Bank KEB Hana">Bank KEB Hana</option>
                                        <option value="Bank Artha Graha">Bank Artha Graha</option>
                                        <option value="Bank OCBC NISP">Bank OCBC NISP</option>
                                        <option value="Bank MNC">Bank MNC</option>
                                        <option value="Bank DBS">Bank DBS</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="BTN">BTN</option>
                                        <option value="Bank DKI">Bank DKI</option>
                                        <option value="Bank BJB">Bank BJB</option>
                                        <option value="Bank BPD DIY">Bank BPD DIY</option>
                                        <option value="Bank Jateng">Bank Jateng</option>
                                        <option value="Bank Jatim">Bank Jatim</option>
                                        <option value="Bank BPD Bali">Bank BPD Bali</option>
                                        <option value="Bank Sumut">Bank Sumut</option>
                                        <option value="Bank Nagari">Bank Nagari</option>
                                        <option value="Bank Riau Kepri">Bank Riau Kepri</option>
                                        <option value="Bank Sumsel Babel">Bank Sumsel Babel</option>
                                        <option value="Bank Lampung">Bank Lampung</option>
                                        <option value="Bank Jambi">Bank Jambi</option>
                                        <option value="Bank Kalbar">Bank Kalbar</option>
                                        <option value="Bank Kalteng">Bank Kalteng</option>
                                        <option value="Bank Kalsel">Bank Kalsel</option>
                                        <option value="Bank Kaltim">Bank Kaltim</option>
                                        <option value="Bank Sulsel">Bank Sulsel</option>
                                        <option value="Bank Sultra">Bank Sultra</option>
                                        <option value="Bank BPD Sulteng">Bank BPD Sulteng</option>
                                        <option value="Bank Sulut">Bank Sulut</option>
                                        <option value="Bank NTB">Bank NTB</option>
                                        <option value="Bank NTT">Bank NTT</option>
                                        <option value="Bank Maluku">Bank Maluku</option>
                                        <option value="Bank Papua">Bank Papua</option>
                                    </select>
                                    {{-- <input class="form-control" list="datalistOptions" name="nama_bank" id="exampleDataList" placeholder="Cari">
                                    <datalist id="datalistOptions">
                                        <option value="Bank Mandiri">
                                        <option value="Bank Bukopin">
                                        <option value="Bank Danamon">
                                        <option value="Bank Mega">
                                        <option value="Bank CIMB Niaga">
                                        <option value="Bank Permata">
                                        <option value="Bank Sinarmas">
                                        <option value="Bank QNB">
                                        <option value="Bank Lippo">
                                        <option value="Bank UOB">
                                        <option value="Panin Bank">
                                        <option value="Citibank">
                                        <option value="Bank ANZ">
                                        <option value="Bank Commonwealth">
                                        <option value="Bank Maybank">
                                        <option value="Bank Maspion">
                                        <option value="Bank J Trust">
                                        <option value="Bank QNB">
                                        <option value="Bank KEB Hana">
                                        <option value="Bank Artha Graha">
                                        <option value="Bank OCBC NISP">
                                        <option value="Bank MNC">
                                        <option value="Bank DBS">
                                        <option value="BCA">
                                        <option value="BNI">
                                        <option value="BRI">
                                        <option value="BTN">
                                        <option value="Bank DKI">
                                        <option value="Bank BJB">
                                        <option value="Bank BPD DIY">
                                        <option value="Bank Jateng">
                                        <option value="Bank Jatim">
                                        <option value="Bank BPD Bali">
                                        <option value="Bank Sumut">
                                        <option value="Bank Nagari">
                                        <option value="Bank Riau Kepri">
                                        <option value="Bank Sumsel Babel">
                                        <option value="Bank Lampung">
                                        <option value="Bank Jambi">
                                        <option value="Bank Kalbar">
                                        <option value="Bank Kalteng">
                                        <option value="Bank Kalsel">
                                        <option value="Bank Kaltim">
                                        <option value="Bank Sulsel">
                                        <option value="Bank Sultra">
                                        <option value="Bank BPD Sulteng">
                                        <option value="Bank Sulut">
                                        <option value="Bank NTB">
                                        <option value="Bank NTT">
                                        <option value="Bank Maluku">
                                        <option value="Bank Papua">
                                    </datalist> --}}
                                </div>
                                <div class="form-group">
                                    <label for="nm_group">Nomor Rekening</label>
                                    <input type="number" min="0" name="no_rekening" class="form-control">
                                </div>
                                <div class="col-md-12">
                                    <div class="text-right">
                                        <button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>List Bank</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab22" data-toggle="tab" href="#home22" role="tab" aria-controls="home" aria-selected="true">Rekening</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home22" role="tabpanel" aria-labelledby="home-tab22">
                            <div class="table-responsive">
                                <table id="bank" class="table table-hover table-bordered table-sm">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Rekening Bank</th>
                                            <th>No Rekening</th>
                                            <th>Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 1;
                                        ?>
                                        @foreach($bank as $b)
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$b->nama_bank}} - {{$b->nama}}</td>
                                            <td>{{$b->no_rekening}}</td>
                                            <td>{{$b->created_at}}</td>
                                            <td>
                                                <a href="#" data-id="{{ $b->id }}" class="btn btn-danger btn-sm swal-confirm">
                                                    <form action="{{ route('mb.destroy', $b->id) }}" id="delete{{ $b->id }}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                    </form>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        $no++;
                                        ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="editrekening" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('mb.updatebank') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Rekening</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Nama Bank : </label>
                        <input type="text" name="nama_bank" class="form-control" id="nama_bank">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">No Rekening : </label>
                        <input type="number" name="no_rekening" class="form-control" id="no_rekening">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Foto : </label>
                        <div class="custom-file">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            <input type="file" name="logo" class="custom-file-input" id="customFile">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('admin.js')
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@if(Session::has('success'))
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: "{!!Session::get('success')!!}"
    })
</script>
@endif

<script>
    $(function() {
        $("#bank").DataTable({
            pageLength: 5
        });
    });

    $(function() {
        $("#bank2").DataTable({
            pageLength: 5
        });
    });

    $('#editrekening').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var nama_bank = button.data('nama_bank') // Extract info from data-* attributes
        var logo = button.data('logo') // Extract info from data-* attributes
        var no_rekening = button.data('no_rekening') // Extract info from data-* attributes

        var modal = $(this)
        modal.find('.modal-title').text('Edit Profile Toko')
        modal.find('#recipient-name').val(recipient)
        modal.find('#nama_bank').val(nama_bank)
        modal.find('#logo').val(logo)
        modal.find('#no_rekening').val(no_rekening)
    })

    $('.swal-confirm').click(function(e) {
        id = e.target.dataset.id;
        swal({
                title: "Apa Anda Yakin?",
                text: "Data Akan Di Hapus Permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    // swal("Poof! Your imaginary file has been deleted!", {
                    //     icon: "success",
                    // });
                    $(`#delete${id}`).submit();
                } else {
                    // swal("Your imaginary file is safe!");
                }
            });
    });
</script>
@endsection