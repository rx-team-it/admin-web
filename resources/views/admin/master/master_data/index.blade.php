{{--  --}}
@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="#" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Kelola Tampilan Buyyer</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">Tampilan Buyyer</div>
        </div>
    </div>
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">                            
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong><i class="fa fa-check-circle"></i>&nbsp; {{ session('success') }}</strong>
        </div>    
    @endif      
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade show" role="alert">                            
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        @foreach ($errors->all() as $error)
        <strong><i class="fa fa-check-times"></i>&nbsp; Gagal!, Pastikan field tidak ada yang kosong ( {{ $error }} )</strong> <br>
        @endforeach      
    </div>      
    @endif
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">Ikon & Nama Header Tab</h4>
                    <div class="card-header-action">
                        @if ($w < 1) 
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modalHeader">
                            <i class="fa fa-plus-circle"></i> Ikon & Nama Baru
                        </a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Header</th>
                                    <th>Ikon/Gambar Header</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($c as $item)  
                                <tr>
                                    <td>{{$item->title}}</td>
                                    <td><img src="{{Storage::disk('s3')->url('public/assets/' . $item->icon)}}" width="80px" /></td>
                                    <td>
                                        <a href="{{ route('mdHeader.delete',$item->id) }}"
                                        class="btn btn-danger btn-sm"><i
                                            class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">Tabel Footer</h4>
                    <div class="card-header-action">
                        @if ($f < 2)
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modalFooter">
                            <i class="fa fa-plus-circle"></i> Footer baru
                        </a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Footer</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($r as $items)  
                                <tr>
                                    <td>{{$items->judul}}</td>
                                    <td>
                                        <a href="{{ route('mdHeader.delete',$items->id) }}"
                                        class="btn btn-danger btn-sm"><i
                                            class="far fa-trash-alt"></i>
                                        </a>
                                        <button class="btn btn-primary btn-sm" value="{{ $items->id }}" onclick="showSublink(this.value)"><i class="far fa-eye" style="color:white" data-toggle="modal" data-target="#modalFooterChildView"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal add -->
<div class="modal fade" id="modalHeader" tabindex="-1" role="dialog" aria-labelledby="modalHeaderLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('mdHeader.post') }}" method="post"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Simpan gambar dan nama baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nm_group">Masukkan Nama Header</label>
                        <input type="text" name="title" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Ikon : </label>
                        <input type='file' name='file' id='file' class='form-control'
                            placeholder="Gambar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalFooter" tabindex="-1" role="dialog" aria-labelledby="modalFooterLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('md.post') }}" method="post"
                enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Footer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nm_group">Masukkan Nama Judul</label>
                        <input type="text" name="judul" class="form-control"
                            placeholder="Contoh: Kebijakan privasi, Syarat & Ketentuan, dll."
                            required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--modal nampilin chilld footer-->
<div class="modal fade" id="modalFooterChildView" tabindex="-1" role="dialog" aria-labelledby="modalFooterChildViewLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
                <div class="card-header">
                    <h4 class="card-title">Tabel Sub Footer</h4>
                    <div class="card-header-action">
                        <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modalFooterChild">
                            <i class="fa fa-plus-circle"></i> Sub Footer Baru
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nama Sub Footer</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="subFooter">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
    </div>
</div>

<!--modal nambah child footer-->
<div class="modal fade" id="modalFooterChild" tabindex="-1" role="dialog" aria-labelledby="modalFooterChildLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('child.footer.post') }}" method="post"   
                enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Sub Footer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="paretntFooter" >Untuk Footer:</label>
                        <select class="form-control" id="paretntFooter" name="parentFooter" required>
                          <option value="">-- Pilih --</option>
                          @foreach ($r as $item)
                          <option value="{{ $item->id }}">{{ $item->judul }}</option>
                          @endforeach
                        </select>
                      </div>
                    <div class="form-group">
                        <label for="nm_group">Masukkan Nama:</label>
                        <input type="text" name="nama" class="form-control"
                            placeholder="Nama Sublink"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="ckeditor">Isi</label>
                        <textarea class="ckeditor" id="ckedtor" name="isi" rows="3" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal edit child footer -->
<div class="modal fade" id="modalFooterChildEdit" tabindex="-1" role="dialog" aria-labelledby="modalFooterChildEditLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/index" method="post" id="edit_modal_child"   
                enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Edit Sub Footer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nm_group">Masukkan Nama Baru:</label>
                        <input type="text" name="nama" id="namaChild" class="form-control"
                            placeholder="Nama Sublink"
                            required value="nanan">
                    </div>
                    <div class="form-group">
                        <label for="ckeditor">Isi</label>
                        <textarea class="ckeditor" id="isi" name="isi" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    function cek() {
    // $('textarea').val('cek');
    CKEDITOR.instances.x.setData('aoasda');
    }

    // untuk tampil chill footer
    function showSublink(x){
        $('.trSF').remove();
        $('.tdSF').remove();
        $('.empty').remove();
        $.ajax({
            url: "get-master-data-child/"+ x +"",
            type: 'GET',
            dataType: 'json',
            cache: false,
            success: function(response){
                console.log(response);
                if(response.length > 0){
                    response.forEach(r => {
                        $('#subFooter').append(
                            `<tr class="trSF">
                                <td class="tdSF">${r.nama}</td>
                                <td class="tdSF">
                                    <a href="delete-master-data-child/${r.id}" class="btn btn-danger btn-sm">
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                                    <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalFooterChildEdit" value="${r.id}" onClick="showEdit(this.value)">
                                        <i class="fas fa-pen"></i>
                                    </button>
                                </td>
                            </tr>`)
                    });
                }else{

                    $('#subFooter').append('<p class="empty">Belum Ada Sub Footer!!</p>')
                }
            }
        })
    }

    // untuk nampil dan edit sub footer
    function showEdit(id){
        $('#edit_modal_child').attr('action', `/masterData/edit-master-data-child/${id}`);
        $.ajax({
            url: "get-master-data-child2/"+ id +"",
            type: 'GET',
            dataType: 'json',
            cache: false,
            success: function(response){
                $('#namaChild').val(response.nama);
                CKEDITOR.instances.isi.setData(response.isi);
            }
        })
    }
</script>

@endsection
@section('admin.js')
<script src="{{ asset('assets/bundles/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/page/ckeditor.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
@endsection
