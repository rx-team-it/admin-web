@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('md.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Edit Master Data</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item"><a href="{{ route('md.index') }}">Master Data</a></div>
            <div class="breadcrumb-item">Edit {{$md[0]->nama}}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Edit Data {{$md[0]->nama}}</h4>
                    {{-- <div class="card-header-action">
                        <a href="#" class="btn btn-primary">
                            <i class="fa fa-arrow-left"></i> Kembali
                        </a>
                    </div> --}}
                </div>
                <div class="card-body">
                    <form action="{{ route('md.edit', $md[0]->id) }}" method="post">
                        @csrf
                        @foreach ($md as $master_data)
                        <div class="form-group">
                            <label for="nm_group">Masukkan Nama Judul</label>
                            <input type="text" name="judul" class="form-control" value="{{ $master_data->judul }}">
                        </div>
                        @endforeach
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('admin.js')
<script src="{{ asset('assets/bundles/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/page/ckeditor.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
@endsection
