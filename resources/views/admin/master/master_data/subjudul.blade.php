@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('md.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Tambah Sub Judul</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item"><a href="{{ route('md.index') }}">Master Data</a></div>
            <div class="breadcrumb-item">Tambah Sub Judul</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Judul {{$w->judul}}</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('md.subjudul', $w->id) }}" method="post">
                        @csrf
                        <div class="form-group">
                            {{-- <input type="text" placeholder="{{$w->id}}"> --}}
                            <label for="nm_group">Masukkan Sub Judul</label>
                            <input type="text" name="nama" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="isi">Isi</label>
                            <textarea id="ckeditor" name="isi"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-md btn-success"><i class="fa fa-check"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('admin.js')
<script src="{{ asset('assets/bundles/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/js/page/ckeditor.js') }}"></script>
@if(Session::has('success'))
<script>
    swal("Berhasil", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });

</script>
@endif
@endsection
