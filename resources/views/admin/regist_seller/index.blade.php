@extends('admin.layouts.admin.index')

@section('admin.content')
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('home.admin') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>List Seller</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home.admin') }}">Beranda</a></div>
            <div class="breadcrumb-item">List Seller Aktif</div>
        </div>
    </div>
    <div class="section-body">
        <a href="{{ route('register.seller') }}" class="btn btn-primary btn-sm">Tambah Seller</a>
        <hr>
        <div class="row">
            @foreach($user as $us)
            <div class="col-12 col-md-4 col-lg-4">
                <article class="article article-style-c">
                    <div class="article-details">
                        <div class="article-category"><a href="#">Mendaftar</a>
                            <div class="bullet"></div> <a href="#">{{ !empty($us) ? $us->created_at : '' }}</a>
                        </div>
                        <div class="article-title float-left">
                            <h2><a href="{{ route('seller.detail', $us->user->id) }}">{{ !empty($us->user) ? $us->user->name : '' }}</a></h2>
                        </div>
                        <div class="article-title float-right">
                            <!-- <h2>
                                {!! $us->nm_role !!}
                            </h2> -->
                        </div>
                        <!-- <p>Ini adalah Deskripsi</p> -->
                        <div class="article-user">
                            @if ($us->user->user_detail->foto_user == null)
                            <img alt="image" src="{{ asset('assets/img/users/user-1.png') }}">
                            @else
                            <img alt="image" src="{{Storage::disk('s3')->url('public/images/profile/' . $us->user->user_detail->foto_user)}}">
                            @endif
                            <div class="article-user-details">
                                <div class="user-detail-name">
                                    <a href="#">{{ !empty($us) ? $us->user->user_detail->address : '' }}</a>
                                </div>
                                <div class="text-job">{{ !empty($us->role) ? $us->role->nm_role : '' }}</div>
                            </div>
                        </div>
						<div class="article-title mt-2">
							Activity
                        </div>
						@foreach($us->user->product as $i => $prod)
						<div class="article-category"><span>{{ !empty($prod) ? $prod->created_at : '' }}</span>
                            <div class="bullet"></div> <span class="text-lowercase"> Seller malakukan input produk: </span> <a href="#">{{ $prod->name  }}</a>
                        </div>
						@endforeach
                    </div>
                </article>
            </div>
            @endforeach
        </div>
        {{ $user->links() }}
    </div>
</section>
@endsection