<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::group([
    // 'middleware' => 'auth:api',

], function ($router) {
    Route::get('list-country', 'Api\LocationController@listCountry')->name('country.list');
    Route::get('get-country/{id}', 'Api\LocationController@getCountry')->name('country.show');
    Route::get('get-state/{country_id}', 'Api\LocationController@getState')->name('state.show');
    Route::get('get-city/{state_id}', 'Api\LocationController@getCity')->name('city.show');
    Route::get('get-urban/{city_id}', 'Api\LocationController@getUrban')->name('urban.show');
    Route::get('get-postal/{urban_id}', 'Api\LocationController@getPostal')->name('postal.show');

    Route::middleware('apikey')->group(function() {
        // get List transaction vendor
        Route::get('list-transactions/vendor/{vendor_code}', 'Api\VendorController@getVendorTransactions')->name('transaction.vendor');
        // get List seller 
        Route::get('list-sellers/vendor/{vendor_code}', 'Api\VendorController@getSellerVendor');
        // get List resellers
        Route::get('list-resellers/vendor/{vendor_code}', 'Api\VendorController@getResellerVendor');
        // get List buyyers
        Route::get('list-buyyers/vendor/{vendor_code}', 'Api\VendorController@getBuyyerVendor');

        // get status membership
        Route::get('status-membership/vendor/{vendor_code}', 'Api\VendorController@getStatusMembership');
        // update status membership
        Route::put('status-membership/vendor/{vendor_code}', 'Api\VendorController@updateStatusMembership');


        // nav
        Route::get('list-navigation/{vendor_code}', 'Api\VendorController@navigation');
        Route::put('list-navigation/{vendor_code}', 'Api\VendorController@updateNavigation');

        Route::post('/payment/midtrans', 'Api\MidtransController@Notification')->name('midtrans.Notification');

        // service kurir buat supersystem
        Route::get('get-kurir-service/{vendor_code}', 'Api\ServiceKurirController@getKuirService');
        Route::post('update-diskon-service/{vendor_code}', 'Api\ServiceKurirController@updateDiskonService');

        // get Tagihan vendor
        Route::post('get-tagihan-vendor', 'Api\VendorController@getTagihanVendor');

    });
});


// Api produk
Route::get('/list', 'Api\ProdukController@produk');
Route::get('/produk-detil/{id}', 'Api\ProdukController@detil');
Route::post('/print_multi_resi', 'Api\ServiceKurirController@print_multi_resi');

//check_service_admin
Route::get('/check-service-admin/{postal_code_origin}/{sub_district_origin}/{city_origin}/{postal_code_destination}/{sub_district_destination}/{city_destination}/{weight}/{group_service}', 'Api\ServiceKurirController@check_service_admin');

//JNE
Route::get('/api-tarif-jne/{from}/{thru}/{weight}', 'Api\ServiceKurirController@api_tarif_jne');
Route::get('/get_thru/{kode_pos}/{kelurahan}', 'Api\ServiceKurirController@get_thru');
Route::get('/create_job_jne_masal/{id}/{service_code_}/{ongkir}/{code_service}', 'Api\ServiceKurirController@create_job_jne_masal');


//create job
Route::get('/create_job_masal/{id}/{kurir}/{service_code_}/{ongkir}/{code_service}', 'Api\ServiceKurirController@create_job_masal');
//create job
Route::post('/create_job_masal_post', 'Api\ServiceKurirController@create_job_masal_post');


// Route::get('/pesanan/register-awb-masal', 'Api\ServiceKurirController@register_awb_masal');

// api pengiriman
Route::get('/pengiriman-api', 'Api\PengirimanController@kurir');
Route::post('/update-pengiriman', 'Api\PengirimanController@update_kurir')->name('update.pengiriman');


