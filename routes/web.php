<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//=========================== ADMIN ROUTES =============================//


/*
|--------------------------------------------------------------------------
| Route Login Admin
|--------------------------------------------------------------------------
|*/

Route::get('/', 'Admin\Auth\LoginController@formlogin')->name('login.admin');
Route::post('/login/admin', 'Admin\Auth\LoginController@postLogin')->name('post.login.admin');
Route::get('/logout/admin', 'Admin\Auth\LoginController@logout')->name('logout.admin');

/*
|--------------------------------------------------------------------------
| Route Beranda
|--------------------------------------------------------------------------
|*/
Route::group([
    'middleware' => 'admin'
], function () {
    Route::get('/beranda', 'HomeController@index')->name('home.admin');
    // ubah password
    Route::get('/ubah-password', 'HomeController@ubah_password')->name('password.admin');
    Route::post('/update-password', 'HomeController@update_password')->name('update.ubah.password');


    /*
    |--------------------------------------------------------------------------
    | Route Category
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'category',
    ], function () {
        Route::get('/index', 'Admin\Category\CategoryController@index')->name('category.index');
        Route::get('/indexchild', 'Admin\Category\CategoryController@child')->name('category.child');
        Route::get('/edit/{id}', 'Admin\Category\CategoryController@edit')->name('category.edit');
        Route::post('/update/{id}', 'Admin\Category\CategoryController@update')->name('category.update');
        Route::post('/store', 'Admin\Category\CategoryController@store')->name('category.store');
        Route::delete('/delete/{id}', 'Admin\Category\CategoryController@destroy')->name('kategori.delete');
        // Route::match(['get', 'post'], '/modal/{id}', 'Admin\Category\CategoryController@modaledit')->name('modal.edit');
        Route::post('/update', 'Admin\Category\CategoryController@update')->name('category.update');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Slider Image
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'image',
    ], function () {
        Route::get('/index', 'Admin\Slider\SliderController@index')->name('slider.home');
        Route::post('/update', 'Admin\Slider\SliderController@update')->name('slider.up');
        Route::post('/create-banner', 'Admin\Slider\SliderController@createbanner')->name('slider.add');
        Route::post('/create-slider', 'Admin\Slider\SliderController@createslider')->name('sliders.add');
        Route::post('/create-bp', 'Admin\Slider\SliderController@createbp')->name('bp.add');
        Route::post('/create-bp1', 'Admin\Slider\SliderController@createbp1')->name('bp1.add');
        Route::post('/create-bw', 'Admin\Slider\SliderController@createbw')->name('bw.add');
        Route::get('/delete/{id}', 'Admin\Slider\SliderController@destroy')->name('slider.delete');

        Route::get('/image-test', 'Admin\Slider\SliderController@indexnew')->name('slider.indexnew');
        Route::delete('/banner-store-delete/{id}', 'Admin\Slider\SliderController@deletebannertoko')->name('banner.toko');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Store
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'toko',
    ], function () {

        //LIST TOKO RESELLER//
        Route::get('/toko-reseller', 'Admin\ListToko\ListController@index_reseller')->name('toko.reseller');
        Route::get('/toko-pending', 'Admin\ListToko\ListController@listpending')->name('toko.pending');
        Route::get('/toko-aktif', 'Admin\ListToko\ListController@listaktif')->name('toko.aktif');
        Route::get('/toko-delete', 'Admin\ListToko\ListController@listdelete')->name('toko.delete');
        Route::get('/toko-block', 'Admin\ListToko\ListController@listblock')->name('toko.block');

        //UPDATE STATUS TOKO DAN ACCESS ROLE MENJADI RESELLER//
        Route::get('/update/{id}', 'Admin\ListToko\ListController@terima')->name('terima.toko');

        //FITUR SEARCH TOKO//
        Route::get('/cari-aktif', 'Admin\ListToko\ListController@cariaktif')->name('cari.aktif');
        Route::get('/cari-pending', 'Admin\ListToko\ListController@caripending')->name('cari.pending');
        Route::get('/cari-delete', 'Admin\ListToko\ListController@caridelete')->name('cari.delete');
        Route::get('/cari-block', 'Admin\ListToko\ListController@cariblock')->name('cari.block');

        // FITUR DETAIL TOKO //
        Route::get('/detail-toko/{id}', 'Admin\ListToko\ListController@detail')->name('toko.detail');

        // FITUR GANTI STATUS TOKO //
        Route::get('/approve/{id}', 'Admin\ListToko\ListController@approve')->name('approve');
        Route::get('/blok/{id}', 'Admin\ListToko\ListController@blok')->name('blok');
        Route::get('/delete/{id}', 'Admin\ListToko\ListController@delete')->name('delete');
        Route::get('/reactive/{id}', 'Admin\ListToko\ListController@reactive')->name('reactive');
    });


    /*
    |--------------------------------------------------------------------------
    | Route Register Seller
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'seller',
    ], function () {

        //LIST SELLER//
        Route::get('/list', 'Admin\Seller\RegisterController@index')->name('index.seller');

        Route::get('/detail/{id}', 'Admin\Seller\SellerController@show')->name('seller.detail');

        Route::get('/register', 'Admin\Seller\RegisterController@register')->name('register.seller');
        Route::post('/register/seller', 'Admin\Seller\RegisterController@daftar')->name('daftar.seller');
    });


    /*
    |--------------------------------------------------------------------------
    | Route Customer
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'customer',
    ], function () {
        Route::get('/list_customer', 'Admin\ListCustomer\ListController@index')->name('customer.index');
        Route::get('/detail/{id}', 'Admin\ListCustomer\ListController@edit')->name('customer.edit');
        Route::get('/list', 'Admin\ListCustomer\ListController@list')->name('list.list');
        Route::get('/blok/{id}', 'Admin\ListCustomer\ListController@blok')->name('customer.blok');
        Route::get('/unblok/{id}', 'Admin\ListCustomer\ListController@unblok')->name('customer.unblock');
        Route::get('/delete/{id}', 'Admin\ListCustomer\ListController@delete')->name('customer.delete');

        // ROUTE DIATAS ADALAH ROUTE OLD //
        Route::get('/user', 'Admin\ListCustomer\ListController@user')->name('user');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Order
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'pesanan',
    ], function () {
        Route::get('/order/detail/{invoice}', 'Admin\Pesanan\PesananController@detail')->name('pesanan.detail');
        Route::get('/order/invoice_pdf/{invoice}', 'Admin\Pesanan\PesananController@invoice_pdf')->name('pesanan.invoice');
        Route::get('/print/{invoice}', 'Admin\Pesanan\PesananController@invoice_print')->name('print.invoice');
        Route::get('/multiple/printinvoice', 'Admin\Pesanan\PesananController@invoicePrint_multi')->name('multi.print.Invoice');

        // print resi depan table
        Route::get('/printresi/{invoice}', 'Admin\Pesanan\PesananController@printResi')->name('print.Resi');
        Route::post('/cetakresi/{invoice}', 'Admin\Pesanan\PesananController@cetakresi')->name('cetak.Resi');
        Route::post('/cetakresi_jne/{invoice}', 'Admin\Pesanan\PesananController@cetakresi_jne')->name('cetak.Resi.jne');

        // regist awb
        Route::get('/regist-awb/{invoice}', 'Admin\Pesanan\PesananController@regist_awb')->name('regist.awb');

        //siap di cetak
        Route::post('/ready-to-print', 'Admin\Pesanan\PesananController@siap_dicetak')->name('readytoprint');

        Route::get('/multiple/printresi', 'Admin\Pesanan\PesananController@printResi_multi')->name('multi.print.Resi');

        Route::post('/multiple/printresi-multi-sudah-bayar', 'Admin\Pesanan\PesananController@printResi_multi_sudah_bayar')->name('multi.print.Resi.sudah.bayar');

        // LIST ORDERAN BARU //
        Route::get('/list', 'Admin\Pesanan\PesananController@list_pesanan')->name('list.order.new');

        // TERMAL TEST  //
        Route::get('/termal-test', 'Admin\Pesanan\PesananController@termaltest')->name('termal.test');

        // EMAIL DEGISN TEST //
        Route::get('/mail-view', 'Admin\Pesanan\PesananResellerController@mailview');

        // LIST ORDER MANUAL //
        Route::get('/list-manual', 'Admin\Pesanan\PesananController@indexnew')->name('order.index');

        // LIST ORDER OTOMATIS //
        Route::get('/list-otomatis', 'Admin\Pesanan\PesananController@indexmidtrans')->name('order.indexmidtrans');

        // DETAIL PEMBAYARAN MANUAL //
        Route::get('/detail/{invoice}', 'Admin\Pesanan\PesananController@pembayaran')->name('order.pembayaran');

        // upload ulang bukti pembayaran
        Route::get('/upload-ulang-bukti/{invoice}', 'Admin\Pesanan\PesananController@reupload')->name('order.reupload');

        //acc bukti pembayaran
        Route::get('/acc-bukti/{invoice}', 'Admin\Pesanan\PesananController@acc_bukti')->name('acc.bukti');
        Route::post('/acc-buktiMulti', 'Admin\Pesanan\PesananController@acc_buktiMulti')->name('acc.bukti.multi');

        // tombol dikirim
        Route::get('/tombolDikirim/{invoice}', 'Admin\Pesanan\PesananController@tombolDikirim')->name('tombol.dikirim');
        // tombol dikemas
        Route::get('/tombolDikemas/{invoice}', 'Admin\Pesanan\PesananController@tombolDikemas')->name('tombol.dikemas');

        // DETAIL PEMBAYARAN OTOMATIS //
        Route::get('/detail-otomatis/{invoice}', 'Admin\Pesanan\PesananController@pembayaran_otomatis')->name('order.pembayaran_otomatis');

        //INPUT NOMOR RESI//
        Route::post('/tracking_number/{id}', 'Admin\Pesanan\PesananController@inputresi')->name('input.resi');

        // GANTI STATUS GAGAL //
        Route::post('/gagal/{invoice}', 'Admin\Pesanan\PesananController@batalkan_order')->name('order.gagal');

        // GANTI STATUS DIKEMAS //
        Route::get('/dikemas/{invoice}', 'Admin\Pesanan\PesananController@statusdikemas')->name('order.dikemas');

        // GANTI STATUS TELAH DIAMBIL DAN SELESAI //
        Route::get('/selesai/{invoice}', 'Admin\Pesanan\PesananController@statusselesai')->name('order.selesai');

        //datatable print
        Route::get('/print', 'Admin\Pesanan\PesananController@indexprint')->name('indexprint');
        Route::get('/printb', 'Admin\Pesanan\PesananController@sudahbayarprint')->name('sudahbayarprint');
        Route::get('/printb1', 'Admin\Pesanan\PesananController@belumbayarprint')->name('belumbayarprint');
        Route::get('/printc', 'Admin\Pesanan\PesananController@pemesanangagalprint')->name('pemesanangagalprint');
        Route::get('/printd', 'Admin\Pesanan\PesananController@dikemasprint')->name('dikemasprint');
        Route::get('/printe', 'Admin\Pesanan\PesananController@dikirimprint')->name('dikirimprint');
        Route::get('/printf', 'Admin\Pesanan\PesananController@selesaiprint')->name('selesaiprint');

        // test pesanan //
        Route::get('/pesanan/test', 'Admin\Pesanan\PesananController@pesananindex')->name('pesanan.test');

        // register AWB masal
        Route::post('/pesanan/register-awb-masal', 'Admin\Pesanan\PesananController@register_awb_masal')->name('register.awb.masal');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Order Reseller
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'pesanan',
    ], function () {
        // LIST RESELLER //
        Route::get('/list-reseller', 'Admin\Pesanan\PesananResellerController@index')->name('pesanan.referall.index');
        Route::get('/order-reseller/{referral_code}', 'Admin\Pesanan\PesananResellerController@list_pesanan_reseller')->name('pesanan.reseller.index');
        Route::get('/detail-pesanan/{invoice}', 'Admin\Pesanan\PesananResellerController@detail_pesanan_reseller')->name('pesanan.detail.reseller');

        // ROUTE INVOICE //
        Route::get('/order/invoice/{invoice}', 'Admin\Pesanan\PesananResellerController@invoice')->name('pesanan.invoice.reseller');

        // LIST PESANAN RESELLER BARU //
        Route::get('/reseller', 'Admin\Pesanan\PesananResellerController@pesanan_reseller')->name('pesanan.reseller');

        // BATALKAN PESANAN //
        Route::post('/order-cancel', 'Admin\Pesanan\PesananResellerController@batalkan_order')->name('cancel.order');
        // TOLAK PEMBATALAN ORDER //
        Route::post('/tolak_pembatalan', 'Admin\Pesanan\PesananResellerController@tolakPembatalanOrder')->name('tolak.pembatalan');
        //ACC PEMBATALAN ORDER
        Route::post('/acc_pembatalan', 'Admin\Pesanan\PesananResellerController@accPembatalanOrder')->name('acc.pembatalan');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Product
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'product',
    ], function () {
        Route::get('/index', 'Admin\Product\ProductController@index')->name('produk.index');
        Route::get('/show/{id}', 'Admin\Product\ProductController@show')->name('produk.show');
        Route::get('/detail/{id}', 'Admin\Product\ProductController@detail')->name('produk.detail');
        Route::get('/productlist/{id}', 'Admin\Product\ProductController@productlist')->name('productlist');
        Route::get('/list_blok', 'Admin\Product\ProductController@listblok')->name('list.produk');
        Route::post('/blok/{id}', 'Admin\Product\ProductController@blok')->name('produk.blok');
        Route::get('/unblok/{id}', 'Admin\Product\ProductController@unblok')->name('produk.unblock');
        Route::post('/delete/{id}', 'Admin\Product\ProductController@delete')->name('produk.delete');

        // ROUTE PRODUK NEW //
        Route::get('/list', 'Admin\Product\ProductController@list_produk')->name('produk.list');


        Route::group([
            'prefix' => 'panel',
        ], function () {
            Route::get('/flashsale', 'Admin\Product\PanelProductController@panel')->name('produk.panel');
            Route::get('/delete/{id}', 'Admin\Product\PanelProductController@delete')->name('panel.delete');
            Route::get('/delete-panel/{id}', 'Admin\Product\PanelProductController@deletePanelGrup')->name('panel-grup.delete');
            Route::post('/flash-tambah', 'Admin\Product\PanelProductController@tambah_product')->name('flash.tambah');
            Route::post('/delete-produk', 'Admin\Product\PanelProductController@delete_produk')->name('panel-produk.delete');

            Route::group([
                'prefix' => 'group',
            ], function () {
                Route::post('/group-tambah', 'Admin\Product\PanelProductController@create_group')->name('group.tambah');
                Route::post('/group-tambah-banner', 'Admin\Product\PanelProductController@create_group_banner')->name('group.tambah.banner');
                Route::post('/group-edit-banner', 'Admin\Product\PanelProductController@banner_edit_foto')->name('group.edit.banner');
                Route::get('/group-hapus-banner/{id}', 'Admin\Product\PanelProductController@banner_hapus_foto')->name('banner.delete');
            });
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Route Product
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'store',
    ], function () {
        Route::get('/index', 'Admin\Store\MasterStoreController@index')->name('store.index');
        Route::post('/update/toko', 'Admin\Store\MasterStoreController@profileupdate')->name('update.store');
        Route::post('/tambah/gambar', 'Admin\Store\MasterStoreController@slidercreate')->name('slider.create');
        Route::post('/tambah/banner', 'Admin\Store\MasterStoreController@bannercreate')->name('banner.create');
        Route::post('/ubah/banner/{id}', 'Admin\Store\MasterStoreController@bannerubah')->name('banner.produk.ubah');
    });

    Route::group([
        'prefix' => 'alamat',
    ], function () {
        Route::get('/', 'Admin\Address\MasterAddressController@index')->name('alamat.index');
        Route::post('/create', 'Admin\Address\MasterAddressController@create')->name('alamat.create');
        Route::get('/edit/{id}', 'Admin\Address\MasterAddressController@edit')->name('alamat.edit');
        Route::post('/update/{id}', 'Admin\Address\MasterAddressController@update')->name('alamat.update');
        Route::post('/edit-pinpoint', 'Admin\Address\MasterAddressController@pinpoint')->name('pinpoint.address');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Product
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'payment',
    ], function () {

        // SALDO DATATABLE //
        Route::get('/index', 'Admin\Payment\PaymentController@index')->name('payment.index');
        Route::get('/indexa', 'Admin\Payment\PaymentController@penarikan_berhasil')->name('payment.berhasil');
        Route::get('/indexb', 'Admin\Payment\PaymentController@penarikan_gagal')->name('payment.gagal');

        // DETAIL SALDO //
        Route::get('/detail/{id}', 'Admin\Payment\PaymentController@detail')->name('payment.detail');

        // MERUBAH STATUS SALDO //
        Route::get('/selesai/{id}', 'Admin\Payment\PaymentController@selesai')->name('payment.selesai');
        Route::get('/tolak/{id}', 'Admin\Payment\PaymentController@tolak')->name('payment.tolak');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Verifikasi KTP
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'verifikasi',
    ], function () {

        //LIST TOKO VERIFIKASI KTP//
        Route::get('/list', 'Admin\Verifikasi\VerifikasiController@index')->name('index.verifikasi');
        Route::get('/{id}', 'Admin\Verifikasi\VerifikasiController@detail')->name('detail.verifikasi');

        // LIST TOKO VERIFIKASI //
        Route::get('/inverif', 'Admin\Verifikasi\VerifikasiController@inverif')->name('inverif');

        //VERIFIKASI STATUS//
        Route::get('/diterima/{id}', 'Admin\Verifikasi\VerifikasiController@diterima')->name('diterima.verifikasi');
        Route::get('/ditolak/{id}', 'Admin\Verifikasi\VerifikasiController@ditolak')->name('ditolak.verifikasi');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Management Reseller
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'group',
    ], function () {

        //LIST Reseller//
        Route::get('/list-reseller', 'Admin\Reseller\GroupResellerController@index')->name('index.GroupReseller');
        Route::get('/list-resellers', 'Admin\Reseller\GroupResellerController@indexR')->name('reseller.GroupReseller');
        Route::post('/register/grup', 'Admin\Reseller\GroupResellerController@daftarGrup')->name('daftar.grup');
        Route::get('/register-reseller', 'Admin\Reseller\GroupResellerController@register')->name('register.reseller');
        Route::post('/register/reseller', 'Admin\Reseller\GroupResellerController@daftar')->name('daftar.reseller');
        Route::get('/list-group-reseller/{id}', 'Admin\Reseller\GroupResellerController@show')->name('show.reseller');
        Route::get('/delete/{id}', 'Admin\Reseller\GroupResellerController@delete')->name('reseller.delete');
        Route::get('/group-reseller-delete/{id_reseller}', 'Admin\Reseller\GroupResellerController@GroupResellerDelete')->name('GroupReseller.delete');
        Route::post('/edit', 'Admin\Reseller\GroupResellerController@edit')->name('edit.grup');
    });


    /*
    |--------------------------------------------------------------------------
    | Route Komisi Reseller
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'komisi',
    ], function () {

        Route::get('/list-komisi', 'Admin\Komisi\KomisiController@index')->name('index.komisi');
        Route::post('/add-komisi', 'Admin\Komisi\KomisiController@add')->name('add.komisi');
        Route::post('/edit-grup/{id}', 'Admin\Komisi\KomisiController@edit')->name('edit.komisi');
        // Route::get('/register-reseller', 'Admin\Reseller\GroupResellerController@register')->name('register.reseller');
        // Route::post('/register/reseller', 'Admin\Reseller\GroupResellerController@daftar')->name('daftar.reseller');
        // Route::get('/list-group-reseller/{id}', 'Admin\Reseller\GroupResellerController@show')->name('show.reseller');
        Route::get('/komisi-delete/{id}', 'Admin\Komisi\KomisiController@delete')->name('komisi.delete');
    });


    /*
    |--------------------------------------------------------------------------
    | Route Referall Reseller
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'referral',
    ], function () {
        Route::get('/referall', 'Admin\Referall\ReferallController@referall')->name('index.referall');
        Route::get('/detail-user/{referral_code}', 'Admin\Referall\ReferallController@detail_user')->name('detail.user.referral');
    });

    /*
    |--------------------------------------------------------------------------
    | Route kurir
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'kurir',
    ], function () {
        Route::get('/kurir', 'Admin\Kurir\KurirController@index')->name('index.kurir');
        Route::post('/add-kurir', 'Admin\Kurir\KurirController@tambah')->name('kurir.tambah');
        Route::post('/kurir-update', 'Admin\Kurir\KurirController@update')->name('update.kurir');
        Route::get('/kurir-hapus/{id}', 'Admin\Kurir\KurirController@hapus')->name('hapus.kurir');
    });

    /*
    |--------------------------------------------------------------------------
    | Route histori
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'kodeunik',
    ], function () {
        Route::get('/index', 'Admin\Kode\KodeController@index')->name('index.kodeunik');
        Route::get('/detail/{invoice}', 'Admin\Kode\KodeController@detail')->name('detail.kodeunik');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Tiketing
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'tiket',
    ], function () {
        Route::get('/list', 'Admin\Tiketing\TiketingSupportController@list_pesan')->name('tiketing');
        Route::get('/detail/{id}', 'Admin\Tiketing\TiketingSupportController@detail_pesan')->name('detail.tiketing');
        Route::post('/reply/{id}', 'Admin\Tiketing\TiketingSupportController@reply')->name('reply.tiketing');
        // Route::post('/balas/{id}', 'Admin\Tiketing\TiketingSupportController@balas_pesan')->name('balas.tiketing');
        Route::get('/tutup-tiket/{id}', 'Admin\Tiketing\TiketingSupportController@close')->name('close.tiketing');

        Route::group([
            'prefix' => 'category',
        ], function () {
            Route::post('/category', 'Admin\Tiketing\TiketingSupportController@category_tiketing')->name('category.post');
            Route::delete('/delete/{id}', 'Admin\Tiketing\TiketingSupportController@category_delete')->name('category.delete');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Route Tiketing
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'aws-img',
    ], function () {
        Route::get('/index', 'Admin\Aws\AwsController@index')->name('aws.index');
    });

    /*
    |--------------------------------------------------------------------------
    | Route Master Data
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'masterData',
    ], function () {
        // halaman master data
        Route::get('/index', 'Admin\MasterData\MasterDataController@index')->name('md.index');
        // hapus master data
        Route::get('/masterdata-delete/{id}', 'Admin\MasterData\MasterDataController@delete')->name('md.delete');
        // hapus data header
        Route::get('/masterdata-delete-header/{id}', 'Admin\MasterData\MasterDataController@deleteheader')->name('mdHeader.delete');
        // hapus data subjudul
        Route::get('/masterdatasub-delete/{id}', 'Admin\MasterData\MasterDataController@deletesub')->name('mds.delete');
        // edit master data
        Route::match(['get', 'post'], '/masterdata-edit/{id}', 'Admin\MasterData\MasterDataController@edit')->name('md.edit');
        // edit data header
        Route::match(['get', 'post'], '/masterdata-edit-header/{id}', 'Admin\MasterData\MasterDataController@editheader')->name('mdHeader.edit');
        // edit data sub
        Route::match(['get', 'post'], '/masterdata-edit-sub/{id}', 'Admin\MasterData\MasterDataController@editsub')->name('mdsub.edit');
        // tambah sub judul
        Route::match(['get', 'post'], '/masterdata-subjudul/{id}', 'Admin\MasterData\MasterDataController@subjudul')->name('md.subjudul');
        // tambah master data
        Route::post('/add-master-data', 'Admin\MasterData\MasterDataController@post')->name('md.post');
        // tambah data header
        Route::post('/add-master-data-header', 'Admin\MasterData\MasterDataController@postheader')->name('mdHeader.post');
        // tambah child footer
        Route::post('/add-master-data-child', 'Admin\MasterData\MasterDataController@childFooterPost')->name('child.footer.post');
        // get show child
        Route::get('/get-master-data-child/{id}', 'Admin\MasterData\MasterDataController@childFooterGet')->name('child.footer.Get');
        // delete child
        Route::get('/delete-master-data-child/{id}', 'Admin\MasterData\MasterDataController@childFooterDelete')->name('child.footer.Delete');
        // gat data child untuk update
        Route::get('/get-master-data-child2/{id}', 'Admin\MasterData\MasterDataController@childFooterGet2')->name('child.footer.Get');
        // edit child
        Route::put('/edit-master-data-child/{id}', 'Admin\MasterData\MasterDataController@childFooterEdit')->name('child.footer.Edit');
    });


    /*
    |--------------------------------------------------------------------------
    | Route Master Bank
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'masterBank',
    ], function () {
        // halaman master data
        Route::get('/index', 'Admin\MasterBank\MasterBankController@index')->name('mb.index');
        Route::post('/mb/store', 'Admin\MasterBank\MasterBankController@uploadbank')->name('mb.uploadbank');
        Route::post('/mb/store2', 'Admin\MasterBank\MasterBankController@uploadbank2')->name('mb.uploadbank2');
        Route::post('/mb/update', 'Admin\MasterBank\MasterBankController@updatebank')->name('mb.updatebank');
        Route::delete('/delete/{id}', 'Admin\MasterBank\MasterBankController@destroy')->name('mb.destroy');
    });

    /*
    |--------------------------------------------------------------------------
    | Route crud vendor
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'vendor',
    ], function () {
        Route::get('/index', 'Admin\Vendor\VendorController@index')->name('vendor.index');
        Route::post('/tambah-vendor', 'Admin\Vendor\VendorController@tambah')->name('vendor.tambah');
    });

    /*
    |--------------------------------------------------------------------------
    | Route saldo reseller
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'history_saldo',
    ], function () {
        Route::get('/index', 'Admin\Saldo\HistorySaldoController@index')->name('histori-saldo.index');
        Route::get('/dtl/{id}', 'Admin\Saldo\HistorySaldoController@dtl')->name('histori-saldo.dtl');
        // Route::post('/tambah-vendor', 'Admin\Vendor\VendorController@tambah')->name('vendor.tambah');
    });


    /*
    |--------------------------------------------------------------------------
    | Route History Penjualan Vendor
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'history-vendor',
    ], function () {
        Route::get('/penjualan', 'Admin\Saldo\HistoryVendorController@penjualan')->name('histori-vendor.penjualan');
        Route::get('/cari-penjualan', 'Admin\Saldo\HistoryVendorController@caripenjualan')->name('cari.penjualan');
        Route::get('/total-penjualan', 'Admin\Saldo\HistoryVendorController@totalPenjualan')->name('histori-vendor.total-penjualan');

    });

    /*
    |--------------------------------------------------------------------------
    | Route Penghasilan vendor
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'penghasilan',
    ], function () {
        Route::get('/index', 'Admin\Penghasilan\PenghasilanController@index')->name('penghasilan');
    });

    /*
    |--------------------------------------------------------------------------
    | Route komisi buyyer
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'komisi-buyyer',
    ], function () {
        Route::get('/index', 'Admin\KomisiBuyyer\KomisiController@index')->name('komisi.buyyer');
        Route::post('/simpan-grup', 'Admin\KomisiBuyyer\KomisiController@simpan')->name('simpan.komisi.buyyer');
        Route::post('/grup-komisi', 'Admin\KomisiBuyyer\KomisiController@grup')->name('grup.komisi.buyyer');
        Route::get('/dtl/{id}', 'Admin\KomisiBuyyer\KomisiController@dtl')->name('detail.komisi.produk');
        Route::get('/komisi-buyyer-delete/{id}', 'Admin\KomisiBuyyer\KomisiController@delete')->name('komisi.produk.delete');
        Route::get('/komisi-dtl-delete/{id}', 'Admin\KomisiBuyyer\KomisiController@delete_dtl')->name('komisi.dtl.delete');
        Route::post('/komisi-dtl-delete-multiple', 'Admin\KomisiBuyyer\KomisiController@delete_dtl')->name('komisi.dtl.delete.multiple');
        Route::get('/preview-edit/{id}', 'Admin\KomisiBuyyer\KomisiController@preview_edit')->name('preview.edit.komisi.buyyer');
        Route::post('/edit', 'Admin\KomisiBuyyer\KomisiController@edit')->name('edit.komisi.buyyer');
        Route::get('/preview', 'Admin\KomisiBuyyer\KomisiController@preview')->name('preview');
    });

    /*
    |--------------------------------------------------------------------------
    | Route kredit topup
    |--------------------------------------------------------------------------
    |*/
    Route::group([
        'prefix' => 'topup',
    ], function () {
        Route::get('/index', 'Admin\KreditTopup\TopupController@index')->name('kredit.topup');
        Route::post('/topup', 'Admin\KreditTopup\TopupController@topup')->name('topup.saldo');
        
    });
});

