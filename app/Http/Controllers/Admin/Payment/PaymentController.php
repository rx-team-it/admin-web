<?php

namespace App\Http\Controllers\Admin\Payment;

use App\Http\Controllers\Controller;
use App\Model\HistoryPayment;
use Illuminate\Http\Request;
use App\Model\TestBankTransaction;
use App\Model\HistoryPaymentRekening;
use App\Model\UserDetails;
use App\Model\HistorySaldoReseller;
use App\Model\UserProfile;
use App\Model\MasterStore;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\KonfirmTarikDana;
use App\Mail\TolakTarikDana;

class PaymentController extends Controller
{
    public function index(request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $history = HistoryPaymentRekening::whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $history = HistoryPaymentRekening::whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $history = HistoryPaymentRekening::orderBy('created_at', 'DESC')->get();
            }

            // dd($datas);

            return datatables()->of($history)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("payment.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Detail</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.payment.index', compact('pesan'));
    }

    public function penarikan_berhasil(request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $history = HistoryPaymentRekening::where('status', '=', 1)->whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $history = HistoryPaymentRekening::where('status', '=', 1)->whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $history = HistoryPaymentRekening::where('status', '=', 1)->orderBy('created_at', 'DESC')->get();
            }

            // dd($datas);

            return datatables()->of($history)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("payment.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Detail</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.payment.index', compact('pesan'));
    }

    public function penarikan_gagal(request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $history = HistoryPaymentRekening::where('status', '=', 2)->whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $history = HistoryPaymentRekening::where('status', '=', 2)->whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $history = HistoryPaymentRekening::where('status', '=', 2)->orderBy('created_at', 'DESC')->get();
            }

            // dd($datas);

            return datatables()->of($history)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("payment.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Detail</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.payment.index', compact('pesan'));
    }

    public function detail($id)
    {
        $history = HistoryPaymentRekening::where(['id' => $id])->with('user', 'store.address', 'payment')->first();
        // return $history->store->address->countrys;
        // $history['saldo'] = HistoryPayment::where(['id' => $history->history_payment_id])->get();
        $user = UserDetails::where(['id_user' => $history->user_id])->with('user')->first();

        $history_payment = DB::table('history_payment')->where(['id' => $history->history_payment_id])->sum('history_payment.komisi');

        $his = HistoryPayment::where(['id' => $history->history_payment_id])->first();

        // return $history;

        $pesan = $this->pesan;

        return view('admin.payment.detail', compact('history', 'history_payment', 'pesan', 'user'));
    }

    public function selesai($id)
    {
        $order = HistoryPaymentRekening::where(['id' => $id])->first();
        $history = HistoryPayment::where(['id' => $order->history_payment_id])->first();

        $dana_reseller = $order->total_harga;
        $dana = $history->komisi;

        $order->update(['status' => 1]);

        $history->update([
            'komisi' => $dana - $dana_reseller,
        ]);

        HistorySaldoReseller::create([
            'user_id' => $history->user_id,
            'user_reff_id' => $history->user_reff_id,
            'nominal' => $order->total_harga,
            'keterangan' => 'Pengurangan saldo'
        ]);

        Mail::to($order->user->email)->send(new KonfirmTarikDana());

        if (Mail::failures()) {
            return redirect()->back()->with('success', 'Email gagal !');
        } else {
            return redirect()->back()->with(['success' => 'Sukses!']);
        }
    }

    public function tolak($id)
    {
        $order = HistoryPaymentRekening::where(['id' => $id])->first();

        $order->update(['status' => 2]);

        Mail::to($order->user->email)->send(new KonfirmTarikDana());

        return redirect()->back()->with('success', 'Sukses Permintaan Tarik Saldo Ditolak');
    }
}
