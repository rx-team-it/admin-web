<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Slider;
use App\Model\HistoryImage;
use Illuminate\Support\Facades\Validator;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    public function index()
    {
        $s = DB::select("SELECT * FROM image_landing where type_content = 'slider'");
        $b = DB::select("SELECT * FROM image_landing where type_content = 'banner'");
        $bp = DB::select("SELECT * FROM image_landing where type_content = 'banner-promo'");
        $bp1 = DB::select("SELECT * FROM image_landing where type_content = 'banner-promo1'");
        $bw = DB::select("SELECT * FROM image_landing where type_content = 'banner-wide'");
        $cb = DB::select("SELECT count(*) as banner FROM image_landing where type_content = 'banner'");
        $cs = DB::select("SELECT count(*) as slider FROM image_landing where type_content = 'slider'");
        $cbp = DB::select("SELECT count(*) as bp FROM image_landing where type_content = 'banner-promo'");
        $cbp1 = DB::select("SELECT count(*) as bp FROM image_landing where type_content = 'banner-promo1'");
        $cbw = DB::select("SELECT count(*) as bw FROM image_landing where type_content = 'banner-wide'");
        $slidder = Slider::wherenull('store_id')->where('type_content', 'slider')->paginate(10);
        $banner = Slider::wherenull('store_id')->wherein('type_content', ['banner', 'banner-wide', 'banner-promo'])->paginate(10);
        $no = 1;
        $pesan = $this->pesan;

        return view('admin.slider.indexnew', compact('banner', 'slidder', 'pesan', 'no', 's', 'b', 'cb', 'cs', 'bp', 'bw', 'cbp', 'cbw', 'bp1', 'cbp1'));
    }

    public function createbanner(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|file|max:5120'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "banner-" . time() . '.' . $file->getClientOriginalExtension();
            // resize gambar
            $img = Image::make($file)->resize(150, 150);
            $newfile = Storage::disk('s3')->put('public/assets/' . $filename, $img->stream());

            $save = Slider::create([
                'type_content' =>  'banner',
                // 'description' => $request->description,
                'urutan_banner' => $request->urutan_banner,
                'image' => $filename,
            ]);
            $save_history = HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        return redirect(route('slider.home'))->with(['success' => 'Banner Berhasil Ditambahkan']);
    }

    public function createslider(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|file|max:5120'
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "banner-" . time() . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file)->resize(900, 300);
            $newfile = Storage::disk('s3')->put('public/assets/' . $filename, $img->stream());

            $save = Slider::create([
                'type_content' =>  'slider',
                // 'description' => $request->description,
                'image' => $filename,
            ]);
            $save_history = HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        return redirect(route('slider.home'))->with(['success' => 'Banner Berhasil Ditambahkan']);
    }

    public function createbp(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|file|max:5120'
        ]);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "banner-" . time() . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file)->resize(900, 160);
            $newfile = Storage::disk('s3')->put('public/assets/' . $filename, $img->stream());

            $save = Slider::create([
                'type_content' =>  'banner-promo',
                // 'description' => $request->description,
                'image' => $filename,
            ]);
            $save_history = HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        return redirect(route('slider.home'))->with(['success' => 'Banner Berhasil Ditambahkan']);
    }

    public function createbp1(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|file|max:5120'
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "banner-" . time() . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file)->resize(1200, 240);
            $newfile = Storage::disk('s3')->put('public/assets/' . $filename, $img->stream());

            $save = Slider::create([
                'type_content' =>  'banner-promo1',
                // 'description' => $request->description,
                'image' => $filename,
            ]);
            $save_history = HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        return redirect(route('slider.home'))->with(['success' => 'Banner Berhasil Ditambahkan']);
    }

    public function createbw(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|file|max:5120'
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "banner-" . time() . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file)->resize(300, 138);
            $newfile = Storage::disk('s3')->put('public/assets/' . $filename, $img->stream());

            $save = Slider::create([
                'type_content' =>  'banner-wide',
                // 'description' => $request->description,
                'image' => $filename,
            ]);
            $save_history = HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        return redirect(route('slider.home'))->with(['success' => 'Banner Berhasil Ditambahkan']);
    }

    public function store(Request $request)
    {
        // 
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request)
    {
        $slidder = Slider::where('id', $request->id)->first();

        $old_img = 'public/assets/' . $slidder->image;
        $filename = !empty($slidder->image) ? $slidder->image : NULL;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "banner-" . time() . '.' . $file->getClientOriginalExtension();

            //delete old img
            Storage::disk('s3')->delete($old_img);

            $newfile = Storage::disk('s3')->put('public/assets/' . $filename, file_get_contents($request->file('file')));
        }

        $slidder->update([
            'type_content' =>  $request->type_content,
            // 'description' => $request->description,
            'image' => $filename,
            'alt' => '..'
        ]);

        HistoryImage::create([
            'gambar' =>  $filename,
            'ukuran' => $ukuran,
            'user_id' => Auth::user()->id,
        ]);
        return redirect()->back()->with(['success' => 'Banner Berhasil Diperbaharui']);
    }

    public function destroy($id)
    {
        $b = DB::delete("DELETE FROM image_landing where id=?", [$id]);

        return redirect()->back()->with(['success' => 'Data berhasil di hapus']);
    }

    public function deletebannertoko($id)
    {
        $slider = Slider::find($id);
        $slider->delete();

        return redirect()->back()->with(['success', 'Banner Berhasil Di Delete']);
    }

    //////////////// FUNCTION BARU SLIDER ///////////////////

    public function indexnew()
    {
        $pesan = $this->pesan;

        return view('admin.slider.indexnew', compact('pesan'));
    }
}
