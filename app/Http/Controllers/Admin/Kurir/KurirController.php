<?php

namespace App\Http\Controllers\Admin\Kurir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MasterKurir;
use App\Model\MasterStore;
use App\Model\MasterAdress;
use App\Model\MasterKurirService;
use DB;

class KurirController extends Controller
{
    public function index()
    {
        $kur = MasterKurir::get();
        $kur->map(function($kurs){
            $kurs['service'] = MasterKurirService::where('kurir_id', $kurs->id)->get();
            $total = 0;
            foreach($kurs['service'] as $ser){
                $total += $ser->diskon;
                if($total !== 0){
                    $kurs['cashback'] = true;
                }else{
                    $kurs['cashback'] = false;
                }
                
            }
            return $kurs;
        });
        // return $kur;
        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $address = MasterAdress::where(['id' => $store->address->id])->first();

        $pesan = $this->pesan;

        return view('admin.kurir.index', compact('kur', 'pesan','store','address'));
    }

    public function tambah(request $request)
    {
        MasterKurir::create([
            'nama' =>  $request->nama
        ]);

        return redirect()->back()->with(['success' => 'Berhasil Mengganti status']);
    }

    public function update(request $request)
    {
        MasterKurir::where('nama', $request->nama)->update([
            'status' => $request->status,
        ]);

        return redirect()->back()->with(['success' => 'Berhasil Mengganti status']);
    }

    public function hapus($id)
    {
        $d = DB::delete("DELETE FROM master_kurir where id=?", [$id]);

        return redirect()->back()->with(['success' => 'Kurir berhasil di hapus']);
    }
}
