<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Model\Cart;
use App\Model\Category;
use App\Model\HistoryProductVendor;
use App\Model\MasterStore;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Product;
use App\Model\ProductDetail;
use App\Model\ProductFlashSale;
use App\Model\ProductImage;
use App\Model\ProductSum;
use App\Model\ProductVarian;
use App\Model\ProductVarianOption;
use App\Model\ProductVendor;
use App\Model\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index(request $request)
    {
        $store = MasterStore::where('status', '=', 1)->orderBy('created_at', 'DESC')->get();

        if ($request->ajax()) {
            return datatables()->of($store)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("produk.show", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-eye"></i> Detail</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.product.index', compact('pesan'));
    }

    public function show(request $request, $id)
    {
        $store = MasterStore::find($id);
        $product = Product::where(['id_store' => $store->id])->with('product_images')->orderBy('created_at', 'DESC')->get();

        if ($request->ajax()) {
            return datatables()->of($product)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("produk.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Show</a>';
                    return $button;
                    // $button = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" class="btn btn-danger">Delete</a>';
                    // $button = '<form action="' . route("produk.blok", $data->id) . '" method="POST">
                    //     ' . csrf_field() . '
                    //     <a class="btn btn-primary btn-sm" href="' . route("produk.detail", $data->id) . '"><i class="fas fa-cog"></i></a>
                    //     <button type="submit"  class="btn btn-danger btn-sm"
                    //         onclick="return confirm(\'Block Product?\')"
                    //         style="">X</a>
                    //     </form>';
                })
                ->addColumn('image', function ($data) {
                    // foreach ($data as $datas) {
                    //     $datas['product_images']->img_product;
                    // }

                    // $image = Storage::Disk("s3")->url("public/images/product/" . $data['img_product']);
                    // return '<img src="' . $image . '" height="80px" width="80px" />';

                    // $url = '. Storage::Disk("s3")->url("public/images/ . $data->image") .';
                    // return '<img src=' . $url . ' border="0" width="40" align="center" />';

                    // $image = '<src="{{Storage::disk('s3')->url('public/images/product/'. $sub2['img']['img_product'])}}" width="320px" height="340px">';
                    // return $image;
                })
                ->rawColumns(['action', 'image'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.product.show', compact('store', 'pesan'));
    }

    public function detail($id)
    {
        $product = Product::where(['id' => $id])->first();
        $productDetail = ProductDetail::where('product_id', $id)->first();

        $product['images'] = ProductImage::where(['product_id' => $product->id])->get();

        $product['varian'] = ProductVarian::where(['product_id' => $product->id])->first();
        if ($product['varian'] !== null) {

            $product['varian']['varian_id'] = ProductVarianOption::where('product_varian_id', $product['varian']['id'])->get();

            foreach ($product['varian']['varian_id'] as $harga) {
                $harga['detail'] = ProductDetail::where('product_varian_option_id', $harga->id)->first();
            }

            foreach ($product['varian']['varian_id'] as $sub) {

                $sub['sub'] = ProductVarian::where('sub_variasi', $sub->product_varian_id)->first();
                if ($sub['sub'] !== null) {
                    $sub['sub']['sub2'] = ProductVarianOption::where('parent_option', $sub->id)->get();

                    foreach ($sub['sub']['sub2'] as $det) {
                        $det['detail'] = ProductDetail::where('product_varian_option_id', $det->id)->first();
                    }
                } else {
                    $sub['sub'] = null;
                }
            }
        } else {
            $product['varian'] == null;
        }

        $pesan = $this->pesan;
        // return $product;

        return view('admin.product.detail', compact('product', 'pesan', 'productDetail'));
    }

    public function blok($id)
    {
        $datas = Product::find($id);
        // $datas->delete();

        // update table produk
        $datas->update([
            'is_block' => 1,
        ]);

        // update table cart
        $cart = Cart::where('product_id', $id)->get();
        foreach ($cart as $test) {
            $test->update([
                'is_block' => 1,
            ]);
        }

        // update table product_vendor
        $d = ProductVendor::where('product_id', $id)->get();
        foreach ($d as $test1) {
            $test1->update([
                'is_block' => 1,
            ]);
        }

        // update table panel_flashsale
        $panel_flashsale = ProductFlashSale::where('product_id', $id)->get();
        foreach ($panel_flashsale as $c) {
            $c->update([
                'is_block' => 1,
            ]);
        }

        // update table wishlist
        $wishlist = Wishlist::where('product_id', $id)->get();

        foreach ($wishlist as $w) {
            $w->update([
                'is_block' => 1,
            ]);
        }

        // return redirect()->route('list.produk')->with('success', 'Product Berhasil Di Blok');
        return redirect()->back()->with('success', 'Product Berhasil Di Blok');
    }

    public function unblok($id)
    {
        $datas = Product::withTrashed()->where('id', $id)->first();
        $datas->update([
            'is_block' => 0,
        ]);
        $datas->restore();

        // update table cart
        $cart = Cart::where('product_id', $id)->get();
        foreach ($cart as $test) {
            $test->update([
                'is_block' => 0,
            ]);
        }

        // update table cart
        $wishlist = Wishlist::where('product_id', $id)->get();
        foreach ($wishlist as $test) {
            $test->update([
                'is_block' => 0,
            ]);
        }

        // update table panel_flashsale
        $panel_flashsale = ProductFlashSale::where('product_id', $id)->get();
        foreach ($panel_flashsale as $c) {
            $c->update([
                'is_block' => 0,
            ]);
        }

        //update table produk vendor
        $product_vendor = Product::where('id', $id)->first();
        $product_vendor->update([
            'is_block' => 0,
        ]);
        

        return redirect()->route('list.produk')->with('success', 'Product Berhasil Di UnBlock');
    }

    public function delete($id)
    {
        $datas = Product::withTrashed()->where('id', $id)->first();
        $datas->forceDelete();

        return redirect()->route('produk.index')->with('success', 'Product Berhasil Di Delete Permanen');
    }

    public function listblok(request $request)
    {
        $datas = Product::where('is_block', '1')->orderBy('created_at', 'DESC')->get();
        // $product = Product::where('is_block', '1')->orderBy('created_at', 'DESC')->with('category')->get();

        if ($request->ajax()) {
            foreach ($datas as $key => $value) {

                $prod_img = ProductImage::where('product_id', $value->id)->first();
                if (!empty($prod_img->img_product)) {
                    $image = Storage::Disk("s3")->url("public/images/product/" . $prod_img->img_product);
                    $datas[$key]['thumbnail'] = '<img alt="" src="' . $image . '" width="80px" height="80px">';
                } else {
                    $datas[$key]['thumbnail'] = "";
                }

                if (strlen($value->name) > 30) {
                    $datas[$key]['name'] = substr($value->name, 0, 30) . '...';
                } else {
                    $datas[$key]['name'] = $value->name;
                }

                $datas[$key]['kategori'] = $value->category->name;
            }

            return datatables()->of($datas)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("produk.unblock", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="fa fa-lock-open"></i> Buka blok</a>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.product.list', compact('pesan'));
    }

    public function productlist($id)
    {
        $store = MasterStore::find($id);
        $products = Product::where(['id_store' => $store->id])->with('category', 'product_images', 'product_varians.product_varian_option')->paginate(6);

        $pesan = $this->pesan;

        return view('admin.product.productlist', compact('store', 'products', 'pesan'));
    }

    public function list_produk(Request $request)
    {
        $product = Product::where('is_block', 0)->orderBy('created_at', 'DESC')->with('category')->with('user')->get();
        // return $product;
        if ($request->ajax()) {

            foreach ($product as $key => $value) {

                $prod_img = ProductImage::where('product_id', $value->id)->first();
                if (!empty($prod_img->img_product)) {
                    $image = Storage::Disk("s3")->url("public/images/product/" . $prod_img->img_product);
                    $product[$key]['thumbnail'] = '<img alt="" src="' . $image . '" width="80px" height="80px">';
                } else {
                    $product[$key]['thumbnail'] = "";
                }

                $prod_stok = ProductDetail::where('product_id', $value->id)->sum("qty");
                if (!empty($prod_stok)) {
                    if ($prod_stok < 10) {
                        $product[$key]['stock'] = '<span class="text-danger">' . $prod_stok . '</span>';
                    } else {
                        $product[$key]['stock'] = '<span>' . $prod_stok . '</span>';
                    }
                } else {
                    $product[$key]['stock'] = '<span class="text-danger">' . '0' . '</span>';
                }
                // get product terjual dari table order status = 5
                // $get_order = Order::where('status', 5)->get();
                // $total_qty = 0;
                $terjual = ProductSum::where('product_id', $value->id)->first();
                    if(!empty($terjual)){
                        $total_qty = $terjual->terjual;
                    }else{
                        $total_qty = 0;
                    }
                $total_product_terjual = HistoryProductVendor::where('product_id', $value->id)->sum("qty");
                if (strlen($value->name) > 30) {
                    $product[$key]['name'] = '<span>'.substr($value->name, 0, 30) . '...'. '</span>';
                } else {
                    $product[$key]['name'] ='<span>'. $value->name . '</span>';
                }
                $product[$key]['name'] = "<span class='font-weight-bold'>".$product[$key]['name']."</span><br><span>Produk yang terjual: $total_qty</span>";
                $product[$key]['kategori'] = '<span>'. $value->category->name. '</span>';
                $product[$key]['created_user'] = '<span>'.$value->user->name . '</span>';
            }

            return datatables()->of($product)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("produk.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-eye"></i></a>
                    <form method="post" action="' . route('produk.blok', $data->id) . '">
                    <input type="hidden" name="_token" value=" ' . csrf_token() . ' ">
                    <button class="edit btn btn-info btn-sm edit-post mt-2" title="Arsip Produk"> <i class="fas fa-archive"></i></button>
                    </form>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.product.indexnew', compact('product', 'pesan'));
    }
}
