<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use App\Model\GroupMasterProduct;
use App\Model\Product;
use App\Model\ProductFlashSale;
use App\Model\ProductImage;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;
use Str;

class PanelProductController extends Controller
{
    public function create_group(Request $request)
    {
        $validate = $this->validate($request, [
            'image' => 'image|mimes:jpg,png,jpeg|max:20000',
        ]);

        if ($request->sesi) {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
                $image = Image::make($file)->resize(1200, 600);
                $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());
    
                $c = GroupMasterProduct::create([
                    'nm_group' => $request->nm_group,
                    'type' => $request->type,
                    'diskon' => $request->diskon,
                    'slug' => Str::slug($request->nm_group, '-'),
                    'event' => $request->min,
                    'sesi_awal' => explode('-',$request->sesi)[0],
                    'sesi_akhir' => explode('-',$request->sesi)[1],
                    'image' => $filename,
                ]);
            }
        } else {
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
                $image = Image::make($file)->resize(1200, 600);
                $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());
    
                $c = GroupMasterProduct::create([
                    'nm_group' => $request->nm_group,
                    'type' => $request->type,
                    'diskon' => $request->diskon,
                    'slug' => Str::slug($request->nm_group, '-'),
                    'event' => $request->min,
                    'sesi_awal' => null,
                    'sesi_akhir' => null,
                    'image' => $filename,
                ]);
            }
        }
        
        // return $c;

        return redirect()->back()->with('message','Group Berhasil Dibuat');
    }

    public function create_group_banner(request $request)
    {
        // return $request;
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(1200, 600);
            $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());

            $banner = GroupMasterProduct::where('id', '=', $request->id);
            $banner->update([
                'image' => $filename,
            ]);
        }
        return redirect()->back()->with('message','foto banner telah di buat');
    }

    public function banner_edit_foto(request $request)
    {
        $file = $request->file('image');
        $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
        $image = Image::make($file)->resize(1200, 600);
        $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());

        $banner = GroupMasterProduct::where('id', '=', $request->id);
            $banner->update([
                'image' => $filename,
            ]);

        return redirect()->back()->with('message','Foto banner telah di ubah');
    }

    public function banner_hapus_foto($id)
    {
        $banner = DB::select('SELECT image FROM group_master_product where id=?',[$id])[0];
        $old_img = 'public/images/BannerImage/' . $banner->image;
        Storage::disk('s3')->delete($old_img);
        
        $updt = DB::select('UPDATE group_master_product
                            SET image = NULL
                            WHERE id = ?',[$id]);

        return redirect()->back()->with('message','foto banner telah di hapus');
    }

    public function panel()
    {
        $product = Product::where('is_block', '=', '0')->orderBy('created_at', 'DESC')->with('product_images_thumbnail')->get();
        $group_product = GroupMasterProduct::orderBy('created_at', 'DESC')->with(['flash_sale.product.product_images_thumbnail'])->get();
        $count_down = GroupMasterProduct::where('type', '=', 'FlashSale')->first(['id','event','sesi_awal','sesi_akhir']);
        if (empty($count_down)) {
            $cd = "null";
        } else {
            $cd = $count_down;
        }
        
        // return $gm;
        $pesan = $this->pesan;

        return view('admin.panel_product.group_product.index', compact('pesan', 'product', 'group_product','cd','count_down'));
    }

    public function tambah_product(Request $request)
    {
        $this->validate($request, [
            'group' => 'required|nullable',
            'product_id' => 'required|nullable'
        ]);

        
        $c = $request->input('group');
        foreach ($request->product_id as $key => $value) {
            $s = new ProductFlashSale();
            $s->product_id = $value;
            $s->id_group_master = $c;
            
            // check produk
            $w = ProductFlashSale::where('id_group_master', '=', $c)
            ->where('product_id', '=', $value)
            ->get();
            
            $t = ProductFlashSale::whereNotIn('id_group_master', [$c])
                ->where('product_id', '=', $value)
                ->get();
                
                // return $t;
                
                // kondisi abis di cek 
            if ($c == 5) {
                if (!empty($t[0]->product_id) || !empty($w[0]->product_id)) {
                    return redirect()->back()->with('gagal','Produk yang di pilih telah masuk ke grup lain');
                } else {
                    $s->save();
                }
            } else {
                if (!empty($w[0]->product_id)) {
                    return redirect()->back()->with('gagal','Produknya udah masuk di grup terpilih kak');
                } else {
                    $s->save();
                }
                
            }
        }
        // return $request;

        return redirect()->back()->with('message','Produk Berhasil Ditambahkan');
    }

    public function delete($id)
    {
        $km = DB::delete("DELETE FROM product_flashsale where product_id=?", [$id]);

        return redirect()->back()->with('message','Produk berhasil di hapus');
    }
    
    public function deletePanelGrup($id)
    {
        $dpg = DB::delete("DELETE FROM group_master_product where id=?", [$id]);
        $d = DB::delete("DELETE FROM product_flashsale where id_group_master=?", [$id]);

        return redirect()->back()->with('message','Grup Produk berhasil di hapus');
    }

    public function delete_produk(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required'
        ]);

        // hapus data multiple
        foreach ($request->product_id ? $request->product_id : 0 == 0 as $key => $value) {
            ProductFlashSale::whereIn('id',explode(",",$value))->delete();
        }
        return redirect()->back()->with('message','Produk berhasil di hapus');
    }
}