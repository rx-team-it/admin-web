<?php

namespace App\Http\Controllers\Admin\Seller;

use App\Http\Controllers\Controller;
use App\Model\Product;
use App\Model\ProductDetail;
use App\Model\ProductImage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SellerController extends Controller
{
    public function show (request $request, $id) 
    {
        $user = User::where('id', '=', $id)->first();
        $products = Product::where('created_user', $id)->where('is_block', 0)->orderBy('created_at', 'DESC')->with('category')->get();

        if ($request->ajax()) {

            foreach ($products as $key => $value) {

                $prod_img = ProductImage::where('product_id', $value->id)->first();
                if (!empty($prod_img->img_product)) {
                    $image = Storage::Disk("s3")->url("public/images/product/" . $prod_img->img_product);
                    $products[$key]['thumbnail'] = '<img alt="" src="' . $image . '" width="80px" height="80px">';
                } else {
                    $products[$key]['thumbnail'] = "";
                }

                $prod_stok = ProductDetail::where('product_id', $value->id)->sum("qty");
                if (!empty($prod_stok)) {
                    if ($prod_stok < 10) {
                        $products[$key]['stock'] = '<p class="text-danger">' . $prod_stok . '</p>';
                    } else {
                        $products[$key]['stock'] = '<p>' . $prod_stok . '</p>';
                    }
                } else {
                    $products[$key]['stock'] = '<p class="text-danger">' . '0' . '</p>';
                }

                if (strlen($value->name) > 30) {
                    $products[$key]['name'] = substr($value->name, 0, 30) . '...';
                } else {
                    $products[$key]['name'] = $value->name;
                }

                $products[$key]['kategori'] = $value->category->name;
            }

            return datatables()->of($products)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("produk.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-eye"></i> Detail</a>
                    <form method="post" action="' . route('produk.blok', $data->id) . '">
                    <input type="hidden" name="_token" value=" ' . csrf_token() . ' ">
                    <button class="edit btn btn-danger btn-sm edit-post mt-2"> <i class="fas fa-ban"></i> Blok</button>
                    </form>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view('admin.seller.detail', compact('user'));
    }
}
