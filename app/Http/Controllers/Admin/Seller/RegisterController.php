<?php

namespace App\Http\Controllers\Admin\Seller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\UserProfile;
use App\Model\AccessRole;
use App\Model\MasterStore;
use App\Model\Order;
use App\Model\UserDetails;
use Illuminate\Support\Facades\Validator;
use App\User;

class RegisterController extends Controller
{
    public function index()
    {
        $user = AccessRole::where('role_id', '=', 2)->paginate(6);

        $pesan = $this->pesan;

        return view('admin.regist_seller.index', compact('user', 'pesan'));
    }

    public function register()
    {
        $pesan = $this->pesan;

        return view('admin.regist_seller.register', compact('pesan'));
    }

    public function daftar(Request $request)
    {
        $rules = [
            'name' => 'required|string|between:4,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'phone' => 'required|numeric|min:9',
        ];

        $messages = [
            'name.between'           => 'Minimal 4 Karakter',
            'name.required'          => 'Nama wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 5 karakter.',
            'password.confirmed'     => 'Password Tidak Sesuai',
            'email.required'         => 'Email wajib diisi.',
            'email.email'            => 'Email tidak valid.',
            'email.unique'           => 'Email sudah terdaftar.',
            'phone.numeric'          => 'Wajib Diisi Dengan Angka',
            'phone.required'         => 'Nomor Telp Wajib Diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        } else {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt($request->password)
            ]);

            AccessRole::create([
                'role_id' => 2, //seller
                'user_id' => $user->id
            ]);

            MasterStore::create([
                'user_id' => $user->id
            ]);

            UserDetails::create([
                'id_user' => $user->id
            ]);
        }

        return response()->json(['status' => 1, 'msg' => 'Seller Berhasil Dibuat']);
    }

    public function re(Request $request)
    {
        $rules = [
            'name' => 'required|string|between:4,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'phone' => 'required|numeric|min:9',
        ];

        $messages = [
            'name.between'           => 'Minimal 4 Karakter',
            'name.required'          => 'Nama wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 5 karakter.',
            'password.confirmed'     => 'Password Tidak Sesuai',
            'email.required'         => 'Email wajib diisi.',
            'email.email'            => 'Email tidak valid.',
            'email.unique'           => 'Email sudah terdaftar.',
            'phone.numeric'          => 'Wajib Diisi Dengan Angka'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password)
        ]);

        AccessRole::create([
            'role_id' => 2, //seller
            'user_id' => $user->id
        ]);

        MasterStore::create([
            'user_id' => $user->id
        ]);

        return redirect()->back()->with(['success' => 'Berhasil Buat Seller']);
    }
}
