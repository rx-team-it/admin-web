<?php

namespace App\Http\Controllers\Admin\ListCustomer;

use App\Http\Controllers\Controller;
use App\Model\AccessRole;
use Illuminate\Http\Request;
use App\Model\UserDetails;
use App\User;
use Illuminate\Support\Facades\DB;

class ListController extends Controller
{
    public function index(request $request)
    {
        $datas = UserDetails::with(['user', 'countrys', 'citys', 'states'])->orderBy('created_at', 'DESC')->get();

        if ($request->ajax()) {
            return datatables()->of($datas)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("customer.edit", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Show</a>';
                    return $button;
                })
                ->addColumn('address', function ($data) {
                    $address = (!empty($data) ? $data->address : '-') . '</br>' .
                        (!empty($data->countrys) ? $data->countrys->name : '-') . '</br>' .
                        (!empty($data->states) ? $data->states->name : '-') . '</br>' .
                        (!empty($data->citys) ? $data->citys->name : '-');
                    return $address;
                })
                ->rawColumns(['action', 'address'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.customer.index', compact('datas', 'pesan'));
    }

    public function edit($id)
    {
        $datas = UserDetails::with(['user', 'countrys', 'citys', 'states'])->find($id);

        $pesan = $this->pesan;

        return view('admin.customer.edit', compact('datas', 'pesan'));
    }

    public function list(request $request)
    {
        $datas = UserDetails::onlyTrashed()->with(['user', 'countrys', 'citys', 'states'])->orderBy('created_at', 'DESC')->get();

        if ($request->ajax()) {
            return datatables()->of($datas)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("customer.unblock", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Un Block</a>';
                    return $button;
                })
                ->addColumn('address', function ($data) {
                    $address = (!empty($data) ? $data->address : '-') . '</br>' .
                        (!empty($data->countrys) ? $data->countrys->name : '-') . '</br>' .
                        (!empty($data->states) ? $data->states->name : '-') . '</br>' .
                        (!empty($data->citys) ? $data->citys->name : '-');
                    return $address;
                })
                ->rawColumns(['action', 'address'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.customer.list', compact('pesan'));
    }

    public function blok($id)
    {
        $datas = User::find($id);
        $datas->delete();

        return redirect()->back()->with('success', 'User Berhasil Di Blok');
    }

    public function unblok($id)
    {
        $datas = User::withTrashed()->where('id', $id)->first();
        $datas->restore();

        return redirect()->back()->with('success', 'User Berhasil Di UnBlock');
    }

    public function delete($id)
    {
        $datas = User::withTrashed()->where('id', $id)->first();
        $datas->forceDelete();

        $role = AccessRole::where(['user_id' => $datas->id])->delete();

        return redirect()->back()->with('success', 'User Berhasil Di Delete Permanen');
    }

    // FUNCTION DIATAS ADALAH FUNCTION OLD DARI LIST USER // 

    public function user()
    {
        // query list user //
        // $user = User::orderBy('created_at', 'DESC')->get();
        // $user = Posts::join('category', 'posts.category_id', '=', 'category.id')
        //     ->select('posts.*', 'category.name as categoy_name', 'category.id as category_id')
        //     ->get();

        $user = User::join('role_users', 'users.id', '=', 'role_users.user_id')
            ->select('users.*', 'role_users.nm_role as role_nm', 'role_users.id as role_users_id')
            ->get();

        // query list user di block menggunakan soft delete //
        $user_blok = User::onlyTrashed()->orderBy('created_at', 'DESC')->get();

        // return $user;

        $pesan = $this->pesan;

        return view('admin.customer.user', compact('user', 'user_blok', 'pesan'));
    }
}
