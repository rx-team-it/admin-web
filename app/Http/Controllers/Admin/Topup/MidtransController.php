<?php

namespace App\Http\Controllers\Admin\Topup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SaldoKreditVendor;

class MidtransController extends Controller
{
    public function Notification(Request $request)
    {
        if ($request->transaction_status == 'pending') {
            $status = 0;
        } else if ($request->transaction_status == 'settlement') {
            $status = 1;
        } else if ($request->transaction_status == 'expire') {
            $status = 2;
        } else {
            $status = 2;
        }

        $order = SaldoKreditVendor::where('invoice_topup', $request->invoice)->first();
        $order->update(['status' => $status]);
    }
}
