<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\HistoryImage;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Model\Product;
use FontLib\Table\Type\name;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function index(request $request)
    {
        $category = Category::getIndex()->with(['parent'])->orderBy('created_at', 'DESC')->paginate(10);
        $parent = Category::getParent()->orderBy('name', 'ASC')->get();
        $ortu = Category::getParent()->whereNotIn('name', ['Default'])->orderBy('name', 'ASC')->get();

        if ($request->ajax()) {
            return datatables()->of($ortu)
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" class="btn btn-danger">Delete</a>';
                    $button = '<form action="' . route("kategori.delete", $data->id) . '" method="POST">
                    ' . csrf_field() . '
                    ' . method_field('delete') . '
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#parent" data-whatever="' . $data->id . '" data-name="' . $data->name . '" data-parent_category="' . $data->parent_category . '"><i class="far fa-edit"></i></button>
                    <button type="submit"  class="btn btn-danger btn-sm"
                        onclick="return confirm(\'Yakin ingin menghapus kategori?\')"
                        style=""><i class="fas fa-times"></i></button>
                    </form>';
                    return $button;
                })
                ->addColumn('image', function ($data) {
                    $image = Storage::Disk("s3")->url("public/images/admin/category/" . $data->image);
                    if ($data->image == null) {
                        return 'kosong';
                    } else {
                        return '<img src="' . $image . '" height="80px" width="80px" />';
                    }

                    // $url = '. Storage::Disk("s3")->url("public/images/ . $data->image") .';
                    // return '<img src=' . $url . ' border="0" width="40" align="center" />';

                    // $image = '<src="{{Storage::disk('s3')->url('public/images/product/'. $sub2['img']['img_product'])}}" width="320px" height="340px">';
                    // return $image;
                })
                ->addColumn('foto_banner_kategori', function ($data) {
                    $image = Storage::Disk("s3")->url("public/images/BannerImageKategori/" . $data->foto_banner_kategori);
                    if ($data->foto_banner_kategori == null) {
                        return 'kosong';
                    } else {
                        return '<img src="' . $image . '" height="80px" width="80px" />';
                    }
                    
                })
                ->rawColumns(['action', 'image', 'foto_banner_kategori'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.category.index', compact('category', 'parent', 'ortu', 'pesan'));
    }

    public function child(Request $request)
    {
        $category = Category::getIndex()->with(['parent'])->orderBy('created_at', 'DESC')->get();

        if ($request->ajax()) {
            return datatables()->of($category)
                // ->addColumn('action', function ($data) {
                //     $button = '<button type="button" data-id="' . $data->id . '" class="btn btn-danger btn-sm swal-confirm">
                //     <form action="' . route("kategori.delete", $data->id) . '" method="POST">
                //     ' . csrf_field() . '
                //     </form>
                //     <i class="fas fa-times"></i>
                //     </button>
                //     <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="' . $data->id . '" data-name="' . $data->name . '" data-parent_category="' . $data->parent_category . '"><i class="far fa-edit"></i></button>';
                //     return $button;
                // })
                ->addColumn('action', function ($data) {
                    $button = '<a href="javascript:void(0)" class="btn btn-danger">Delete</a>';
                    $button = '<form action="' . route("kategori.delete", $data->id) . '" method="POST">
                    ' . csrf_field() . '
                    ' . method_field('delete') . '
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="' . $data->id . '" data-name="' . $data->name . '" data-parent_category="' . $data->parent_category . '"><i class="far fa-edit"></i></button>
                    <button type="submit"  class="btn btn-danger btn-sm"
                        onclick="return confirm(\'Yakin ingin menghapus sub kategori?\')"
                        style=""><i class="fas fa-times"></i></button>
                    </form>';
                    return $button;
                })
                // ->addColumn('action', function ($data) {
                //     $button = '<a href="#" class="edit btn btn-success btn-sm">Edit</a>';
                //     $button = '<form action="' . route("kategori.delete", $data->id) . '" method="POST">
                //     ' . csrf_field() . '
                //     ' . method_field('delete') . '
                //     <a class="btn btn-primary btn-sm" href="' . route("category.edit", $data->id) . '"><i class="fas fa-cog"></i></a>
                //     <button type="submit"  class="btn btn-danger btn-sm swal-confirm"
                //         style="">X</button>
                //     </form>';
                //     return $button;
                // $button = '<form action="' . route('kategori.delete', $data->id) . '" id="delete' . $data->id . '" method="POST">
                //         ' . csrf_field() . '
                //         ' . method_field('delete') . '
                //         <button type="submit" data-id="' . $data->id . '" class="btn btn-danger swal-confirm">Delete</button>
                //         </form>';
                // return $button;
                // })
                ->addColumn('image', function ($data) {
                    $image = Storage::Disk("s3")->url("public/images/admin/category/" . $data->image);
                    return '<img src="' . $image . '" height="80px" width="80px" />';

                    // $url = '. Storage::Disk("s3")->url("public/images/ . $data->image") .';
                    // return '<img src=' . $url . ' border="0" width="40" align="center" />';

                    // $image = '<src="{{Storage::disk('s3')->url('public/images/product/'. $sub2['img']['img_product'])}}" width="320px" height="340px">';
                    // return $image;
                })
                ->addColumn('parent', function ($data) {
                    $parent = (!empty($data->parent) ? $data->parent->name : '-');
                    return $parent;
                })
                ->rawColumns(['action', 'parent', 'image'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.category.index', compact('pesan'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'foto_banner_kategori' => 'image|file|max:1024'
        ]);

        $kat = $request->name;
        $sub_kat = $request->name1;
        
        if (empty($sub_kat)) {
            $kategori = $kat;
            $p = Category::where('name', 'like', '%' . $kategori .  '%')->get();
            if (!empty($p[0]->name)) {
                return redirect()->back()->with('message_error', 'Kategori telah tersedia');
            } else {
                $file = $request->file('foto_banner_kategori');
                $filename = time() . ($request->foto_banner_kategori) . '.' . $file->getClientOriginalExtension();
                $image = Image::make($file)->resize(1200, 600);
                $file = Storage::disk('s3')->put('public/images/BannerImageKategori/' . $filename, $image->stream());

                Category::create([
                    'name' =>  $kategori,
                    'slug' => Str::slug($request->name, '-'),
                    'parent_category' => $request->parent_category,
                    'foto_banner_kategori' => $filename,
                ]);
            }
        } elseif (empty($kat)) {
            $kategori = $sub_kat;
            $p = Category::where('name', 'like', '%' . $kategori .  '%')->get();
            if (!empty($p[0]->name)) {
                return redirect()->back()->with('message_error', 'Sub Kategori telah tersedia');
            } else {
                Category::create([
                    'name' =>  $kategori,
                    'slug' => Str::slug($request->name, '-'),
                    'parent_category' => $request->parent_category,
                ]);
            }
        } else {
            return redirect()->back()->with('message_error', 'Data kosong');
        }

        return redirect(route('category.index'))->with('success', 'Kategori Baru Ditambahkan!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $parent = Category::getParent()->orderBy('name', 'ASC')->get();

        $pesan = $this->pesan;

        return view('admin.category.edit', compact('category', 'parent', 'pesan'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'file' => 'image|file|max:1024',
            'foto_banner_kategori' => 'image|file|max:1024'
        ]);

        $category = Category::where('id', $request->id)->first();

        $old_img = 'public/images/admin/category/' . $category->image;
        $filename = !empty($category->image) ? $category->image : NULL;

        if ($request->hasFile('file')) {
            // icon
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "category-" . time() . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(120, 120);

            //delete old img
            Storage::disk('s3')->delete($old_img);

            $newfile = Storage::disk('s3')->put(
                'public/images/admin/category/' . $filename,
                $image->stream()
            );

            HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        if ($request->foto_banner_kategori) {
            // banner
            $file_banner = $request->file('foto_banner_kategori');
            $filename_banner = "banner-category-" . time() . '.' . $file_banner->getClientOriginalExtension();
            $image = Image::make($file_banner)->resize(1200, 600);
            $file_banner = Storage::disk('s3')->put('public/images/BannerImageKategori/' . $filename_banner, $image->stream());
        }

        $categoryCek = Category::where('parent_category', '!=',  null)->where('name', $request->name)->first();

        if (!empty($categoryCek)) {
            // return 'child';
            $category->update([
                'name' => $request->name,
                'slug' => $request->name . '-' . random_int(10, 99),
                'parent_category' => $request->parent_category,
                'image' => $filename,
            ]);
        } else {
            // return 'parent';
            $category->update([
                'name' => $request->name,
                'slug' => $request->name,
                'parent_category' => $request->parent_category,
                'image' => $filename,
                'foto_banner_kategori' => $filename_banner,
            ]);
        }

        return redirect(route('category.index'))->with('success', 'Kategori Telah Diupdate!');
    }

    public function destroy($id)
    {
        $category = Category::withCount(['child'])->find($id);
        $s = Product::where('id_category', '=', $id)->get();
        // return $category;
        if ($category->child_count == 0) {
            foreach ($s as $key => $value) {
                $value->update(['id_category' => "10"]);
            }
            if ($category->slug == 'semua-produk') {
                return redirect(route('category.index'))->with('message_error', 'Kategori Default tidak boleh dihapus!');
            }
            $category->delete();
            return redirect(route('category.index'))->with('success', 'Kategori Dihapus!');
        }
        return redirect(route('category.index'))->with('message_error', 'Kategori Ini Memiliki Anak Kategori');
    }

    // public function modaledit(request $request, $id)
    // {
    //     if ($request->isMethod('post')) {
    //         $category = $request->all();

    //         $update = Category::where(['id' => $id])->update([
    //             'name' => $request->name,
    //             'parent_category' => $request->parent_category,
    //         ]);
    //     }

    //     return redirect(route('category.index'))->with(['success' => 'Kategori Telah Diupdate!']);
    // }
}
