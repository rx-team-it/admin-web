<?php

namespace App\Http\Controllers\Admin\Reseller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\GroupReseller;
use App\Model\Komisi;
use App\Model\GroupMaster;
use Illuminate\Support\Facades\DB;

class GroupResellerController extends Controller
{
    public function index(request $request)
    {
        $r = GroupMaster::orderBy('created_at', 'DESC')->get();
        $reseller = DB::table('users')
            ->leftJoin('access_role', 'users.id', '=', 'access_role.user_id')
            ->leftJoin('role', 'access_role.id', '=', 'role.id')
            ->leftJoin('group_reseller', 'users.id', '=', 'group_reseller.id_reseller')
            ->where('access_role.role_id', 4)
            ->whereNull('group_reseller.group')
            ->get([
                'users.id',
                'users.name',
                'users.email',
                'role.nm_role',
                'group_reseller.group'
            ]);

        $no = 1;

        $pesan = $this->pesan;

        return view('admin.group_reseller.index', compact('r', 'no', 'reseller', 'pesan'));
    }

    public function indexR(Request $request)
    {
        $r = GroupMaster::orderBy('created_at', 'DESC')->get();

        $res = DB::table('users')
            ->leftJoin('access_role', 'users.id', '=', 'access_role.user_id')
            ->leftJoin('role', 'access_role.id', '=', 'role.id')
            ->leftJoin('group_reseller', 'users.id', '=', 'group_reseller.id_reseller')
            ->where('access_role.role_id', 4)
            ->whereNull('group_reseller.group')
            ->get([
                'users.id',
                'users.name',
                'role.nm_role',
                'group_reseller.group'
            ]);

        $pesan = $this->pesan;

        return view('admin.group_reseller.index', compact('res', 'r', 'pesan'));
    }

    public function daftarGrup(Request $request)
    {
        $this->validate($request, [
            'nm_grup' => 'unique:group_master|string|max:50',
        ]);

        if ($request->nominal == null) {
            $nominals = $request->nominal1;
        } else {
            $nominals = $request->nominal;
        }
        
        $u = GroupMaster::create([
            'nm_grup' => $request->nm_grup,
            'nominal' =>  $nominals,
            'satuan' =>  $request->satuan,
        ]);

        return redirect()->route('index.GroupReseller')->with('success', 'Berhasil Membuat Grup & Komisi');
    }

    public function daftar(Request $request)
    {
        $group = $request->input('group');
        foreach ($request->id_reseller as $key => $value) {
            $s = new GroupReseller();
            $s->id_reseller = $value;
            $s->group = $group;
            $s->save();
        }

        return redirect()->back()->with(['success' => 'Reseller berhasil di masukkan ke grup']);
    }

    public function show($id)
    {
        $g1 = DB::table('users')
            ->leftJoin('access_role', 'users.id', '=', 'access_role.user_id')
            ->leftJoin('role', 'access_role.id', '=', 'role.id')
            ->leftJoin('group_reseller', 'users.id', '=', 'group_reseller.id_reseller')
            ->where('access_role.role_id', 4)
            ->where('group_reseller.group', $id)
            ->get([
                'users.id',
                'group_reseller.id_reseller',
                'users.name',
                'role.nm_role',
                'group_reseller.group'
            ]);

        $pesan = $this->pesan;

        return view('admin.group_reseller.dtl', compact('g1', 'pesan'));
    }

    public function edit(request $request)
    {
        // return $request;
        $md = GroupMaster::where('id', $request->id)->first();
        $md->update([
                        'nm_grup' => $request->nm_grup,
                        'satuan' => $request->satuan,
                        'nominal' => $request->nominal
                    ]);

        return redirect(route('index.GroupReseller'))->with(['success' => 'Data Berhasil di Update']);
    }

    public function GroupResellerDelete($id_reseller)
    {
        $d = DB::delete("DELETE FROM group_reseller where id_reseller=?", [$id_reseller]);

        return redirect()->back()->with(['success' => 'Reseller berhasil di hapus dari grup']);
    }

    public function delete($id)
    {
        // $km = DB::delete("DELETE FROM komisi_master where id_group=?", [$id]);
        $gm = GroupMaster::where(['id' => $id])->delete();

        $gr = GroupReseller::where(['group' => $id])->delete();
        // $gm = GroupMaster::with(['child'])->find($id);
        // $gm->delete();

        return redirect()->back()->with(['success' => 'Grup berhasil di hapus']);
    }
}
