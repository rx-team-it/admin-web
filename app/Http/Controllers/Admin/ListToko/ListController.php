<?php

namespace App\Http\Controllers\Admin\ListToko;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MasterStore;
use App\Model\AccessRole;
use App\Model\Slider;
use App\Model\MasterAdress;
use Illuminate\Support\Facades\Mail;
use App\Mail\PengajuanTokoDiTerima;
use App\User;

class ListController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Function List Toko
    |--------------------------------------------------------------------------
    |*/

    public function index_reseller()
    {
        $storee = MasterStore::where('status', '!=', null)->where('name_site', '!=', null)->get();
        // return $storee;
        $pesan = $this->pesan;

        return view('admin.toko.list_toko.index', compact('storee', 'pesan'));
    }

    // LIST TOKO PENDING //
    public function listpending()
    {
        $storee = MasterStore::where('status', '=', 0)->orderBy('created_at', 'DESC')->with('user_profile')->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.pending', compact('storee', 'pesan'));
    }

    // LIST TOKO DI BLOK //

    public function listblock()
    {
        $storee = MasterStore::where('status', '=', 2)->orderBy('created_at', 'DESC')->with('user_profile')->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.blok', compact('storee', 'pesan'));
    }


    // LIST TOKO DI DELETE//

    public function listdelete(request $request)
    {
        $storee = MasterStore::where('status', '=', 3)->orderBy('created_at', 'DESC')->with('user_profile')->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.delete', compact('storee', 'pesan'));
    }

    // FUNCTION LIST TOKO AKTIF //

    public function listaktif()
    {
        $storee = MasterStore::where('status', '=', 1)->orderBy('created_at', 'DESC')->with('user_profile')->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.aktif', compact('storee', 'pesan'));
    }

    //UBAH STATUS TOKO DAN UBAH ROLE BUYYER MENJADI RESELLER//
    public function terima($id)
    {
        $store = MasterStore::where(['id' => $id])->first();
        $user = User::where(['id' => $store->user_id])->first();
        $master_address = MasterAdress::where(['id_user' => $store->user_id])->first();;
        // if (!$master_address) {
        //     MasterAdress::create([
        //         'id_user' => $store->user_id
        //     ]);
        // }
        $store->update(['status' => 1]);
        $store->update(['status_verifikasi' => 1]);

         
        MasterAdress::create([
            'id_store' => $id,
            'id_user' => $store->user_id
        ]);

        
        if ($store->save()) {
            $role = AccessRole::where(['user_id' => $store->user_id])->first();
            $role->update(['role_id' => 4]);
        }
        // Mail::to($user->email)->send(new PengajuanTokoDiTerima());

        return redirect()->back()->with(['success', 'Ubah Status Berhasil']);
    }

    // DETAIL TOKO //
    public function detail($id)
    {
        $store = MasterStore::where(['id' => $id])->first();
        $address = MasterAdress::where(['id_store' => $store->id])->first();
        $datas = Slider::where(['store_id' => $store->id])->get();

        $pesan = $this->pesan;

        return view('admin.toko.detail', compact('store', 'address', 'datas', 'pesan'));
    }


    /*
    |--------------------------------------------------------------------------
    | Function Search Toko
    |--------------------------------------------------------------------------
    |*/

    public function cariaktif(request $request)
    {
        $storee = MasterStore::where('name_site', $request->cari)->orWhere('name_site', 'like', '%' . $request->cari . '%')->where('status', '=', 1)->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.aktif', compact('storee', 'pesan'));
    }

    public function caripending(request $request)
    {
        $storee = MasterStore::where('name_site', $request->cari)->orWhere('name_site', 'like', '%' . $request->cari . '%')->where('status', '=', 0)->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.pending', compact('storee', 'pesan'));
    }

    public function caridelete(request $request)
    {
        $storee = MasterStore::where('name_site', $request->cari)->orWhere('name_site', 'like', '%' . $request->cari . '%')->where('status', '=', 3)->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.delete', compact('storee', 'pesan'));
    }

    public function cariblock(request $request)
    {
        $storee = MasterStore::where('name_site', $request->cari)->orWhere('name_site', 'like', '%' . $request->cari . '%')->where('status', '=', 2)->paginate(6);

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.blok', compact('storee', 'pesan'));
    }




    /*
    |--------------------------------------------------------------------------
    | Function Ganti Status Toko
    |--------------------------------------------------------------------------
    |*/


    public function approve($id)
    {
        $datas = MasterStore::where(['id' => $id])->first();
        $datas->update(['status' => 1]);

        return redirect()->back()->with('success', 'Success');
    }

    public function blok($id)
    {
        $datas = MasterStore::where(['id' => $id])->first();
        $datas->update(['status' => 2]);

        return redirect()->back()->with('success', 'Success');
    }

    public function delete($id)
    {
        $datas = MasterStore::where(['id' => $id])->first();
        $user = User::find($datas->user_id);
        $datas->update(['status' => 3]);


        $user->delete();

        return redirect()->back()->with('success', 'Success');
    }

    public function reactive($id)
    {
        $store = MasterStore::where(['id' => $id])->first();

        $store->update(['status' => 1]);

        $user = User::withTrashed()->find($store->user_id);
        $user->restore();

        return redirect()->back()->with('success', 'Success');

    }
}
