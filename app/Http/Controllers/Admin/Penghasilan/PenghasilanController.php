<?php

namespace App\Http\Controllers\Admin\Penghasilan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class PenghasilanController extends Controller
{
    public function index()
    {
        // total penghasilan
        // $jual = DB::table("order")
        //             ->join("history_product_vendor", "order.id", "=", "history_product_vendor.order_id")
        //             ->select(DB::raw("SUM(history_product_vendor.price) as price"))
        //             ->where('order.status', '=', 5)
        //             ->groupBy("order.id")
        //             ->get();
        $jual = DB::table("order")
            ->select("order.id", 
            DB::raw("order.total + order.total_komisi_produk as gross"),
            DB::raw("order.laba_bersih as total"))
            ->where('order.status', '=', 5)
            ->orderBy("id", "DESC")
            ->get();
        $grand = $jual->sum('total');

        // detail penghasilan
        $penjualan = DB::table("order")
                    ->select("order.id", 
                        "order.name", 
                        "order.invoice as invoice", 
                        "order.created_at",
                        "order.laba_bersih",
                        DB::raw("SUM(order.total) as price"))
                        ->where('order.status', '=', 5)
                    ->groupBy("order.id")
                    ->orderBy("id", "DESC")
                    ->get();
        $grand_pen =$penjualan->sum('laba_bersih');

        return view('admin.penghasilan.index', compact('grand','penjualan'));
    }
}
