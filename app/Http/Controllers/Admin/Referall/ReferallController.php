<?php

namespace App\Http\Controllers\Admin\Referall;

use App\Http\Controllers\Controller;
use App\Model\HistoryPayment;
use Illuminate\Http\Request;
use App\Model\UserReferall;
use App\User;

class ReferallController extends Controller
{
    public function referall()
    {
        $user = UserReferall::where('user_id', auth()->user()->id)->first();
        $user['get'] = UserReferall::where('referral_related', $user->referral_code)->with('user')->get();

        $pesan = $this->pesan;

        return view('admin.referall.index', compact('user', 'pesan'));
    }

    public function detail_user($referral_code)
    {
        $user = UserReferall::where('referral_code', $referral_code)->with('user')->first();
        $user['payment'] = HistoryPayment::where('user_reff_id', $user->user_referral_id)->first();

        $pesan = $this->pesan;

        // return $user;

        return view('admin.referall.detail_user', compact('pesan', 'user'));
    }
}
