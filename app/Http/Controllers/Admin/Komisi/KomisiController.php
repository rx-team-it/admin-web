<?php

namespace App\Http\Controllers\Admin\Komisi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\Komisi;
use App\Model\GroupMaster;

class KomisiController extends Controller
{
    public function index()
    {
        $r = Komisi::all();
        $g = GroupMaster::all();

        $pesan = $this->pesan;

        return view('admin.komisi.index', compact('r', 'g', 'pesan'));
    }

    public function add(request $request)
    {
        $r = Komisi::all();

        if ($r[0]->id_group == $request->group) {
            return redirect()->back()->with('error', 'Gagal Input!, Grup Sudah Punya Komisi');
        } else {
            Komisi::create([
                'id_group' =>  $request->group,
                'nominal' =>  $request->nominal,
                'satuan' =>  $request->satuan,
            ]);
        }

        return redirect()->back()->with('success', 'Berhasil tambah komisi grup');
    }

    public function edit($id)
    {
        $r1 = Komisi::where('id', $id)
            ->get();

        $pesan = $this->pesan;

        return view('admin.komisi.index', compact('r1', 'pesan'));
    }

    public function delete($id)
    {
        $d = DB::delete("DELETE FROM komisi_master where id=?", [$id]);

        return redirect()->back()->with(['success' => 'Berhasil Hapus Komisi']);
    }
}
