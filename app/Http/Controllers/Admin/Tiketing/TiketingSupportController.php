<?php

namespace App\Http\Controllers\Admin\Tiketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\TiketingSupport;
use App\Model\MasterCategory;
use App\Model\ReplyTiketing;
use App\Model\UserDetails;
use Illuminate\Support\Facades\Auth;

class TiketingSupportController extends Controller
{
    public function list_pesan()
    {
        $category_tiketing = MasterCategory::get();
        $message_tiketing = TiketingSupport::orderBy('id', 'DESC')->with('category', 'user')->get();
        $belum_dibalas = TiketingSupport::orderBy('id', 'DESC')->with('category', 'user')->where('status', '=', 2)->get();
        $closed = TiketingSupport::orderBy('id', 'DESC')->with('category', 'user')->where('status', '=', 3)->get();
        
        $hitung_blm_balas = $belum_dibalas->count();
        $pesan = $this->pesan;

        return view('admin.tiketing.list_pesan', compact('category_tiketing', 'message_tiketing', 'pesan', 'belum_dibalas', 'closed','hitung_blm_balas'));
    }

    public function detail_pesan($id)
    {
        $message_tiketing = TiketingSupport::find($id);
        $message_tiketing['reply'] = ReplyTiketing::where(['tiketing_id' => $message_tiketing->id])->get();
        $foto_profile = UserDetails::where(['id_user' => $message_tiketing->user_id])->first();

        $pesan = $this->pesan;

        return view('admin.tiketing.detail_pesan', compact('message_tiketing', 'foto_profile', 'pesan'));
    }

    public function reply(Request $request, $id)
    {
        $message_tiketing = TiketingSupport::find($id);
        $message_tiketing['reply'] = ReplyTiketing::where(['tiketing_id' => $message_tiketing->id])->get();

        ReplyTiketing::create([
            'tiketing_id' => $message_tiketing->id,
            'user_id' => auth()->user()->id,
            'pesan' => $request->pesan,
        ]);

        $status = TiketingSupport::find($id);

        $status->update(['status' => 1]);

        return redirect()->back()->with(['success', 'Berhasil Dikirim']);
    }

    // FUNCTION UNTUK CLOSED KELUHAN //

    public function close($id)
    {
        $tiketing = TiketingSupport::where(['id' => $id])->first();
        $tiketing->update(['status' => 3]);

        return redirect()->back()->with(['success' => 'Keluhan Berhasil Di Tutup']);
    }

    // -------------------- //

    // public function balas_pesan(Request $request, $id)
    // {
    //     $message_tiketing = TiketingSupport::find($id)->with('category', 'user')->first();

    //     if ($message_tiketing->status == 0) {
    //         TiketingSupport::create([
    //             'is_parent' => $message_tiketing->id,
    //             'user_id' => $message_tiketing->user_id,
    //             'category_id' => $message_tiketing->category_id,
    //             'judul' => $message_tiketing->judul,
    //             'nama_pembalas' => Auth::user()->name,
    //             'message' => $request->message,
    //         ]);
    //     } else {
    //     }

    //     if ($message_tiketing->status == 0) {
    //         $message_tiketing->update(['status' => 1]);
    //     }

    //     return redirect()->back()->with(['success', 'Keluhan Berhasil Dibalas']);
    // }

    public function category_tiketing(Request $request)
    {
        MasterCategory::create([
            'name' => $request->name
        ]);

        return redirect()->back()->with(['success', 'Kategori Berhasil Dibuat']);
    }

    public function category_delete($id)
    {
        $category_tiketing = MasterCategory::find($id);
        $category_tiketing->delete();

        return redirect()->back()->with('delete', 'Kategori Berhasil Dihapus');
    }
}
