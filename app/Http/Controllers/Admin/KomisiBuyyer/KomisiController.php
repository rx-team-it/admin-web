<?php

namespace App\Http\Controllers\Admin\KomisiBuyyer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\KomisiBuyyer;
use App\Model\GrupKomisiBuyyer;
use App\Model\ProductDetail;
use App\Model\ProductVarianOption;
use DB;
use PhpOffice\PhpSpreadsheet\Calculation\Financial\Securities\Price;
use Symfony\Component\VarDumper\Cloner\Data;

class KomisiController extends Controller
{
    public function index()
    {
        $product = Product::where('is_block', '=', '0')->orderBy('created_at', 'DESC')->with('product_images_thumbnail')->with('product_details')->with('grup_komisi_buyyer')->get();
        // ddd($product[11]->product_komisi_buyyer);
        // ddd($product[0]);
        // return $product;
        // ddd($product);
        // $product = Product::where('is_block', '=', '0')->orderBy('created_at', 'DESC')->with('product_images_thumbnail')->get();
        // $product = DB::select("SELECT
        //         a.id, 
        //         a.name,
        //         a.diskon,
        //         a.type_diskon,
        //         b.price,
        //         b.product_varian_option_id,
        //         c.img_product
        //     FROM
        //         product a
        //             LEFT JOIN
        //         product_detail b ON a.id = b.product_id
        //             LEFT JOIN
        //         product_image c ON a.id = c.product_id
        //     GROUP BY a.id
        //         ORDER BY a.created_at desc");
        $a = KomisiBuyyer::all();
        // $x = $product[0]->product_details[1]->price;
        // return $x;
        // return min($x);
        // ddd($product);
        return view('admin.komisi-buyyer.index', compact('product', 'a'));
    }

    public function simpan(request $request)
    {
        $this->validate($request, [
            'grup' => 'unique:komisi_buyyer|string|max:50',
        ]);

        KomisiBuyyer::create([
            'grup' =>  $request->grup,
            'nominal' =>  $request->nominal,
            'satuan' =>  $request->satuan,
        ]);

        return redirect()->back()->with('status', 'Berhasil tambah komisi grup');
    }

    public function preview_edit($id)
    {
        $komisi = KomisiBuyyer::where('id', $id)->first();

        $dtl = DB::select("SELECT 
                a.*, b.id as id_komisi, b.grup, b.nominal, b.satuan, c.name, d.price, d.fix_price, d.product_varian_option_id, d.product_id as id_noVarian, e.name as varian, f.type_diskon, f.diskon, g.name as nameVarian
            FROM
                grup_komisi_buyyer a
                    LEFT JOIN
                komisi_buyyer b ON a.grup_id = b.id
                    LEFT JOIN
                development.product c ON a.product_id = c.id
                    LEFT JOIN
                product_detail d ON a.product_id = d.product_id
                    LEFT JOIN 
                product_varian_option e ON d.product_varian_option_id = e.id
                    LEFT JOIN 
                product f ON a.product_id = f.id
                    LEFT JOIN
                product_varian_option g ON e.parent_option = g.id
                    WHERE b.id=?", [$id]);
        // return $komisi;

        return view('admin.komisi-buyyer.preview_edit', compact('dtl', 'komisi'));
    }

    public function edit(request $request)
    {
        $by_id_varians = $request->product_varian_id;
        $by_id_products = $request->product_id;
        $fix_price_forVarians = $request->fix_price_varian;
        $fix_price_forIds = $request->fix_price;


        // varian
        if(!empty($by_id_varians)){
            for ($i=0; $i < count($by_id_varians); $i++) { 
                $id = $by_id_varians[$i];
                $data = $fix_price_forVarians[$i];
                ProductDetail::where('product_varian_option_id', $id)->update(['fix_price' => $data]); 
            }
        }

        // biasa
        if(!empty($by_id_products)){
            for ($i=0; $i < count($by_id_products); $i++) { 
                $id = $by_id_products[$i];
                $data = $fix_price_forIds[$i];
                ProductDetail::where('product_id', $id)->update(['fix_price' => $data]); 
            }
        }
        
        $md = KomisiBuyyer::where('id', $request->id)->first();
        $md->update([
            'grup' => $request->grup,
            'satuan' => $request->satuan,
            'nominal' => $request->nominal
        ]);

        return redirect(route('komisi.buyyer'))->with(['success' => 'Data Berhasil di Update']);
    }

    public function grup(request $request)
    {
        // terima jika syarat terpenuhi
        
            $grup = $request->input('grup');
    
            if (empty($grup)) {
                return redirect()->back()->with(['status_gagal' => 'Pilih Grup dahulu']);
            } else {
                if(empty($request->failed)){
                    foreach ($request->product_id as $key => $value) {
                        $s = new GrupKomisiBuyyer();
                        // cek dulu di grupnya udah ada produk yang sama atau belum 
                        $a = GrupKomisiBuyyer::where('grup_id', '=', $grup)->where('product_id', '=', $value)->count();
                        $b = GrupKomisiBuyyer::whereNotIn('grup_id', [$grup])->where('product_id', '=', $value)->count();
                        // kalo udah ada produk yang sama di dalam satu grup jadi gagal input
                        if ($a > 0) {
                            return redirect()->back()->with(['status_gagal' => 'Satu produk sudah ada di grup terpilih']);
                        } elseif ($b > 0) {
                            return redirect()->back()->with(['status_gagal' => 'Produk sudah masuk di grup lain']);
                        } else {
                            $s->product_id = $value;
                            $s->grup_id = $grup;
                            $s->save();
        
                            $fix_price_varians = $request->fix_price_varian;
                            $fix_price_byIds = $request->fix_price_byId;
        
                            if (!empty($request->id_varian)) {
                                for ($i = 0; $i < count($request->id_varian); $i++) {
                                    $id = $request->id_varian[$i];
                                    $data = $fix_price_varians[$i];
                                    ProductDetail::where('product_varian_option_id', $id)->update(['fix_price' => $data]);
                                }
                            }
        
                            if (!empty($request->id_produk)) {
                                for ($i = 0; $i < count($request->id_produk); $i++) {
                                    $id = $request->id_produk[$i];
                                    $data = $fix_price_byIds[$i];
                                    ProductDetail::where('product_id', $id)->update(['fix_price' => $data]);
                                }
                            }
                        }
                    }
                }else{
                    return redirect()->back()->with(['status_gagal' => 'Gagal!! terdapat produk memiliki komisi melebihi harga produk. Silahkan cek kembali!']);
                };
            }
    
            return redirect()->back()->withInput()->with(['status' => 'Data Berhasil di masukkan ke grup']);
            // tolak jika syarat tidak terpenuhi
            
        
    }

    public function dtl($id)
    {
        $cek = GrupKomisiBuyyer::where('grup_id', $id)->get();
            if (!empty($cek)) {
                $x = GrupKomisiBuyyer::where('grup_id', $id)->with('product')->with('product_detail.product_varian_options.parent_options')->get();
                $komisi = KomisiBuyyer::where('id',  $id)->first();
            }
        

        return view('admin.komisi-buyyer.dtl', compact('x', 'komisi'));

    }

    public function delete($id)
    {
        $dtl = DB::select("SELECT 
                d.price, d.fix_price, d.product_varian_option_id, d.product_id, f.type_diskon, f.diskon
            FROM
                grup_komisi_buyyer a
                    LEFT JOIN
                komisi_buyyer b ON a.grup_id = b.id
                    LEFT JOIN
                development.product c ON a.product_id = c.id
                    LEFT JOIN
                product_detail d ON a.product_id = d.product_id
                    LEFT JOIN 
                product_varian_option e ON d.product_varian_option_id = e.id
                    LEFT JOIN 
                product f ON a.product_id = f.id
                    WHERE b.id=?", [$id]);

        // ddd($dtl);
        
        for ($i=0; $i < count($dtl); $i++) { 
            $price = $dtl[$i]->price;
            $product_varian_option_id = $dtl[$i]->product_varian_option_id;
            $product_id = $dtl[$i]->product_id;

            if (empty($product_varian_option_id)) {
                // select by product id
                $type_diskon = $dtl[$i]->type_diskon;
                if (empty($type_diskon)) {
                    // fix_price = price
                    $data = [
                        'fix_price' => $price
                    ];
                    ProductDetail::where('product_id', $product_id)->update($data);
                }elseif($type_diskon == 'persen'){
                    // fix_price = price - (price*diskon) / 100;
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - ($price*$diskon) / 100;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_id', $product_id)->update($data);

                }elseif($type_diskon == 'nominal'){
                    // fix_price = price - diskon
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - $diskon;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_id', $product_id)->update($data);

                }

            }else{
                // select by varian id
                $type_diskon = $dtl[$i]->type_diskon;
                if (empty($type_diskon)) {
                    // fix_price = price
                    $data = [
                        'fix_price' => $price
                    ];
                    ProductDetail::where('product_varian_option_id', $product_varian_option_id)->update($data);
                }elseif($type_diskon == 'persen'){
                    // fix_price = price - (price*diskon) / 100;
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - ($price*$diskon) / 100;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_varian_option_id', $product_varian_option_id)->update($data);

                }elseif($type_diskon == 'nominal'){
                    // fix_price = price - nominal
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - $diskon;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_varian_option_id', $product_varian_option_id)->update($data);

                }
            }

            

        }


        // kalo grup di hapus 
        $d = DB::delete("DELETE FROM komisi_buyyer where id=?", [$id]);
        // ->childnya juga ikut kehapus
        $d1 = DB::delete("DELETE FROM grup_komisi_buyyer WHERE grup_id = ?", [$id]);

        return redirect()->back()->with(['status' => 'Data berhasil di hapus']);
    }

    public function delete_dtl(request $request)
    {
        $i = implode(',', $request->product_id);

        $dtl = DB::select("SELECT
                a.price, a.fix_price, a.product_varian_option_id, a.product_id, b.type_diskon, b.diskon
            FROM
                product_detail a
            LEFT JOIN
                product b ON a.product_id = b.id
            WHERE a.product_id in ($i)");

        // set to default price
        for ($i=0; $i < count($dtl); $i++) { 
            $price = $dtl[$i]->price;
            $product_varian_option_id = $dtl[$i]->product_varian_option_id;
            $product_id = $dtl[$i]->product_id;
            

            if (empty($product_varian_option_id)) {
                // select by product id
                $type_diskon = $dtl[$i]->type_diskon;
                if (empty($type_diskon)) {
                    // fix_price = price
                    $data = [
                        'fix_price' => $price
                    ];
                    ProductDetail::where('product_id', $product_id)->update($data);
                }elseif($type_diskon == 'persen'){
                    // fix_price = price - (price*diskon) / 100;
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - ($price*$diskon) / 100;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_id', $product_id)->update($data);

                }elseif($type_diskon == 'nominal'){
                    // fix_price = price - diskon
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - $diskon;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_id', $product_id)->update($data);

                }

            }else{
                // select by varian id
                $type_diskon = $dtl[$i]->type_diskon;
                if (empty($type_diskon)) {
                    // fix_price = price
                    $data = [
                        'fix_price' => $price
                    ];
                    ProductDetail::where('product_varian_option_id', $product_varian_option_id)->update($data);
                }elseif($type_diskon == 'persen'){
                    // fix_price = price - (price*diskon) / 100;
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - ($price*$diskon) / 100;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_varian_option_id', $product_varian_option_id)->update($data);

                }elseif($type_diskon == 'nominal'){
                    // fix_price = price - nominal
                    $diskon = $dtl[$i]->diskon;
                    $fix_price = $price - $diskon;
                    $data = [
                        'fix_price' => $fix_price
                    ];
                    ProductDetail::where('product_varian_option_id', $product_varian_option_id)->update($data);

                }
            }

            

        }

        $i = implode(',', $request->product_id);

        // delete dari grup
        $d1 = DB::delete("DELETE FROM grup_komisi_buyyer WHERE product_id in ($i)");

        return redirect()->back()->with(['status' => 'Data berhasil di hapus']);
    }

    public function preview(Request $request)
    {
        $ids = $request->ids;
        $idk = $request->id_komisiBuyyer;
        // $data = Product::whereIn('id', $ids)->with('product_details')->get();

        $data = DB::select('SELECT
                a.*, b.price, b.product_varian_option_id, c.name as varian, d.name as nameVarian
             FROM
                 product a
                     LEFT JOIN
                 product_detail b ON a.id = b.product_id
                     LEFT JOIN
                 product_varian_option c ON b.product_varian_option_id = c.id
                    LEFT JOIN
                 product_varian_option d ON c.parent_option = d.id
                    WHERE
                 a.id IN (' . implode(',', array_map('intval', $ids)) . ')');

        // $data = ProductVarianOption::whereIn('product_id', $ids)->whereNotNull('parent_option')->with('product_detail')->get();

        $komisi_buyyer = KomisiBuyyer::where('id', $idk)->first();
        $response = [
            'komisi_buyyer' => $komisi_buyyer,
            'data' => $data,
        ];
        return response($response, 200);
        // $data = ProductDetail::where('product_id', 112)->with('varian_option')->get();
        // return $data;
    }
}
