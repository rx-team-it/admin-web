<?php

namespace App\Http\Controllers\Admin\Verifikasi;

use App\Http\Controllers\Controller;
use App\Model\AccessRole;
use App\Model\MasterAdress;
use Illuminate\Http\Request;
use App\Model\MasterStore;
use App\Model\UserReferall;

class VerifikasiController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $order = MasterStore::where('status_verifikasi', '=', 0)->whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $order = MasterStore::where('status_verifikasi', '=', 0)->whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $order = MasterStore::where('status_verifikasi', '=', 0)->orderBy('created_at', 'DESC')->get();
            }

            return datatables()->of($order)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("detail.verifikasi", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Detail</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label_verifikasi'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.verifikasi.index', compact('pesan'));
    }

    public function detail($id)
    {
        $history = MasterStore::with('address')->find($id);

        $pesan = $this->pesan;

        return view('admin.verifikasi.detail', compact('history', 'pesan'));
    }

    public function diterima($id)
    {
        $order = MasterStore::where(['id' => $id])->first();
        
        MasterAdress::create([
            'id_store' => $id,
            'id_user' => $order->user_id
        ]);

        $order->update(['status_verifikasi' => 1]);
        $order->update(['status' => 1]);

        if ($order->save()) {
            $role = AccessRole::where(['user_id' => $order->user_id])->first();
            $role->update(['role_id' => 4]);
        }

        return redirect()->back()->with('success', 'Success');
    }

    public function ditolak($id)
    {
        $order = MasterStore::where(['id' => $id])->first();

        $order->update(['status_verifikasi' => 2]);

        return redirect()->back()->with('success', 'Success');
    }

    // FUNCTION TEST //

    public function verifikasilist()
    {
        $verifikasi = MasterStore::orderBy('created_at', 'DESC')->get();
        $verifikasi_diterima = MasterStore::orderBy('created_at', 'DESC')->get();
        $verifikasi_ditolak = MasterStore::orderBy('created_at', 'DESC')->get();

        $pesan = $this->pesan;

        return view('admin.verifikasi.indexnew', compact('pesan', 'verifikasi', 'verifikasi_diterima', 'verifikasi_ditolak'));
    }
}
