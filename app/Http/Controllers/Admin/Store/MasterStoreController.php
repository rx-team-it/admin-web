<?php

namespace App\Http\Controllers\Admin\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MasterStore;
use App\Model\MasterAdress;
use App\Model\Slider;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Validator;

class MasterStoreController extends Controller
{
    public function index()
    {
        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $slider = Slider::where(['store_id' => $store->id])->get();
        $address = MasterAdress::where(['id_store' => $store->id])->first();
        $alamat = MasterAdress::where(['id_store' => $store->id])->get();

        // cek banner-produk
        $get_banner_produk = Slider::where('type_content', '=', 'banner-produk')->get();
        $banner_produk = $get_banner_produk->count();

        $pesan = $this->pesan;

        return view('admin.store.index', compact('store', 'slider', 'address', 'alamat', 'pesan','banner_produk','get_banner_produk'));
    }
    public function bannercreate(request $request)
    {
        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $file = $request->file('image');
        $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
        $image = Image::make($file)->resize(1200, 600);
        $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());

        $c = Slider::create([
            'image' => $filename,
            'store_id' => $store->id,
            'type_content' => 'banner-produk',
        ]);

        return redirect()->back()->with(['success', 'Foto banner produk berhasil di simpan']);
    }

    public function bannerubah(request $request)
    {
        // return $request;
        $file = $request->file('image');
        $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
        $image = Image::make($file)->resize(1200, 600);
        $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());

        $banner = Slider::where('id', '=', $request->id);
        $banner->update([
            'image' => $filename,
        ]);

        return redirect()->back()->with(['success', 'Foto berhasil di ubah']);
    }

    public function profileupdate(Request $request)
    {
        $profile = MasterStore::where('id', $request->id)->first();

        $old_img = 'public/images/profiletoko/' . $profile->image;
        $filename = !empty($profile->image) ? $profile->image : NULL;

        $validate = $this->validate($request, [
            'logo' => 'image|mimes:jpg,png,jpeg|max:20000',
        ]);

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
            Storage::disk('s3')->delete($old_img);
            $image = Image::make($file)->resize(150, 150);
            $file = Storage::disk('s3')->put('public/images/profiletoko/' . $filename, $image->stream());

            MasterAdress::where('id_store', 1)->update([
                'address' => $request->address_id
            ]);

            $profile->update([
                'name_site' =>  $request->name_site,
                'logo' => $filename,
                'mini_logo' => $filename,
                // 'address_id' => $request->address_id,
                'contact_email' => $request->contact_email,
                'contact_phone' => $request->contact_phone,
                'description' => $request->description,
            ]);
        } else {
            // MasterAdress::where('id_store', 1)->update([
            //     'address' => $request->address_id
            // ]);
            $profile->update([
                'name_site' =>  $request->name_site,
                // 'address_id' => $request->address_id,
                'contact_email' => $request->contact_email,
                'contact_phone' => $request->contact_phone,
                'description' => $request->description,
            ]);
        }

        return redirect()->back()->with(['success', 'Profile Toko Berhasil diperbarui']);
    }

    public function slidercreate(Request $request)
    {
        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();

        $validate = $this->validate($request, [
            'image' => 'image|mimes:jpg,png,jpeg|max:20000',
        ]);

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(1200, 600);
            $file = Storage::disk('s3')->put('public/images/BannerImage/' . $filename, $image->stream());

            Slider::create([
                'type_content' =>  $request->type_content,
                'store_id' => $store->id,
                'description' => $request->description,
                'image' => $filename,
                'alt' => '..'
            ]);
        }

        return redirect()->back()->with(['success', 'Slider Berhasil Di tambahkan']);
    }
}
