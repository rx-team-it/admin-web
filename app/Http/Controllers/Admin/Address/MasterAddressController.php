<?php

namespace App\Http\Controllers\Admin\Address;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MasterAdress;
use App\Model\MasterStore;
use App\Model\Ro_Provinces;
use App\Model\ProvinceData;
use Illuminate\Support\Facades\DB;

class MasterAddressController extends Controller
{
    public function index()
    {
        $pesan = $this->pesan;

        return view('admin.address.index', compact('pesan'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'postal_code' =>  'required|string|min:4',
            'name' => 'required|string|min:6',
            'phone' => 'required|numeric|min:9',
            'address' => 'required|string|min:9',
        ]);

        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();

        MasterAdress::create([
            'id_user' =>  auth()->user()->id,
            'id_store' => $store->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'district' => $request->district,
            'province' => $request->province,
            'city' => $request->city,
            'urban' => $request->urban,
            'postal_code' => $request->postal_code,
        ]);

        return redirect()->back()->with(['success' => 'Alamat Baru Ditambahkan!']);
    }

    public function edit($id)
    {
        $address = MasterAdress::where(['id' => $id])->first();

        // $countrys = Ro_Provinces::all();
        $countrys = DB::table('province_data')
            ->select("province_name as label", "province_code as value")
            ->orderby('province_name', 'ASC')
            ->get();
        // return $countrys;
        $pesan = $this->pesan;

        return view('admin.address.edit', compact('address', 'countrys', 'pesan'));
    }

    public function pinpoint(Request $request)
    {
        $update = MasterAdress::where(['id' => $request->id])->update([
            'latitude' => $request->lat,
            'longtitude' => $request->long,
            'pin_point' => 1,
        ]);
        return redirect()->back()->with('success', 'Berhasil Atur Pinpoint');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'postal_code' =>  'required|string|min:4',
            'name' => 'required|string|min:6',
            'phone' => 'required|numeric|min:9',
            'address' => 'required|string|min:9',
        ]);

        //$master = MasterAdress::find($id);
        if ($request->lat == null) {
            $pin = 0;
        } else {
            $pin = 1;
        }
        //if (!$master) {
        $master = MasterAdress::where(['id' => $id])->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'district' => $request->district,
            'province' => $request->province,
            'city' => $request->city,
            'urban' => $request->urban,
            'postal_code' => $request->postal_code,
            'latitude' => $request->lat,
            'longtitude' => $request->long,
            'pin_point' => $pin,
        ]);
        /* } else {
            MasterAdress::where(['id' => $id])->update([
                'name' => $request->name,
                'phone_number' => $request->phone_number,
                'address' => $request->address,
                'postal_code' => $request->postal_code,
            ]);
        } */

        return redirect((route('store.index')))->with(['success' => 'Alamat Berhasil Diedit!']);
    }
}
