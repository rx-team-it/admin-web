<?php

namespace App\Http\Controllers\Admin\Kode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\UserReferall;
use App\Model\OrderDetail;
use App\Model\ConfirmPayment;
use App\Model\ProductVarian;
use App\Model\ProductDetail;
use App\Model\ProductVarianOption;

class KodeController extends Controller
{
    public function index(request $request)
    {
        $r = Order::all();
        // $w = DB::select("SELECT extract(year from created_at) as yr, extract(month from created_at) as mon,
        //         sum(kode_unik) as kode_unik
        //     from development.order
        //     group by extract(year from created_at), extract(month from created_at)
        //     order by yr, mon")[0];
        $orstat = Order::select(DB::raw("sum(kode_unik) as count"))->whereYear('created_at', date('Y'))->groupBy(DB::raw("Month(created_at)"))->pluck('count');
        $months = Order::select(DB::raw("Month(created_at) as month"))->whereYear('created_at', date('Y'))->groupBy(DB::raw("Month(created_at)"))->pluck('month');

        $datase = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        foreach ($months as $index => $month) {
            $datase[$month - 1] = $orstat[$index];
        }
        $tb = DB::select("SELECT sum(kode_unik) kode FROM development.order")[0];
        $no = 1;
        $pesan = $this->pesan;

        return view('admin.kodeunik.index', compact('pesan', 'r', 'no', 'datase', 'tb'));
    }


    public function detail($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order['product'] = OrderDetail::where(['order_id' => $order->id])->get();
        $order['confirm_payment'] = ConfirmPayment::where(['order_id' => $order->id])->first();
        $pesan = $this->pesan;

        return view('admin.kodeunik.detail', compact('pesan', 'order'));
    }
}
