<?php

namespace App\Http\Controllers\Admin\Saldo;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\HistorySaldo;
use App\Model\UserReferall;
use App\Model\HistoryPayment;
use App\Model\HistorySaldoReseller;
use App\Model\Order;
use App\User;
use Illuminate\Support\Facades\Auth;

class HistorySaldoController extends Controller
{
    public function index()
    {
        $user = UserReferall::where('user_id', auth()->user()->id)->first();
        $user['get'] = UserReferall::where('referral_related', $user->referral_code)->with('user')->get();
        $xx = DB::select("SELECT
                name, email, user_referral_id, referral_related, komisi, b.created_at
            FROM
                users a
                    LEFT JOIN
                user_referrals b ON a.id = b.user_id
                    LEFT JOIN
                history_payment c ON b.user_referral_id = c.user_reff_id
            WHERE
                referral_related = '$user->referral_code'");
        // return $xx;
        $no = 1;
        return view('admin.history_saldo.index', compact('user', 'no', 'xx'));
    }

    public function dtl($id)
    {
        $his_min = UserReferall::where('user_referral_id', $id)->first();
        // return $his_min;
        $c = HistorySaldoReseller::where('user_reff_id', $his_min->user_referral_id)->get();
        foreach ($c as $b) {
            $b['user_id'] = Order::where([
                'id' => $b->order_id,
            ])->get();
        }

        // return $c;

        // $order = Order::whereNotNull('user_reff')->orderBy('id', 'DESC')->get();
        // foreach ($order as $pesan) {
        //     $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
        //     $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        //     $pesan['keterangan'] = HistorySaldoReseller::where('user_reff_id', $his_min->user_referral_id)->get();
        // }

        // return $order;
        // return $his_min;

        return view('admin.history_saldo.dtl', compact('his_min', 'c'));
    }
}
