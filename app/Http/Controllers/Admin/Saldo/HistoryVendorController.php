<?php

namespace App\Http\Controllers\Admin\Saldo;

use App\Http\Controllers\Controller;
use App\Model\HistoryProductVendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Order;

class HistoryVendorController extends Controller
{
    private function getMonthlyReports($year, $month)
    {
        $rawQuery = 'DATE(created_at) as date, count(`id`) as count';
        $rawQuery .= ', sum(price) AS amount';

        $reportsData = DB::table('history_product_vendor')->select(DB::raw($rawQuery))
            ->where(DB::raw('YEAR(created_at)'), $year)
            ->where(DB::raw('MONTH(created_at)'), $month)
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get();

        $reports = [];
        foreach ($reportsData as $report) {
            $key = substr($report->date, -2);
            $reports[$key] = $report;
            $reports[$key]->omzet = $report->amount;
        }

        return collect($reports);
    }

    public function penjualan(Request $request)
    {

        $penjualan = Order::where('status', 5)->with('order_details')->orderBy("id", "DESC")->get();
        // return $penjualan;

        // Menghitung total dari semua invoice
            $jual = DB::table("order")
            ->select("order.id", 
                "order.created_at as created_at",
                "order.name as name_product", 
                "order.name as name_product", 
                "order.name as name_product", 
                "order.invoice as invoice",
                DB::raw("order.total + order.total_komisi_produk as gross"),
                "order.kode_unik as kode_unik",
                DB::raw("order.biaya_layanan + order.kode_unik as biaya_layanan"),
                DB::raw("order.laba_bersih as total"),
                "order.cashback as cashback",
                "order.ongkos_kirim_fix as ongkir",
            )
            ->where('order.status', '=', 5)
            ->orderBy("id", "DESC")
            ->get();

            $order_id = [];
            foreach ($jual as $j) {
                array_push($order_id, $j->id);
            }


            $jualDetail = DB::table("order_detail")
            ->select(
                "order_detail.price as price",
                "order_detail.harga_diskon as harga_awal",
                "order_detail.nilai_komisi as komisi",
                "order_detail.type_komisi as type_komisi",
                "order_detail.diskon as diskon",
                "order_detail.fix_price as fix_price",
                "order_detail.qty as qty"
            )
            ->whereIn('order_detail.order_id', $order_id)
            ->get();

            foreach ($jualDetail as $key => $p) {
                // buat harga
                if ($p->harga_awal != null) {
                    $p->harga_awal = $p->harga_awal; 
                }else{
                    $p->harga_awal = $p->price; 
                }

                // type komisi persen rubah ke satuan nominal
                if($p->type_komisi == 'persentase'){
                    $p->komisi = $p->harga_awal * $p->komisi / 100; 
                }else{
                    $p->komisi = $p->komisi;
                }

            }

        $pendapatan = $jual->sum('total');
        $gross = $jual->sum('gross');
        $ongkir = $jual->sum('ongkir');
        $biaya_layanan = $jual->sum('biaya_layanan');
        $cashback = $jual->sum('cashback');
        $biaya_layanan = $jual->sum('biaya_layanan');
        $qty = $jualDetail->sum('qty');
        $fix_price = $jualDetail->sum('fix_price');
        $komisi = $jualDetail->sum('komisi');
        $harga_awal = $jualDetail->sum('harga_awal');

        $pesan = $this->pesan;

        return view('admin.history_vendor.penjualan', compact('pesan', 'pendapatan', 'gross', 'ongkir', 'biaya_layanan', 'qty', 'cashback', 'fix_price', 'komisi', 'harga_awal', 'penjualan'));
    }

    public function caripenjualan(request $request)
    {
        $storee = HistoryProductVendor::whereDate('created_at', '=', date('Y-m-d'))->sum('price');

        return $storee;

        $pesan = $this->pesan;

        return view('admin.toko.list_toko.aktif', compact('storee', 'pesan'));
    }

    public function totalPenjualan(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    // $penjualan = HistoryProductVendor::where('user_reff', '=', NULL)->whereDate('created_at', '=', $request->from_date)->get();
                    // Menghitung total dari semua invoice
                            $jual = DB::table("order")
                            ->select("order.id", 
                                "order.created_at as created_at",
                                "order.name as name_product", 
                            "order.name as name_product", 
                                "order.name as name_product", 
                                "order.invoice as invoice",
                                DB::raw("order.total + order.total_komisi_produk as gross"),
                                "order.kode_unik as kode_unik",
                                DB::raw("order.biaya_layanan + order.kode_unik as biaya_layanan"),
                                DB::raw("order.laba_bersih as total"),
                                "order.cashback as cashback",
                                "order.ongkos_kirim_fix as ongkir",
                            )
                            ->where('order.status', '=', 5)
                            ->whereDate('order.created_at', '=', $request->from_date)
                            ->orderBy("id", "DESC")
                            ->get();

                            $order_id = [];
                            foreach ($jual as $j) {
                                array_push($order_id, $j->id);
                            }


                            $jualDetail = DB::table("order_detail")
                            ->select(
                                "order_detail.price as price",
                                "order_detail.harga_diskon as harga_awal",
                                "order_detail.nilai_komisi as komisi",
                                "order_detail.type_komisi as type_komisi",
                                "order_detail.diskon as diskon",
                                "order_detail.fix_price as fix_price",
                                "order_detail.qty as qty"
                            )
                            ->whereIn('order_detail.order_id', $order_id)
                            ->get();

                            foreach ($jualDetail as $key => $p) {
                                // buat harga
                                if ($p->harga_awal != null) {
                                    $p->harga_awal = $p->harga_awal; 
                                }else{
                                    $p->harga_awal = $p->price; 
                                }

                                // type komisi persen rubah ke satuan nominal
                                if($p->type_komisi == 'persentase'){
                                    $p->komisi = $p->harga_awal * $p->komisi / 100; 
                                }else{
                                    $p->komisi = $p->komisi;
                                }

                            }

                        $pendapatan = $jual->sum('total');
                        $gross = $jual->sum('gross');
                        $ongkir = $jual->sum('ongkir');
                        $biaya_layanan = $jual->sum('biaya_layanan');
                        $cashback = $jual->sum('cashback');
                        $biaya_layanan = $jual->sum('biaya_layanan');
                        $qty = $jualDetail->sum('qty');
                        $fix_price = $jualDetail->sum('fix_price');
                        $komisi = $jualDetail->sum('komisi');
                        $harga_awal = $jualDetail->sum('harga_awal');
                        return response([$harga_awal, $komisi, $fix_price, $qty, $biaya_layanan, $cashback, $ongkir, $gross, $pendapatan]);
                } else {
                    //kita filter dari tanggal awal ke akhir
                            $jual = DB::table("order")
                            ->select("order.id", 
                                "order.created_at as created_at",
                                "order.name as name_product", 
                                "order.name as name_product", 
                                "order.name as name_product", 
                                "order.invoice as invoice",
                                DB::raw("order.total + order.total_komisi_produk as gross"),
                                "order.kode_unik as kode_unik",
                                DB::raw("order.biaya_layanan + order.kode_unik as biaya_layanan"),
                                DB::raw("order.laba_bersih as total"),
                                "order.cashback as cashback",
                                "order.ongkos_kirim_fix as ongkir",
                            )
                            ->where('order.status', '=', 5)
                            // ->whereBetween('order.created_at', array($request->from_date, $request->to_date))
                            ->whereDate('created_at', '>=', $request->from_date)
                            ->whereDate('created_at', '<=', $request->to_date)
                            ->orderBy("id", "DESC")
                            ->get();

                            $order_id = [];
                            foreach ($jual as $j) {
                                array_push($order_id, $j->id);
                            }


                            $jualDetail = DB::table("order_detail")
                            ->select(
                                "order_detail.price as price",
                                "order_detail.harga_diskon as harga_awal",
                                "order_detail.nilai_komisi as komisi",
                                "order_detail.type_komisi as type_komisi",
                                "order_detail.diskon as diskon",
                                "order_detail.fix_price as fix_price",
                                "order_detail.qty as qty"
                            )
                            ->whereIn('order_detail.order_id', $order_id)
                            ->get();

                            foreach ($jualDetail as $key => $p) {
                                // buat harga
                                if ($p->harga_awal != null) {
                                    $p->harga_awal = $p->harga_awal; 
                                }else{
                                    $p->harga_awal = $p->price; 
                                }

                                // type komisi persen rubah ke satuan nominal
                                if($p->type_komisi == 'persentase'){
                                    $p->komisi = $p->harga_awal * $p->komisi / 100; 
                                }else{
                                    $p->komisi = $p->komisi;
                                }

                            }

                        $pendapatan = $jual->sum('total');
                        $gross = $jual->sum('gross');
                        $ongkir = $jual->sum('ongkir');
                        $biaya_layanan = $jual->sum('biaya_layanan');
                        $cashback = $jual->sum('cashback');
                        $biaya_layanan = $jual->sum('biaya_layanan');
                        $qty = $jualDetail->sum('qty');
                        $fix_price = $jualDetail->sum('fix_price');
                        $komisi = $jualDetail->sum('komisi');
                        $harga_awal = $jualDetail->sum('harga_awal');
                        return response([$harga_awal, $komisi, $fix_price, $qty, $biaya_layanan, $cashback, $ongkir, $gross, $pendapatan]);
                        // return response([$pendapatan, $gross, $ongkir, $biaya_layanan, $cashback, $qty, $fix_price, $komisi, $harga_awal]);
                }
            }
        }
    }
}
