<?php

namespace App\Http\Controllers\Admin\MasterBank;

use App\Http\Controllers\Controller;
use App\Model\MasterBankVendor;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class MasterBankController extends Controller
{
    public function index()
    {
        $bank = MasterBankVendor::where('user_id', auth()->user()->id)->where('isMidtrans', '=', 0)->get();
        $bank2 = MasterBankVendor::where('user_id', auth()->user()->id)->where('isMidtrans', '=', 1)->get();

        $pesan = $this->pesan;

        return view('admin.master.master_bank.index', compact('pesan', 'bank', 'bank2'));
    }

    public function uploadbank(Request $request)
    {
        MasterBankVendor::create([
            'user_id' => auth()->user()->id,
            'nama_bank' => $request->nama_bank,
            'nama' => $request->nama,
            'no_rekening' => $request->no_rekening,
            'isMidtrans' => 0
        ]);

        return redirect()->back()->with(['success' => 'Rekening Telah Di Buat']);
    }

    public function uploadbank2(Request $request)
    {
        MasterBankVendor::create([
            'user_id' => auth()->user()->id,
            'nama_bank' => $request->nama_bank,
            'isMidtrans' => 1
        ]);

        return redirect()->back()->with(['success' => 'Rekening Telah Di Buat']);
    }

    public function updatebank(Request $request)
    {
        $bank = MasterBankVendor::where('id', $request->id)->first();

        $old_img = 'public/images/admin/rekening/' . $bank->logo;
        $filename = !empty($bank->logo) ? $bank->logo : NULL;

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $ukuran = $file->getSize();
            $filename = "bank-" . time() . '.' . $file->getClientOriginalExtension();
            $image = Image::make($file)->resize(120, 120);

            //delete old img
            Storage::disk('s3')->delete($old_img);

            $newfile = Storage::disk('s3')->put(
                'public/images/admin/rekening/' . $filename,
                $image->stream()
            );

            MasterBankVendor::create([
                'user_id' => auth()->user()->id,
                'nama_bank' => $request->nama_bank,
                'no_rekening' => $request->no_rekening,
                'logo' => $filename
            ]);
        }

        return redirect()->back()->with(['success' => 'Rekening Telah Di Update']);
    }

    public function destroy($id)
    {
        $bank = MasterBankVendor::find($id);
        $bank->delete();

        return redirect(route('mb.index'))->with(['success' => 'Rekening Berhasil Di Hapus!']);
    }
}
