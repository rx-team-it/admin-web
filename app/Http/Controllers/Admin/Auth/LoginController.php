<?php

namespace App\Http\Controllers\Admin\Auth;

use App\User;
use App\Model\Nav_Grup;
use App\Model\RoleUsers;
use App\Model\MasterVendor;
use Illuminate\Http\Request;
use App\Model\Nav_Grup_Parent;
use App\Model\Nav_Parent_Child;
use App\Model\Nav_Child_of_Child;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    public function formlogin()
    {
        return view('admin.auth.login');
    }

    public function postLogin(Request $request)
    {
        $rules = [
            'email' => 'required|confirmed',
            'password' => 'required|confirmed',
        ];

        $messages = [
            'password.required'      => 'Password wajib diisi.',
            'password.confirmed'     => 'Password Tidak Sesuai',
            'email.required'         => 'Email wajib diisi.',
            'email.confirmed'        => 'Email Tidak Sesuai',
        ];

        $CheckUser = User::where('email', $request->email)->first();

        $remember = $request->remember ? true : false;

        if (empty($CheckUser)) {
            return redirect()->back()->with('error', 'Email Tidak Ditemukan');
        } else {
            if (Hash::check($request->password, $CheckUser->password)) {
                if (!Auth::attempt([
                    'email' => $request->email,
                    'password' => $request->password
                ], $remember)) {
                    return redirect()->back()->with('error', 'Silahkan Isi Dengan Benar');
                }
                // get status membership
                $status = DB::table('status_membership')->first();
                Session::put('status_membership', $status->jenis);

                // // get nav from api supersystem
                // $curl = curl_init();

                // curl_setopt_array($curl, array(
                // CURLOPT_URL => 'https://abbabill.com/api/get-navigation',
                // CURLOPT_RETURNTRANSFER => true,
                // CURLOPT_ENCODING => '',
                // CURLOPT_MAXREDIRS => 10,
                // CURLOPT_TIMEOUT => 0,
                // CURLOPT_FOLLOWLOCATION => true,
                // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                // CURLOPT_CUSTOMREQUEST => 'GET',
                // CURLOPT_HTTPHEADER => array(
                //     'Accept: application/json'
                // ),
                // ));

                // $response = curl_exec($curl);

                // curl_close($curl);
                
                // Session::put('navigasi', json_decode($response));

                // get nav
                
                // get Group
                $menu = Nav_Grup::all();
                // get parent with relation
                $menu->map(function($menus){
                    $menus['parent_model'] = Nav_Grup_Parent::where('grup_id', $menus->id)->get();
                        // get child_parent with relation
                        $menus['parent_model']->map(function($child){
                            $child['child_of_parent'] = Nav_Parent_Child::where('parent_id', $child->id)->get();
                                // get child_of_child with relation
                                $child['child_of_parent']->map(function($coc){
                                    $coc['child_of_child'] = Nav_Child_of_Child::where('cop_id', $coc->id)->get();
                                    return $coc;
                                });
                                // end child_of_child
                            return $child;
                        });
                        // end child_parent
                    return $menus;
                });
                Session::put('navigasi', $menu);


                return redirect()->route('home.admin')->with(['success' => 'Login Berhasil']);
            } else {
                return redirect()->back()->with('error', 'Password anda salah.!');
            }
        }

        // $userdata = [
        //     'email'    => $request->email,
        //     'password' => $request->password,
        // ];

        // $customMessages = [
        //     'password' => 'Password salah',
        //     'email'    => 'Email Tidak Ditemukan',
        // ];

        // $rules = [
        //     'email' => 'required|email|exists:users,email',
        //     // 'password' => 'required|password|exists:users,password'
        // ];

        // // Validate the inputs.
        // $validator = Validator::make($userdata, $rules, $customMessages);

        // // Check if the form validates with success.
        // if ($validator->fails()) {
        //     return back()->with('error', $validator->messages()->all()[0]);
        // } else {
        //     (Auth::attempt($userdata));
        //     return redirect()->route('home.admin');
        // }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login.admin')->with(['success' => 'Logout Berhasil']);
    }
}
