<?php

namespace App\Http\Controllers\Admin\KreditTopup;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\SaldoKreditVendor;
use App\Model\SaldoKreditVendorSecond;
use App\Model\Transaction_Topup;
use App\Model\Transaction_Topup_Second;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;
use Session;
use Illuminate\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class TopupController extends Controller
{
    public function index()
    {
        $data_topup = SaldoKreditVendor::orderBy('created_at', 'desc')->get();
        $data_saldo = SaldoKreditVendor::where(['id_user' => Auth::user()->id, 'status' => 1])->sum('nominal');
        // return $data_saldo;
        $data_order = DB::table('order')->where(['status' => 5])->get();
        

        return view('admin.kredittopup.index',compact('data_topup','data_saldo','data_order'));
    }

    public function topup(request $request)
    {
        // minimal topup 2.5jt
        $min = $request->nominal1;
        $min_pil = $request->nominal;

        if ($min == null) {
            $input_nominal = $min_pil;
        }else {
            $input_nominal = $min;
        }

        $date = Carbon::now();
        $invoice_topup = 'TOP' . '-' . date("Ymd") . Str::random(3);
        $url = 'https://abbabill-dev.site/QRIS/' . $invoice_topup;
        $now = date('Y-m-d H:i:s');
        
        $top = SaldoKreditVendor::create([
            'id_user' => $request->id_user,
            'nominal' => $input_nominal,
            'status' => 0,
            'invoice_topup' => $invoice_topup,
            'url' => $url,
            'expired' => date('Y-m-d H:i:s', strtotime($now . ' + 1 days')),
        ]);
        
        $top_payment = SaldoKreditVendorSecond::create([
            'id_user' => $request->id_user,
            'nominal' => $input_nominal,
            'status' => 0,
            'invoice_topup' => $invoice_topup,
            'url' => $url,
            'expired' => date('Y-m-d H:i:s', strtotime($now . ' + 1 days')),
        ]);

        $json_data = json_decode($this->payment_abbabill_charge($top), true);
        // dd($json_data);
        $res = $json_data['Data'];
        Transaction_Topup::create([
            'id_saldo' => (int)$top->id,
            'transaction_id' => $res['token'],
            'bank' => null,
            'payment_type' => 'SNAP',
            'va_number' =>  null,
            'gross_amount' =>  $input_nominal,
            'transaction_time' => $date,
            'transaction_status' => 'pending',
            'url' => $res['redirect_url'],
        ]);
        
        Transaction_Topup_Second::create([
            'id_saldo' => (int)$top->id,
            'transaction_id' => $res['token'],
            'bank' => null,
            'payment_type' => 'SNAP',
            'va_number' =>  null,
            'gross_amount' =>  $input_nominal,
            'transaction_time' => $date,
            'transaction_status' => 'pending',
            'url' => $res['redirect_url'],
        ]);

        $top1 = SaldoKreditVendor::where('invoice_topup', $invoice_topup)->first();
        $top1->update([
            'url' => $res['redirect_url'],
        ]);
        $top1_payment = SaldoKreditVendorSecond::where('invoice_topup', $invoice_topup)->first();
        $top1_payment->update([
            'url' => $res['redirect_url'],
        ]);
        $top['redirect_url'] = $res['redirect_url'];


        return redirect($top['redirect_url']);
    }

    public function payment_abbabill_charge($top)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            // CURLOPT_URL => env('URL_ABBABILL_PAY') . '/midtrans/charge',
            CURLOPT_URL => 'https://payment.abbabill-dev.site/midtrans/charge',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '
            {
                "platform": "midtrans",
                "vendor_code": "' . Session::get('sites')->vendor_code . '",
                "payment_type" : "gopay",
                "is_snap": "TRUE",
                "bank": "QRIS",
                "transaction_time": "' . Carbon::now() . '",
                "transaction_details": {
                    "order_id":  "' . $top->invoice_topup . '",
                    "gross_amount": "' . $top->nominal . '"
                },
                "customer_details": {
                    "first_name": "' . auth()->user()->name . '",
                    "last_name": "-",
                    "email": "' . auth()->user()->email . '",
                    "phone": "' . auth()->user()->phone . '"
                }
            }',
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json',
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}
