<?php

namespace App\Http\Controllers\Admin\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AccessRole;
use App\Model\UserDetails;
use Illuminate\Support\Facades\Validator;
use App\User;

class VendorController extends Controller
{
    public function index()
    {
        $user = AccessRole::where('role_id', '=', 1)->get();

        return view('admin.vendor.index', compact('user'));
    }

    public function tambah(Request $request)
    {
        $rules = [
            'name' => 'required|string|between:4,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'phone' => 'required|numeric|min:9',
        ];

        $messages = [
            'name.between'           => 'Minimal 4 Karakter',
            'name.required'          => 'Nama wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 5 karakter.',
            'password.confirmed'     => 'Password Tidak Sesuai',
            'email.required'         => 'Email wajib diisi.',
            'email.email'            => 'Email tidak valid.',
            'email.unique'           => 'Email sudah terdaftar.',
            'phone.numeric'          => 'Wajib Diisi Dengan Angka',
            'phone.required'         => 'Nomor Telp Wajib Diisi'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json(['status' => 0, 'error' => $validator->errors()->toArray()]);
        } else {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => bcrypt($request->password)
            ]);

            AccessRole::create([
                'role_id' => 1, //seller
                'user_id' => $user->id
            ]);

            UserDetails::create([
                'id_user' => $user->id
            ]);
        }

        return response()->json(['status' => 1, 'msg' => 'Vendor Berhasil Dibuat']);
    }
}
