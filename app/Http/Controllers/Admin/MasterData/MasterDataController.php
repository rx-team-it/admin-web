<?php

namespace App\Http\Controllers\Admin\MasterData;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MasterData;
use App\Model\HistoryImage;
use App\Model\MasterDataChild;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;
use DB;
use Str;

class MasterDataController extends Controller
{
    public function index()
    {
        $r = MasterData::where('type', 'footer')->orderBy('created_at', 'DESC')->with(['child'])->get();
        $c = MasterData::where('type', 'header')->orderBy('created_at', 'DESC')->get();
        $f = MasterData::where('type', 'footer')->count();
        $w = MasterData::where('type', 'header')->count();
        // return $r;
        $pesan = $this->pesan;
        return view('admin.master.master_data.index', compact('pesan', 'r', 'c', 'f', 'w'));
    }

    public function post(request $request)
    {
        MasterData::create([
            'judul' =>  $request->judul,
            'type' =>  'footer',
        ]);

        return redirect()->back()->with(['success' => 'Data Berhasil di Simpan!']);
    }

    public function postheader(request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $ukuran = $file->getSize();
            $filename = "category-" . time() . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file);
            $newfile = Storage::disk('s3')->put(
                'public/assets/' . $filename,
                $img->stream()
            );
            $t = DB::update("UPDATE master_vendor set logo=?, mini_logo=? where user_id=?", [$filename, $filename, Auth::user()->id]);

            MasterData::create([
                'icon' =>  $filename,
                'ukuran' => $ukuran,
                'title' =>  $request->title,
                'type' =>  'header',
            ]);

            HistoryImage::create([
                'gambar' =>  $filename,
                'ukuran' => $ukuran,
                'user_id' => Auth::user()->id,
            ]);
        }

        return redirect()->back()->with(['success' => 'Data Berhasil di Simpan!']);
    }

    public function delete($id)
    {
        $km = DB::delete("DELETE FROM master_data where id=?", [$id]);

        return redirect(route('md.index'))->with(['success' => 'Data Telah Dihapus!']);
    }

    public function deletesub($id)
    {
        $km = DB::delete("DELETE FROM master_data_child where id=?", [$id]);

        return redirect(route('md.index'))->with(['success' => 'Data Telah Dihapus!']);
    }

    public function deleteheader($id)
    {
        $km = DB::delete("DELETE FROM master_data where id=?", [$id]);
        MasterDataChild::where('id_master', $id)->delete();


        return redirect(route('md.index'))->with(['success' => 'Data Telah Dihapus!']);
    }

    public function edit(request $request, $id)
    {
        $r = MasterData::all();
        $md = MasterData::where('id', $id)->get();
        $pesan = $this->pesan;

        if ($request->isMethod('post')) {

            $t = DB::update("UPDATE master_data set judul=? where id=?", [$request->judul, $id]);

            return redirect(route('md.index'))->with(['success' => 'Data Berhasil di Update']);
        }
        return view('admin.master.master_data.edit', compact('r', 'md', 'pesan'));
    }

    public function editheader(request $request, $id)
    {
        $r = MasterData::all();
        $md = MasterData::where('id', $id)->get();
        $pesan = $this->pesan;

        if ($request->isMethod('post')) {

            $t = DB::update("UPDATE master_data set judul=? where id=?", [$request->judul, $id]);

            return redirect(route('md.index'))->with(['success' => 'Data Berhasil di Update']);
        }
        return view('admin.master.master_data.edit', compact('r', 'md', 'pesan'));
    }

    public function editsub(request $request, $id)
    {
        $r = MasterDataChild::all();
        $md = MasterDataChild::where('id', $id)->get();
        $pesan = $this->pesan;

        if ($request->isMethod('post')) {

            $t = DB::update("UPDATE master_data_child set nama=?, isi=? where id=?", [$request->nama, $request->isi, $id]);

            return redirect(route('md.index'))->with(['success' => 'Data Berhasil di Update']);
        }
        return view('admin.master.master_data.editsub', compact('r', 'md', 'pesan'));
    }

    public function subjudul(request $request, $id)
    {
        $w = MasterData::where('id', $id)->first();
        $md = MasterDataChild::where('id', $id)->get();

        if ($request->isMethod('post')) {

            MasterDataChild::create([
                'id_master' =>  $w->id,
                'nama' =>  $request->nama,
                'slug' =>  Str::slug($request->nama, '-'),
                'isi' =>  $request->isi,
            ]);

            return redirect(route('md.index'))->with(['success' => 'Data Berhasil di Tambah']);
        }

        $pesan = $this->pesan;
        return view('admin.master.master_data.subjudul', compact('md', 'pesan', 'w'));
    }

    // child footer
    public function childFooterPost(Request $request)
    {
        $this->validate($request, [
            'parentFooter' => 'required',
            'nama' => 'required',
            'nama' => 'required',
            'isi' => 'required'
        ]);


        MasterDataChild::create([
            'id_master' => $request->parentFooter,
            'nama' => $request->nama,
            'slug' => Str::slug($request->nama, '-'),
            'isi' => $request->isi,
        ]);
        return redirect()->back()->with(['success' => 'Data Berhasil di Simpan!']);
    }

    public function childFooterGet($id)
    {
        $arr = MasterDataChild::where('id_master', $id)->get();
        $empty = 'kosong';
        if ($arr) {
            return json_encode($arr);
        } else {
            return $empty;
        }
    }

    public function childFooterDelete($id)
    {
        MasterDataChild::where('id', $id)->delete();
        return redirect()->back()->with(['success' => 'Data Berhasil di Hapus!']);
    }

    public function childFooterGet2($id)
    {
        $arr = MasterDataChild::where('id', $id)->first();

        return json_encode($arr);
    }

    public function childFooterEdit(Request $request, $id)
    {

        // ddd($request);
        MasterDataChild::where('id', $id)->update([
            'nama' => $request->nama,
            'isi' => $request->isi,
        ]);

        return redirect()->back()->with(['success' => 'Data Berhasil di Update!']);
    }
}
