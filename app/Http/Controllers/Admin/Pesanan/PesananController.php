<?php

namespace App\Http\Controllers\Admin\Pesanan;

use PDF;
use App\User;
use App\Model\Order;
use App\Model\Komisi;
use App\Model\Product;
use App\Model\MasterData;
use App\Model\MasterStore;
use App\Model\OrderDetail;
use App\Model\Transaction;
use App\Model\UserDetails;
use App\Model\HistorySaldo;
use App\Model\MasterAdress;
use App\Model\UserReferall;
use Illuminate\Support\Str;
use App\Mail\PesananDikemas;
use App\Model\GroupReseller;
use App\Model\ProductDetail;
use App\Model\ProductVarian;
use Illuminate\Http\Request;
use App\Model\ConfirmPayment;
use App\Model\HistoryPayment;
use App\Mail\admin_tolak_orderan;
use App\Model\ProductVarianOption;
use Illuminate\Support\Facades\DB;
use App\Model\HistoryProductVendor;
use App\Model\HistorySaldoReseller;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Model\GroupMaster;
use App\Model\MasterKurir;
use App\Model\ProductSum;

class PesananController extends Controller
{

    public function detail($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->with('store')->first();
        $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
            'product_detail.varian_option.varian'
        ])->get();

        $product = ProductVarianOption::with('parent_option')->get();
        $varian = ProductVarian::with('sub_variasi')->get();

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->parent_option)) {
                $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
            }

            return $details;
        });

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
            }

            return $details;
        });

        // return $order['detail'];
        // return $order;

        // $store = MasterStore::where('user_id', auth()->user()->id)->first();
        $store = DB::table('master_store')->join('master_address', 'master_address.id', '=', 'master_store.address_id')->first();
        // return $store;
        $pesan = $this->pesan;
        // return $address;

        return view('admin.pesanan.detail', compact('order', 'product', 'varian', 'pesan', 'store'));
    }

    public function invoice_print($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->with('store')->first();
        $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
            'product_detail.varian_option.varian'
        ])->get();


        $product = ProductVarianOption::with('parent_option')->get();
        $varian = ProductVarian::with('sub_variasi')->get();

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->parent_option)) {
                $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
            }

            return $details;
        });

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
            }

            return $details;
        });

        // return $order['detail'];

        // $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $store = DB::table('master_store')->join('master_address', 'master_address.id', '=', 'master_store.address_id')->first();


        return view('admin.pesanan.invoice_print', compact('order', 'store'));
    }

    public function invoicePrint_multi(Request $request)
    {
        if (empty($request->data)) {
            return redirect()->back()->with(['error' => 'Pilih Beberapa']);
        }
        $id_order = explode(",", $request->datas);
        $invoice = [];
        for ($i = 0; $i < count($id_order); $i++) {
            $order = Order::where(['id' => $id_order[$i]])->with('store')->first();
            $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
                'product_detail.varian_option.varian'
            ])->get();

            $product = ProductVarianOption::with('parent_option')->get();
            $varian = ProductVarian::with('sub_variasi')->get();

            $order['detail']->map(function ($details) {
                if (!empty($details->product_detail->varian_option->parent_option)) {
                    $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
                }

                return $details;
            });

            $order['detail']->map(function ($details) {
                if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                    $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
                }

                return $details;
            });

            $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
            $address = MasterAdress::where(['id_user' => auth()->user()->id])->first();
            $invoice[] = $order;
        }
        // return $invoice;
        return view('admin.pesanan.invoice_print_multi', compact('order', 'store', 'invoice', 'address'));
    }

    public function printResi($invoice)
    {

        $order = Order::where(['invoice' => $invoice])->with('store')->first();
        // return $order;
        $order->update([
            'ready_to_print' => 0,
        ]);
        $exp = \Carbon\Carbon::parse($order->updated_at)->addDays(4)->format('d M Y');
        $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
            'product_detail.varian_option.varian'
        ])->get();

        $varian = ProductVarian::with('sub_variasi')->get();
        $product = ProductVarianOption::with('parent_option')->get();

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->parent_option)) {
                $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
            }

            return $details;
        });
        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
            }

            return $details;
        });

        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $address = DB::table('master_store')->join('master_address', 'master_address.id', '=', 'master_store.address_id')->first();

        $tracking_num = Order::where(['invoice' => $invoice])->first();
        $service = Order::where(['invoice' => $invoice])->first();
        $service['order_detail'] = OrderDetail::where('order_id', $tracking_num->id)->get();
        $itemnya = [];
        foreach ($service['order_detail'] as $item) {
            $name_product =  substr($item->name_product, 0, 30);
            $item_category = substr($item->category, 0, 19);
            $itemnya[] = ([
                'item_name' => '' . $name_product . '',
                'item_desc' => '' . $name_product . '',
                'item_category' => '' . $item_category . '',
                'item_quantity' =>  $item->qty,
                'declared_value' => $item->price,
                'weight' => (float)$item->berat
            ]);
        }
        $service['items'] = json_encode($itemnya);

        $service['default_store'] = $default_store = MasterAdress::where(['id_store' => 1])->first();
        $detail = OrderDetail::where(['order_id' => $order->id])->get();


        return view('admin.pesanan.print.print_resi', compact('order', 'product', 'varian', 'store', 'invoice', 'exp', 'address'));
    }

    public function printResi_multi(Request $request)
    {
        $id_order = explode(",", $request->datas);
        $invoice = [];

        for ($i = 0; $i < count($id_order); $i++) {
            $order = Order::where(['id' => $id_order[$i]])->with('store')->first();
            $exp = \Carbon\Carbon::parse($order->updated_at)->addDays(4)->format('d M Y');
            $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
                'product_detail.varian_option.varian'
            ])->get();

            $product = ProductVarianOption::with('parent_option')->get();
            $varian = ProductVarian::with('sub_variasi')->get();

            $order['detail']->map(function ($details) {
                if (!empty($details->product_detail->varian_option->parent_option)) {
                    $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
                }

                return $details;
            });

            $order['detail']->map(function ($details) {
                if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                    $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
                }

                return $details;
            });

            $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
            $tracking_num = Order::where(['invoice' => $order->invoice])->first();
            $service = Order::where(['invoice' => $invoice])->first();
            $service['order_detail'] = OrderDetail::where('order_id', $tracking_num->id)->get();
            $itemnya = [];
            foreach ($service['order_detail'] as $item) {
                $name_product =  substr($item->name_product, 0, 30);
                $itemnya[] = ([
                    'item_name' => '' . $name_product . '',
                    'item_desc' => '' . $name_product . '',
                    'item_category' => '' . $item->category . '',
                    'item_quantity' =>  $item->qty,
                    'declared_value' => $item->price,
                    'weight' => (float)$item->berat
                ]);
            }
            $service['items'] = json_encode($itemnya);
            return  $service['items'];

            $service['default_store'] = $default_store = MasterAdress::where(['id_store' => 1])->first();
            $resi = json_decode($this->create_resi_service($service), true);
            // return $resi;
            $create_resi = $resi['data']['resi'];
            $tracking_num->update([
                'status' => 3,
                'tracking_number' => $create_resi, //'JKT-' . Str::random(8)
                'kode_boking' => '' . ENV('KURIR_CODE') . '' . $service->id . ''
                // 'tracking_number' => 'JKT-' . Str::random(8)
            ]);

            $detail = OrderDetail::where(['order_id' => $order->id])->get();
            foreach ($detail as $det) {
                $det['prod_detail'] = ProductDetail::where(['id' => $det->product_detail_id])->first();
                $det['prod_detail']->update([
                    'qty' => $det['prod_detail']['qty'] - $det->qty,
                ]);
            }
            $invoice[] = $order;
        }
        // return $invoice;

        return view('admin.pesanan.print.print_multiple', compact('order', 'product', 'varian', 'store', 'invoice', 'exp'));
    }

    public function invoice_pdf($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $detail = OrderDetail::where(['order_id' => $order->id])->with([
            'product_detail.varian_option.varian', 'order.store'
        ])->get();

        $product = ProductVarianOption::with('parent_option')->get();
        $varian = ProductVarian::with('sub_variasi')->get();

        $detail->map(function ($details) {
            if (!empty($details->product_detail->varian_option->parent_option)) {
                $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
            }

            return $details;
        });

        $detail->map(function ($details) {
            if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
            }

            return $details;
        });

        $pdf = PDF::loadView('admin.pesanan.invoice_pdf', [
            'order' => $order,
            'detail' => $detail
        ]);

        return $pdf->stream('invoice-pdf.pdf');
    }

    public function list_pesanan(Request $request)
    {   

        
        // $kurir = MasterKurir::select('nama')->where('status', 1)->get();
        // $fix_kurir = [];
        // foreach($kurir as $property => $value){
        //     $fix_kurir[] = $value['nama'];
        // }
        // $fix_kurir = json_encode($fix_kurir);
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => 'https://kurir.abbabill.com/api/checkServiceAdmin',//env('KURIR_URL') . '/api/checkService',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => '',
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => true,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS =>
        //     '{
        //         "postal_code_tujuan":"13910",
        //         "sub_district_tujuan":"Cakung",
        //         "city_tujuan":"Jakarta Timur",
        //         "postal_code_origin":"15157",
        //         "sub_district_origin":"Karang Tengah",
        //         "city_origin":"TANGERANG",
        //         "berat":"100",
        //         "kurir": ["jne", "anteraja", "jnt", "id"],
        //         "group":[
        //             "regular"
        //         ]
        //     }',
        //     CURLOPT_HTTPHEADER => array(
        //         'Content-Type: application/json',
        //     ),
        // ));

        // $response = curl_exec($curl);
        // curl_close($curl);
        // // dd($weight);
        // // dd($response);
        // return json_decode($response, true);
       
        $api_tarif = json_decode($this->api_tarif_jne(), true);
    
        // return $api_tarif['price'];
        $order = Order::where(['user_reff' => NULL])->orderBy('created_at', 'DESC')->get();
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $order = Order::where('user_reff', '=', NULL)->whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $order = Order::where('user_reff', '=', NULL)->whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            if (!empty($request->slug) && $request->slug != 'no') {
                $slug = $request->slug;
                $order = Order::where(['courrier' => $slug, 'user_reff' => NULL])->orderBy('created_at', 'DESC')->get();
                return $order;
            } else if ($request->slug == '') {
                $order = Order::where(['user_reff' => NULL])->orderBy('created_at', 'DESC')->get();
                return $order;
            } else if (!empty($request->service)) {
                $service = $request->service;

                $order = Order::where('group_service', $service)
                ->where('user_reff', NULL)
                ->where('status', 1)
                ->orderBy('created_at', 'DESC')->get();
                return $order;
            }
        }
        // tampilan datatable per-tab
        $belumbayar = [];
        $sudahbayar = [];
        $gagal = [];
        $dikemas = [];
        $dikirim = [];
        $selesai = [];
        $cekpembayaran = [];
        $cekdetail = [];
        $uploadulang = [];
        $batal = [];
        $ready_to_print = [];

        foreach ($order as $key => $value) {

            if ($value->ready_to_print == true) {
                $ready_to_print[] = $value;
            }

            if ($value->status == 0) {
                $belumbayar[] = $value;
            }
            if ($value->status == 1) {
                $sudahbayar[] = $value;
            }
            if ($value->status == 2) {
                $gagal[] = $value;
            }
            if ($value->status == 3) {
                $dikemas[] = $value;
            }
            if ($value->status == 4) {
                $dikirim[] = $value;
            }
            if ($value->status == 5) {
                $selesai[] = $value;
            }
            if ($value->status == 6) {
                $batal[] = $value;
            }
            if ($value->status == 7) {
                $cekpembayaran[] = $value;
            }
            if ($value->status == 8) {
                $uploadulang[] = $value;
            }
        }


        $request_pembatalan = Order::where(['request_cancel' => 1])->get();
        // return $request_pembatalan;

        // hitung orderan pertab
        $hitung_order = $order->count();
        $hitung_belum_bayar = count($belumbayar);
        $hitung_cek_pembayaran = count($cekpembayaran);
        $hitung_upload_ulang = count($uploadulang);
        $hitung_udah_bayar = count($sudahbayar);
        $hitung_dikemas = count($dikemas);
        $hitung_dikirim = count($dikirim);
        $hitung_selesai = count($selesai);
        $hitung_gagal = count($gagal);
        $hitung_batal = count($batal);
        $hitung_ready = count($ready_to_print);

        $default_store = MasterAdress::where(['id_store' => 1])->first();
        $pesan = $this->pesan;
        return view('admin.pesanan.list_pesanan', compact('order', 'pesan', 'sudahbayar', 'gagal', 'dikemas', 'dikirim', 'selesai', 'belumbayar', 'cekpembayaran', 'uploadulang', 'batal', 'hitung_order', 'hitung_udah_bayar', 'hitung_belum_bayar', 'hitung_cek_pembayaran', 'hitung_upload_ulang', 'hitung_dikemas', 'hitung_dikirim', 'hitung_selesai', 'hitung_gagal', 'hitung_batal', 'request_pembatalan', 'ready_to_print', 'hitung_ready', 'default_store'));
    }

    public function termaltest()
    {
        return view('admin.pesanan.termal_test');
    }

    public function indexprint(Request $request)
    {
        // tampilan datatable per-tab
        $order = Order::where(['user_reff' => NULL])->orderBy('created_at', 'DESC')->get();
        if ($request->ajax()) {
            if (!empty($request->slug)) {
                $slug = $request->slug;
                $order = Order::where(['courrier' => $slug, 'user_reff' => NULL])->orderBy('created_at', 'DESC')->get();
                return $order;
            } else if ($request->slug == '') {
                $order = Order::where(['user_reff' => NULL])->orderBy('created_at', 'DESC')->get();
                return $order;
            }
        }
        // return $order;
        $belumbayar = [];
        $sudahbayar = [];
        $gagal = [];
        $dikemas = [];
        $dikirim = [];
        $selesai = [];
        $cekpembayaran = [];
        $uploadulang = [];
        $batal = [];
        $ready_to_print = [];

        foreach ($order as $key => $value) {

            if ($value->ready_to_print == true) {
                $ready_to_print[] = $value;
            }

            if ($value->status == 0) {
                $belumbayar[] = $value;
            }
            if ($value->status == 1) {
                $sudahbayar[] = $value;
            }
            if ($value->status == 2) {
                $gagal[] = $value;
            }
            if ($value->status == 3) {
                $dikemas[] = $value;
            }
            if ($value->status == 4) {
                $dikirim[] = $value;
            }
            if ($value->status == 5) {
                $selesai[] = $value;
            }
            if ($value->status == 6) {
                $batal[] = $value;
            }
            if ($value->status == 7) {
                $cekpembayaran[] = $value;
            }
            if ($value->status == 8) {
                $uploadulang[] = $value;
            }
        }

        // return $sudahbayar;

        $request_pembatalan = Order::where(['request_cancel' => 1])->get();
        // return $request_pembatalan;

        // hitung orderan pertab
        $hitung_order = $order->count();
        $hitung_belum_bayar = count($belumbayar);
        $hitung_cek_pembayaran = count($cekpembayaran);
        $hitung_upload_ulang = count($uploadulang);
        $hitung_udah_bayar = count($sudahbayar);
        $hitung_dikemas = count($dikemas);
        $hitung_dikirim = count($dikirim);
        $hitung_selesai = count($selesai);
        $hitung_gagal = count($gagal);
        $hitung_batal = count($batal);
        $hitung_ready = count($ready_to_print);

        $pesan = $this->pesan;

        return view('admin.pesanan.laporan', compact('order', 'pesan', 'sudahbayar', 'gagal', 'dikemas', 'dikirim', 'selesai', 'belumbayar', 'cekpembayaran', 'uploadulang', 'batal', 'hitung_order', 'hitung_udah_bayar', 'hitung_belum_bayar', 'hitung_cek_pembayaran', 'hitung_upload_ulang', 'hitung_dikemas', 'hitung_dikirim', 'hitung_selesai', 'hitung_gagal', 'hitung_batal', 'request_pembatalan', 'ready_to_print', 'hitung_ready'));
    }

    // FUNCTION LIST ORDER MANUAL //
    public function indexnew(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $order = Order::where(['user_reff' => NULL])->whereDate('created_at', '=', $request->from_date)->where('isMidtrans', '=', 0)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $order = Order::where(['user_reff' => NULL])->whereBetween('created_at', array($request->from_date, $request->to_date))->where('isMidtrans', '=', 0)->get();
                }
            }
            //load data default
            else {
                $order = Order::where(['user_reff' => NULL])->orderBy('created_at', 'DESC')->where('isMidtrans', '=', 0)->get();
            }

            return datatables()->of($order)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("order.pembayaran", $data->invoice) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Detail Pembayaran</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="' . route("pesanan.detail", $data->invoice) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="delete btn btn-primary btn-sm"><i class="far fa-edit"> Invoice</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.pesanan.indexnew', compact('pesan'));
    }

    // FUNCTION LIST ORDER OTOMATIS //
    public function indexmidtrans(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $order = Order::where(['user_reff' => NULL])->whereDate('created_at', '=', $request->from_date)->where('isMidtrans', '=', 1)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $order = Order::where(['user_reff' => NULL])->whereBetween('created_at', array($request->from_date, $request->to_date))->where('isMidtrans', '=', 1)->get();
                }
            }
            //load data default
            else {
                $order = Order::where(['user_reff' => NULL])->orderBy('created_at', 'DESC')->where('isMidtrans', '=', 1)->get();
            }

            return datatables()->of($order)
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("order.pembayaran_otomatis", $data->invoice) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Detail Pembayaran</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="' . route("pesanan.detail", $data->invoice) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="delete btn btn-primary btn-sm"><i class="far fa-edit"> Invoice</a>';
                    return $button;
                })
                ->rawColumns(['action', 'status_label'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $pesan = $this->pesan;

        return view('admin.pesanan.indexnew', compact('pesan'));
    }

    // FUNCTION DETAIL PEMBAYARAN MANUAL //
    public function pembayaran($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order['product'] = OrderDetail::where(['order_id' => $order->id])->get();
        $order['confirm_payment'] = ConfirmPayment::where(['order_id' => $order->id])->latest()->first();

        // track resi
        $user = Order::where('invoice', $invoice)->first();
        $default_store = MasterAdress::where('id_store', 1)->first();

        $awb = $user->tracking_number;

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://apiv2.jne.co.id:10102/tracing/api/list/v1/cnote/'. $awb,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => 'username=TESTAPI&api_key=25c898a9faea1a100859ecd9ef674548',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true)['history'];
        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => env('KURIR_URL') . '/api/tracking', //'https://kurir.abbabill-dev.site/api/tracking',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => '',
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => true,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS => '{
        //             "kurir":"anter_aja",
        //             "waybill_no":' . $awb . '
        //         }',
        //     CURLOPT_HTTPHEADER => array(
        //         'Content-Type: application/json'
        //     ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // $response = json_decode($response, true)['data'];

        $pesan = $this->pesan;
        return view('admin.pesanan.pembayaran', compact('order', 'default_store', 'pesan'));
    }

    // function acc bukti pembayaran di status 7
    public function acc_bukti($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order->update([
            'status' => 1
        ]);

        return redirect()->back()->with(['success' => 'Pembayaran di terima']);
    }

    public function acc_buktiMulti(Request $request)
    {
        $order = $request->input('idorder');
        if (empty($order)) {
            return redirect()->back()->with(['error', 'Belum ada yang di pilih']);
        }
        foreach ($order as $value) {
            $order = Order::find($value);
            $order->update([
                'status' => 1
            ]);
        }
        // return $order;
        return redirect()->back()->with(['success' => 'Pembayaran di terima']);
    }

    // function reupload bukti pembayaran
    public function reupload($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order->update([
            'status' => 8
        ]);

        return redirect()->back()->with(['success' => 'Lakukan upload ulang bukti pembayaran untuk pembeli']);
    }

    //Aksi dikirim//
    public function tombolDikirim($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order->update([
            'status' => 4
        ]);

        return redirect()->back()->with(['success' => 'Status order berubah menjadi dikirim']);
    }
    //Aksi dikemas//
    public function tombolDikemas($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order->update(['status' => 3]);
        $user = User::where(['id' => $order->user_id])->first();

        Mail::to("$user->email")->send(new PesananDikemas());

        $detail = OrderDetail::where(['order_id' => $order->id])->get();
        foreach ($detail as $det) {
            $det['prod_detail'] = ProductDetail::where(['id' => $det->product_detail_id])->first();
            $det['prod_detail']->update([
                'qty' => $det['prod_detail']['qty'] - $det->qty,
            ]);
        }


        return redirect()->back()->with(['success' => 'Status order berubah menjadi dikemas']);
    }

    // FUNCTION DETAIL PEMBAYARAN OTOMATIS //
    public function pembayaran_otomatis($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order['product'] = OrderDetail::where(['order_id' => $order->id])->get();
        $order['confirm_payment'] = ConfirmPayment::where(['order_id' => $order->id])->first();

        $pesan = $this->pesan;

        return view('admin.pesanan.pembayaran_otomatis', compact('order', 'pesan'));
    }

    // FUNCTION INPUT RESI DI DETAIL PEMBAYARAN //
    public function inputresi(Request $request)
    {
        $resi = Order::where('id', $request->id)->first();
        $user = User::where(['id' => $resi->user_id])->first();


        Mail::to($user->email)->send(new admin_tolak_orderan());

        $resi->update([
            'tracking_number' =>  Str::random(8),
            'status' => 4,
        ]);

        return redirect()->back()->with(['success', 'Nomor Resi Berhasil Di Input']);
    }

    ///////////////////GA DIPAKE //////////////////////////////////////////////////////////
    public function batalkan_order(Request $request, $invoice)
    {
        $message = Order::where('invoice', $invoice)->first();
        $message->update([
            'reply_message' =>  $request->reply_message,
            'status' => 6,
        ]);
        return redirect()->back()->with(['success' => 'Pesanan Berhasil Dibatalkan']);
    }
    ////////////////////SAMPE SINI ////////////////////////////////////////////////////////


    // FUNCTION UNTUK GANTI STATUS DI DETAIL PEMBAYARAN //
    public function statusgagal($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $user = User::where(['id' => $order->user_id])->first();
        $order->update(['status' => 2]);

        Mail::to($user->email)->send(new admin_tolak_orderan());

        return redirect()->back()->with(['success' => 'Status Berhasil Diperbarui']);
    }

    public function statusdikemas($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        return $order;
        $order->update(['status' => 3]);
        $user = User::where(['id' => $order->user_id])->first();

        // Mail::to("$user->email")->send(new PesananDikemas());

        $detail = OrderDetail::where(['order_id' => $order->id])->get();
        foreach ($detail as $det) {
            $det['prod_detail'] = ProductDetail::where(['id' => $det->product_detail_id])->first();
            $det['prod_detail']->update([
                'qty' => $det['prod_detail']['qty'] - $det->qty,
            ]);
        }

        return redirect()->back()->with(['success' => 'Status Berhasil Diperbarui']);
    }

    public function statusselesai($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order_detail = OrderDetail::where('order_id', $order->id)->get();
        // return $order_detail;

        $total_qty = 0;
        foreach($order_detail as $od){
            $total_qty += $od->qty;

            // insert or update in product_Sum table
            $product_sum = ProductSum::where('product_id', $od->product_id)->first();
            // $product_sum = ProductSum::where('product_id', 2)->first();

            if (!empty($product_sum)) {
                $product_sum->update([
                    'terjual' => $product_sum->terjual + $od->qty
                ]);
            }else{
                ProductSum::create([
                    'product_id' => $od->product_id,
                    'terjual' => $od->qty,
                    'views' => 0,
                    'ulasan' => 0,
                    'rating' => 0
                ]);
            }

        }
        // return $product_sum;

        $user = User::where(['id' => $order->user_id])->first();
        $order->update(['status' => 5]);
        $nominal = 0;
        $user_history = HistoryPayment::where('user_reff_id', $order->user_reff)->first();

        if (!empty($user_history->user_reff_id)) {

            $user_reseller = UserReferall::where('user_referral_id', $user_history->user_reff_id)->first();
            if (!empty($user_reseller)) {
                $group = GroupReseller::where('id_reseller', $user_reseller->user_id)->first();
                $group_master = GroupMaster::where('id', $group->group)->first();
                // $komisi = Komisi::where('id_group', $group->group)->first();

                if ($group_master->satuan == "persentase") {
                    $total__ = 0;
                    // $nominal = ($order->total * $komisi->nominal) / 100;
                    foreach($order_detail as $od){
                        $nominal = ($od->fix_price * $group_master->nominal) / 100;
                        $total_ = $nominal *  $od->qty;
                        $total__ += $total_;
                    }
                    $nominal = $total__;
                } else {
                    $nominal = $total_qty * $group_master->nominal;
                }
            }
        } else {
            $user_reseller = UserReferall::where('user_referral_id', $order->user_reff)->first();
            if (!empty($user_reseller)) {
                $group = GroupReseller::where('id_reseller', $user_reseller->user_id)->first();
                $group_master = GroupMaster::where('id', $group->group)->first();
                // $komisi = Komisi::where('id_group', $group->group)->first();
                if ($group_master->satuan == "persentase") {
                    $total__ = 0;
                    foreach($order_detail as $od){
                        $nominal = ($od->fix_price * $group_master->nominal) / 100;
                        $total_ = $nominal *  $od->qty;
                        $total__ += $total_;
                    }
                    $nominal = $total__;
                } else {
                    $nominal = $total_qty * $group_master->nominal;
                }

            }
        }

        if (empty($user_history)) {
            HistoryPayment::create([
                'user_id' => $order->user_id,
                'user_reff_id' => $order->user_reff,
                'total_order' => 1,
                'total_harga' => $order->total,
                'komisi' => $nominal
            ]);

            HistorySaldoReseller::create([
                'user_id' => $order->user_id,
                'user_reff_id' => $order->user_reff,
                'order_id' => $order->id,
                'nominal' => $nominal,
                'keterangan' => 'Pendapatan masuk dari orderan ' . $invoice
            ]);

            $komisi = HistorySaldo::create([
                'user_id' => $order->user_id,
                'user_reff_id' => $order->user_reff,
                'order_id' => $order->id,
                'komisi' => $nominal
            ]);
        } else {
            HistoryPayment::where('user_id', $user->id)->update([
                'total_order' => $user_history->total_order + 1,
                'total_harga' => $user_history->total_harga + $order->total,
                'komisi' => $user_history->komisi + $nominal,
            ]);
            HistorySaldoReseller::create([
                'user_id' => $order->user_id,
                'user_reff_id' => $order->user_reff,
                'order_id' => $order->id,
                'nominal' => $nominal,
                'keterangan' => 'Pendapatan masuk dari orderan ' . $invoice
            ]);

            $komisi = HistorySaldo::create([
                'user_id' => $order->user_id,
                'user_reff_id' => $order->user_reff,
                'order_id' => $order->id,
                'komisi' => $nominal
            ]);
        }

        $order['order_detail'] = OrderDetail::where('order_id', $order->id)->get();
        foreach ($order['order_detail'] as $item) {
            $history =  HistoryProductVendor::create([
                'user_id' => $user->id,
                'user_reff' => $item->user_reff,
                'order_id' => $item->order_id,
                'product_id' => $item->product_id,
                'product_detail_id' => $item->product_detail_id,
                'name_product' => $item->name_product,
                'price' => $item->price,
                'qty' => $item->qty,
                'status' => 1,
            ]);
        }

        return redirect()->back()->with(['success' => 'Status Berhasil Diperbarui']);
    }

    // PESANAN TEST //
    public function pesananindex()
    {
        $datas = Order::where(['user_reff' => NULL])->orderBy('created_at', 'ASC')->get();

        return view('admin.pesanan.test.pesananindex', compact('datas'));
    }

    public function printResi_multi_sudah_bayar(Request $request)
    {
        // return $request;
        $items = [];
        if ($request->idorder) {
            foreach ($request->idorder as $key => $val) {
                if (in_array($val, $request->idorder)) {
                    $service = Order::where(['id' => $val])->first();
                    $order = Order::where(['id' => $val])->first();
                    $order->update([
                        'admin_courrier' => $request->admin_courrier[$key],
                        'admin_service' => $request->admin_service[$key],
                        'admin_service_code' => $request->admin_service_code[$key],
                        'admin_ongkos_kirim' => $request->admin_ongkos_kirim[$key],
                    ]);
                    $exp = \Carbon\Carbon::parse($service->updated_at)->addDays(4)->format('d M Y');
                    $service['detail'] = OrderDetail::where('order_id', $val)->get();
                    $service['default_store'] = MasterAdress::where(['id_store' => 1])->first();
                    $itemnya = [];
                    foreach ($service['detail'] as $item) {
                        $name_product =  substr($item->name_product, 0, 30);
                        $item_category = substr($item->category, 0, 19);
                        $itemnya[] = ([
                            'item_name' => '' . $name_product . '',
                            'item_desc' => '' . $name_product . '',
                            'item_category' => '' . $item_category . '',
                            'item_quantity' =>  $item->qty,
                            'declared_value' => $item->price,
                            'weight' => (float)$item->berat
                        ]);
                    }
                    $service['items'] = json_encode($itemnya);
                    $resi = json_decode($this->create_resi_service_que($service), true);
                    
                    // $create_resi = $resi['data']['resi'];
                    // $service['resi'] = $create_resi;

                    // $resi_get[] = $create_resi;
                    $items[] = $service;
                }
            }
            // return $items;
            return redirect()->back();
            // return view('admin.pesanan.print.print_multiple_sudah_bayar', compact('items','exp'));        
        } else {
            return redirect()->back()->with('error', 'Belum ada yg di pilih !');
        }
    }

    //siap di cetak
    public function siap_dicetak(Request $request)
    {
        $items = [];
        if ($request->ready_to_print) {
            foreach ($request->ready_to_print as $key => $val) {
                if (in_array($val, $request->ready_to_print)) {
                    $service = Order::where(['id' => $val])->first();
                    $order = Order::where(['id' => $val])->first();
                    $order->update([
                        'ready_to_print' => 0,
                    ]);
                    $exp = \Carbon\Carbon::parse($service->updated_at)->addDays(4)->format('d M Y');
                    $service['detail'] = OrderDetail::where('order_id', $val)->get();
                    $service['default_store'] = MasterAdress::where(['id_store' => 1])->first();
                    $itemnya = [];
                    foreach ($service['detail'] as $item) {
                        $name_product =  substr($item->name_product, 0, 30);
                        $item_category = substr($item->category, 0, 19);
                        $itemnya[] = ([
                            'item_name' => '' . $name_product . '',
                            'item_desc' => '' . $name_product . '',
                            'item_category' => '' . $item_category . '',
                            'item_quantity' =>  $item->qty,
                            'declared_value' => $item->price,
                            'weight' => (float)$item->berat
                        ]);
                    }

                    $items[] = $service;
                }
            }
            // return $items;
            return view('admin.pesanan.print.print_multiple_sudah_bayar', compact('items', 'exp'));
        } else {
            return redirect()->back()->with('error', 'Belum ada yg di pilih !');
        }
    }

    public function cetakresi($invoice, Request $request)
    {
        $order = Order::where(['invoice' => $invoice])->with('store')->first();
        $exp = \Carbon\Carbon::parse($order->updated_at)->addDays(4)->format('d M Y');
        $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
            'product_detail.varian_option.varian'
        ])->get();

        $varian = ProductVarian::with('sub_variasi')->get();
        $product = ProductVarianOption::with('parent_option')->get();

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->parent_option)) {
                $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
            }

            return $details;
        });
        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
            }

            return $details;
        });

        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $address = MasterAdress::where(['id_user' => auth()->user()->id])->first();
        $tracking_num = Order::where(['invoice' => $invoice])->first();
        $tracking_num->update([
            //post_check_admin
            'admin_courrier' => $request->admin_courrier,
            'admin_service' => $request->admin_service,
            'admin_service_code' => $request->admin_service_code,
            'admin_ongkos_kirim' => $request->admin_ongkos_kirim,
            //end_post_check_admin
        ]);
        $service = Order::where(['invoice' => $invoice])->first();
        $service['order_detail'] = OrderDetail::where('order_id', $tracking_num->id)->get();
        $itemnya = [];
        $sum_declare_value = 0;
        foreach ($service['order_detail'] as $item) {
            $name_product =  substr($item->name_product, 0, 30);
            $item_category = substr($item->category, 0, 19);
            $sum_declare_value += $item->price;
            $itemnya[] = ([
                'item_name' => '' . $name_product . '',
                'item_desc' => '' . $name_product . '',
                'item_category' => '' . $item_category . '',
                'item_quantity' =>  $item->qty,
                'declared_value' => $item->price,
                'weight' => (float)$item->berat
            ]);
        }
        $service['sum_declare'] = $sum_declare_value;
        $service['items'] = json_encode($itemnya);
        // return  $service['items'];

        $service['default_store'] = $default_store = MasterAdress::where(['id_store' => 1])->first();
        // return $service['default_store'];
        // $resi = json_decode($this->create_resi_service($service), true);
        // $create_resi = $resi['data']['resi'];
        // kondisi tracking_num ketika status !=1 //
        if ($tracking_num->status == 1 || $tracking_num->tracking_number != NULL) {
            $resi = json_decode($this->create_resi_service($service), true);
            // return $resi['data']['status'];
            // dd($resi);
            if ($resi['data']['status'] == 'Error' && $resi['data']['reason'] == 'JOB Number Already Exist') {
                return redirect()->back()->with('error', 'ada kesalahan! job telah terdaftar');
            } elseif ($resi['data']['status'] == 'False' && $resi['data']['reason'] == 'Error') {
                return redirect()->back()->with('error', 'ada kesalahan! Status False, reason Error');
            } else {
                $tracking_num->update([

                    // //post_check_admin
                    'admin_courrier' => $request->admin_courrier,
                    'admin_service' => $request->admin_service,
                    'admin_service_code' => $request->admin_service_code,
                    'admin_ongkos_kirim' => $request->admin_ongkos_kirim,
                    //end_post_check_admin
    
                    'status' => 3,
                    'tracking_number' => $resi['data']['no_tiket'], //'JKT-' . Str::random(8)
                    'kode_boking' => '' . ENV('KURIR_CODE') . '' . $service->id . '',
                    'ready_to_print' => true
                    // 'tracking_number' => 'JKT-' . Str::random(8)
                ]);
                return redirect()->back()->with('success', 'Berhasil');
            }
            
            // $create_resi = $resi['data']['resi'];

            // if($tracking_num->courrier == 'jne'){
            //     $dest_code = $resi['data']['dest_code'];
            //     $sorting_code = null;
            // }else if ($tracking_num->courrier == 'id'){
            //     $sorting_code = $resi['data']['sortingCode'];
            //     $dest_code = null;
            // }else{
            //     $dest_code = null;
            //     $sorting_code = null;
            // }
            
        
            // $tracking_num->update([

            //     // //post_check_admin
            //     'admin_courrier' => $request->admin_courrier,
            //     'admin_service' => $request->admin_service,
            //     'admin_service_code' => $request->admin_service_code,
            //     'admin_ongkos_kirim' => $request->admin_ongkos_kirim,
            //     //end_post_check_admin

            //     'status' => 3,
            //     'tracking_number' => $create_resi, //'JKT-' . Str::random(8)
            //     'kode_boking' => '' . ENV('KURIR_CODE') . '' . $service->id . '',
            //     'ready_to_print' => true,
            //     'dest_code' => $dest_code,
            //     'sorting_code' => $sorting_code
            //     // 'tracking_number' => 'JKT-' . Str::random(8)
            // ]);
        }

        // return $tracking_num;
        $detail = OrderDetail::where(['order_id' => $order->id])->get();
        foreach ($detail as $det) {
            $det['prod_detail'] = ProductDetail::where(['id' => $det->product_detail_id])->first();
            $det['prod_detail']->update([
                'qty' => $det['prod_detail']['qty'] - $det->qty,
            ]);
        }
        // return $order;
        // return $tracking_num;
        return redirect()->back();
    }

    public function create_resi_service($service)
    {
        // dd($service);
        // return $service;
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => env('KURIR_URL') . '/api/createJOB',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "kurir": "' . strtolower($service->admin_courrier) . '",
            "order_id": "' . substr($service->invoice,0,15) . '",
            "service": "' . $service->service_code . '",
            "shipper": {
                "postal_code_origin": "' . $service->default_store->postal_code . '",
                "sub_district_origin":"' . $service->default_store->district . '",
                "city_origin":"' . $service->default_store->city . '",
                "alamat": "' . $service->default_store->address . '",
                "name": "' . $service->default_store->name . '",
                "phone": "' . $service->default_store->phone . '",
                "email": "' . Auth::user()->email . '",
                "postcode": "' . $service->default_store->postal_code . '"
            },
            "receiver": {
                "postal_code_tujuan": "' . $service->postal_code . '",
                "sub_district_tujuan":"' . $service->city . '",
                "city_tujuan":"' . $service->state . '",
                "alamat": "' . $service->address . '",
                "name": "' . $service->name . '",
                "phone": "' . $service->phone . '",
                "email": "' . $service->email . '",
                "postcode": "' . $service->postal_code . '"
            },
            "items": ' . $service['items'] . ',
            "berat_total": '. $service->berat_pengiriman .',
            "declared_value": '.$service->sum_declare.'
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        // dd($response);
        return $response;
    }


    // regist-awb
    public function regist_awb($invoice)
    {
        $a = Order::where('invoice', '=', $invoice)->first();
        $a->update([
            'register_awb' => 1,
            'status' => 4
        ]);

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => env('KURIR_URL') . '/api/register_awb',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "order_id": "'. $a->invoice .'",
            "awb_number":"'. $a->tracking_number .'",
            "kurir" : "'.strtolower($a->admin_courrier).'"
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // return $response;
        return redirect()->back()->with(['success' => 'Register Awb berhasil']);

    }

    public function api_tarif_jne()
    {
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://apiv2.jne.co.id:10102/tracing/api/pricedev',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'username=SINTESA&api_key=ac7cd9010cd5b441d132d023498885de&from=CGK10000&thru=BDO10000&weight=1',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return $response;
        
    }
    public function register_awb_masal(Request $request)
    {
        $invoice = $request->invoice;
        // dd($invoice);

        foreach ($invoice as $i ) {

            // get order
            $order = Order::where('invoice', $i)
            ->select('kode_boking')->first();

            // post servis kurir
            $curl = curl_init();
    
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://kurir.abbabill-dev.site/api/register_awb',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "order_id":"'.$order->kode_boking.'",
                "awb_number":"'.$order->kode_boking.'",
                "kurir" : "jne"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            ));
    
            $response = curl_exec($curl);
    
            curl_close($curl);
            $data = json_decode($response, true);

            // update register awb
            if ($data['responseCode'] == 200) {
                Order::where('invoice', $i)->update(
                    ['register_awb' => 1]
                );
            }
            
        }

        return redirect()->back();   
    }

}
