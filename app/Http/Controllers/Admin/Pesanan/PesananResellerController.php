<?php

namespace App\Http\Controllers\Admin\Pesanan;

use App\Http\Controllers\Controller;
use App\Model\Order;
use App\User;
use Illuminate\Http\Request;
use App\Model\UserReferall;
use App\Model\OrderDetail;
use App\Model\ConfirmPayment;
use App\Model\HistorySaldo;
use App\Model\ProductVarianOption;
use App\Model\ProductVarian;
use App\Model\MasterStore;
use App\Model\MasterAdress;
use App\Model\ProductDetail;
use App\Model\HistoryPayment;
use App\Model\HistorySaldoReseller;
use App\Model\GroupReseller;
use App\Model\HistoryProductVendor;
use App\Model\Komisi;
use Illuminate\Support\Str;

class PesananResellerController extends Controller
{
    
    // LIST RESELLER //
    public function index()
    {
        $user = UserReferall::where('user_id', auth()->user()->id)->first();
        $user['get'] = UserReferall::where('referral_related', $user->referral_code)->with('user')->get();

        $pesan = $this->pesan;

        return view('admin.pesanan.reseller.list_referall', compact('user', 'pesan'));
    }

    public function list_pesanan_reseller($referral_code)
    {
        $user = UserReferall::where('referral_code', $referral_code)->first();
        $pesanan = Order::where('user_reff', $user->user_referral_id)->where('isMidtrans', '=', 0)->get();
        $pesanan_midtrans = Order::where('user_reff', $user->user_referral_id)->where('isMidtrans', '=', 1)->get();

        $pesan = $this->pesan;

        return view('admin.pesanan.reseller.list_pesanan_reseller', compact('user', 'pesanan', 'pesan', 'pesanan_midtrans'));
    }

    public function detail_pesanan_reseller($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order['product'] = OrderDetail::where(['order_id' => $order->id])->get();
        $order['confirm_payment'] = ConfirmPayment::where(['order_id' => $order->id])->latest()->first();
        $order['komisi'] = HistorySaldo::where(['order_id' => $order->id])->first();

        // track resi
        $user = Order::where('invoice', $invoice)->first();
        $default_store = MasterAdress::where('id_store', 1)->first();
        $awb = json_encode($user->tracking_number);
        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => env('KURIR_URL') . '/api/tracking', //'https://kurir.abbabill-dev.site/api/tracking',
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => '',
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => true,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => 'POST',
        //     CURLOPT_POSTFIELDS => '{
        //             "kurir":"anter_aja",
        //             "waybill_no":' . $awb . '
        //         }',
        //     CURLOPT_HTTPHEADER => array(
        //         'Content-Type: application/json'
        //     ),
        // ));

        // $response = curl_exec($curl);

        // curl_close($curl);
        // $response = json_decode($response, true)['data'];
        // return $response['data'];
        // return $response;

        $pesan = $this->pesan;
        return view('admin.pesanan.reseller.pesanan_detaill_reseller', compact('order', 'pesan'));
    }

    ///YANG DIPAKE///
    public function pesanan_reseller(request $request)
    {
        $order = Order::where('user_reff', '!=', null)->orderBy('created_at', 'DESC')->get();
        // return $order;
        if ($request->ajax()) {
            if (!empty($request->slug)) {
                $slug = $request->slug;
                $order = Order::where('courrier' ,'=', $slug)->where('user_reff', '!=', null)->orderBy('created_at', 'DESC')->get();
                $order->map(function ($reseller) {
                    $reseller['reseller'] = UserReferall::where('user_referral_id', $reseller->user_reff)->with('user')->first();
                    $reseller['komisi'] = HistorySaldo::where(['order_id' => $reseller->id])->first();
                });
                return $order;
            } else if ($request->slug == '') {
                $order = Order::where('user_reff', '!=', null)->orderBy('created_at', 'DESC')->get();
                return $order;
            }
        }

        $order = Order::whereNotNull('user_reff')->orderBy('id', 'DESC')->get();
        $belumbayar = [];
        $sudahbayar = [];
        $gagal = [];
        $dikemas = [];
        $dikirim = [];
        $selesai = [];
        $cekpembayaran = [];
        $uploadulang = [];
        $batal = [];
        $siapcetak = [];

        foreach ($order as $key => $value) {
            if ($value->siapcetak == true) {
                $siapcetak[] = $value;
            }
            if ($value->status == 0) {
                $belumbayar[] = $value;
            }
            if ($value->status == 1) {
                $sudahbayar[] = $value;
            }
            if ($value->status == 2) {
                $gagal[] = $value;
            }
            if ($value->status == 3) {
                $dikemas[] = $value;
            }
            if ($value->status == 4) {
                $dikirim[] = $value;
            }
            if ($value->status == 5) {
                $selesai[] = $value;
            }
            if ($value->status == 6) {
                $batal[] = $value;
            }
            if ($value->status == 7) {
                $cekpembayaran[] = $value;
            }
            if ($value->status == 8) {
                $uploadulang[] = $value;
            }
        }
        $request_pembatalan = Order::where(['request_cancel' => 1])->get();

        // hitung di tab datatable
        $hitung_order = $order->count();
        $hitung_belum_bayar = count($belumbayar);
        $hitung_udah_bayar = count($sudahbayar);
        $hitung_cek_pembayaran = count($cekpembayaran);
        $hitung_upload_ulang = count($uploadulang);
        $hitung_dikemas = count($dikemas);
        $hitung_dikirim = count($dikirim);
        $hitung_selesai = count($selesai);
        $hitung_gagal = count($gagal);
        $hitung_batal = count($batal);
        $hitungcetak = count($siapcetak);

        foreach ($order as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($sudahbayar as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($cekpembayaran as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($uploadulang as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($belumbayar as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($gagal as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($batal as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($dikemas as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($dikirim as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($selesai as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        foreach ($siapcetak as $pesan) {
            $pesan['reseller'] = UserReferall::where('user_referral_id', $pesan->user_reff)->with('user')->first();
            $pesan['komisi'] = HistorySaldo::where(['order_id' => $pesan->id])->first();
        }
        $pesan = $this->pesan;
        $no = 1;
        // return $sudahbayar;
        return view('admin.pesanan.reseller.list_pesanan', compact('order', 'pesan', 'no', 'sudahbayar', 'belumbayar', 'gagal', 'dikemas', 'dikirim', 'selesai', 'batal', 'cekpembayaran', 'uploadulang', 'hitung_order', 'hitung_belum_bayar', 'hitung_udah_bayar', 'hitung_dikemas', 'hitung_dikirim', 'hitung_selesai', 'hitung_gagal', 'hitung_batal', 'hitung_cek_pembayaran', 'hitung_upload_ulang', 'request_pembatalan','siapcetak','hitungcetak'));
    }

    // function acc bukti pembayaran di status 7
    public function acc_bukti($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order->update([
            'status' => 1
        ]);

        return redirect()->back()->with(['success', 'Pembayaran di terima']);
    }

    // function reupload bukti pembayaran
    public function reupload($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->first();
        $order->update([
            'status' => 8
        ]);

        return redirect()->back()->with(['success', 'Lakukan upload ulang bukti pembayaran untuk pembeli']);
    }

    public function batalkan_order(Request $request)
    {
        $message = Order::where('id', $request->id)->first();
        $message->update([
            'reply_message' =>  $request->reply_message,
            'status' => 6,
        ]);
        // return $message;

        return redirect()->back()->with(['success' => 'Pesanan Berhasil Dibatalkan']);
    }

    //FUNCTION UNTUK REQUEST PEMBATALAN ORDER //
    public function tolakPembatalanOrder(Request $request)
    {
        $message = Order::where('id', $request->id)->first();
        // return $request;
        $message->update([
            'reply_message' =>  $request->reply_message,
            'request_cancel' => 3,
        ]);
        return redirect()->back()->with(['success' => 'Request Pembatalan Berhasil Ditolak']);
    }

    public function accPembatalanOrder(Request $request)
    {
        $message = Order::where('id', $request->id)->first();
        // return $request;
        $message->update([
            'request_cancel' => 3,
            'status' => 6,
        ]);
        return redirect()->back()->with(['success' => 'Request Pembatalan Berhasil Diterima']);
    }

    public function invoice($invoice)
    {
        $order = Order::where(['invoice' => $invoice])->with('store')->first();
        $order['detail'] = OrderDetail::where(['order_id' => $order->id])->with([
            'product_detail.varian_option.varian'
        ])->get();


        $product = ProductVarianOption::with('parent_option')->get();
        $varian = ProductVarian::with('sub_variasi')->get();

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->parent_option)) {
                $details['parent_option'] = ProductVarianOption::where('id', $details->product_detail->varian_option->parent_option)->first();
            }

            return $details;
        });

        $order['detail']->map(function ($details) {
            if (!empty($details->product_detail->varian_option->varian->sub_variasi)) {
                $details['sub_variasi'] = ProductVarian::where('id', $details->product_detail->varian_option->varian->sub_variasi)->first();
            }

            return $details;
        });

        $store = MasterStore::where(['user_id' => auth()->user()->id])->first();
        $address = MasterAdress::where(['id_user' => auth()->user()->id])->first();

        $pesan = $this->pesan;

        return view('admin.pesanan.reseller.invoice', compact('order', 'product', 'varian', 'pesan', 'store', 'address'));
    }

    public function mailview()
    {
        return view('email.mailtest');
    }
}
