<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SaldoKreditVendor;
use App\Model\Transaction_Topup;

class MidtransController extends Controller
{
    public function Notification(Request $request)
    {

        if ($request->transaction_status == 'pending') {
            $status = 0;
        } else if ($request->transaction_status == 'settlement') {
            $status = 1;
        } else if ($request->transaction_status == 'expire') {
            $status = 2;
        } else {
            $status = 2;
        }

        $id_topup = SaldoKreditVendor::where('invoice_topup', $request->invoice)->first();
        if(!empty($id_topup)){
            $id_topup->update([
                'status' => $status
            ]);
        }

        Transaction_Topup::where('invoice_topup', $request->invoice)
            ->update([
                'payment_type' => $request->payment_type,
                'gross_amount' =>  $request->nominal,
                'transaction_time' => $request->transaction_time,
                'transaction_status' => $request->transaction_status,
            ]);
    }
}
