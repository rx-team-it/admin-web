<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\MasterStore;
use App\Model\ProductImage;
use App\Model\ProductVendor;
use App\Model\ProductVarian;
use App\Model\ProductVarianOption;
use App\Model\ProductDetail;
use App\Model\Cart;
use App\Model\ProductFlashSale;
use DB;

class ProdukController extends Controller
{
    public function produk(){

        $p = Product::orderBy('created_at', 'DESC')->get();
        
        return response()->json(['status' => 'success', 'data' => $p]);
    }

    public function detil($product_id){
        $product = ProductImage::where(['product_id' => $product_id])->get();

        return response()->json(['status' => 'success', 'data' => $product]);
    }
}
