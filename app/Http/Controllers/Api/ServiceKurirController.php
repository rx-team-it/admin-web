<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\MasterKurir;
use App\Model\MasterKurirService;
use App\Model\MasterAdress;
use App\Model\MasterStore;
use App\Model\ProvinceData;
use DB;
use Illuminate\Http\Request;


class ServiceKurirController extends Controller
{
    public function print_multi_resi(Request $request){

            try {
                    $order = Order::where('id', $request->id)->first();
                    $order->update([
                        'status' => $request->status,
                        'tracking_number' => $request->tracking_number,
                        'ready_to_print' => true,
                        'kode_boking' => '' . ENV('KURIR_CODE') . '' . $request->id . '',
                        'dest_code' => $request->dest_code,
                        'sorting_code' => $request->sorting_code,
                    ]);
                    return response()->json([
                        'status' => '200',
                        'message' => 'success',
                        'awb' => $order->tracking_number
                    ]);                                 
            } catch (\Exception $e) {
                                      
                    return response()->json([
                        'status' => '400',
                        'awb' => 'gagal buat resi, silahkan create ulang',
                        'error' => $e->getMessage()
                    ]);
            }        
    }

    //check service api kurir
    public function check_service_admin($postal_code_origin, $sub_district_origin, $city_origin, $postal_code_destination, $sub_district_destination, $city_destination, $weight, $group_service)
    {
        $kurir = MasterKurir::select('nama')->where('status', 1)->get();
        $fix_kurir = [];
        foreach($kurir as $property => $value){
            $fix_kurir[] = $value['nama'];
        }
        $fix_kurir = json_encode($fix_kurir);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://27.112.78.69:1113/kurir/checkservice',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "postal_code_tujuan":"'.$postal_code_destination.'",
            "sub_district_tujuan":"'.$sub_district_destination.'",
            "city_tujuan":"'.$city_destination.'",
            "postal_code_origin":"'.$postal_code_origin.'",
            "sub_district_origin":"'.$sub_district_origin.'",
            "city_origin":"'.$city_origin.'",
            "berat":"'.$weight.'",
            "kurir":[
                "jne"
            ]
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        // dd($response);
        return json_decode($response, true);
        // return response()->json(['status' => 'success', 'data' =>  $response]);
    }


    // buat supersystem -------------------------------------------------------------------------------------
    // get 
    public function getKuirService($vendor_code)
    {
        if($vendor_code != env("VENDOR_CODE",'AB388')) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }

        $kurir = MasterKurir::all();
        $kurir->map(function($k){
            $k['service'] = MasterKurirService::where('kurir_id', $k->id)->get();
            return $k;
            // return $k;
        });

        return response([
            'status' => 200,
            'data' => $kurir
        ]);
    }

    // update
    public function updateDiskonService(Request $request)
    {
        $ids =  $request[0];
        $diskons = $request[1];

        for ($i=0; $i < count($ids); $i++) { 
            $id = $ids[$i];
            $diskon = $diskons[$i];
            MasterKurirService::where('id', $id)->update(['diskon' => $diskon]);
        }

        return response([
            'status' => 200,
        ]);
    }

    //jne api tarif
    public function api_tarif_jne($from, $thru, $weight)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://apiv2.jne.co.id:10101/tracing/api/pricedev',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'username=SINTESA&api_key=ac7cd9010cd5b441d132d023498885de&from='.$from.'&thru='.$thru.'&weight='.$weight.'',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $response = json_decode($response, true);
        return response()->json([
            'status'=>9999,'data' => $response['price']
            ]);
    }

    public function get_thru($kode_pos, $kelurahan){
        $thru = DB::select('
            SELECT DEST FROM service_kurir.master_jne where kode_pos = ? AND kelurahan like ?
        ', [$kode_pos, $kelurahan])[0];

        return response()->json([
            'status' => 200,
            'data' => $thru
        ]);
    }

    public function create_job_masal($id, $kurir, $service_code_, $ongkir, $code_service)
    {
        $service = Order::where(['id' => $id])->first();
        $berat_total = $service->berat_pengiriman;

        //data penerima
        $nama_penerima =  $service->name;
        $alamat_penerima =  $service->address;
        // $alamat_penerima = substr($service ,0,20);
        $kota_penerima = $service->state;
        $kodepos_penerima = $service->postal_code;
        $provinsi_penerima = $service->province;
        $no_penerima = $service->phone;
        $kelurahan = $service->urban;
        $kecamatan_penerima = $service->city;
        $email_penerima = $service->email;
        $order_id = substr($service->invoice ,0,15);
        $service['order_detail'] = OrderDetail::where('order_id', $service->id)->get();
        $itemnya = [];
        $declared_value = 0;
        foreach ($service['order_detail'] as $item) {
            $name_product =  substr($item->name_product, 0, 30);
            $item_category = substr($item->category, 0, 19);
            $declared_value += $item->price;

            $itemnya[] = ([
                'item_name' => '' . $name_product . '',
                'item_desc' => '' . $name_product . '',
                'item_category' => '' . $item_category . '',
                'item_quantity' =>  $item->qty,
                'declared_value' => $item->price,
                'weight' => (float)$item->berat
            ]);
        }
        $service['items'] = json_encode($itemnya);

        $get_pengirim = MasterAdress::where('id_store', 1)->first();
        $get_pengirim['prov'] =  ProvinceData::where('province_code', $get_pengirim->province)->select('province_name')->first();
        //data pengirim
        $nama_pengirim =  $get_pengirim->name;
        $alamat_pengirim =  $get_pengirim->address;
        // $alamat_pengirim = substr($alamat_pengirim ,0,20);
        $kota_pengirim = $get_pengirim->city;
        $kodepos_pengirim = $get_pengirim->postal_code;
        $provinsi_pengirim = $get_pengirim['prov']['province_name'];
        $no_pengirim = $get_pengirim->phone;
        $kecamatan_pengirim = $get_pengirim->district;
        $get_pengirim_user = MasterStore::where('id', 1)->first();
        $email_pengirim = $get_pengirim_user->contact_email;

        $service_code = $service->service_code;

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://kurir.abbabill.com/api/createJOB',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "kurir": "'.$kurir.'",
            "order_id": "'.$order_id.'",
            "service": "'.$service_code.'",
            "shipper": {
                "postal_code_origin": "'.$kodepos_pengirim.'",
                "sub_district_origin":"'.$kecamatan_pengirim.'",
                "city_origin":"'.$kota_pengirim.'",
                "alamat": "'.$alamat_pengirim.'",
                "name": "'.$nama_pengirim.'",
                "phone": "'.$no_pengirim.'",
                "email": "'.$email_pengirim.'",
                "postcode": "'.$kodepos_pengirim.'"
            },
            "receiver": {
                "postal_code_tujuan": "'.$kodepos_penerima.'",
                "sub_district_tujuan":"'.$kecamatan_penerima.'",
                "city_tujuan":"'.$kota_penerima.'",
                "alamat": "'.$alamat_penerima.'",
                "name": "'.$nama_penerima.'",
                "phone": "'.$no_penerima.'",
                "email": "'.$email_penerima.'",
                "postcode": "'.$kodepos_penerima.'"
            },
            "items":' . $service['items'] . ',
            "berat_total":'.$berat_total.',
            "declared_value": '.$declared_value.'
        }',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        $get_or = Order::where('id', $id)->first();
        if($response['data']['status'] == "Sukses")
        {

            $get_or->update([
                'tracking_number' => $response['data']['no_tiket'],
                'status' => 3,
                'kode_boking' => $response['data']['no_tiket'],
                'admin_service' => $service_code_,
                'admin_ongkos_kirim' => $ongkir,
                'admin_service_code' => $code_service,
                'admin_courrier' => "JNE",
            ]);
            return response()->json([
                'status'=>200,
                'data' => $response,
                'order' => $get_or,
            ]);
        }else{
            return response()->json([
                'status'=> 400,
                'data' => $response,
            ]);
        }
    }

    public function create_job_jne_masal($id, $service_code_, $ongkir, $code_service)
    {   

        $get_order = Order::where('id', $id)->first();
        $get_order['order_detail'] = OrderDetail::where('order_id',$get_order->id)->get();
        $sum_qty = 0;
        foreach($get_order['order_detail'] as $od)
        {
            $sum_qty += $od->qty;
        }
        
        $get_pengirim = MasterAdress::where('id_store', 1)->first();
        $get_pengirim['prov'] =  ProvinceData::where('province_code', $get_pengirim->province)->select('province_name')->first();

        //data pengirim
        $nama_pengirim=  $get_pengirim->name;
        $alamat_pengirim =  $get_pengirim->address;
        $alamat_pengirim = substr($alamat_pengirim ,0,20);
        $kota_pengirim = $get_pengirim->city;
        $kodepos_pengirim = $get_pengirim->postal_code;
        $provinsi_pengirim = $get_pengirim['prov']['province_name'];
        $no_pengirim = $get_pengirim->phone;

        //data penerima
        $nama_penerima=  $get_order->name;
        $alamat_penerima =  $get_order->address;
        $alamat_penerima = substr($alamat_penerima ,0,20);
        $kota_penerima = $get_order->state;
        $kodepos_penerima = $get_order->postal_code;
        $provinsi_penerima = $get_order->province;
        $no_penerima = $get_order->phone;
        $kelurahan = $get_order->urban;

        //origin
        $origin_desc = $get_pengirim['prov']['province_name'];
        $origin_code = "CGK10000";

        //dest
        $destination_desc = $get_order->province;
        $thru = DB::select('
            SELECT DEST FROM service_kurir.master_jne where kode_pos = ? AND kelurahan like ?
        ', [$kodepos_penerima, $kelurahan])[0];
        
        $destination_code = $thru->DEST;

        //shipping
        $service_code =  $service_code_;
        $berat = $get_order->berat_pengiriman / 1000;
        if($berat <= 1){
            $berat = 1;
        }
        $goods_amount = $get_order->total;
        $weight = $berat;
        $qty =  $sum_qty;
        $delivery_price = $ongkir;
        $book_code =  substr($get_order->invoice,0,15);
        

        $from_field = 'username=SINTESA&api_key=ac7cd9010cd5b441d132d023498885de&SHIPPER_NAME='.$nama_pengirim.'&SHIPPER_ADDR1='.$alamat_pengirim.'&SHIPPER_ADDR2='.$alamat_pengirim.'&SHIPPER_ADDR3='.$alamat_pengirim.'&SHIPPER_CITY='.$kota_pengirim.'&SHIPPER_ZIP='.$kodepos_pengirim.'&SHIPPER_REGION='.$provinsi_pengirim.'&SHIPPER_COUNTRY=INDONESIA&SHIPPER_CONTACT='.$nama_pengirim.'&SHIPPER_PHONE='.$no_pengirim.'&RECEIVER_NAME='.$nama_penerima.'&RECEIVER_ADDR1='.$alamat_penerima.'&RECEIVER_ADDR2='.$alamat_penerima.'&RECEIVER_ADDR3='.$alamat_penerima.'&RECEIVER_CITY='.$kota_penerima.'&RECEIVER_ZIP='.$kodepos_penerima.'&RECEIVER_REGION='.$provinsi_penerima.'&RECEIVER_COUNTRY=INDONESIA&RECEIVER_CONTACT='.$nama_penerima.'&RECEIVER_PHONE='.$no_penerima.'&ORIGIN_DESC='.$origin_desc.'&ORIGIN_CODE='.$origin_code.'&DESTINATION_DESC='.$destination_desc.'&DESTINATION_CODE='.$destination_code.'&SERVICE_CODE='.$service_code.'&WEIGHT='.$weight.'&QTY='.$qty.'&GOODS_DESC=test&GOODS_AMOUNT='.$goods_amount.'&INSURANCE_FLAG=0&INSURANCE_AMOUNT=0&DELIVERY_PRICE='.$delivery_price.'&BOOK_CODE=ITSINTESA12345&AWB_TYPE=FREE';

        dd($from_field);
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://apiv2.jne.co.id:10101/job/direct',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $from_field,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded',
            'User-Agent: (Filled with framework request, Ex: Java-Request)'
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response, true);
        return $response;
        $get_or = Order::where('id', $id)->first();
        if($response['status'] == "Sukses")
        {

            $get_or->update([
                'tracking_number' => $response['no_tiket'],
                'status' => 3,
                'kode_boking' => $response['no_tiket'],
                'admin_service' => $service_code,
                'admin_ongkos_kirim' => $ongkir,
                'admin_service_code' => $code_service,
                'admin_courrier' => "JNE",
            ]);
        }
        return response()->json([
            'status'=>200,
            'data' => $response,
            'order' => $get_or,
        ]);

    }

    public function create_job_masal_post(Request $request)
    {
    
    //    dd($request->datas);
        foreach($request->datas as $data)
        {
            
            $service = Order::where(['id' => $data['id']])->first();
        
            $berat_total = $service->berat_pengiriman;
    
            //data penerima
            $nama_penerima =  $service->name;
            $alamat_penerima =  $service->address;
            $kota_penerima = $service->state;
            $kodepos_penerima = $service->postal_code;
            $provinsi_penerima = $service->province;
            $no_penerima = $service->phone;
            $kelurahan = $service->urban;
            $kecamatan_penerima = $service->city;
            $email_penerima = $service->email;
            $order_id = substr($service->invoice ,0,15);
            $service['order_detail'] = OrderDetail::where('order_id', $service->id)->get();
            $itemnya = [];
            $declared_value = 0;
            foreach ($service['order_detail'] as $item) {
                $name_product =  substr($item->name_product, 0, 30);
                $item_category = substr($item->category, 0, 19);
                $declared_value += $item->price;
    
                $itemnya[] = ([
                    'item_name' => '' . $name_product . '',
                    'item_desc' => '' . $name_product . '',
                    'item_category' => '' . $item_category . '',
                    'item_quantity' =>  $item->qty,
                    'declared_value' => $item->price,
                    'weight' => (float)$item->berat
                ]);
            }
            $service['items'] = json_encode($itemnya);
    
            $get_pengirim = MasterAdress::where('id_store', 1)->first();
            $get_pengirim['prov'] =  ProvinceData::where('province_code', $get_pengirim->province)->select('province_name')->first();
            //data pengirim
            $nama_pengirim =  $get_pengirim->name;
            $alamat_pengirim =  $get_pengirim->address;
            $kota_pengirim = $get_pengirim->city;
            $kodepos_pengirim = $get_pengirim->postal_code;
            $provinsi_pengirim = $get_pengirim['prov']['province_name'];
            $no_pengirim = $get_pengirim->phone;
            $kecamatan_pengirim = $get_pengirim->district;
            $get_pengirim_user = MasterStore::where('id', 1)->first();
            $email_pengirim = $get_pengirim_user->contact_email;
            $service_code = $service->service_code;
    
            $curl = curl_init();
    
            curl_setopt_array($curl, array(
            CURLOPT_URL => env('KURIR_URL') . '/api/createJOB',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "kurir": "'.strtolower($data['kurir']).'",
                "order_id": "'.$order_id.'",
                "service": "'.$service_code.'",
                "shipper": {
                    "postal_code_origin": "'.$kodepos_pengirim.'",
                    "sub_district_origin":"'.$kecamatan_pengirim.'",
                    "city_origin":"'.$kota_pengirim.'",
                    "alamat": "'.$alamat_pengirim.'",
                    "name": "'.$nama_pengirim.'",
                    "phone": "'.$no_pengirim.'",
                    "email": "'.$email_pengirim.'",
                    "postcode": "'.$kodepos_pengirim.'"
                },
                "receiver": {
                    "postal_code_tujuan": "'.$kodepos_penerima.'",
                    "sub_district_tujuan":"'.$kecamatan_penerima.'",
                    "city_tujuan":"'.$kota_penerima.'",
                    "alamat": "'.$alamat_penerima.'",
                    "name": "'.$nama_penerima.'",
                    "phone": "'.$no_penerima.'",
                    "email": "'.$email_penerima.'",
                    "postcode": "'.$kodepos_penerima.'"
                },
                "items":' . $service['items'] . ',
                "berat_total":'.$berat_total.',
                "declared_value": '.$declared_value.'
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            ));
    
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response, true);

            $response_data[] = $response;
            $get_or = Order::where('id', $data['id'])->first();
            if($response['data']['status'] == "Sukses")
            {
    
                $get_or->update([
                    'tracking_number' => $response['data']['no_tiket'],
                    'status' => 3,
                    'kode_boking' => $response['data']['no_tiket'],
                    'admin_service' => $data['service_code_'],
                    'admin_ongkos_kirim' => $data['ongkir'],
                    'admin_service_code' => $data['code_service'],
                    'admin_courrier' => "JNE",
                ]);
            }
        }
        return response()->json([
            'status' => 200,
            'data' => $response_data
        ]);

    }


    public function register_awb_jne_masal()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://apiv2.jne.co.id:10102/cnoteretails/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('username' => 'TESTAPI','api_key' => '25c898a9faea1a100859ecd9ef674548','ORDER_ID' => '','AWB_NUMBER' => ''),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded',
            'User-Agent: '
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    // public function register_awb_masal()
    // {
    //     return 'okk';
    // }
}
