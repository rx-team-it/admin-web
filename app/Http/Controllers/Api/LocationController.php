<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Country;
use App\Model\State;
use App\Model\City;
use App\Model\Ro_Cities;
use App\Model\Ro_Provinces;
use App\Model\Ro_Subdistricts;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    public function listCountry()
    {
        $data = Country::all();
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function getCountry($records)
    {
        // $state = Ro_Provinces::where('province_id', $id)->first();
        // return response()->json(['status' => 'success', 'data' => $state]);
        $records = DB::table('province_data')
            ->select("province_name as label", "province_code as value")
            ->get();
        return response()->json(['status' => 'success', 'data' => $records]);
    }


    public function getState($country_id)
    {
        $datas = DB::table('postal_code_data')
            ->select('city as label', 'city as value')
            ->where('province_code', $country_id)
            ->orderBy('city', 'ASC')
            ->groupBy('city')
            ->get();
        return response()->json(['status' => 'success', 'data' => $datas]);
        // $datas = Ro_Cities::where(['province_id' => $country_id])->get();
        // return response()->json(['status' => 'success', 'data' => $datas]);
    }

    public function getCity($state_id)
    {
        $records = DB::table('postal_code_data')
            ->select('sub_district as label', 'sub_district as value')
            ->where('city', $state_id)
            ->orderBy('sub_district', 'ASC')
            ->groupBy('sub_district')
            ->get();
        return response()->json(['status' => 'success', 'data' => $records]);
        // $state = Ro_Subdistricts::where('city_id', $state_id)->get();
        // return response()->json(['status' => 'success', 'data' => $state]);
    }

    public function getUrban($city_id)
    {
        $datas = DB::table('postal_code_data')
            ->select('urban as label', 'urban as value')
            ->where('sub_district', $city_id)
            ->orderBy('urban', 'ASC')
            ->groupBy('urban')
            ->get();
        return response()->json(['status' => 'success', 'data' => $datas]);
    }

    public function getPostal($urban_id)
    {
        $datas = DB::table('postal_code_data')
            ->select('postal_code as label', 'postal_code as value')
            ->where('urban', $urban_id)
            ->orderBy('postal_code', 'ASC')
            ->get();

        return response()->json(['status' => 'success', 'data' => $datas]);
        // $state = Ro_Cities::where('city_id', $state_id)->get();
    }
}
