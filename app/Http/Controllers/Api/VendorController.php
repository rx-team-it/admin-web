<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\AccessRole;
use App\Model\MasterVendor;
use App\Model\Nav_Child_of_Child;
use App\Model\Nav_Grup;
use App\Model\Nav_Grup_Parent;
use App\Model\Nav_Parent_Child;
use App\Model\Order;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendorController extends Controller
{
    // get list transaction vendor
    public function getVendorTransactions($vendor_code) {

        if($vendor_code != env("VENDOR_CODE",'AB388')) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }
        
        $penjualan = Order::where('status', 5)->with('order_details')->orderBy("id", "DESC")->get();
        // return $penjualan;

        // Menghitung total dari semua invoice
            $jual = DB::table("order")
            ->select("order.id", 
                "order.created_at as created_at",
                "order.name as name_product", 
                "order.name as name_product", 
                "order.name as name_product", 
                "order.invoice as invoice",
                DB::raw("order.total + order.total_komisi_produk as gross"),
                "order.kode_unik as kode_unik",
                DB::raw("order.biaya_layanan + order.kode_unik as biaya_layanan"),
                DB::raw("order.laba_bersih as total"),
                "order.cashback as cashback",
                "order.ongkos_kirim_fix as ongkir",
            )
            ->where('order.status', '=', 5)
            ->orderBy("id", "DESC")
            ->get();

            $order_id = [];
            foreach ($jual as $j) {
                array_push($order_id, $j->id);
            }


            $jualDetail = DB::table("order_detail")
            ->select(
                "order_detail.price as price",
                "order_detail.harga_diskon as harga_awal",
                "order_detail.nilai_komisi as komisi",
                "order_detail.type_komisi as type_komisi",
                "order_detail.diskon as diskon",
                "order_detail.fix_price as fix_price",
                "order_detail.qty as qty"
            )
            ->whereIn('order_detail.order_id', $order_id)
            ->get();

            foreach ($jualDetail as $key => $p) {
                // buat harga
                if ($p->harga_awal != null) {
                    $p->harga_awal = $p->harga_awal; 
                }else{
                    $p->harga_awal = $p->price; 
                }

                // type komisi persen rubah ke satuan nominal
                if($p->type_komisi == 'persentase'){
                    $p->komisi = $p->harga_awal * $p->komisi / 100; 
                }else{
                    $p->komisi = $p->komisi;
                }

            }

        $pendapatan = $jual->sum('total');
        $gross = $jual->sum('gross');
        $ongkir = $jual->sum('ongkir');
        $biaya_layanan = $jual->sum('biaya_layanan');
        $cashback = $jual->sum('cashback');
        $biaya_layanan = $jual->sum('biaya_layanan');
        $qty = $jualDetail->sum('qty');
        $fix_price = $jualDetail->sum('fix_price');
        $komisi = $jualDetail->sum('komisi');
        $harga_awal = $jualDetail->sum('harga_awal');

        // $pesan = $this->pesan;

        return responSuccessGet(compact('pendapatan', 'gross', 'ongkir', 'biaya_layanan', 'qty', 'cashback', 'fix_price', 'komisi', 'harga_awal', 'penjualan'));
    }

    public function getSellerVendor($vendor_code) {
        if($vendor_code != env("VENDOR_CODE",'AB388')) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }
        
        $user = AccessRole::where('role_id', '=', 2)->paginate(10);

        $collection = $user->getCollection()->map(function($user) {
            
            $user['user'] = User::where('id', $user->user_id)->first();

            return $user;
        });
        
        $user->setCollection($collection);

        return responSuccessGet(compact('user'));
    }

    public function getResellerVendor($vendor_code) {
        if($vendor_code != env("VENDOR_CODE",'AB388')) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }
        
        $user = AccessRole::where('role_id', '=', 4)->paginate(10);

        $collection = $user->getCollection()->map(function($user) {
            
            $user['user'] = User::where('id', $user->user_id)->first();

            return $user;
        });
        
        $user->setCollection($collection);

        return responSuccessGet(compact('user'));
    }

    public function getBuyyerVendor($vendor_code) {
        if($vendor_code != env("VENDOR_CODE",'AB388')) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }
        
        $user = AccessRole::where('role_id', '=', 3)->paginate(10);

        $collection = $user->getCollection()->map(function($user) {
            
            $user['user'] = User::where('id', $user->user_id)->first();

            return $user;
        });
        
        $user->setCollection($collection);

        return responSuccessGet(compact('user'));
    }

    public function getStatusMembership($vendor_code)
    {
        if ($vendor_code != env("VENDOR_CODE", "AB388")) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }

        $status = DB::table('status_membership')->first();

        return response([
            'status' => 'ok',
            'data' => $status->jenis
        ]);
    }

    public function updateStatusMembership(Request $request, $vendor_code)
    {

        $request->validate([
            'jenis' => 'required'
        ]);

        if ($vendor_code != env("VENDOR_CODE", "AB388")) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }

        DB::table('status_membership')->update([
            'jenis' => $request->jenis
        ]);


        return response([
            'status' => 'terupdate'
        ]);
    }

    public function navigation($vendor_code)
    {
        if ($vendor_code != env("VENDOR_CODE", "AB388")) {
            return responFailValidator('Error: Vendor code tidak sesuai');
        }
        // get Group
        $menu = Nav_Grup::all();
        // get parent with relation
        $menu->map(function($menus){
            $menus['parent_model'] = Nav_Grup_Parent::where('grup_id', $menus->id)->get();
                // get child_parent with relation
                $menus['parent_model']->map(function($child){
                    $child['child_of_parent'] = Nav_Parent_Child::where('parent_id', $child->id)->get();
                        // get child_of_child with relation
                        $child['child_of_parent']->map(function($coc){
                            $coc['child_of_child'] = Nav_Child_of_Child::where('cop_id', $coc->id)->get();
                            return $coc;
                        });
                        // end child_of_child
                    return $child;
                });
                // end child_parent
            return $menus;
        });

        return response($menu);
    }

    public function updateNavigation(Request $request)
    {

        $menu = Nav_Grup_Parent::whereIn('id', $request)->get();
        
            $menu->map(function($menus){
                $menus->update(['status' => 1]);
                $menus['child_of_parent'] = Nav_Parent_Child::where('parent_id', $menus->id)->get();
                
                $menus['child_of_parent']->map(function($child){
                    $child->update(['status' => 1]);
                    $child['child_of_child'] = Nav_Child_of_Child::where('cop_id', $child->id)->get();
                    $child['child_of_child']->map(function($coc){
                        $coc->update(['status' => 1]);
                    });
                    return $child;
                });
                return $menus;
            });

        //kondisi untuk checkbox yang kosong 
        $menu = Nav_Grup_Parent::whereNotIn('id', $request)->get();
        
            $menu->map(function($menus){
                $menus->update(['status' => 0]);
                $menus['child_of_parent'] = Nav_Parent_Child::where('parent_id', $menus->id)->get();
                
                $menus['child_of_parent']->map(function($child){
                    $child->update(['status' => 0]);
                    $child['child_of_child'] = Nav_Child_of_Child::where('cop_id', $child->id)->get();
                    $child['child_of_child']->map(function($coc){
                        $coc->update(['status' => 0]);
                    });
                    return $child;
                });
                return $menus;
            });
        
        
        
        return 'update';
    }

    public function getTagihanVendor(Request $request)
    {
        $from_date = $request->from_date;
        $to_date = $request->to_date;


        
        
        if (!$from_date) {
            $qris = DB::select("SELECT 
            invoice, bank, kode_unik, biaya_layanan,status, created_at
            FROM
                `order`
            WHERE 
                bank = 'QRIS' 
            AND
                status=5
            ORDER BY created_at ASC
                
            ");

            $kurir = DB::select("SELECT order.created_at, order.invoice, order.service, order.ongkos_kirim_fix, master_kurir_service.diskon
                FROM 
                    `order`
                LEFT JOIN 
                    `master_kurir_service` ON order.service = master_kurir_service.service_code
                WHERE 
                    order.service != 'pickup'
                AND order.status = 5
                ORDER BY created_at ASC
            ");


        } else {

            $qris = DB::select("SELECT 
            invoice, bank, kode_unik, biaya_layanan,status, created_at
            FROM
                `order`
            WHERE 
                created_at >= '$from_date' AND 
                created_at < '$to_date' AND
                bank = 'QRIS'
                AND status = 5
            ");

            $kurir = DB::select("SELECT order.created_at, order.invoice, order.service, order.ongkos_kirim_fix, master_kurir_service.diskon
            FROM 
                `order`
            LEFT JOIN 
                `master_kurir_service` ON order.service = master_kurir_service.service_code
            WHERE 
                order.created_at >= '$from_date' AND order.created_at <= '$to_date'
                AND order.service != 'pickup'
                AND order.status=5
            ");
        }

        $total_kode_unik = 0;
        foreach ($qris as $key) {
            $total_kode_unik += $key->kode_unik ;
        };
        
        $total_biaya_layanan = 0;
        foreach ($qris as $key) {
            $total_biaya_layanan += $key->biaya_layanan ;
        };

        for ($i=0; $i < count($kurir); $i++) { 
            $kurir[$i]->total = $kurir[$i]->ongkos_kirim_fix - $kurir[$i]->ongkos_kirim_fix / 100 * $kurir[$i]->diskon;
        };

        $total_biaya_kurir = 0;
        foreach ($kurir as $key) {
            $total_biaya_kurir += $key->total;
        }

        return response([
            'status' => 200,
            'data' => $qris,
            'kurir' => $kurir,
            'total_kode_unik' => $total_kode_unik,
            'total_biaya_layanan' => $total_biaya_layanan,
            'total_biaya_kurir' => $total_biaya_kurir,
            'from' => $from_date,
            'to' => $to_date
        ]);
    }
}
