<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\MasterKurir;
use DB;

class PengirimanController extends Controller
{
    public function kurir()
    {
        $k = MasterKurir::all();

        return response()->json(['status' => 'berhasil', 'data' => $k]);
    }

    public function update_kurir(request $request)
    {
        $cek = MasterKurir::where('capital', $request->nama)->first();
        if ($cek->status == 1) {
            $cek->update(['status_supersystem' => $request->status, 'status' => 0]);
        } else {
            $cek->update(['status_supersystem' => $request->status]);
        }

        $k = MasterKurir::all();

        return response()->json(['data' => $cek]);
    }
}
