<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Model\TiketingSupport;
use App\Model\MasterVendor;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $data;

    public function __construct()
    {
        $data = [
            // 'request' => $guard,
        ];

        $this->data = $data;

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user('user_id');
            if (!empty(Auth::user())) {
                $this->pesan = TiketingSupport::with('category', 'user')->where('status', '=', 2)->get();
            }

            return $next($request);
        });
        $this->master_site();
    }

    public function master_site()
    {
        $urls = preg_replace("(^https?://)", "", env('APP_URL'));
        // $urls = "admin.abbabill-dev.site";
        $data = MasterVendor::where('domain', 'like', '%' . $urls . '%')->first();        
        session(['sites' => $data]);
        // ddd($data);
    }
}
