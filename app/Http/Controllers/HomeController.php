<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\AccessRole;
use App\Model\MasterStore;
use App\Model\Order;
use App\Model\OrderDetail;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Model\TiketingSupport;
use App\Model\UserReferall;
use App\Model\HistoryProductVendor;
use App\Model\SaldoKreditVendor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $this->user = Auth::user('user_id');

            $this->pesan = TiketingSupport::with('category', 'user')->where('status', '=', 2)->get();

            return $next($request);
        });
    }

    public function index(request $request)
    {

        // --req today
        // $x =Order::whereDate('created_at', Carbon::today())
        // ->select(
        //     DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
        //     DB::raw("sum(order.laba_bersih) as total")
        // )
        // ->get();
        // return $x;

        // --req one week
        // $x =Order::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
        // ->select(
        //     DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
        //     DB::raw("sum(order.laba_bersih) as total")
        // )
        // ->get();
        // return $x;

        // --req one month
        // $x =Order::whereMonth('created_at', date('m'))
        // ->whereYear('created_at', date('Y'))
        // ->select(
        //     DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
        //     DB::raw("sum(order.laba_bersih) as total")
        // )
        // ->get();
        // return $x;
        


        // TOTAL RESELLER //
        $users = UserReferall::where('user_id', auth()->user()->id)->first();
        $users['get'] = UserReferall::where('referral_related', $users->referral_code)->get();
        $users['reseller_total'] = UserReferall::where('referral_related', $users->referral_code)->with('user')->count();
        $penjualan = HistoryProductVendor::all();

        // return $users;

        // TOTAL TOKO //
        $store = [];
        $storeaktif = [];
        $storepending = [];
        foreach ($users['get'] as $item) {
            $item['store'] = MasterStore::where('user_id', $item->user_id)->first();
            // return $item->user->id;
            if ($item !== null) {
                $store = MasterStore::where(['user_id' => $item->user_id])->count();
                $storeaktif = MasterStore::where(['user_id' => $item->user_id])->where('status', '=', 1)->count();
                $storepending = MasterStore::where(['user_id' => $item->user_id])->where('status', '=', 0)->count();
            }
        }

        // Pesanan baru //
        $belumbayar = Order::where('user_reff', '=', null)->where('status', '=', 0)->orderBy('created_at', 'DESC')->get();
        $sudahbayar = Order::where('user_reff', '=', null)->where('status', '=', 1)->orderBy('created_at', 'DESC')->get();
        $dikemas = Order::where('user_reff', '=', null)->where('status', '=', 3)->orderBy('created_at', 'DESC')->get();
        $dikirim = Order::where('user_reff', '=', null)->where('status', '=', 4)->orderBy('created_at', 'DESC')->get();
        $selesai = Order::where('user_reff', '=', null)->where('status', '=', 5)->orderBy('created_at', 'DESC')->get();
        $cekpembayaran = Order::where('user_reff', '=', null)->where('status', '=', 7)->orderBy('created_at', 'DESC')->get();
        $uploadulang = Order::where('user_reff', '=', null)->where('status', '=', 8)->orderBy('created_at', 'DESC')->get();

        // hitung orderan pertab
        $hitung_belum_bayar = count($belumbayar);
        $hitung_udah_bayar = count($sudahbayar);
        $hitung_dikemas = count($dikemas);
        $hitung_dikirim = count($dikirim);
        $hitung_selesai = count($selesai);
        $total = Order::where('status', 5)->count();
        $hitung_cekpembayaran = count($cekpembayaran);
        $hitung_upload_ulang = count($uploadulang);

        // hitung pesanan baru
        $pesananbaru = Order::where('status', '=', 1)->orWhere('status', '=', 7)->whereNull('user_reff')->count();
        // return $pesananbaru;


        // PENJUALAN //


        $storeaktif = DB::table('master_store')->where('status', '=', 1)->count();
        $storepending = DB::table('master_store')->where('status', '=', 0)->count();

        $orstat = AccessRole::where('role_id', '4')->select(DB::raw("COUNT(*) as count"))->whereYear('created_at', date('Y'))->groupBy(DB::raw("Month(created_at)"))->pluck('count');
        $months = AccessRole::where('role_id', '4')->select(DB::raw("Month(created_at) as month"))->whereYear('created_at', date('Y'))->groupBy(DB::raw("Month(created_at)"))->pluck('month');

        $orstat = HistoryProductVendor::select('price')
            ->select(DB::raw("COUNT(*) as count"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('count');

        $months = HistoryProductVendor::select('price')
            ->select(DB::raw("Month(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');

        $datase = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        foreach ($months as $index => $month) {
            $datase[$month - 1] = $orstat[$index];
        }

        $order = Order::where(['user_reff' => NULL])->with('order_details')->where('status', '=', 5)->get();

        $pesan = $this->pesan;

        // statistik grafik
        $pes = Order::select(\DB::raw("sum(laba_bersih) as count"))
                    ->whereYear('created_at', date('Y'))
                    ->where('order.status', '=', 5 )
                    ->groupBy(\DB::raw("Month(created_at)"))
                    ->pluck('count');

        $m = Order::select('laba_bersih')
            ->select(DB::raw("Month(created_at) as month"))
            ->whereYear('created_at', date('Y'))
            ->where('order.status', '=', 5 )
            ->groupBy(DB::raw("Month(created_at)"))
            ->pluck('month');
        
        $d = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        foreach ($m as $index => $month) {
            $d[$month - 1] = $pes[$index];
        }
        $d = json_encode($d, true);

        // pie chart
        $browsers = DB::select('SELECT
                COUNT(name_product) as value, SUBSTRING(name_product, 1, 30) as name
            FROM 
                order_detail a
            LEFT JOIN
                `order` b ON a.order_id = b.id
            WHERE
                b.`status` = 5
            GROUP BY
                name_product
            ORDER BY COUNT(name_product) desc
            LIMIT 10');
        $dataPoints = [];

        foreach ($browsers as $browser) {
            
            $dataPoints[] = [
                "value" => $browser->value,
                "name" => $browser->name
            ];
        }
        $dataPoints = json_encode($dataPoints, true);
        // return $browsers;
        // end pie chart

        // get total gross & laba
        $jual = Order::whereDate('created_at', Carbon::today())
        ->select("order.id", 
        DB::raw("order.total + order.total_komisi_produk as gross"),
        DB::raw("order.laba_bersih as total"))
        ->where('order.status', '=', 5)
        ->orderBy("id", "DESC")
        ->get();

        $pendapatan = $jual->sum('total');
        $gross = $jual->sum('gross');

        $data_saldo = SaldoKreditVendor::where(['id_user' => Auth::user()->id, 'status' => 1])->sum('nominal');
        // filter tahun grafik
        $test = Order::select(DB::raw('YEAR(created_at) as tahun'))->where('status', '=', '5')->groupBy('tahun')->get();
        if ($request->ajax()) {
            // filter pendapatan kotor
            if (!empty($request->from_date)) {
                $pk = DB::table("order")
                    ->select("order.id", 
                    DB::raw("order.total + order.total_komisi_produk as gross"),
                    DB::raw("order.laba_bersih as total"))
                    ->where('order.status', '=', 5)
                    ->whereBetween('created_at', array($request->from_date, $request->to_date))
                    ->orderBy("id", "DESC")
                    ->get();
                $kb = $pk->sum('gross');
                $kb1 = $pk->sum('total');
                return array($kb,$kb1);
            }
            if (!empty($request->tahun)) {
                $tahun = $request->tahun;
                $pes = Order::select(\DB::raw("sum(laba_bersih) as count"))
                    ->whereYear('created_at', $tahun)
                    ->where('order.status', '=', 5 )
                    ->groupBy(\DB::raw("Month(created_at)"))
                    ->pluck('count');

                $m = Order::select('laba_bersih')
                    ->select(DB::raw("Month(created_at) as month"))
                    ->whereYear('created_at', $tahun)
                    ->where('order.status', '=', 5 )
                    ->groupBy(DB::raw("Month(created_at)"))
                    ->pluck('month');
                
                $d = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                foreach ($m as $index => $month) {
                    $d[$month - 1] = $pes[$index];
                }
                return $d;
            } 
            // else if (empty($request->tahun)) {
            //     $pes = Order::select(\DB::raw("sum(laba_bersih) as count"))
            //         ->whereYear('created_at', date('Y'))
            //         ->where('order.status', '=', 5 )
            //         ->groupBy(\DB::raw("Month(created_at)"))
            //         ->pluck('count');

            //     $m = Order::select('laba_bersih')
            //         ->select(DB::raw("Month(created_at) as month"))
            //         ->whereYear('created_at', date('Y'))
            //         ->where('order.status', '=', 5 )
            //         ->groupBy(DB::raw("Month(created_at)"))
            //         ->pluck('month');
                
            //     $d = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            //     foreach ($m as $index => $month) {
            //         $d[$month - 1] = $pes[$index];
            //     }
            //     return $d;
            // } 
            else if(!empty($request->today)){
                $x =Order::whereDate('created_at', Carbon::today())
                ->select(
                    DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
                    DB::raw("sum(order.laba_bersih) as total")
                )
                ->where('status', 5)
                ->get();
                return $x;
            } else if($request->week){
                $x =Order::whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])
                ->select(
                    DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
                    DB::raw("sum(order.laba_bersih) as total")
                )
                ->where('status', 5)
                ->get();
                return $x;
            } else if($request->month){
                $x =Order::whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))
                ->select(
                    DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
                    DB::raw("sum(order.laba_bersih) as total")
                )
                ->where('status', 5)
                ->get();
                return $x;
            } else if($request->year){
                $x =Order::whereYear('created_at', date('Y'))
                ->select(
                    DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
                    DB::raw("sum(order.laba_bersih) as total")
                )
                ->where('status', 5)
                ->get();
                return $x;
            } else if($request->all){
                $x =Order::select(
                    DB::raw("sum(order.total + order.total_komisi_produk) as gros"),
                    DB::raw("sum(order.laba_bersih) as total")
                )
                ->where('status', 5)
                ->get();
                return $x;
            }
        } 


        return view('admin.home', compact('datase', 'users', 'store', 'storepending', 'storeaktif', 'order', 'pesan', 'hitung_udah_bayar', 'hitung_dikemas', 'hitung_dikirim', 'hitung_selesai', 'pesananbaru','pes','d','dataPoints','total', 'pendapatan', 'gross','data_saldo', 'test','browsers'));
    }

    public function ubah_password()
    {

        return view('admin.ubah_password');
    }

    public function update_password(request $request)
    {   
        try {
            $request->validate([
                'password' => 'required|confirmed|min:6',
            ]);
            $id = Auth::user()->id;
            $user = User::where('id', $id)->first();

            $password_lama = $request->password_lama;
            
            if (Hash::check($password_lama, $user->password)) {
                $user->update([
                    'password' => bcrypt($request->get('password')),
                ]);
                return redirect()->back()->with(['success' => 'Password Berhasil Diperbarui']);
            } else {
                return redirect()->back()->with(['failed' => 'password salah']);
            }
    
        } catch (\Throwable $th) {
            return redirect()->back()->with(['failed' => 'Konfirmasi password tidak sama']);
        }
    }
}
