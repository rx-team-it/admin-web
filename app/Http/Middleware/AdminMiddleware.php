<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\RoleUsers;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (!empty(auth()->user())) {
            $access = RoleUsers::where('user_id', auth()->user()->id)->first();
            if (!empty($access) && ($access->nm_role == 'admin')) {
                return $next($request);
            } else {
                return redirect()->route('login.admin')->with('error', 'Maaf Hanya Untuk Akun Admin');
            }
        } else {
            return redirect()->route('home.admin');
        }
        //return $next($request);
    }
}
