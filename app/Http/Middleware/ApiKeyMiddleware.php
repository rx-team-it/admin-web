<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiKeyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {   
        if(empty($request->header('api-key'))){
            return responFailValidator('API KEY empty');
        }
        if($request->header('api-key') !=  env("API_KEY_VENDOR", "123456789")) {
            return responFailValidator('Wrong API KEY');
        }
        return $next($request)->header('Access-Control-Allow-Origin', '*')->header('Access-Control-Allow-Methods','*')->header('Access-Control-Allow-Header', '*');
    }
}
