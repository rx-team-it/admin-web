<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterKurirService extends Model
{
    protected $table = 'master_kurir_service';
    protected $guarded = [''];
}
