<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nav_Grup_Parent extends Model
{
    protected $table = "nav_grup_parent";
    protected $guarded = [''];
}
