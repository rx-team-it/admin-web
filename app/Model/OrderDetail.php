<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $table = 'order_detail';
	protected $primaryKey = 'id';
	protected $guarded = [''];
	protected $appends = ['status_label'];

	public function getStatusLabelAttribute()
	{
		if ($this->status == 0) {
			return '<span class="badge badge-secondary">Proses</span>';
		} elseif ($this->status == 1) {
			return '<span class="badge badge-primary">Approve</span>';
		} elseif ($this->status == 2) {
			return '<span class="badge badge-warning">Blok</span>';
		} elseif ($this->status == 3) {
			return '<span class="badge badge-danger">Delete</span>';
		}
		return '<span class="badge badge-success">Selesai</span>';
	}

	public function order()
	{
		return $this->belongsTo('App\Model\Order');
	}

	public function product()
	{
		return $this->belongsTo('App\Model\Product');
	}

	public function finance()
	{
		return $this->hasMany('App\Model\Finance', 'detail_id');
	}

	public function product_detail()
	{
		return $this->belongsTo('App\Model\ProductDetail', 'product_detail_id');
	}

	public function getCreatedAtAttribute($value)
	{
		return Carbon::parse($value)->format('d-m-Y');
	}

	public function orders()
	{
		return $this->belongsTo('App\Model\Order', 'order_id');
	}
}
