<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ProductDetail;
use App\Model\ProductVarianOption;
use App\Model\Product;

class GrupKomisiBuyyer extends Model
{
    protected $table = 'grup_komisi_buyyer';
    protected $guarded = [''];

    public function product()
    {
        return $this->belongsTo('\App\Model\Product', 'product_id', 'id');
    }

    public function product_detail()
    {
        return $this->hasMany('\App\Model\ProductDetail', 'product_id', 'product_id');
    }

    public function varian(){
        // return $this->hasManyThrough(ProductDetail::class, Product::class, 'id', 'product_id', 'product_id', 'id');
        // return $this->hasManyThrough(ProductVarianOption::class, ProductDetail::class, 'product_varian_option_id', 'id', 'product_id', 'product_varian_option_id');
    }

    // public function product_varian()
    // {
    //     return $this->hasMany('\App\Model\ProductVarian', )
    // }
    
}
