<?php

namespace App\Model;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class HistorySaldoReseller extends Model
{
    protected $table = 'history_saldo_reseller';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
