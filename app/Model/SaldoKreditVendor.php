<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SaldoKreditVendor extends Model
{
    protected $table = 'saldo_kredit_vendor';
    protected $guarded = [''];
}
