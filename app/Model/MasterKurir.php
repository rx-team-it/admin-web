<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterKurir extends Model
{
    protected $table = 'master_kurir';
    protected $guarded = [''];
}
