<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction_Topup extends Model
{
    protected $table = 'transaction_topup';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
