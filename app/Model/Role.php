<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    protected $primaryKey = 'id';
    protected $fillable = ['nm_role'];

    public function user()
    {
        return $this->belongsToMany('App\User');
    }
}
