<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nav_Parent_Child extends Model
{
    protected $table = "nav_parent_child";
    protected $guarded = [''];
}
