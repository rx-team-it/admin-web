<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PostalCodeData extends Model
{
    protected $table = 'postal_code_data';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
