<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GroupReseller extends Model
{
    protected $table = 'group_reseller';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
