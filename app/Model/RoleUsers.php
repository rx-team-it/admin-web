<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUsers extends Model
{
    use SoftDeletes;

    protected $table = 'role_users';

    public function countrys()
    {
        return $this->belongsTo('App\Model\Country', 'country');
    }

    public function citys()
    {
        return $this->belongsTo('App\Model\City', 'city');
    }

    public function states()
    {
        return $this->belongsTo('App\Model\State', 'state');
    }
}
