<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ro_Provinces extends Model
{
    protected $table = 'tb_ro_provinces';
    protected $primaryKey = 'province_id';
    protected $guarded = [''];

    protected $timestamp;

    public function state()
    {
        return $this->hasMany(Ro_Cities::class);
    }

    public function address()
    {
        return $this->hasMany('App\Model\MasterAdress');
    }
}
