<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterAdress extends Model
{
    protected $table = 'master_address';
    protected $primaryKey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->is_default == 0) {
            return '<span class="badge badge-success">Mati</span>';
        } elseif ($this->is_default == 1) {
            return '<span class="badge badge-warning">Aktif</span>';
        }
    }

    public function countrys()
    {
        return $this->belongsTo('App\Model\ProvinceData', 'province');
    }

    public function states()
    {
        return $this->belongsTo('App\Model\PostalCodeData', 'city');
    }

    public function citys()
    {
        return $this->belongsTo('App\Model\PostalCodeData', 'sub_district');
    }
    public function urbans()
    {
        return $this->belongsTo('App\Model\PostalCodeData', 'urban');
    }
    public function postalcodes()
    {
        return $this->belongsTo('App\Model\PostalCodeData', 'postal_code');
    }

    public function get_province()
    {
        return $this->belongsTo('App\Model\ProvinceData', 'province');
    }
}
