<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SaldoKreditVendorSecond extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'saldo_vendor';
    protected $guarded = [''];
}
