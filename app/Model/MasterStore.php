<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterStore extends Model
{
	use SoftDeletes;

	protected $table = 'master_store';
	protected $primaryKey = 'id';
	protected $guarded = [''];
	protected $appends = ['status_label', 'status_label_verifikasi'];

	public function getStatusLabelAttribute()
	{
		if ($this->status == 0) {
			return '<button type="button" class="btn btn-warning btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Pending
            </button>';
		} elseif ($this->status == 1) {
			return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
            <i class="fas fa-store"></i> Aktif
            </button>';
		} elseif ($this->status == 2) {
			return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Blok
            </button>';
		} elseif ($this->status == 3) {
			return '<button type="button" class="btn btn-secondary btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Delete
            </button>';
		}
		return 'Selesai';
	}

	public function getStatusLabelVerifikasiAttribute()
	{
		if ($this->status_verifikasi == 0) {
			return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Pending
            </button>';
		} elseif ($this->status_verifikasi == 1) {
			return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Diterima
            </button>';
		} elseif ($this->status_verifikasi == 2) {
			return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Ditolak
            </button>';
		}
		return 'Selesai';
	}

	public function user()
	{
		return $this->belongsTo('App\Model\UserProfile');
	}

	public function image()
	{
		return $this->belongsTo('App\Model\Slider');
	}

	public function countrys()
    {
        return $this->belongsTo('App\Model\Ro_Provinces', 'province');
    }

    public function citys()
    {
        return $this->belongsTo('App\Model\Ro_Subdistricts', 'district');
    }

    public function states()
    {
        return $this->belongsTo('App\Model\Ro_Cities', 'city');
    }

	public function getCreatedAtAttribute($value)
	{
		return Carbon::parse($value)->format('d-m-Y');
	}

	public function user_profile()
	{
		return $this->belongsTo('App\Model\UserProfile', 'user_id');
	}

	public function address()
	{
		return $this->belongsTo('App\Model\MasterAdress', 'address_id');
	}
}
