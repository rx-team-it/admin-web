<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterBankVendor extends Model
{
    protected $table = 'master_bank_vendor';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
