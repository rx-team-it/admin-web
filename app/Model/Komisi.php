<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Komisi extends Model
{
    protected $table = 'komisi_master';
    protected $guarded = [''];
}
