<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ReplyTiketing extends Model
{
    protected $table = 'reply_tiketing_support';
    protected $primarykey = 'id';
    protected $guarded = [''];

    public function category()
    {
        return $this->belongsTo('App\Model\MasterCategory');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function tiketing()
    {
        return $this->belongsTo('App\Model\TiketingSupport', 'tiketing_id');
    }
}
