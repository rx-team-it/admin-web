<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KomisiBuyyer extends Model
{
    protected $table = 'komisi_buyyer';
    protected $guarded = [''];
}
