<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupMaster extends Model
{
    protected $table = 'group_master';
    protected $guarded = [''];

    public function child()
    {
        return $this->hasMany('App\Model\GroupReseller', 'group');
    }
}
