<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $primarykey = 'id';
    protected $guarded = [''];
    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }
}
