<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductVarian extends Model
{
    protected $table = 'product_varian';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function product_varian_option()
    {
        return $this->hasMany('App\Model\ProductVarianOption', 'product_varian_id');
    }

    public function sub_variasi()
    {
        return $this->belongsTo(ProductVarian::class);
    }
}
