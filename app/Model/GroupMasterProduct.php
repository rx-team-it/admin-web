<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupMasterProduct extends Model
{
    protected $table = 'group_master_product';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function flash_sale()
    {
        return $this->hasMany('App\Model\ProductFlashSale','id_group_master');
    }
}
