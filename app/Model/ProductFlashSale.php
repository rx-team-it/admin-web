<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductFlashSale extends Model
{
    protected $table = 'product_flashsale';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function product_images_thumbnail()
    {
        return $this->hasOne('App\Model\ProductImage', 'product_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }
}
