<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Nav_Child_of_Child extends Model
{
    protected $table = "nav_child_of_child";
    protected $guarded = [''];
}
