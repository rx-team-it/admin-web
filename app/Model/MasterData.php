<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterData extends Model
{
    protected $table = 'master_data';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function child()
    {
        return $this->hasMany('App\Model\MasterDataChild', 'id_master');
    }
}
