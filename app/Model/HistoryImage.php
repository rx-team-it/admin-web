<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryImage extends Model
{
    protected $table = 'history_image';
    protected $guarded = [''];
}
