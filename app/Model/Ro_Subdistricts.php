<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ro_Subdistricts extends Model
{
    protected $table = 'tb_ro_subdistricts';
    protected $primaryKey = 'subdistrict_id';
    protected $guarded = [''];

    protected $timestamp;

    public function state()
    {
        return $this->belongsTo(Ro_Cities::class);
    }

    public function address()
    {
        return $this->hasMany('App\Model\MasterAdress');
    }
}
