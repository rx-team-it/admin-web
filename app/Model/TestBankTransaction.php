<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TestBankTransaction extends Model
{
    protected $table = 'test_bank_transaction';
    protected $primarykey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return '<span class="badge badge-success">Berhasil</span>';
        } else if ($this->status == 0) {
            return '<span class="badge badge-secondary">Pengecekan</span>';
        } else if ($this->status == 2) {
            return '<span class="badge badge-secondary">Ditolak</span>';
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\UserProfile', 'user_id');
    }

    public function toko()
    {
        return $this->belongsTo('App\Model\MasterStore', 'store_id');
    }
}
