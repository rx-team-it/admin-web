<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $primaryKey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function product()
    {
        return $this->belongsTo('App\Model\Product', 'product_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Model\MasterStore', 'store_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Model\Country');
    }

    public function address()
    {
        return $this->belongsTo('App\Model\MasterAdress');
    }

    public function detail()
    {
        return $this->belongsTo('App\Model\OrderDetail');
    }

    public function order_details()
    {
        return $this->hasMany('App\Model\OrderDetail', 'order_id');
    }

    public function finance()
    {
        return $this->hasMany('App\Model\Finance', 'order_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Model\ConfirmPayment', 'user_id');
    }

    public function history_product_vendor () 
    {
        return $this->hasMany('App\Model\HistoryProductVendor', 'order_id');
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return '<div class="badge badge-danger"><i class="fas fa-exclamation-circle"></i> Belum Bayar</div>';
        } elseif ($this->status == 1) {
            return '<div class="badge badge-primary"><i class="fas fa-check"></i> Sudah Bayar</div>';
        } elseif ($this->status == 2) {
            return '<div class="badge badge-danger"><i class="fas fa-exclamation-circle"></i> Pembayaran Gagal</div>';
        } elseif ($this->status == 3) {
            return '<div class="badge badge-primary"><i class="fas fa-box"></i> Dikemas';
        } elseif ($this->status == 4) {
            return '<div class="badge badge-primary"><i class="fas fa-plane-departure"></i> Dikirim';
        } elseif ($this->status == 5) {
            return '<div class="badge badge-success"><i class="fas fa-check"></i> Selesai';
        } elseif ($this->status == 6) {
            return '<div class="badge badge-danger"><i class="fas fa-times"></i> Dibatalkan';
        } elseif ($this->status == 7) {
            return '<div class="badge badge-warning"><i class="fas fa-times"></i> Cek Pembayaran';
        } elseif ($this->status == 8) {
            return '<div class="badge badge-warning"><i class="fas fa-times"></i> Upload Ulang';
        }
        return 'Selesai';
        // if ($this->status == 0) {
        //     return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
        //     <i class="fas fa-exclamation-circle"></i> Belum Bayar
        //     </button>';
        // } elseif ($this->status == 1) {
        //     return '<button type="button" class="btn btn-primary btn-sm btn-icon icon-left">
        //     <i class="fas fa-check"></i> Sudah Bayar
        //     </button>';
        // } elseif ($this->status == 2) {
        //     return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
        //     <i class="fas fa-exclamation-circle"></i> Pembayaran Gagal
        //     </button>';
        // } elseif ($this->status == 3) {
        //     return '<button type="button" class="btn btn-info btn-sm btn-icon icon-left">
        //     <i class="fas fa-box"></i> Dikemas
        //     </button>';
        // } elseif ($this->status == 4) {
        //     return '<button type="button" class="btn btn-info btn-sm btn-icon icon-left">
        //     <i class="fas fa-plane-departure"></i> Dikirim
        //     </button>';
        // } elseif ($this->status == 5) {
        //     return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
        //     <i class="fas fa-check"></i> Selesai
        //     </button>';
        // } elseif ($this->status == 6) {
        //     return '<button type="button" class="btn btn-warning btn-sm btn-icon icon-left">
        //     <i class="fas fa-times"></i> Dibatalkan
        //     </button>';
        // } elseif ($this->status == 7) {
        //     return '<button type="button" class="btn btn-warning btn-sm btn-icon icon-left">
        //     <i class="fas fa-times"></i> Cek Pembayaran
        //     </button>';
        // } elseif ($this->status == 8) {
        //     return '<button type="button" class="btn btn-warning btn-sm btn-icon icon-left">
        //     <i class="fas fa-times"></i> Upload Ulang
        //     </button>';
        // }
        // return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
        // <i class="fas fa-check"></i> Selesai
        // </button>';
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y H:i');
    }
}
