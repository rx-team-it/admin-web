<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AccessRole extends Model
{

    protected $table = 'access_role';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public static $rules = [
        'role_id' => 'required',
        'user_id' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Model\Role', 'role_id');
    }
}
