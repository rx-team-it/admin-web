<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistorySaldo extends Model
{
    protected $table = 'history_saldo';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
