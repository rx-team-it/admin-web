<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductSum extends Model
{
        protected $table = 'product_sum';
        protected $primaryKey = 'id';
        protected $guarded = [''];
}
