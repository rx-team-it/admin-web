<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProvinceData extends Model
{
    protected $table = 'province_data';
    protected $primaryKey = 'province_code';
    protected $guarded = [''];
}
