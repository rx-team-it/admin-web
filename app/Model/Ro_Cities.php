<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ro_Cities extends Model
{
    protected $table = 'tb_ro_cities';
    protected $primaryKey = 'city_id';
    protected $guarded = [''];

    protected $timestamp;

    public function country()
    {
        return $this->belongsTo(Ro_Provinces::class);
    }

    public function state()
    {
        return $this->hasMany(Ro_Subdistricts::class);
    }

    public function address()
    {
        return $this->hasMany('App\Model\MasterAdress');
    }

    public function city()
    {
        return $this->hasMany(Ro_Subdistricts::class);
    }
}
