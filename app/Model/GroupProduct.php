<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupProduct extends Model
{
    protected $table = 'group_product';
    protected $guarded = [''];
}
