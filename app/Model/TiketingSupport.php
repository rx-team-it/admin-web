<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TiketingSupport extends Model
{
    protected $table = 'master_tiketing';
    protected $primarykey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return '<button type="button" class="btn btn-primary btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Baru
            </button>';
        } else if ($this->status == 1) {
            return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
            <i class="fas fa-check"></i> Sudah Dibalas
            </button>';
        } else if ($this->status == 2) {
            return '<button type="button" class="btn btn-warning btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Belum Dibalas
            </button>';
        } else if ($this->status == 3) {
            return '<button type="button" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="fas fa-exclamation-circle"></i> Closed
            </button>';
        }
    }

    public function category()
    {
        return $this->belongsTo('App\Model\MasterCategory');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
