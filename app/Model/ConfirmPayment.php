<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ConfirmPayment extends Model
{
    protected $table = 'confirm_payment';
    protected $primarykey = 'id';
    protected $guarded = [''];

    public function order()
    {
        return $this->belongsTo('App\Model\Order', 'order_id');
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return 'Belum Bayar';
        } elseif ($this->status == 1) {
            return 'Sudah Bayar';
        } elseif ($this->status == 2) {
            return 'Pemabayaran Gagal';
        } elseif ($this->status == 3) {
            return 'DiKemas';
        } elseif ($this->status == 4) {
            return 'Dikirim';
        } elseif ($this->status == 5) {
            return 'Selesai';
        }
        return 'Selesai';
    }
}
