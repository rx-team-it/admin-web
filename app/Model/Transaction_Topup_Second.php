<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction_Topup_Second extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'transaction_topup';
    protected $guarded = [''];
}
