<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->hasMany(City::class);
    }

    public function address()
    {
        return $this->hasMany('App\Model\MasterAdress');
    }

    public function city()
    {
        return $this->hasMany(City::class);
    }
}
