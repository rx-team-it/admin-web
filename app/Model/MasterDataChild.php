<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterDataChild extends Model
{
    protected $table = 'master_data_child';
    protected $primaryKey = 'id';
    protected $guarded = [''];
}
