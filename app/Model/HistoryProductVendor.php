<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class HistoryProductVendor extends Model
{
    protected $table = 'history_product_vendor';
    protected $primaryKey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->status == 1) {
            return '<button type="button" class="btn btn-success btn-sm btn-icon icon-left">
            <i class="fas fa-check"></i> Terjual
            </button>';
        }
        return 'Selesai';
    }

    public function getStatusAttribute()
    {
        if ($this->attributes['status'] == 0) {
            return '<span class="badge badge-pill badge-warning">Sedang Di Proses </span>';
        } elseif ($this->attributes['status'] == 1) {
            return '<span class="badge badge-pill badge-danger">Di Tolak</span>';
        } elseif ($this->attributes['status'] == 2) {
            return '<span class="badge badge-pill badge-success">Selesai</span>';
        }
        return 'selesai';
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }
}
