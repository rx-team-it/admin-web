<?php

//GET FILE AWS
function getAWS_BUCKET($value)
{
    $url = 'https://'. env('AWS_BUCKET') .'.s3-'. env('AWS_DEFAULT_REGION') .'.amazonaws.com/public/'.$value;

    return $url;
}

// data success diget
function responSuccessGet($m=[]) {
   
    return response()->json([
        'status' => 204,
        'message' => "data berhasil retriving",
        'data' => $m,
    ]);
}


// respon gagal karena validasi
function responFailValidator($message="") {
    return  response()->json([
        'status'=> 401,
        'message' => $message,
    ]);
}


