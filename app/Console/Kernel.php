<?php

namespace App\Console;

use App\Model\Order;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Commands\CekStatusOrderCron::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command(
            $this->update()
        )->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    public function update()
    {
        $get_resi = Order::where('status', '=', '3')->first(['courrier','tracking_number','id']);

        if (!empty($get_resi)) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => env('KURIR_URL') . '/api/tracking',
            // CURLOPT_URL => 'https://kurir.abbabill-dev.site/api/tracking',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "kurir":"'.$get_resi->courrier.'",
                "waybill_no":"'.$get_resi->tracking_number.'"
            }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $a = json_decode($response, true)['data'][0]['manifest_code'];

            if ($a != '101' || $a != '0') {
                $get_resi = Order::where('status', '=', 3)->update([
                    'status' => 4
                ]);
            } else if ($a == '0' || $a == '200') {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                // CURLOPT_URL => 'https://abbabill-dev.site/api/update_status_kurir',
            CURLOPT_URL => env('KURIR_URL') .'/api/update_status_kurir',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('id' => ''.$get_resi->id.'','status' => '5'),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            }
        } else {
            return "Data status dikemas kosong";
        }
    }
}
