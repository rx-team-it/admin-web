<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsToMany('App\Model\Role');
    }

    public function hasAnyRole($role)
    {
        return null !== $this->role()->where('nm_role', $role)->first();
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function nm_role()
    {
        return $this->belongsTo('App\Model\RoleUsers', 'user_id');
    }

    public function user_detail() {
        return $this->hasOne('App\Model\UserDetails', 'id_user');
    }

    public function product() {
        return $this->hasMany('App\Model\Product', 'created_user')->orderBy('created_at', 'DESC')->limit(1);
    }

    public function role_user() {
        return $this->hasOne('App\Model\AccessRole', 'user_id');
    }
} 
